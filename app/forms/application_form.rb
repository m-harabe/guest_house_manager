class ApplicationForm

  include Virtus.model
  include ActiveModel::Model

  # サブクラスで定義したattributeのキーのリストを返す
  # Virtusのattribute_setメソッドを利用 https://github.com/solnic/virtus/blob/master/lib/virtus/attribute_set.rb
  def self.attribute_keys
    self.attribute_set.each.inject([]) {|list, (k,v)| list << k.name}
  end

  # プロパティのセット 継承先の attr_accessor を更新
  # @params [Hash] フォームオブジェクトにセットするハッシュ（keyが合致するもののみ対象）
  def match_attributes=(attributes)
    attributes.each do |key,value|
      send(key.to_s + '=', value) rescue next
    end
  end

  # 自身の持つプロパティ（インスタンス変数）のハッシュを返却
  # @return [Hash] key: インスタンス変数名（シンボル） value: 格納されている値
  def attributes
    return_attributes = {}
    except_field = [:@errors]
    instance_variables.each do |attribute|
      next if except_field.index(attribute)
      return_attributes[attribute[1..-1].to_sym] = instance_variable_get(attribute)
    end
    return return_attributes
  end

  # 検索条件を抽出して保存用にパラメータ作成
  # search_xxxx という名称のフォームからクッキーに保存するためのハッシュを生成
  def search_conditions
    ret_search_params = Hash.new()
    _attributes = instance_variables
    # 入力された検索条件のオブジェクト名から"@"以降の文字を取ってHashに格納する
    _attributes.each do |attribute|
      if attribute.match(/^@search_(.+)$/)
        val = instance_variable_get(attribute)
        # ret_search_params[attribute[1..-1]] = val if val.present?
        ret_search_params[attribute[1..-1]] = (val || '')
      end
    end
    return ret_search_params
  end

  def add_error(name, code, **attributes)
    self.errors.add :base, I18n.t("errors.#{name}.#{code}", attributes)
  end

  def output_error_log(ex, title = '')
    ErrorUtil.error_log_and_notify ex: ex, message: "Form modules #{title}"
  end

end
