class Work::CleaningStatusListForm < ApplicationForm

  TERM = 6
  PRESENTER = Presenter::Work::ReservationDatePresenter

  # 検索条件
  attribute :search_room_ids,         Array
  attribute :search_area_id,          Integer
  attribute :search_date_from,        Date
  attribute :search_date_to,          Date

  # 一覧表示用
  attribute :room_id_hash,                      Hash # {room_id => Room},...
  attribute :room_reservation_hash,             Hash # {room_id => {check_in_day => ReservationDatePresenter} },...
  attribute :prepearing_count_by_check_in_date, Hash # {check_in_date => count},... 日付別未清掃件数


  # validation
  validates_with Work::CleaningStatusListValidator

  def initialize(params = {})
    super()
    if params.present?
      self.match_attributes = params
      @search_room_ids.delete('')
      @search_room_ids = @search_room_ids.uniq.map(&:to_i)
    end
    @room_id_hash = Room.visible.with_building
    @room_id_hash = @room_id_hash.where(id: @search_room_ids) if @search_room_ids.present?
    @room_id_hash = @room_id_hash.index_by(&:id)

    if params.blank?
      @search_date_from = Date.today
      @search_date_to = Date.today + TERM
      @search_room_ids = @room_id_hash.keys
    end
  end

  def fetch_list
    # ReservationDateのcheck_in_dayを元に検索
    reservation_list = Reservation.search_not_cancel
                                  .check_in_day_is(@search_date_from..@search_date_to)
                                  .where(room_id: @search_room_ids)
                                  .order(:check_in_date)
    generate_room_reservation_hash(reservation_list)

    # 日別の未清掃件数合計
    _prepearing_list = Reservation.where(status: Settings.RESERVATION.STATUS.PREPARING,
                                         room_id: @search_room_ids,
                                         check_in_date: @search_date_from..@search_date_to)
                                  .group(:check_in_date)
                                  .pluck(:check_in_date, Arel.sql('COUNT(*)'))
    # 配列 [{check_in_date => count},...] を１つのハッシュに格納 {check_in_date => count},...
    @prepearing_count_by_check_in_date = _prepearing_list.inject({}) {|h, ar| h.merge(ar[0] => ar[1])}
  end

private

  def generate_room_reservation_hash(reservation_list)
    # {room_id => {check_in_day => Reservation, ...}, ... }
    reservation_list.each do |rv|
      @room_reservation_hash[rv.room_id] ||= {}
      @room_reservation_hash[rv.room_id].merge!(rv.check_in_date.to_s => rv)
    end

    @search_room_ids.each do |room_id|
      @room_reservation_hash[room_id.to_i] ||= {}
      reservation_dates = {}
      # 部屋ごとに１件目の予約情報を取り出す
      previous_reservation = @room_reservation_hash[room_id.to_i]&.values&.first

      (@search_date_from..@search_date_to).each do |date|
        # is_today = date.to_s == Date.today.to_s

        # この部屋の１件目の予約日が検索開始日以前の場合でもセルに表示させるようにする
        if previous_reservation && @search_date_from > previous_reservation.check_in_date
          reservation = Marshal.load(Marshal.dump(previous_reservation))
          previous_reservation = nil # 最初に取得したら後は不要
        else
          reservation = @room_reservation_hash[room_id.to_i][date.to_s]
        end

        # 予約あり（チェックイン日）
        if reservation
          @room_reservation_hash[room_id.to_i][date.to_s] = PRESENTER.new(date: date, search_date_to: @search_date_to, reservation: reservation)
          reservation_dates = reservation.reservation_dates.index_by(&:check_in_day)

        # 予約あり（ステイ中）
        elsif reservation_dates[date]
          @room_reservation_hash[room_id][date.to_s] = PRESENTER.new(date: date, search_date_to: @search_date_to, cell_mode: PRESENTER::CELL_MODE_STAYING)

        # 予約なし
        else
          @room_reservation_hash[room_id][date.to_s] = PRESENTER.new(date: date, search_date_to: @search_date_to, cell_mode: PRESENTER::CELL_MODE_NONE)
          reservation_dates = {}
        end
      end
    end
  end

end
