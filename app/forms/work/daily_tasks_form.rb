class Work::DailyTasksForm < ApplicationForm

  TERM = 6
  PRESENTER = Presenter::Work::WeeklyScheduledPresenter

  # 検索条件
  attribute :search_date_start, Date
  attribute :search_date_end,   Date
  # attribute :search_date_end, Date, default: -> page, attribute { Time.zone.today + TERM } # 日付デフォルト値設定参考
  attribute :search_room_ids,   Array
  attribute :search_area_id,    Integer
  attribute :search_task,       Integer

  # 一覧表示用
  attribute :room_id_hash,                      Hash # {room_id => Room},...
  attribute :room_reservation_hash,             Hash # {room_id => {check_in_day => DailyTaskPresenter} },...
  attribute :prepearing_count_by_check_in_date, Hash # {check_in_date => count},... 日付別未清掃件数

  # 集計
  attribute :today_preparing_count, Integer, default: 0
  attribute :in_count_by_date,      Integer, default: Hash.new(0)
  attribute :out_count_by_date,     Integer, default: Hash.new(0)

  # validation
  validates_with Work::DailyTasksValidator

  def initialize(params = {})
    super()
    if params.present?
      self.match_attributes = params
      @search_room_ids.delete('')
      @search_room_ids = @search_room_ids.uniq.map(&:to_i)
    end
    @room_id_hash = Room.visible.with_building
    @room_id_hash = @room_id_hash.where(id: @search_room_ids) if @search_room_ids.present?
    @room_id_hash = @room_id_hash.index_by(&:id)

    if params.blank?
      @search_date_start = Date.today
      @search_date_end = Date.today + TERM
      @search_room_ids = @room_id_hash.keys
    end
  end

  def fetch_list
    # チェックアウトデータ / room_idをキーにしたハッシュにReservationを格納する
    checkout_by_room_id = Hash.new{|hash, key| hash[key] = []}
    Reservation.search_not_cancel
               .where(check_out_date: (@search_date_start..@search_date_end), room_id: @search_room_ids)
               .order(:check_out_date)
               .each { |r| checkout_by_room_id[r.room_id] << r }

    # チェックインデータ / room_idをキーにしたハッシュにReservationを格納する
    checkin_by_room_id = Hash.new{|hash, key| hash[key] = []}
    Reservation.search_not_cancel
               .where(room_id: @search_room_ids)
               .check_in_day_is(@search_date_start..@search_date_end)
               .order(:check_in_date)
               .each { |r| checkin_by_room_id[r.room_id] << r }

    @room_id_hash.keys.each do |room_id|
      @room_reservation_hash[room_id] =
        PRESENTER.new(room_id: room_id, term: TERM, date_start: @search_date_start,
                      out_reservations: checkout_by_room_id[room_id],
                      in_reservations: checkin_by_room_id[room_id])
    end
    fech_count
  end

private

  def fech_count
    # @out_count_by_date, @in_count_by_date = Hash.new(0), Hash.new(0)
    @room_reservation_hash.each do |room_id, weekly|
      weekly.daily_task_list.each do |daily|

        # 本日の未清掃件数
        if daily.target_date.today? && daily.try(:res_in).try(:preparing?)
          @today_preparing_count += 1
        end

        # 日別のIN/OUT件数集計
        case daily.display_type
        when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_OUT
          @out_count_by_date[daily.target_date] += 1
        when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_IN
          @in_count_by_date[daily.target_date] += 1
        when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_OUT_IN
          @out_count_by_date[daily.target_date] += 1
          @in_count_by_date[daily.target_date] += 1
        end
      end
    end
  end


end
