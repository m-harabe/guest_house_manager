class ReservationManagement::DailyListForm < ApplicationForm

  # 検索条件
  attribute :search_check_in_day,        Date
  attribute :search_check_in_date_flag,  Boolean

  # 一覧表示用
  attribute :daily_list,                 Array[Hash]

  def initialize(params = nil)
    super

    self.match_attributes = params if params
    @search_check_in_day ||= Date.current
    @search_check_in_date_flag ||= false

    get_list
  end

#####################################################################
# チューブリンクONの件
# この画面のバリデーション
# 部屋別一覧の検索処理
#####################################################################
  private

  def get_list
    @daily_list = []
    ########################## テスト用 ####################################
    # @search_check_in_day = '2018-06-07'
    # date_reservation = ReservationDate.where(check_in_day: @search_check_in_day)
    #                                   .eager_load(:nationality)
    #                                   .index_by(&:room_id)
    reservation_list = Reservation.search_not_cancel
                                  .check_in_day_is(@search_check_in_day)
                                  .eager_load(:reservation_dates)
                                  .eager_load(:nationality)
    # check_in_day: 宿泊日 / check_in_date: チェックイン日
    reservation_list = reservation_list.where(check_in_date: @search_check_in_day) if @search_check_in_date_flag

    reservation_list = reservation_list.index_by(&:room_id)

    # Room.preload(:building).each do |room|
    #   reservation = room_date[room.id].try(:reservation)
    #   @daily_list << RoomDateReservationRow.new(room, reservation, room_date)
    # end

    # Room.preload(:building).each do |room|
    Room.visible.with_building.each do |room|
      @daily_list << RoomDateReservationRow.new(room, reservation_list[room.id], @search_check_in_day)
    end
  end
end

class RoomDateReservationRow
  attr_accessor :reservation,
                :room,
                :search_check_in_day

  def initialize(room, reservation, search_check_in_day)
    @reservation = reservation
    @room = room
    @search_check_in_day = search_check_in_day
    @today = Date.today
  end

  def room_name
    @room.full_code_name
  end

  # def status
  #   @reservation&.status_name
  # end

  def visitor_name
    return '' unless @reservation
    # nationality = @reservation&.nationality&.name
    "#{@reservation&.display_visitor_name}様"
  end

  def nationality
    _nn = @reservation&.nationality&.name
    _nn.nil? ? '' : "【#{_nn}】"
  end

  def total_count
    @reservation&.total_count
  end

  def number_of_nights
    @reservation&.number_of_nights
  end

  def is_check_in_today
    return false unless @reservation&.check_in_date == @today
    status = Settings.RESERVATION.STATUS
    (@reservation&.status == status.PREPARING || @reservation&.status == status.POSSIBLE)
  end

  def check_in_date
    return '' unless @reservation&.check_in_date
    I18n.l(@reservation.check_in_date, format: :middle_h)
  end

  def check_in_time
    return '' unless @reservation&.scheduled_check_in_time
    I18n.l(@reservation.scheduled_check_in_time, format: :time)
  end

  def check_out_date
    return '' unless @reservation&.check_out_date
    I18n.l(@reservation.check_out_date, format: :middle_h)
  end

  def site
    @reservation&.site_name
  end

  # TODO
  def pay_off
    ''
  end

  def note
    @reservation&.note
  end

end