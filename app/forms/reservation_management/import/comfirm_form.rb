class ReservationManagement::Import::ComfirmForm < ApplicationForm

  attribute     :import_reservation_list, Array
  attribute     :success_count,           Integer
  attribute     :error_count,             Integer
  attr_accessor :file

  STRT_ROW  = 2.freeze
  PRESENTER = Presenter::ReservationManagement::ImportReservationPresenter

  # validation
  # validates :file, presence: true
  validate :validate_and_fetch_list

  def initialize(params:)
    super()
    @file = params.dig(:file)
  end

private

  # 行データの読み込み（リスト作成）とバリデーションを行う
  def validate_and_fetch_list
    if @file.nil?
      # self.errors.add :base, 'ファイルを選択してください。'
      add_error :common, :require_select, label: 'ファイル'
      return
    end

    nationalities = Nationality.visible.select(:id, :name).index_by(&:name)
    # Gem:roo https://github.com/roo-rb/roo
    xlsx = Roo::Excelx.new(@file.tempfile)
    # 扱うのは１つ目のシートのみ
    sheet = xlsx.sheet(0)
    validate_header_row sheet.row(1)
    (STRT_ROW..sheet.last_row).each do |row_idx|
      @import_reservation_list << PRESENTER.new(excel_row: sheet.row(row_idx), nationalities: nationalities)
    end
  rescue FileImport::FormatError # 行データ読み込み失敗
    self.errors.add :base, I18n.t('errors.import.file_format_error')
  end

  def validate_header_row(headers)
    # TODO 順序
    # ヘッダーが不足している場合のみエラー
    # 定数CSV_HEADERS が 読み込んだヘッダーの部分集合である場合に true を返す
    unless PRESENTER::Const::CSV_HEADERS.to_set.subset?(headers.to_set)
      add_error :import, :file_header_error
    end
  end
end