class ReservationManagement::Import::CreateForm < ApplicationForm

  attribute     :import_reservation_list, Array
  attribute     :success_count,           Integer
  attribute     :error_count,             Integer
  attr_accessor :file

  STRT_ROW  = 2
  PRESENTER = Presenter::ReservationManagement::ImportReservationPresenter

  # validation
  # validates :file, presence: true
  # validate :validate_file

  def initialize(file:)
    super
    @file = file
    fetch_list_by_file
  end

  private

  def fetch_list_by_file
    Rails.logger.info 'A-------------------start fetch_list_by_file'
    nationalities = Nationality.visible.select(:id, :name).index_by(&:name)
    # Gem:roo https://github.com/roo-rb/roo
    xlsx = Roo::Excelx.new(@file.tempfile)
    # 扱うのは１つ目のシートのみ
    sheet = xlsx.sheet(0)
    (STRT_ROW..sheet.last_row).each do |row_idx|
      @import_reservation_list << PRESENTER.new(excel_row: sheet.row(row_idx), nationalities: nationalities)
    end
    Rails.logger.info 'A-------------------end fetch_list_by_file'
  end

  def validate_file

  end

end