class ReservationManagement::MonthlyListForm < ApplicationForm

  # 検索条件

  # TODO: Roomの取得方法 要表示ステータス対応
  attribute :search_room_id,          Integer, default: Room.visible.with_building.pluck(:id).first
  attribute :search_check_in_month,   String
  attribute :search_original_month,   Date
  attribute :search_site_code,        String
  attribute :search_visitor_name,     String
  attribute :search_mail_address,     String
  attribute :search_prev_next_month,  Date

  # 集計情報
  attribute :sum_total,               Integer, default: 0
  attribute :sum_room_charge,         Integer, default: 0
  attribute :sum_cleaning_fee,        Integer, default: 0
  attribute :sum_meal_fee,            Integer, default: 0

  # 一覧表示用
  attribute :reservation_list,        Array[Reservation]
  attribute :monthly_list,            Array[Hash]
  attribute :room_full_code_name,     String
  attribute :room,                    String

  # 非表示物件の表示フラグ
  attribute :show_all_rooms,          Boolean, default: false

  PRESENTER = Presenter::ReservationManagement::ReservationDatePresenter
  CSV_PRESENTER = Presenter::ReservationManagement::ReservationDateCsvPresenter

  def initialize(params = nil, is_csv: false, show_all_rooms: false)
    super(params)
    @show_all_rooms = show_all_rooms if show_all_rooms
    self.match_attributes = params if params
    @search_check_in_month ||= I18n.l(Date.current, format: :year_month)
    @room = Room.with_building.find_by(id: @search_room_id)
    is_csv ? fetch_csv_list : fetch_list
  end

  def csv_file_name
    # timestamp = Time.zone.now.strftime('%Y_%m_%d_%H_%M_%S')
    file_name = @search_check_in_month + '_' + @room.full_code_name_with_underscore + '_'
    file_name << Time.zone.now.strftime('%Y%m%d%H%M%S') + '.csv'
  end

  private

  # 検索開始日をセット
  # 前月・次月ボタン押下時はどちらかの月を検索月に再セットし、検索月セレクトの値を上書きする
  def set_prev_next_month
    if @search_prev_next_month.present?
      @search_check_in_month = I18n.l(@search_prev_next_month, format: :year_month)
    end
    @search_prev_next_month = nil
    # TimeWithZoneに変換
    parse_start_date(@search_check_in_month)
  end

  # 画面のYY年MM月表示をTimeWithZoneに変換
  # @param str [String] YY年MM月
  # @return [ActiveSupport::TimeWithZone] 該当月の1日
  def parse_start_date(str)
    (str.gsub(/['年','月']/, '-') << '01').in_time_zone
  end

  def fetch_list
    @monthly_list = []

    date_from = set_prev_next_month
    date_to = date_from.in_time_zone.end_of_month

    # 前月・次月表示用（今月を基準に＋１, -１する）
    @search_original_month = date_from.to_date

    # まずは対象日のReservationDateを取得して、紐付くReservationIDを抽出する
    reservation_ids = ReservationDate.where(check_in_day: date_from..date_to, room_id: @search_room_id)
                                     .pluck(:reservation_id)

    if reservation_ids.present?
      # 検索月外への月またぎデータも取得
      @reservation_list = Reservation.where(id: reservation_ids)
                                     .search_not_cancel
                                     .eager_load(:reservation_dates, :nationality)
                                     .order(:check_in_date)
                                     .index_by(&:check_in_date)
    end

    # 指定月の1日〜月末の日付を一覧に表示するが、前月末の予約が該当月の初日をまたいでいる場合は、前月の日付から表示する
    if @reservation_list.present?
      date_from = @reservation_list.keys.first if @reservation_list.keys.first < date_from
    end

    # 物件名
    @room_full_code_name = @room.full_code_name

    # 部屋複数ある施設は一棟貸し不可日リスト作成
    if @room.building_room_count > 1
      @check_in_day_list = ReservationDate.eager_load(:reservation)
                                          .merge(Reservation.search_not_cancel)
                                          .where(check_in_day: date_from..date_to, building_id: @room.building_id)
                                          .pluck(:check_in_day)
      # 'yyyy-mm-dd' に変換
      @check_in_day_list.map!(&:to_s)
      @check_in_day_list = @check_in_day_list.uniq
    end

    today = Date.today
    # ActiveSupport::TimeWithZone 型をDate型に変更
    # ※ date_from, date_to はTimeWithZone、@reservation_list.keys はDate型
    (date_from.to_date..date_to.to_date).each do |date|
      generate_reservation_date_table(date, today)
    end

    # 集計情報を計算
    calc_total_amount
  end

  def generate_reservation_date_table(date, today)
    add_class = date == today ? ' today' : ''
    reservation = @reservation_list[date] if @reservation_list.present?
    if reservation
      @position = 1
      @row_count = reservation.number_of_nights
      # TODO: StringUtil#toggle
      @html_class = (@html_class == 'odd' ? 'even' : 'odd')

      # @monthly_list << PRESENTER.new(date: date, position: @position,html_class: @html_class + add_class, reservation: reservation)
      @monthly_list << PRESENTER.new(date: date, html_class: @html_class + add_class, reservation: reservation)
      return
    end

    if @position
      @position += 1

      # 連泊中の中日のケース
      if @position < @row_count
        return
      # 連泊最終日のケース
      elsif @position == @row_count
        @position = @row_count = nil
        return
      # 1泊の予約の翌日が予約のないケース
      elsif @position > @row_count
        @position = @row_count = nil
      end
    end
    # 予約がない場合は日付だけを持つオブジェクト作成
    @html_class = (@html_class == 'odd' ? 'even' : 'odd')
    @monthly_list << PRESENTER.new(date: date, html_class: @html_class + add_class, check_in_day_list: @check_in_day_list, room: @room)
  end

  def calc_total_amount
    @monthly_list.each do |p|
      next if p.reservation.nil?
      @sum_total        += p.reservation.total
      @sum_room_charge  += p.reservation.room_charge
      @sum_cleaning_fee += p.reservation.cleaning_fee
      @sum_meal_fee     += p.reservation.meal_fee
    end
  end

  def fetch_csv_list
    @monthly_list = []

    date_from = set_prev_next_month
    date_to = date_from.in_time_zone.end_of_month

    # 月またぎをしないようにReservationDateを取得
    reservation_dates = ReservationDate.search_not_cancel
                                       .eager_load(reservation: :nationality)
                                       .where(check_in_day: date_from..date_to, room_id: @search_room_id)
                                       .order(:check_in_day)
                                       .index_by(&:check_in_day)
    # 物件情報
    @room = Room.with_building.find_by(id: @search_room_id)

    reservation_date_hash = {}
    (date_from.to_date..date_to.to_date).each do |date|
      row = CSV_PRESENTER.new
      row.set_date(date: date, room_name: @room.full_code_name_with_underscore)

      if reservation_dates.key?(date)
        row.set_reservation(reservation_date: reservation_dates[date])
      end
      @monthly_list << row
    end
  end

end
