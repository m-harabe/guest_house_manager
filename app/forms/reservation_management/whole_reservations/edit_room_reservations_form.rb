# 一棟貸情報 各部屋毎の予約情報の編集用フォームクラス
class ReservationManagement::WholeReservations::EditRoomReservationsForm < ReservationManagement::WholeReservations::EditForm
  include CommonValidator
  attribute :room_reservations,    Array

  # validation
  # TODO: ステータスのバリデーション
  validate :validate_room_reservations

  def initialize(id: nil, params: nil)
    super(id: id)
    @total_count = @adult_count + @child_count
    @reservations = whole_reservation.reservations
    @room_names = Room.select(:id, :name).where(building_id: whole_reservation.building_id).index_by(&:id)

    # モーダルフォームの入力値をリストで保持
    @room_reservations = params if params
  end

  private

  def validate_room_reservations
    valid_require_and_number
    valid_total if self.errors.empty?
  end

  # 必須チェック、数値チェック
  def valid_require_and_number
    @totals = Hash.new(0)
    @room_reservations.each do |r|
      r.each do |k, v|
        next if ['reservation_id', 'room_id'].include?(k)

        # 各カラムの合計値を保持
        @totals[k.to_sym] += v.to_i
        next if v.present? && num?(v)
        add_error(:reservation,
                  :invalid_room_reservations,
                  room_name: room_names[r.fetch(:room_id).to_i].name,
                  column: I18n.t("activerecord.attributes.reservation.#{k}"))
      end
    end
  end

  # 合計チェック
  def valid_total
    error_key = []
    error_key << :adult_count if @adult_count != @totals[:adult_count]
    error_key << :child_count if @child_count != @totals[:child_count]
    error_key << :room_charge if @room_charge != @totals[:room_charge]
    error_key << :cleaning_fee if @cleaning_fee != @totals[:cleaning_fee]
    error_key << :meal_fee if @meal_fee != @totals[:meal_fee]
    error_key.each do |k|
      add_error(:reservation,
                :invalid_room_reservation_total,
                column: I18n.t("activerecord.attributes.reservation.#{k}"))
    end
  end
end
