class ReservationManagement::WholeReservations::EditPaymentForm < ReservationManagement::WholeReservations::EditForm

  # validation
  # TODO: ステータスのバリデーション
  validates :adult_count, presence: true
  validates :child_count, presence: true
  validates :room_charge, presence: true
  validates :cleaning_fee, presence: true
  validates :meal_fee, presence: true
  validates :site_code, presence: true

  def initialize(id: nil, params: nil)
    super(id: id, params: params)
    @total_count = @adult_count.to_i + @child_count.to_i
  end

end