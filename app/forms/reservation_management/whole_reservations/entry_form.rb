class ReservationManagement::WholeReservations::EntryForm < ApplicationForm

  attribute :id,                      Integer
  attribute :site_code,               String
  attribute :building_id,             Integer
  attribute :building_name,           String
  attribute :reserve_date,            Date
  attribute :check_in_date,           Date
  attribute :check_out_date,          Date
  attribute :number_of_nights,        Integer
  attribute :nationality_id,          Integer
  attribute :visitor_name,            String
  attribute :visitor_name_kana,       String
  attribute :mail_address,            String
  attribute :tel,                     String
  attribute :adult_count,             Integer, default: 0
  attribute :child_count,             Integer, default: 0
  attribute :total_count,             Integer, default: 0
  attribute :room_charge,             Integer
  attribute :cleaning_fee,            Integer
  attribute :meal_fee,                Integer
  attribute :total,                   Integer
  attribute :accommodation_tax,       Integer
  # attribute :handling_charge,         Integer
  attribute :note,                    String
  attribute :scheduled_check_in_time, DateTime
  attribute :display_check_in_time,   String   # scheduled_check_in_time の表示用 HH:MM

  # validation
  validate :validate_display_check_in_time
  validates_with ReservationManagement::WholeReservationValidator

  def initialize(params:)
    super()
    self.match_attributes = params
    if params.dig(:display_check_in_time).present?
      # フォームの「予定CI時刻」の入力の有無で実際DBに登録するDateTime型のscheduled_check_in_timeをセット
      @scheduled_check_in_time = @display_check_in_time.blank? ? nil : (@check_in_date.to_s + ' ' + @display_check_in_time)
    end
    @reserve_date ||= Date.current
    building = Building.find_by(id: @building_id)
    @building_name = "#{building.code}: #{building.name}"
    @total_count = @adult_count.to_i + @child_count.to_i
    @total = @room_charge.to_i + @cleaning_fee.to_i + @meal_fee.to_i
    # self.match_attributes = Reservation.new.attributes if (id.nil? && params.nil?)
  end

  private

  # DBのカラムに無い表示用の項目なので、From独自でバリデーションを行う
  # 冗長だがバリデータオブジェクトを生成してメソッドを利用
  def validate_display_check_in_time
    return if @display_check_in_time.blank?
    return if @display_check_in_time =~ CommonValidator::TIME_REGEX
    add_error("common", "format_time", {label: I18n.t("activerecord.attributes.reservation.display_check_in_time")} )
    @display_check_in_time = nil
  end
end