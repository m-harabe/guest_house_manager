class ReservationManagement::WholeReservations::EditForm < ApplicationForm
  include CommonValidator

  attribute :id,                      Integer
  attribute :site_code,               String
  attribute :status,                  String
  attribute :building_id,             Integer
  attribute :building_name,           String
  attribute :reserve_date,            Date
  attribute :check_in_date,           Date
  attribute :scheduled_check_in_time, DateTime
  attribute :display_check_in_time,   String
  attribute :check_out_date,          Date
  attribute :number_of_nights,        Integer
  attribute :nationality_id,          Integer
  attribute :nationality_name,        String
  attribute :visitor_name,            String
  attribute :visitor_name_kana,       String
  attribute :mail_address,            String
  attribute :tel,                     String
  attribute :adult_count,             Integer, default: 0
  attribute :child_count,             Integer, default: 0
  attribute :total_count,             Integer
  attribute :room_charge,             Integer
  attribute :cleaning_fee,            Integer
  attribute :meal_fee,                Integer
  attribute :total,                   Integer
  attribute :accommodation_tax,       Integer
  # attribute :handling_charge,         Integer
  attribute :note,                    String
  attribute :whole_reservation,       WholeReservation
  attribute :reservations,            Reservation
  attribute :room_names,              Hash
  attribute :is_available,            Boolean
  # attribute :callback,                String

  # validation
  # TODO: ステータスのバリデーション
  # validate :validate

  def initialize(id: nil, params: nil)
    super()
    @id = id || params[:id]
    @whole_reservation = WholeReservation.find(@id)
    @is_available = @whole_reservation.available?
    self.match_attributes = @whole_reservation.attributes
    self.match_attributes = params if params
  end

  # 更新成功後のビュー表示用
  def templete_val_for_update
    @whole_reservation    = WholeReservation.where(id: @id).eager_load(:nationality, :building, :reservations).take
    self.match_attributes = @whole_reservation.attributes
    @building_name        = whole_reservation.building.code_name
    @nationality_name     = whole_reservation.nationality_name

    @reservations = whole_reservation.reservations
    @room_names   = Room.select(:id, :name).where(building_id: whole_reservation.building_id).index_by(&:id)
  end

  def get_label(key)
    I18n.t("activerecord.attributes.reservation.#{key}")
  end

end