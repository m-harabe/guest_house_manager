class ReservationManagement::WholeReservations::ShowForm < ApplicationForm

  attribute :id,                      Integer
  attribute :link_reservation_id,     Integer # パンくず用リンク元の予約情報ID（あれば）
  attribute :site_code,               String
  attribute :status,                  String
  attribute :building_id,             Integer
  attribute :building_name,           String
  attribute :status,                  String
  attribute :reserve_date,            Date
  attribute :check_in_date,           Date
  attribute :scheduled_check_in_time, DateTime
  attribute :check_out_date,          Date
  attribute :number_of_nights,        Integer
  attribute :nationality_id,          Integer
  attribute :nationality_name,        String
  attribute :visitor_name,            String
  attribute :visitor_name_kana,       String
  attribute :mail_address,            String
  attribute :tel,                     String
  attribute :adult_count,             Integer
  attribute :child_count,             Integer
  attribute :total_count,             Integer
  attribute :room_charge,             Integer
  attribute :cleaning_fee,            Integer
  attribute :meal_fee,                Integer
  attribute :total,                   Integer
  attribute :accommodation_tax,       Integer
  attribute :note,                    String
  attribute :reservations,            Array
  attribute :room_names,              Hash
  attribute :is_available,            Boolean
  attribute :registration_type,       Integer

  def initialize(id: nil, reservation_id: nil)
    super()

    whole_reservation = WholeReservation.where(id: id).eager_load(:nationality, :building, :reservations).take if id
    return unless whole_reservation

    # 部屋名を取得
    @room_names = Room.select(:id, :name).where(building_id: whole_reservation.building_id).index_by(&:id)

    self.match_attributes   = whole_reservation.attributes
    @reservations           = whole_reservation.reservations
    @building_name          = whole_reservation.building.code_name
    @nationality_name       = whole_reservation.nationality_name
    @total_count            = whole_reservation.total_count
    @link_reservation_id    = reservation_id # パンくず用
    @is_available           = whole_reservation.available?
  end

end
