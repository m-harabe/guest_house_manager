class ReservationManagement::WholeReservations::EditBasicForm < ReservationManagement::WholeReservations::EditForm

  # validation
  # TODO: ステータスのバリデーション
  validates :reserve_date, presence: true
  validates :status, presence: true
  validates :visitor_name, presence: true
  validates :nationality_id, presence: true

  validate :validate_format
  validate :validate_date

  def initialize(id: nil, params: nil)
    super(id: id, params: params)
    if params
      # フォームの「予定CI時刻」の入力の有無で実際DBに登録するDateTime型のscheduled_check_in_timeをセット
      @scheduled_check_in_time = @display_check_in_time.blank? ? nil : (@whole_reservation.check_in_date.to_s + ' ' + @display_check_in_time)
    else
      # 画面初期表示時はDateTime型のカラムの値を表示用に "HH:MM" の文字列に変換
      @display_check_in_time = I18n.l(@whole_reservation.scheduled_check_in_time, format: :time) if @whole_reservation.scheduled_check_in_time
    end
  end

  private

  def validate_format
    return if @errors.present?
    add_error('common', 'format_mail', label: get_label(:mail_address)) unless mail?(@mail_address)

    return if visitor_name_kana.blank?
    add_error("common", "fomat_kana", {label: get_label(:visitor_name_kana)} ) unless kana?(visitor_name_kana)
  end

  def validate_date
    validator = ApplicationValidator.new
    unless validator.date?(@reserve_date.to_s)
      add_error('common', 'require', label: I18n.t("activerecord.attributes.reservation.reserve_date"))
    end
  end

  def validate_display_check_in_time
    return if @display_check_in_time.blank?
    return if time?(@display_check_in_time)
    add_error("common", "format_time", {label: I18n.t("activerecord.attributes.reservation.display_check_in_time")} )
    @display_check_in_time = nil
  end

end