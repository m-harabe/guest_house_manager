class ReservationManagement::Suitebook::EntryForm < ApplicationForm

  attribute :reservation_id,          Integer
  attribute :whole_reservation_id,    Integer
  attribute :site_code,               String
  attribute :status,                  String
  attribute :room_id,                 Integer
  attribute :reserve_date,            Date
  attribute :check_in_date,           Date
  attribute :check_out_date,          Date
  attribute :number_of_nights,        Integer
  attribute :nationality_id,          Integer
  attribute :visitor_name,            String
  attribute :mail_address,            String
  attribute :tel,                     String
  attribute :adult_count,             Integer, default: 0
  attribute :child_count,             Integer, default: 0
  attribute :total_count,             Integer, default: 0
  attribute :room_charge,             Integer
  attribute :cleaning_fee,            Integer
  attribute :meal_fee,                Integer
  attribute :total,                   Integer
  attribute :handling_charge,         Integer
  attribute :note,                    String
  attribute :scheduled_check_in_time, DateTime
  attribute :accommodation_tax,       Integer

  attribute :suitebook_reservation,   SuitebookReservation

  # validation
  validates_with ReservationManagement::ReservationValidator

  # 新規登録時、更新時共通の初期化処理
  # TODO ながいのでリファクタしたい
  def initialize(suitebook_reservation)
    super()

    @suitebook_reservation = suitebook_reservation
    set_reservation

    # @reservation_id = reservation_id
    # self.match_attributes = Reservation.find_by(id: @reservation_id).attributes if @reservation_id
    # if params
    #   self.match_attributes = params
    #   # フォームの「予定CI時刻」の入力の有無で実際DBに登録するDateTime型のscheduled_check_in_timeをセット
    #   @scheduled_check_in_time = @display_check_in_time.blank? ? nil : (@check_in_date.to_s + ' ' + @display_check_in_time)
    # else
    #   # 画面初期表示時はDateTime型のカラムの値を表示用に "HH:MM" の文字列に変換
    #   @display_check_in_time = I18n.l(@scheduled_check_in_time, format: :time) if @scheduled_check_in_time
    # end

    # # 何故か本番環境のみ、default: Date.today がサーバーの起動日のまま進まないので別途対応
    # # Virtusのデフォルト値設定がシングルトンなのか？？
    # @reserve_date ||= Date.current
    # @total_count = @adult_count.to_i + @child_count.to_i
    # @total = @room_charge.to_i + @cleaning_fee.to_i
  end

  private

  def set_reservation

  end


end