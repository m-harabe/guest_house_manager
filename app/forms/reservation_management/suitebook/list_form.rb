class ReservationManagement::Suitebook::ListForm < ApplicationForm

  SEARCH_MAX_COUNT = 500

  # 検索条件

  # attribute :search_room_id,          Integer, default: Room.visible.pluck(:id).first
  attribute :search_import_date_from, Date
  attribute :search_import_date_to,   Date
  attribute :search_check_in_date,    Date
  attribute :search_check_out_date,   Date
  attribute :search_status,           Integer

  attribute :search_site_code,        String
  attribute :search_visitor_name,     String
  attribute :search_mail_address,     String
  attribute :message,                 String

  # 一覧表示用
  attribute :suitebook_reservations,  Array[SuitebookReservation]

  def initialize(params = nil)
    super(params)
    if params
      self.match_attributes = params
    else
      @search_import_date_from = Date.current
      @search_import_date_to = Date.current
    end
    fetch_list
  end

  private

  def fetch_list

    q = {}
    q[:created_at_gteq]            = search_import_date_from if search_import_date_from
    q[:created_at_lteq_end_of_day] = search_import_date_to if search_import_date_to
    q[:check_in_date_gteq]         = search_check_in_date if search_check_in_date
    q[:check_out_date_lteq]        = search_check_out_date if search_check_out_date
    q[:visitor_name_cont]          = search_visitor_name if search_visitor_name
    q[:status_eq]                  = search_status if search_status

    # q[:offer_route_eq]      = search_offer_route if search_offer_route
    # q[:status_in]           = search_status
    # q[:send_mail_status_in] = search_send_mail_status
    # q[:screening_result_in] = search_screening_result
    # q[:kana1_cont]          = search_kana1 if search_kana1
    # q[:tel_for_search_eq]   = search_tel.gsub("-", "") if search_tel
    # q[:mail_eq]             = search_mail if search_mail

    search = SuitebookReservation.eager_load(room: [:building]).ransack(q)
    @count = search.result.count
    @message = I18n.t "messages.search_count", count: @count, max: SEARCH_MAX_COUNT if @count > SEARCH_MAX_COUNT
    @suitebook_reservations = search.result.limit(SEARCH_MAX_COUNT).display_order
  end

end
