# 未使用
class ReservationManagement::Reservations::EditReservationDateForm < ApplicationForm

  attribute :id,                  Integer
  attribute :reservation,         Reservation
  attribute :reservation_id,      Integer
  attribute :reservation_dates,   Array # 画面の行リストで各パラメータのハッシュを格納

  validate :validate_reservation_date

  def initialize(reservation_id, params = nil)
    super()
    @reservation_id = reservation_id
    # TODO JOINSでもいいかも
    # TODO order by checkindayを指定
    @reservation = Reservation.where(id: @reservation_id).eager_load(:reservation_dates).last
    @reservation_dates = params[:reservation_dates] if params
    @is_available = @reservation.available?
  end

  private

  def set_reservation_dates
    # @reservation_dates = @reservation.reservation_dates.index_by(&:id).tap do |reservation_date|
    @reservation.reservation_dates.index_by(&:id).tap do |rd|
      # パラメータのリストをループしてIDが合致するモデルにセット
      @reservation_dates.each do |attr|
        next unless rd.key? attr.dig(:reservation_date_id).to_i
        model = rd[attr.dig(:reservation_date_id).to_i]
        model.room_charge = attr.dig(:room_charge).to_i
        model.cleaning_fee = attr.dig(:cleaning_fee).to_i
        model.meal_fee = attr.dig(:meal_fee).to_i
      end
    end
  end

  def validate_reservation_date

    validate_require if errors.empty?
    validate_total if errors.empty?
  end

  def validate_require
    @reservation_dates.each.with_index(1) do |params, idx|
      add_error 'common', 'list_gt_zero', row: idx, name: '宿泊料' unless params[:room_charge] =~ /^[0-9]+$/
      add_error 'common', 'list_gt_zero', row: idx, name: '清掃費' unless params[:cleaning_fee] =~ /^[0-9]+$/
      add_error 'common', 'list_gt_zero', row: idx, name: '食事代' unless params[:meal_fee] =~ /^[0-9]+$/
    end
  end

  def validate_total
    set_reservation_dates
    if @reservation.room_charge != @reservation.reservation_dates.pluck(:room_charge).inject(:+)
      add_error 'common', 'invalid_total', label: '宿泊料'
    end
    if @reservation.cleaning_fee != @reservation.reservation_dates.pluck(:cleaning_fee).inject(:+)
      add_error 'common', 'invalid_total', label: '清掃費'
    end
    if @reservation.meal_fee != @reservation.reservation_dates.pluck(:meal_fee).inject(:+)
      add_error 'common', 'invalid_total', label: '食事代'
    end
  end

end