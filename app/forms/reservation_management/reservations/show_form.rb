class ReservationManagement::Reservations::ShowForm < ApplicationForm

  attribute :id,                       Integer
  attribute :site_code,                String
  attribute :status,                   String
  attribute :room_id,                  Integer
  attribute :whole_reservation_id,     Integer
  attribute :room_name,                String
  attribute :reserve_date,             Date
  attribute :check_in_date,            Date
  attribute :scheduled_check_in_time,  DateTime
  attribute :check_out_date,           Date
  attribute :number_of_nights,         Integer
  attribute :nationality_id,           Integer
  attribute :nationality_name,         String
  attribute :visitor_name,             String
  attribute :visitor_name_kana,        String
  attribute :mail_address,             String
  attribute :tel,                      String
  attribute :adult_count,              Integer
  attribute :child_count,              Integer
  attribute :total_count,              Integer
  attribute :room_charge,              Integer
  attribute :cleaning_fee,             Integer
  attribute :meal_fee,                 Integer
  attribute :total,                    Integer
  attribute :point_use,                Integer
  attribute :total_handling_charge,    Integer
  attribute :accommodation_tax,        Integer
  attribute :note,                     String
  attribute :owner_note,               String
  attribute :reservation_dates,        Array
  attribute :is_whole,                 Boolean
  attribute :is_available,             Boolean
  attribute :registration_type,        Integer

  def initialize(params: nil, id: nil, callback: nil)
    super()

    # TODO order by checkindayを指定
    reservation = Reservation.where(id: id).eager_load(:nationality, :room, :reservation_dates).take if id
    return unless reservation
    self.match_attributes   = reservation.attributes
    @reservation_dates      = reservation.reservation_dates
    @room_name              = reservation.room.full_code_name
    @nationality_name       = reservation.nationality&.name
    @total_count            = reservation.total_count
    @total_handling_charge  = reservation.handling_charge
    @is_whole               = reservation.whole?
    @is_available           = reservation.available?
    @callback               = callback
  end

  private

end