class Modal::SearchReservation::PreviousReservationForm < ApplicationForm

  attribute :check_in_day,          Date
  attribute :number_of_nights,      Integer
  attribute :reservation_id,        Integer
  attribute :building_id,           Integer
  attribute :room_id,               Integer
  attribute :room_name,             String

  attribute :building_list,         Array
  attribute :reservation_date_list, Array


  def initialize(params)
    self.match_attributes = params
    fetch_room_name
    search_reservation
  end

  private

  # def search_building_list
  #   @building_list = Building.all.eager_load(:rooms)
  # end

  def fetch_room_name
    if @building_id
      building = Building.find_by(id: @building_id)
      @room_name = "#{building.code}: #{building.name}"
    else
      room = Room.with_building.find_by(id: @room_id)
      @room_name = room.full_code_name_with_underscore
    end
  end

  def search_reservation
    conditions = {}.tap do |c|
      c[:room_id] = @room_id if @room_id
      c[:building_id] = @building_id if @building_id
      c[:check_in_date] = @check_in_day
      c[:number_of_nights] = @number_of_nights
      c[:reservation_id] = @reservation_id.to_i if @reservation_id.to_i > 0
    end
    @reservation_date_list = ReservationDate.search_room_reservation(conditions).preload(:reservation, {room: :building}).limit(5)
  end

end
