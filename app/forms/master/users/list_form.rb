class Master::Users::ListForm < ApplicationForm

  attribute :list,             Array

  def initialize
    find_user_list
  end

  private

  def find_user_list
    @list = User.list_by_role
  end

end
