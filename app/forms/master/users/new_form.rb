class Master::Users::NewForm < Master::Users::UserBaseForm

  def initialize(params=nil)
    super
    self.match_attributes = params if params
  end

end