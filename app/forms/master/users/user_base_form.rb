class Master::Users::UserBaseForm < ApplicationForm

  attribute :id,                      Integer
  attribute :account,                 String
  attribute :role,                    Integer
  attribute :name,                    String
  attribute :short_name,              String
  attribute :email,                   String
  attribute :tel,                     String
  attribute :fax,                     String
  attribute :password,                String
  attribute :password_confirmation,   String
  attribute :user,                    User

  # ------------------------------------------------------------------
  # Validations
  # ------------------------------------------------------------------
  # presence
  validates :account,    presence: true
  validates :role,       presence: true
  validates :name,       presence: true
  validates :password,   presence: true
  validates :password_confirmation, presence: true

  # length
  validates :account,    length: { in: 4..20 }, allow_blank: true
  validates :password,   length: { in: 6..128 }, allow_blank: true
  # validates :tel,        length: { maximum: 13 }, allow_blank: true # 11桁＋ハイフォン2つ
  # validates :fax,        length: { maximum: 13 }, allow_blank: true

  # inclusion
  # validates :role, inclusion: { in: %w(10 20 31) }
  validates :role, inclusion: { in: [1,2,3,4] }, allow_blank: true # 0 SYSTEM以外

  # format
  # validates :mail, format: { with: mail_REGEX }, allow_blank: true

  # uniquness
  # validates :account,    uniqueness: true, allow_blank: true

  # validation
  validate :validate_form

  def initialize(params)
    super
  end

  private

  # フォームバリデーションの実施

  def get_label(key)
    I18n.t("activerecord.attributes.user.#{key}")
  end

  # バリデーション（Userに関してはフォーム側でバリデーションする）
  def validate_form
    # validate_require
    validate_form_unique_columns if errors.empty?
    validate_form_format if errors.empty?
    # validate_form_length if errors.empty?
    # パスワードの設定は新規登録時のみ受け付ける（仮パスワード発行用）
    validate_form_password if errors.empty? && @id.nil?
  end

  # 形式チェック
  def validate_form_format
    validator = ApplicationValidator.new
    add_error 'common', 'format_code_underscore', label: get_label(:account) unless validator.code?(account, true)
    add_error 'common', 'format_mail', label: get_label(:email) unless validator.mail?(email)
    add_error 'common', 'format_num_hyphen', label: get_label(:tel) unless validator.num_hyphen?(tel)
  end

  # 一意性チェック
  def validate_form_unique_columns
    user_account = User.where(account: account)
    user_account.where.not(id: id) if id
    add_error 'common', 'already_exists', label: get_label(:account) unless user_account.blank?

    # MEMO: メールアドレスは複数事務所で共有されているようなので、一旦バリデーションをオフ
    # if mail.present?
    #   user_mail = User.where(mail: mail)
    #   user_mail.where.not(id: id) if id
    #   add_error 'common', 'already_exists', label: get_label(:mail) unless user_mail.blank?
    # end
  end

  # パスワードの必須チェック(新規登録時のみ)
  def validate_form_password
    if @password.blank? || @password_confirmation.blank?
      add_error "common", "require", label: get_label(:password)
    else
      add_error("user", "invalid_password") unless @password == @password_confirmation
    end
  end
end