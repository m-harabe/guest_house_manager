class Master::Buildings::EntryRoomForm < ApplicationForm

  attribute :room_id,              Integer
  attribute :building_id,          Integer
  attribute :name,                 String
  attribute :room_no,              Integer
  attribute :is_visible,           Boolean
  attribute :suitebook_room_idnum, Integer
  attribute :note,                 String

  attribute :total_floor_area,     Integer,    default: 0
  attribute :standard_people,      Integer,    default: 0
  attribute :max_people,           Integer,    default: 0
  attribute :bath_count,           Integer,    default: 0
  attribute :toilet_count,         Integer,    default: 0
  attribute :secret_number,        Integer

  attribute :room_list,            Array

  attr_accessor :building

  # validation
  validates_with Master::RoomValidator

  def initialize(building_id: nil, room_id: nil, params: nil)
    super()
    @building_id = building_id || params.try(:fetch, :building_id)
    @room_id = room_id
    @building = Building.eager_load(:rooms).find_by(id: @building_id) if @building_id
    if room_id
      room = Room.eager_load(:profile).find_by(id: room_id)
      if room
        self.match_attributes = room.attributes
        self.match_attributes = room.profile.attributes
        @building = room.building
      end
    end

    # 登録更新時のパラーメータセット
    self.match_attributes = params if params
    @name.strip! if @name
  end

  def fetch_rooms
    @room_list = Room.eager_load(:profile).where(building_id: building_id).display_order
  end

end