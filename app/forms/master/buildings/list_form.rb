class Master::Buildings::ListForm < ApplicationForm

  attribute :list,             Array

  def initialize(params = nil)
    find_building_list
  end

  private

  def find_building_list
    @list = Building.all.eager_load(:rooms, :area, :owner)
  end

end
