class Master::Buildings::EntryForm < ApplicationForm

  attribute :id,                   Integer
  attribute :owner_id,             Integer
  attribute :code,                 String
  attribute :name,                 String
  attribute :reward_rate,          Float
  attribute :postal_code,          String
  attribute :address,              String
  attribute :room_count,           Integer
  attribute :is_visible,           Boolean
  attribute :note,                 String

  attribute :total_floor_area,     Integer
  attribute :standard_people,      Integer
  attribute :max_people,           Integer
  attribute :bath_count,           Integer
  attribute :toilet_count,         Integer
  attribute :secret_number,        Integer

  # validation
  validates_with Master::BuildingValidator

  def initialize(params: nil, id: nil)
    super()
    if id
      building = Building.eager_load(:profile).find_by(id: id)
      self.match_attributes = building.profile.attributes
      self.match_attributes = building.attributes
      @id = id # IDは上記match_attributesの順序に影響されるため、明示的に再セット
    end
    self.match_attributes = params if params
  end

end