class Master::Buildings::ShowForm < ApplicationForm

  attribute :building_id,          Integer
  attribute :code,                 String
  attribute :name,                 String
  attribute :postal_code,          String
  attribute :address,              String
  attribute :room_count,           Integer
  attribute :is_visible,           Boolean
  attribute :note,                 String
  attribute :area_name,            String
  attribute :owner_name,           String
  attribute :reward_rate,          Float

  attribute :total_floor_area,     Integer
  attribute :standard_people,      Integer
  attribute :max_people,           Integer
  attribute :bath_count,           Integer
  attribute :toilet_count,         Integer
  attribute :secret_number,        Integer

  attribute :room_list,            Array

  def initialize(building_id)
    super()
    # building = Building.eager_load(:profile, {rooms: :profile}).find_by(id: building_id)
    building = Building.eager_load(:profile, :area, :owner).find_by(id: building_id)
    self.match_attributes = building.profile.attributes
    self.match_attributes = building.attributes
    @area_name = building.area&.name
    @owner_name = building.owner&.name || '未設定'
    @room_list = Room.eager_load(:profile).where(building_id: building_id).display_order
    @building_id = building_id
  end
end