class Summary::SalesListForm < ApplicationForm

  # 検索条件
  attribute :search_room_ids,         Array
  attribute :search_area_id,          Integer
  attribute :search_check_in_month,   String
  attribute :search_original_month,   Date
  attribute :search_prev_next_month,  Date

  # 一覧表示用
  attribute :day_count,               Integer
  # attribute :monthly_total,           Integer, default: 0
  attribute :monthly_total,           Hash
  attribute :rooms,                   Hash # {room_id => Room},...
  attribute :room_id_hash,            Hash # {room_id => Room},...

  # 非表示物件の表示フラグ
  attribute :show_all_rooms,          Boolean, default: false
  # 年月数字6桁（精算書リンク生成用）
  attribute :year_month,              String

  def initialize(params = nil, is_show_all_rooms: false)
    super(params)
    @show_all_rooms = true if is_show_all_rooms
    if show_all_rooms
      @rooms = Room.with_invisible_building
    else
      @rooms = Room.visible.with_building
    end
    @rooms = @rooms.where(id: @search_room_ids) if @search_room_ids.present?
    @rooms = @rooms.index_by(&:id)

    if params.present?
      self.match_attributes = params
      @search_room_ids.delete('')
      @search_room_ids = @search_room_ids.uniq.map(&:to_i)
    else
      @search_room_ids = @rooms.keys
      @search_check_in_month ||= I18n.l(Date.current, format: :year_month)
    end
  end

  def fetch_list(is_csv: false)
    @monthly_list = []
    @monthly_total = Hash.new(0)

    date_from = set_prev_next_month
    date_to = date_from.in_time_zone.end_of_month

    # 日数
    @day_count = date_to.day

    # 前月・次月表示用（今月を基準に＋１, -１する）
    @search_original_month = date_from.to_date

    # DISTINCT reservation_dates.reservation_id: 予約件数、前月からの月マタギの連泊も１件にカウント
    room_sum_list = ReservationDate.search_not_cancel
                                   .where(check_in_day: date_from..date_to, room_id: @search_room_ids)
                                   .group(:room_id)
                                   .pluck(Arel.sql(
                                          'reservation_dates.room_id,
                                           COUNT(*),
                                           SUM(reservation_dates.room_charge),
                                           SUM(reservation_dates.cleaning_fee),
                                           SUM(reservation_dates.meal_fee),
                                           SUM(reservation_dates.total),
                                           SUM(reservations.adult_count),
                                           SUM(reservations.child_count),
                                           COUNT(DISTINCT reservation_dates.reservation_id)'
                                          )
                                   )

    room_sum_list.each do |ar|
      # room_idをキーにしたハッシュの値に各項目をさらにハッシュで格納していく
      room_id = ar[0]
      @room_id_hash[room_id] = {}

      # 稼働日
      operation_count = ar[1].to_i

      # 宿泊料合計
      sum_room_charge = ar[2].to_i

      # 清掃費合計
      sum_cleaning_fee = ar[3].to_i

      # 食事代合計
      sum_meal_fee = ar[4].to_i

      # 売上合計
      sum_total = ar[5].to_i

      # ゲスト数
      sum_guest = ar[6].to_i + ar[7].to_i

      # OCC 稼働率 = 稼働日 / 日数
      occ = operation_count.quo(@day_count)

      # DOR 1日あたりの平均宿泊人数 = ゲスト数 / 稼働日数
      dor = sum_guest.quo(operation_count)

      # ADR 平均客室単価 = 月間売上 / 稼働日 （四捨五入）
      adr = sum_total.quo(operation_count)

      # 人数単価 = ADR / ODR
      unit_price = adr.quo(dor) rescue 0

      # RevPAR 販売可能な客室1室あたりの売上 = ADR * OCC
      rev_par = (adr * occ)

      # Revenue = RevPAR * 30
      # revenue = rev_par * 30

      # 計算した値をビュー用、CSV用で加工してハッシュに格納
      @room_id_hash[room_id][:operation_count] = operation_count
      @room_id_hash[room_id][:sum_guest] = sum_guest
      unless is_csv
        # 一覧表示用月次合計金額
        # @monthly_total += sum_total
        @monthly_total[:total] += sum_total
        @monthly_total[:total_room_charge] += sum_room_charge
        @monthly_total[:total_cleaning_fee] += sum_cleaning_fee
        @monthly_total[:total_meal_fee] += sum_meal_fee
        @room_id_hash[room_id][:sum_room_charge] = sum_room_charge.to_s(:delimited)
        @room_id_hash[room_id][:sum_cleaning_fee] = sum_cleaning_fee.to_s(:delimited)
        @room_id_hash[room_id][:sum_meal_fee] = sum_meal_fee.to_s(:delimited)
        @room_id_hash[room_id][:sum_total] = sum_total.to_s(:delimited)
        @room_id_hash[room_id][:occ] = ActiveSupport::NumberHelper.number_to_percentage(occ.to_f * 100, precision: 1)
        @room_id_hash[room_id][:dor] = dor.to_f.round(1)
        @room_id_hash[room_id][:adr] = adr.to_f.round.to_s(:delimited)
        @room_id_hash[room_id][:unit_price] = unit_price.to_f.round.to_s(:delimited)
        @room_id_hash[room_id][:rev_par] = rev_par.round.to_s(:delimited)
        @room_id_hash[room_id][:reservation_count] = ar[8]
      else
        @room_id_hash[room_id][:sum_room_charge] = sum_room_charge.to_s(:delimited)
        @room_id_hash[room_id][:sum_cleaning_fee] = sum_cleaning_fee.to_s(:delimited)
        @room_id_hash[room_id][:sum_meal_fee] = sum_meal_fee.to_s(:delimited)
        @room_id_hash[room_id][:sum_total] = sum_total.to_s(:delimited)
        @room_id_hash[room_id][:occ] = ActiveSupport::NumberHelper.number_to_percentage(occ.to_f * 100, precision: 1)
        @room_id_hash[room_id][:dor] = dor.to_f.round(1)
        @room_id_hash[room_id][:adr] = adr.to_f.round(2)
        @room_id_hash[room_id][:unit_price] = unit_price.to_f.round(2)
        @room_id_hash[room_id][:rev_par] = rev_par.to_f.round(2)
        @room_id_hash[room_id][:reservation_count] = ar[8]
      end
    end
  end

  def csv_file_name
    file_name = @search_check_in_month + '全体売上_' + Time.zone.now.strftime('%Y%m%d%H%M%S') + '.csv'
  end

  def get_settlement_by_room
    # @year_month は「次月」,「前月」のときにずれるので、@search_original_month を検索条件に使う
    Settlement.select(:id, :room_id, :created_at).where(year_month: search_original_month.strftime('%Y%m')).index_by(&:room_id)
  end

  private

  # 検索開始日をセット
  # 前月・次月ボタン押下時はどちらかの月を検索月に再セットし、検索月セレクトの値を上書きする
  def set_prev_next_month
    if @search_prev_next_month.present?
      @search_check_in_month = I18n.l(@search_prev_next_month, format: :year_month)
    end
    @year_month = search_check_in_month[0..3] + search_check_in_month[5..6]
    @search_prev_next_month = nil
    # TimeWithZoneに変換
    parse_start_date(@search_check_in_month)
  end

  # 画面のYY年MM月表示をTimeWithZoneに変換
  # @param str [String] YY年MM月
  # @return [ActiveSupport::TimeWithZone] 該当月の1日
  def parse_start_date(str)
    return unless str
    (str.gsub(/['年','月']/, '-') << '01').in_time_zone
  end

end
