#
# 宿泊税計算
# 宿泊者1人1泊につき以下の宿泊料金が徴収される
# - 2万円未満のもの 200円
# - 2万円以上5万円未満のもの 500円
# - 5万円以上のもの 1,000円
#
#
class Summary::Reports::AccommodationTaxCsvForm < ApplicationForm

  LIMITS = [20000, 50000]
  TAXES  = [  200,   500,   1000]

  ACTION_NAME_DAILY = 'accommodation_tax'.freeze
  ACTION_NAME_ROOM = 'accommodation_tax_for_room'.freeze
  ACTION_NAME_BUILDING = 'accommodation_tax_for_building'.freeze
  ACTION_NAME_DATE_ROOM = 'accommodation_tax_for_date_room'.freeze
  FILE_NAMES = {
    ACTION_NAME_DAILY => '日別宿泊税',
    ACTION_NAME_ROOM => '部屋別宿泊税',
    ACTION_NAME_BUILDING => '施設別宿泊税',
    ACTION_NAME_DATE_ROOM => '日別部屋別宿泊税'
  }.freeze

  attr_accessor :check_in_day_range,
                :csv_row_datas,
                :buildings,
                :rooms,
                :file_name

  def initialize(search_month:, action_name:)
    @csv_row_datas = {}
    parse_range_date(search_month: search_month, action_name: action_name)
    case action_name
    when ACTION_NAME_DAILY
      fetch_daily_list
    when ACTION_NAME_ROOM
      fetch_room_list
    when ACTION_NAME_BUILDING
      fetch_building_list
    when ACTION_NAME_DATE_ROOM
      fetch_daily_room_list
    end
  end

  private

  # 画面の'YY年MM月'表示をDateオブジェクトに変換後、月初-月末の範囲オブジェクト作成およびファイル名生成
  # @param [String] search_month YY年MM月
  # @param [String] action_name 呼び出し元のコントローラアクション名
  def parse_range_date(search_month:, action_name:)
    year_month = search_month.gsub(/[年,月]/, '/').to_date rescue Date.current
    @check_in_day_range = year_month.all_month
    @file_name = I18n.l(year_month, format: :year_month) + '_' + FILE_NAMES[action_name] + '_' + Time.zone.now.strftime('%Y%m%d%H%M%S') + '.csv'
  end

  # 日別に1ヶ月間の全施設トータルの宿泊税を集計
  # @csv_row_datas [Hash<String, Hash<Symbol, Object>>] 日別（CSVの行別）の集計データ
  # e.g) { "2018-09-27" => { 200 => 10, 500 => 2, 1000 => 0, 'tax_total' => 3000 } }
  #         ↑日付            ↑税区分 ↑合計人数               ↑税額総合計     ↑金額
  def fetch_daily_list
    # 日付をキーにしたバリユーの中のネストしたハッシュを 0 で初期化
    @check_in_day_range.each { |date| @csv_row_datas[date] = Hash.new { |hsh, key| 0 } }
    ReservationDate.search_not_cancel.where(check_in_day: @check_in_day_range).each do |rd|
      row_data = @csv_row_datas[rd.check_in_day]
      generate_csv_row(reservation_date: rd, row_data: row_data) if row_data
    end
  end

  # 部屋別に1ヶ月間の宿泊税を集計
  # @csv_row_datas [Hash<String, Hash<Symbol, Object>>] 日別（CSVの行別）の集計データ
  # e.g) { room_id => { name => "1001_紡 東福寺南門", 200 => 40, 500 => 2, 1000 => 0, 'tax_total' => 9000 } }
  #        ↑部屋ID               ↑部屋名            ↑税区分 ↑合計人数                   ↑税額総合計     ↑金額
  def fetch_room_list
    @rooms = Room.display_order.with_building.index_by(&:id)
    @rooms.keys.each { |id| @csv_row_datas[id] = Hash.new { |hsh, key| 0 } }
    ReservationDate.search_not_cancel.where(check_in_day: @check_in_day_range).each do |rd|
      row_data = @csv_row_datas[rd.room_id]
      generate_csv_row(reservation_date: rd, row_data: row_data) if row_data
    end
  end

  # 施設別に1ヶ月間の宿泊税を集計
  # @csv_row_datas [Hash<String, Hash<Symbol, Object>>] 施設別（CSVの行別）の集計データ
  # e.g) { building_id => { name => "1001_紡 東福寺南門", 200 => 40, 500 => 2, 1000 => 0, 'tax_total' => 9000 } }
  #        ↑施設ID                  ↑施設名             ↑税区分 ↑合計人数                   ↑税額総合計     ↑金額
  def fetch_building_list
    @buildings = Building.display_order.index_by(&:id)
    @buildings.keys.each { |id| @csv_row_datas[id] = Hash.new { |hsh, key| 0 } } # 初期値０のハッシュ
    ReservationDate.search_not_cancel.where(check_in_day: @check_in_day_range).each do |rd|
      row_data = @csv_row_datas[rd.building_id]
      generate_csv_row(reservation_date: rd, row_data: row_data) if row_data
    end
  end

  # 日別に各部屋のの宿泊税を集計
  # @csv_row_datas [Hash<String, Hash<Symbol, Object>>] 日別（CSVの行別）の集計データ
  # e.g) { "2018-09-27" => { 200 => 10, 500 => 2, 1000 => 0, 'tax_total' => 3000 } }
  #         ↑日付            ↑税区分 ↑合計人数               ↑税額総合計     ↑金額
  # def fetch_daily_room_list

  # end

  # 日別集計のハッシュ生成
  def generate_csv_row(reservation_date:, row_data:)

    base_tax, total_count = calc_base_tax(reservation_date)
    return unless base_tax

    # 税区分ごとに合計人数を加算
    row_data[base_tax] += total_count

    # 税区分ごとに予約件数（日数）を加算
    # row_data[base_tax.to_s + '_count'] += 1

    # 税額総合計
    row_data[:tax_total] += (base_tax * total_count)
  end

  # 税区分を算出
  # @param [ReservationDate]
  # @return [Integer] base_tax 税区分（一人あたりの宿泊税学）
  # @return [Integer] total_count 宿泊人数
  # @raise [ZeroDivisionError] 一棟貸しで人数を分配して０にした場合
  # 宿泊人数０で金額が０でない場合でも宿泊税は０になるので不整合が生じる
  # とりあえず落ちないように暫定的な例外処理を行っている
  def calc_base_tax(reservation_date)
    # 部屋の宿泊人数
    total_count = reservation_date.reservation.total_count
    begin
      # 一人あたりの宿泊料金
      target = reservation_date.total / total_count
    rescue ZeroDivisionError # total_countが0の場合
      return nil, nil
    end

    # 宿泊料金によって決定する税区分を取得
    base_tax = nil
    LIMITS.each_with_index do |limit, idx|
      if target < limit
        base_tax = TAXES[idx]
        break
      end
    end
    # 税区分の上限
    base_tax ||= TAXES[-1]
    return base_tax, total_count
  end
end
