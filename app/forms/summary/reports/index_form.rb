class Summary::Reports::IndexForm < ApplicationForm

  # 検索条件
  attr_accessor :search_month

  def initialize
    @search_month ||= I18n.l(Date.current, format: :year_month)
  end
end