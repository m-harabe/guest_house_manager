class Master::OwnersController < ApplicationController

  def index
    # [
    #   { name: "ヤン・ウェンリー", postal_code: '534-0001', address: '大阪府大阪市都島区毛馬町１−１−１', tel: '090-0000-0000', bank_name: '東京三菱UFJ銀行', account_number: '1234567890'},
    #   { name: "大谷　翔平", postal_code: '530-0001', address: '大阪府大阪市北区梅田１２３', tel: '090-0000-0000', bank_name: 'GMOあおぞらネット銀行', account_number: '1234567890'},
    #   { name: "佐藤てるあき", postal_code: '534-0002', address: '大阪府大阪市北区曽根崎新地', tel: '090-0000-0000', bank_name: '東京三菱UFJ銀行', account_number: '1234567890'},
    # ].each do |owner|
    #   Owner.create!(owner)
    # end

    # 非表示をどうするか
    @owners = Owner.preload(:buildings).all
  end

  def new
    @owner = Owner.new
  end


  def edit
    @owner = Owner.find_by(id: params[:id])
    if @owner.nil?
      flash[:error] = "オーナー情報が存在しません。"
      redirect_to master_owners_path
    end
    @building_list = Building.includes(:area).where(owner_id: @owner.id).display_order
  end

  def create
    @owner = Owner.new(create_params)
    if @owner.valid?
      @owner.save!
      flash[:success] = "#{@owner.name}様のオーナー情報を登録しました。続けて施設の編集画面からオーナーとの紐付けをおこなってください。"
      redirect_to master_owners_edit_path(@owner)
    else
      render :new
    end
  rescue => ex
    output_error_log(ex, "オーナー 登録処理")
    flash[:error] = "エラーが発生したため、処理を中止ました。"
    redirect_to master_owners_path
  end

  def update
    @owner = Owner.find_by(id: update_params[:id])
    @owner.match_attributes = update_params

    if @owner.valid?
      ActiveRecord::Base.transaction do
        @owner.save!
        flash[:success] = "#{@owner.name}様のオーナー情報を更新しました。"
        redirect_to master_owners_path
      end
    else
      @building_list = Building.includes(:area).where(owner_id: @owner.id).display_order
      render :edit
    end
  rescue => ex
    output_error_log(ex, "オーナー 更新処理")
    flash[:error] = "エラーが発生したため、処理を中止ました。"
    redirect_to master_owners_path
  end

  private

  def create_params
    params.require(:owner).permit(
      :name, :postal_code, :address1, :address2, :tel, :mail_address, :bank_name, :account_type, :account_number,
      :swift_code, :account_name, :language, :note, :is_visible
    )
  end

  def update_params
    params.require(:owner).permit(
      :name, :postal_code, :address1, :address2, :tel, :mail_address, :mail_address2, :mail_address3, :bank_name, :account_type, :account_number,
      :swift_code, :account_name, :language, :note, :is_visible,
      :id, building_ids: [])
  end

end
