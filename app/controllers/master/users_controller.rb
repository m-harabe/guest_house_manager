class Master::UsersController < ApplicationController

  def index
    unless User.current_user.role <= Settings.USER.ROLE_MANAGER
      redirect_to dashboard_path
    end
    @form = Master::Users::ListForm.new
  end

  def new
    @form = Master::Users::NewForm.new
  end

  def create
    @form = Master::Users::NewForm.new(create_params)
    # @form.errors.clear
    begin
      if @form.valid?
        user = User.create!(create_params)
        flash[:success] = "新規アカウント【#{user.account}: #{user.name}】を登録しました。"
        redirect_to master_users_path
      else
        render "new"
      end
    rescue => ex
      output_error_log(ex, 'アカウント一覧 登録処理')
      flash[:error] = I18n.t("errors.common.rollback", {action: "登録"})
      redirect_to master_users_new_path
    end
  end

  def edit
    @user = User.find_by(id: params[:id])
    if @user.nil?
      flash[:error] = "ユーザー情報が存在しません。"
      redirect_to master_users_path
    end
  end

  def update
    @user = User.find(update_params[:id])
    atttibutes = update_params

    # パスワードが空の場合はパラメーターのキーを削除
    if atttibutes[:password].blank? && atttibutes[:password_confirmation].blank?
      atttibutes.delete(:password)
      atttibutes.delete(:password_confirmation)
    end

    # パラメータの値でDeviseのバリデーションを通す
    @user.match_attributes = atttibutes

    if @user.valid?
      @user.save!
      flash[:success] = "アカウント: #{@user.account} #{@user.name}】のアカウント情報を更新しました。"
      redirect_to master_users_path
    else
      render :edit
    end
  rescue => ex
    output_error_log(ex, 'アカウント編集 更新処理')
    flash[:error] = "エラーが発生したため、処理を中止ました。"
    render :edit
  end

  # def update
  #   @user = User.find(update_params[:id])

  #   # パラメータの値でDeviseのバリデーションを通す
  #   @user.match_attributes = update_params
  #   @user.valid?(:update_other)

  #   # パスワードが空のエラーの場合は無視する
  #   if @user.errors.details[:password][0][:error] == :blank
  #     @user.errors.details.delete(:password)
  #     @user.errors.messages.delete(:password)
  #   end

  #   if @user.errors.empty?
  #     # パスワード無しを許可する更新処理
  #     result = @user.update_without_current_password(update_params)
  #     raise if result

  #     flash[:success] = "アカウント: #{@user.account} #{@user.name}】のアカウント情報を更新しました。"
  #     redirect_to master_users_path
  #   else
  #     render :edit
  #   end
  # rescue => ex
  #   output_error_log(ex, 'アカウント編集 更新処理')
  #   flash[:error] = "エラーが発生したため、処理を中止ました。"
  #   render :edit
  # end

  def delete
    user = User.find_by(id: params[:id], is_visible: true)
    if user.nil?
      flash[:error] = "既に無効化されているか、有効なユーザー情報が存在しません。"
      redirect_to master_users_path
    end
    begin
      user.update!(is_visible: false)
      flash[:success] = "【#{user.account}: #{user.name}】のアカウントを無効にしました。"
      redirect_to master_users_path
    rescue => ex
      output_error_log(ex, 'アカウント一覧 無効化処理')
      flash[:error] = "無効化処理に失敗しました。"
      redirect_to master_users_path
    end
  end

  def restore
    user = User.find_by(id: params[:id], is_visible: false)
    if user.nil?
      flash[:error] = "既に有効化されているか、無効なユーザー情報が存在しません。"
      redirect_to master_users_path
    end
    begin
      user.update!(is_visible: true)
      flash[:success] = "【#{user.account}: #{user.name}】のアカウントを有効にしました。"
      redirect_to master_users_path
    rescue => ex
      output_error_log(ex, 'アカウント一覧 有効化処理')
      flash[:error] = "有効化処理に失敗しました。"
      redirect_to master_users_path
    end
  end

  private

  def create_params
    params.require(:master_users_new_form).permit(:role, :account, :name, :short_name, :email, :tel, :password, :password_confirmation)
  end

  def update_params
    params.require(:user).permit(:id, :role, :name, :short_name, :email, :tel, :password, :password_confirmation)
  end

end
