class Master::SettlementTemplateController < ApplicationController

  # 更新対象カラムの文字列
  PERMITTED_LANGUAGE_PRAMETERS = %w(
    title
    honorific
    building
    address
    company_name
    company_info1
    company_info2
    company_info3
    visitor_name
    nationality
    number_of_nights
    total_count
    count
    adult_count
    child_count
    room_charge
    cleaning_fee
    total
    site
    handling_charge
    reservation_date
    analysis_title
    sales
    target_days
    working_days
    guest_count
    occ
    adr
    rev_par
    other_income
    other_cost
    reward_rate
    payment_amount
    next_month_title
    internet_income
    free_income1
    free_income2
    free_income3
    free_income4
    free_income5
    total_income
    site_fee
    electrical_fee
    gas_fee
    water_fee
    internet_fee
    site_controller_fee
    free_fee1
    free_fee2
    free_fee3
    free_fee4
    free_fee5
    total_fee
    payee
    note1
    note2
    note3
    note4
  )

  def index
    @template = {}
    SettlementTemplate::LANGUAGE_LIST.each do |lang|
      @template[lang] = SettlementTemplate.find_or_create_by(language: lang)
    end
  end

  # remote: true
  def update
    @target = params[:template][:target]
    attrs = update_attrs
    @template = {}
    SettlementTemplate::LANGUAGE_LIST.each do |lang|
      @template[lang] = SettlementTemplate.find_or_create_by(language: lang)
      @template[lang].update! attrs[lang]
    end
  rescue => ex
    output_error_log(ex, "精算書テンプレート 更新処理")
    @is_error = true
    return 'ERROR'
  end

  private

  def update_params
    permitted_list = [:target]
    SettlementTemplate::LANGUAGE_LIST.each do |lang|
      PERMITTED_LANGUAGE_PRAMETERS.each do |key|
        permitted_list << "#{lang}_#{key}".to_sym
      end
    end
    params.require(:template).permit(permitted_list)
  end

  # パラメータを国別のハッシュにわける
  def update_attrs
    attrs = {}
    lang_params = update_params
    SettlementTemplate::LANGUAGE_LIST.each do |lang|
      attrs[lang] = {} unless attrs.key?(lang)
      lang_params.each do |k, v|
        next if k[0..1] != lang
        key = k[3..-1]
        attrs[lang][key] = v
      end
    end
    attrs
  end

end
