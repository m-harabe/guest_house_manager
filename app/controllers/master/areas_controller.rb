class Master::AreasController < ApplicationController

  def index
    @areas = Area.preload(:buildings).all.selecter
  end

  def new
    @area = Area.new
  end

  def edit
    @area = Area.find_by(id: params[:id])
    if @area.nil?
      flash[:error] = "エリア情報が存在しません。"
      redirect_to master_areas_path
    end
    @building_list = Building.includes(:area).display_order
  end


  def create
    @area = Area.new(create_params)
    if @area.valid?
      @area.save!
      flash[:success] = "【コード: #{@area.area_cd} #{@area.name}】のエリアを登録しました。続けて施設の登録をおこなってください。"
      redirect_to master_areas_edit_path(@area)
    else
      render :new
    end
  rescue => ex
    output_error_log(ex, "エリア 登録処理")
    flash[:error] = "エラーが発生したため、処理を中止ました。"
    redirect_to master_areas_path
  end

  def update
    @area = Area.find_by(id: update_params[:id])
    @area.match_attributes = update_params

    if @area.valid?
      ActiveRecord::Base.transaction do
        # 施設のチェックを全てOFFに変更した場合
        @area.buildings.map{ |b| b.update!(area_id: nil) } if update_params[:building_ids].blank?
        @area.save!
        @area.touch unless @area.saved_changes?
        flash[:success] = "【コード: #{@area.area_cd} #{@area.name}】のエリアを更新しました。"
        redirect_to master_areas_path
      end
    else
      render :edit
    end
  rescue => ex
    output_error_log(ex, "エリア 更新処理")
    flash[:error] = "エラーが発生したため、処理を中止ました。"
    redirect_to master_areas_path
  end



  # def delete
  #   user = User.find_by(id: params[:id], is_visible: true)
  #   if user.nil?
  #     flash[:error] = "既に無効化されているか、有効なユーザー情報が存在しません。"
  #     redirect_to master_users_path
  #   end
  #   begin
  #     user.update!(is_visible: false)
  #     flash[:success] = "【#{user.account}: #{user.name}】のアカウントを無効にしました。"
  #     redirect_to master_users_path
  #   rescue => ex
  #     output_error_log(ex, 'アカウント一覧 無効化処理')
  #     flash[:error] = "無効化処理に失敗しました。"
  #     redirect_to master_users_path
  #   end
  # end

  # def restore
  #   user = User.find_by(id: params[:id], is_visible: false)
  #   if user.nil?
  #     flash[:error] = "既に有効化されているか、無効なユーザー情報が存在しません。"
  #     redirect_to master_users_path
  #   end
  #   begin
  #     user.update!(is_visible: true)
  #     flash[:success] = "【#{user.account}: #{user.name}】のアカウントを有効にしました。"
  #     redirect_to master_users_path
  #   rescue => ex
  #     output_error_log(ex, 'アカウント一覧 有効化処理')
  #     flash[:error] = "有効化処理に失敗しました。"
  #     redirect_to master_users_path
  #   end
  # end

  private

  def create_params
    params.require(:area).permit(:name, :area_cd, :is_visible)
  end

  def update_params
    params.require(:area).permit(:id, :name, :area_cd, :is_visible, building_ids: [])
  end

end
