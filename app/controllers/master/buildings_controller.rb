class Master::BuildingsController < ApplicationController

  # GET /buildings
  #
  def index
    @form = Master::Buildings::ListForm.new
  end

  def new
    @form = Master::Buildings::EntryForm.new
  end

  def create
    @form = Master::Buildings::EntryForm.new(params: create_params)
    if @form.valid?
      building = Master::BuildingEntryService.create_update(attributes: create_params)
      flash[:success] = "施設情報を登録しました。部屋を複数登録する場合は続けて「部屋を追加する」ボタンから行ってください。"
      redirect_to master_buildings_show_path building.id
    else
      render :new
    end
  end

  def show
    @form = Master::Buildings::ShowForm.new(params[:id])
    @error_ids = flash[:error_ids] if flash[:error_ids]
  end

  def edit
    @form = Master::Buildings::EntryForm.new(id: params[:id])
  end

  def update
    @form = Master::Buildings::EntryForm.new(params: create_params, id: create_params[:id])
    if @form.valid?
      building = Master::BuildingEntryService.create_update(attributes: create_params)
      flash[:success] = "施設情報を更新しました。"
      redirect_to master_buildings_show_path building.id
    else
      render :edit
    end
  end

  def invisible
    building = Building.find_by(id: params[:id], is_visible: true)

    # 有効な予約が残っていないかをチェック
    status = Settings.RESERVATION.STATUS
    # error_reservation = Reservation.find_by(room_id: building.rooms.pluck(:id), status: [status.PREPARING, status.POSSIBLE, status.STAYING])
    error_reservation = Reservation.where(room_id: building.rooms.pluck(:id), status: [status.PREPARING, status.POSSIBLE, status.STAYING])
    if error_reservation.blank?
      building.update!(is_visible: false)
      flash[:success] = "施設【#{building.code}】#{building.name}を「非表示」にしました。"
    else
      flash[:error] = "施設【#{building.code}】#{building.name}はステータスが完了していない予約情報が残っているため、非表示にできません。"
      flash[:error_ids] = error_reservation.ids
    end

  rescue ActiveRecord::RecordNotFound
    flash[:error] = "非表示対象となるデータが存在しませんでした。他のユーザーによって既に更新されている可能性があります。"
  rescue
    flash[:error] = "部屋情報の非表処理で予期せぬエラーが発生しました。"
  ensure
    redirect_to master_buildings_show_path(params[:id])
  end

  def visible
    building = Building.find_by(id: params[:id], is_visible: false)
    building.update!(is_visible: true)
    flash[:success] = "施設【#{building.code}】#{building.name}を「表示」にしました。"
  rescue ActiveRecord::RecordNotFound
    flash[:error] = "表示対象となるデータが存在しませんでした。他のユーザーによって既に更新されている可能性があります。"
  rescue
    flash[:error] = "部屋情報の表示処理で予期せぬエラーが発生しました。"
  ensure
    redirect_to master_buildings_show_path(params[:id])
  end

  def new_room
    @form = Master::Buildings::EntryRoomForm.new(building_id: params[:building_id])
    render 'edit_room'
  end

  def create_room
    @form = Master::Buildings::EntryRoomForm.new(params: create_room_params)
    if @form.valid?
      room = Master::RoomEntryService.create_update(attributes: create_room_params)
      flash.now[:success] = "部屋「#{room.name}」を追加しました。"
      @form.fetch_rooms
    end
  end

  def edit_room
    @form = Master::Buildings::EntryRoomForm.new(room_id: params[:room_id])
  end

  def update_room
    @form = Master::Buildings::EntryRoomForm.new(params: create_room_params)
    if @form.valid?
      room = Master::RoomEntryService.create_update(attributes: create_room_params)
      flash.now[:success] = "部屋情報 #{room.name}を更新しました。"
      @form.fetch_rooms
    end
    render :create_room
  end

  def invisible_room
    # @form = Master::Buildings::ShowForm.new(params[:building_id])
    room = Room.find_by(id: params[:room_id], is_visible: true)
    room.is_visible = false
    room.save!
    flash[:success] = "部屋情報 #{room.name}を「非表示」にしました。"
  rescue ActiveRecord::RecordNotFound
    flash[:error] = "非表示対象となるデータが存在しませんでした。他のユーザーによって既に更新されている可能性があります。"
  rescue
    flash[:error] = "部屋情報の非表処理で予期せぬエラーが発生しました。"
  ensure
    redirect_to master_buildings_show_path(params[:building_id])
  end

  def visible_room
    room = Room.find_by(id: params[:room_id], is_visible: false)
    room.is_visible = true
    room.save!
    flash.now[:success] = "部屋情報 #{room.name}を「表示」にしました。"
  rescue ActiveRecord::RecordNotFound
    flash[:error] = "表示対象となるデータが存在しませんでした。他のユーザーによって既に更新されている可能性があります。"
  rescue
    flash[:error] = "部屋情報の表示処理で予期せぬエラーが発生しました。"
  ensure
    redirect_to master_buildings_show_path(params[:building_id])
  end

  private

  def create_params
    params.require(:master_buildings_entry_form).permit(
      Master::Buildings::EntryForm.attribute_keys
    )
  end

  def create_room_params
    params.require(:master_buildings_entry_room_form).permit(
      %i(building_id room_id name room_no note total_floor_area standard_people max_people bath_count toilet_count secret_number suitebook_room_idnum)
    )
  end

end
