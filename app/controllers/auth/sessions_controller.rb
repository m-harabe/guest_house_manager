class Auth::SessionsController < Devise::SessionsController

  # skip_before_filter :verify_authenticity_token ,:only=>[:create]

  layout "login"

  def new
    super
  end

  def create
    super
    # User.current_user = current_user unless current_user.nil?

    # set_special_user_setting if special_user?
  end

  # ログイン / ログアウト後のリダイレクト先変更
  def after_sign_in_path_for(resource)
    dashboard_path
  end

  def after_sign_out_path_for(resource)
    flash[:notice] = "ログアウトしました。"
    new_user_session_path
  end

  private

  def special_user?
    special_users = ['admin', '1002']
    special_users.include?(User.current_user.account)
  end

  # remember_created_at カラムに時刻を入れることでログイン状態を保持
  def set_special_user_setting
    User.current_user.remember_me!
  end
end
