class Auth::RegistrationsController < Devise::RegistrationsController

  # skip_before_filter :check_user_tenant
  layout "login"

  def new
    super
  end

  def create
    super
  end

  def edit
    super
  end

  # アカウントの更新処理
  # Devise でユーザーがパスワードなしでアカウント情報を変更するのを許可 http://easyramble.com/user-account-update-without-password-on-devise.html
  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)
    #if update_resource(resource, account_update_params)
    if resource.update_without_current_password(account_update_params)
      yield resource if block_given?
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      # sign_in resource_name, resource, :bypass => true
      # modify for Rails5
      bypass_sign_in(resource)

      # respond_with resource, :location => after_update_path_for(resource)
      # アカウント編集画面は別ウィンドウで起動させているため、top ではなく edit にリダイレクト
      redirect_to edit_user_registration_path
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  protected

  def after_sign_up_path_for(resource)
    dashboard_path
  end

  # def after_update_path_for(resource)
  #   edit_user_registration_path + "?tenant_cd=#{Apartment::Tenant.current}"
  # end

  def account_update_params
    devise_parameter_sanitizer.permit(:account_update) do |user|
      user.permit(:account, :email, :name, :kana, :tel, :password, :password_confirmation, :current_password)
    end
    super
  end

  private

  # strong parametersを設定し、accountを許可
    def configure_permitted_parameters
      # devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:account, :password, :password_confirmation, :remember_me) }
      # devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:account, :password, :remember_me) }
      # devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:account, :email, :name, :kana, :tel, :password, :password_confirmation, :current_password) }
      devise_parameter_sanitizer.permit(:sign_up) do |user|
        user.permit(:account, :password, :password_confirmation, :remember_me)
      end
      devise_parameter_sanitizer.permit(:sign_in) do |user|
        user.permit(:account, :password, :remember_me)
      end
      # devise_parameter_sanitizer.permit(:account_update) do |user|
      #   user.permit(:account, :email, :name, :kana, :tel, :password, :password_confirmation, :current_password)
      # end
      devise_parameter_sanitizer.permit(:account_update, keys: [:account, :name, :kana, :tel])
    end

    def update_resource(resource, params)
      resource.update_without_current_password(params)
    end
end
