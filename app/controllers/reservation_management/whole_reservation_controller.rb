class ReservationManagement::WholeReservationController < ApplicationController

  # コールバック変数の保持
  before_action :fetch_callback

  def new
    @form = ReservationManagement::WholeReservations::EntryForm.new(params: params)
  end

  def create
    @form = ReservationManagement::WholeReservations::EntryForm.new(params: create_params)
    if @form.valid?
      service = ReservationManagement::EntryWholeReservationService.new(is_new: true)
      whole_reservation = service.create(@form.attributes)
      flash[:success] = "#{I18n.l(@form.check_in_date, format: :middle)}の一棟貸の予約情報を登録しました。部屋別、日別の金額は自動で配分されていますので確認してください。"
      redirect_to reservation_management_whole_reservation_show_path(whole_reservation.id, nil, @callback)
    else
      render :new
    end
  end

  def show
    @form = ReservationManagement::WholeReservations::ShowForm.new(id: params[:id], reservation_id: params[:reservation_id])
  end

  def edit_basic
    @form = ReservationManagement::WholeReservations::EditBasicForm.new(id: params[:id])
  end

  # remote: true
  def update_basic
    @form = ReservationManagement::WholeReservations::EditBasicForm.new(params: update_basic_params)
    if @form.valid?
      attributes = update_basic_params.merge(
        scheduled_check_in_time: @form.scheduled_check_in_time,
        set_nil: true)
      ReservationManagement::EditWholeReservationService.update_basic(attributes)
      @form.templete_val_for_update
      flash.now[:success] = "基本情報を更新しました。"
    end
  rescue => ex
    render "shared/ajax_error"
  end

  def edit_payment
    @form = ReservationManagement::WholeReservations::EditPaymentForm.new(id: params[:id])
  end

  # remote: true
  def update_payment
    @form = ReservationManagement::WholeReservations::EditPaymentForm.new(params: update_payment_params)
    if @form.valid?
      ReservationManagement::EditWholeReservationService.update_payment(update_payment_params)
      @form.templete_val_for_update
      flash.now[:success] = "請求情報を更新しました。各部屋毎の予約情報の内訳も洗替えされていますので確認してください。"
    end
  rescue => ex
    render "shared/ajax_error"
  end

  def edit_room_reservations
    @form = ReservationManagement::WholeReservations::EditRoomReservationsForm.new(id: params[:id])
  end

  # remote: true
  def update_room_reservations
    id = params[:reservation_management_whole_reservations_edit_room_reservations_form][:id]
    @form = ReservationManagement::WholeReservations::EditRoomReservationsForm.new(
      id: id,
      params: update_room_reservations_params
    )
    if @form.valid?
      ReservationManagement::EditWholeReservationService.update_room_reservations(
        whole_reservation_id: id,
        attr_list: update_room_reservations_params
      )
      @form.templete_val_for_update
      flash.now[:success] = "各部屋毎の予約情報を更新しました。変更した予約情報の日別の人数、金額の内訳も洗替えされていますので確認してください。"
    end
  rescue => ex
    render "shared/ajax_error"
  end

  def cancel
    _p = forward_status_params
    ReservationManagement::UpdateStatusService.cancel_status(current_status: _p[:status], whole_reservation_id: _p[:id])
    flash[:success] = '一棟貸情報と紐付く予約情報を全てキャンセルしました。'
  rescue Service::UpdateFailure => ex
    flash[:error] = ex.message
  ensure
    redirect_to reservation_management_whole_reservation_show_path(_p[:id], _p[:link_reservation_id], @callback)
  end

  private

  def create_params
    params.require(:reservation_management_whole_reservations_entry_form).permit(
      ReservationManagement::WholeReservations::EntryForm.attribute_keys
    )
  end

  def update_basic_params
    params.require(:reservation_management_whole_reservations_edit_basic_form).permit(
      %i(id reserve_date display_check_in_time status nationality_id visitor_name visitor_name_kana nationality_id tel mail_address note)
    )
  end

  def update_payment_params
    params.require(:reservation_management_whole_reservations_edit_payment_form).permit(
      %i(id adult_count child_count room_charge cleaning_fee meal_fee site_code)
    )
  end

  # def update_room_reservations_params_id
  #   params.require(:reservation_management_whole_reservations_edit_room_reservations_form).permit(:id)
  # end

  # 各部屋毎の予約情報更新用
  # params[:room_reservations][{"reservation_id"=>"53", "adult_count"=>"5", "child_count"=>"3", "room_charge"=>"53333", "cleaning_fee"=>"3333"},...]
  def update_room_reservations_params
    permissions = %i(reservation_id room_id adult_count child_count room_charge cleaning_fee meal_fee)
    params.require(:room_reservations).map { |r| r.permit(permissions) }
  end

  def forward_status_params
    params.require(:reservation_management_whole_reservations_show_form).permit(%i(id status link_reservation_id))
  end
end
