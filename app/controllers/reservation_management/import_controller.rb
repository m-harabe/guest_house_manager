class ReservationManagement::ImportController < ApplicationController

  # GET /import
  #
  def index
  end

  # POST /import/comfirm
  #
  def comfirm
    @form = ReservationManagement::Import::ComfirmForm.new(params: params[:reservation_import_form])
    if @form.valid?
      # todo
      ReservationManagement::ImportService.new.import(form: @form)
    else
      flash[:error] = @form.errors.full_messages
      redirect_to action: :index
    end
  rescue => ex
    output_error_log(ex, 'インポート 確認')
    flash[:error] = I18n.t('errors.common.rollback', action: 'インポート')
    redirect_to action: :index
  end

  # POST /import/create
  #
  def create
    redirect_to action: :result, wk_reservation_ids: params[:wk_reservation_ids]
    # if params.dig(:reservation_import_form).nil?
    #   flash.now[:error] = 'ファイルを選択してください。'
    # else
    #   @form = ReservationManagement::Import::CreateForm.new(file: import_params[:file])
    #   service = ReservationManagement::ImportService.new(form: @form)
    #   service.import
    # end
  rescue => ex
    ErrorUtil.log_and_notify(ex: ex, title: 'ReservationManagement::ImportController#create')
    # render "shared/ajax_error"
    flash[:error] = I18n.t('errors.common.rollback', action: 'インポート')
    redirect_to action: :result
  end

  def redirect_index
    redirect_to action: :index
  end

  def result
    p "result-------------------params"
    p params
  end

  private

  def import_params
    params.require(:reservation_import_form).permit(:file)
  end
end
