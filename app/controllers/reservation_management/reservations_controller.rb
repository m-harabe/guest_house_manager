class ReservationManagement::ReservationsController < ApplicationController

  protect_from_forgery except: :search

  # コールバック変数のセット
  before_action -> {
    set_callback(:reservations_search)
  }, only: [:search, :transition_search]

  SEARCH_MAX_COUNT = 100

  # GET /reservation_management/reservations/
  #
  def index
    clear_search_conditions
    @search = Reservation.ransack
    @reservations = []
  end

  # POST /reservation_management/reservations/search
  #
  def search
    @search = Reservation.includes(:nationality, room: :building).ransack(search_params)
    @count = @search.result.count
    @message = I18n.t "messages.search_count", count: @count, max: SEARCH_MAX_COUNT if @count > SEARCH_MAX_COUNT
    @search.sorts = 'id desc'
    @reservations = @search.result.limit(SEARCH_MAX_COUNT)
    set_search_conditions(search_params)
  end

  # 検索条件をセッションから取得して一覧表示
  def transition_search
    search_conditions = fetch_search_conditions
    p search_conditions
    # viewでselect-2-multi の選択を明示的に再現するための変数を渡す
    gon.search_conditions = search_conditions
    @search = Reservation.includes(:nationality, room: :building).ransack(search_conditions)
    @count = @search.result.count
    @message = I18n.t "messages.search_count", count: @count, max: SEARCH_MAX_COUNT if @count > SEARCH_MAX_COUNT
    @search.sorts = 'id desc'
    @reservations = @search.result.limit(SEARCH_MAX_COUNT)
    render :index
  end

  private

  def search_params
    # params.require(:q).permit!
    # NOTE: room_building_area_id_eq エリアセレクトはルームセレクト取得用なので、検索条件に含めない
    params.require(:q).permit(
      :check_in_date_gteq, :check_out_date_lteq, :reserve_date_gteq, :reserve_date_lteq,
      :number_of_nights_gteq, :number_of_nights_lteq,
      :nationality_id_eq, :site_code_eq, :registration_type_eq,
      :visitor_name_or_visitor_name_kana_cont, :mail_address_or_tel_cont, :note_or_owner_note_cont,
      :note_present, :point_use_gt,
      room_id_in: [], status_in: []
    )
  end

end
