class ReservationManagement::EntryController < ApplicationController

  # コールバック変数の保持
  before_action :fetch_callback

  # GET /new
  #
  def new
    @form = ReservationManagement::Reservations::EntryForm.new(params: params)
  end

  def edit
    # TODO: キャンセル済のデータにアクセスしない実装にする
    @form = ReservationManagement::Reservations::EntryForm.new(reservation_id: params[:id])
  end

  def show
    @form = ReservationManagement::Reservations::ShowForm.new(id: params[:id])
  end

  def create
    @form = ReservationManagement::Reservations::EntryForm.new(params: create_params)
    if @form.valid?
      service = ReservationManagement::EntryService.new
      reservation = service.apply(@form.attributes)
      redirect_to reservation_management_show_path(reservation.id, @callback), flash: {success: "予約情報を登録しました。日別の請求金額は自動で配分されていますので確認してください。"}
    else
      render :new
    end
  end

  def update
    # TODO: キャンセル済のデータにアクセスしない実装にする
    @form = ReservationManagement::Reservations::EntryForm.new(params: update_params, reservation_id: update_params[:reservation_id])
    if @form.valid?
      service = ReservationManagement::EntryService.new

      # TODO: この辺処理をフォームに移譲したい
      attributes = @form.whole_reservation_id ? update_params_for_whole : update_params
      attributes = attributes.merge(
        id: @form.id,
        scheduled_check_in_time: @form.scheduled_check_in_time,
        set_nil: true)
      reservation = service.apply(attributes)
      redirect_to reservation_management_show_path(reservation.id, @callback), flash: {success: "予約情報を更新しました。"}
    else
      render :edit
    end
  end

  def edit_reservation_date
    @form = ReservationManagement::Reservations::EditReservationDateForm.new(params[:reservation_id])
  end

  def update_reservation_date
    @edit_form = ReservationManagement::Reservations::EditReservationDateForm.new(
              params[:reservation_management_reservations_edit_reservation_date_form][:reservation_id],
              update_reservation_date_params)
    if @edit_form.valid?
      # ActiveRecord::StaleObjectError

      # TODO: サービスにフォームオブジェクトをそのままわたしてる
      reservation = ReservationManagement::UpdateReservationDateService.update(@edit_form.reservation)
      flash.now[:success] = '請求内訳を更新しました。'
      # 参照画面のFormをつくりなおし
      @form = ReservationManagement::Reservations::ShowForm.new(id: reservation.id)
    end
  end

  def bulk_forward_status
    _p = forward_status_params
    ReservationManagement::UpdateStatusService.forward_status(reservation_id: _p[:id], current_status: _p[:status], is_bulk: true)
    flash[:success] = 'ステータスを更新しました。'
  rescue Service::UpdateFailure => ex
    flash[:error] = ex.message
  ensure
    redirect_to reservation_management_show_path(_p[:id], @callback)
  end

  def forward_status
    _p = forward_status_params
    ReservationManagement::UpdateStatusService.forward_status(reservation_id: _p[:id], current_status: _p[:status])
    flash[:success] = 'ステータスを更新しました。'
  rescue Service::UpdateFailure => ex
    flash[:error] = ex.message
  ensure
    redirect_to reservation_management_show_path(_p[:id], @callback)
  end

  def cancel
    _p = forward_status_params
    ReservationManagement::UpdateStatusService.cancel_status(current_status: _p[:status], reservation_id: _p[:id])
    flash[:success] = '予約情報をキャンセルしました。'
  rescue Service::UpdateFailure => ex
    flash[:error] = ex.message
  ensure
    redirect_to reservation_management_show_path(_p[:id], @callback)
  end

  private

  # 登録
  def create_params
    params.require(:reservation_management_reservations_entry_form).permit(
      ReservationManagement::Reservations::EntryForm.attribute_keys
    )
  end

  # 更新
  def update_params
    params.require(:reservation_management_reservations_entry_form).permit(
      ReservationManagement::Reservations::EntryForm.attribute_keys
    )
  end

  # 更新(一棟貸し予約情報の個別更新) 更新可能な項目を制限
  def update_params_for_whole
    params.require(:reservation_management_reservations_entry_form).permit(
      :reservation_id, :site_code, :reserve_date, :nationality_id, :visitor_name, :mail_address, :tel, :note
    )
  end

  # 請求情報内訳の更新
  def update_reservation_date_params
    params.permit(reservation_dates: %i(reservation_date_id room_charge cleaning_fee meal_fee))
  end

  def forward_status_params
    params.require(:reservation_management_reservations_show_form).permit(%i(id status))
  end

end
