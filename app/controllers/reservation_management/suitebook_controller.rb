class ReservationManagement::SuitebookController < ApplicationController

  # コールバック変数のセット
  before_action -> { set_callback(:suitebook) }, only: [:index, :search, :transition_search, :show]

  # GET /reservation/suitebook
  #
  def index
    clear_search_conditions
    # @suitebooks = SuitebookReservation.eager_load(room: [:building]).order(id: :desc)
    @form = ReservationManagement::Suitebook::ListForm.new()
    set_search_conditions
  end

  # POST /reservation/suitebook/search
  #
  def search
    @form = ReservationManagement::Suitebook::ListForm.new(search_params)
    set_search_conditions
  end

  # Get /reservation/suitebook/transition_search
  #
  # 検索条件をセッションから取得して一覧表示
  def transition_search
    @form = ReservationManagement::Suitebook::ListForm.new(fetch_search_conditions)
    render :index
  end

  def show
    @suitebook = SuitebookReservation.find_by(id: params[:id])
  end

  def cancel
    suitebook = SuitebookReservation.status_error.find(params[:id])
    suitebook.update!(status: Settings.SUITEBOOK_RESERVATION_STATUS.CANCEL)
    flash[:success] = "suitebook予約ID: #{suitebook.suitebook_idnum} のステータスを「破棄」に変更しました。"
    # TODO: transition_searchに変更
    redirect_to action: :index
  end

  # def delete
  #   suitebook = SuitebookReservation.status_error.find(params[:id])
  #   suitebook.destroy
  #   redirect_to action: transition_search, flash: {success: "suitebook予約ID: #{suitebook.suitebook_idnum} のデータを削除しました。"}
  # end

  # def show
  #   @suitebook = SuitebookReservation.find_by(id: params[:id])
  # end

  def entry
    id = params[:id]
    @suitebook = SuitebookReservation.find_by(id: id)

    if @suitebook && (@suitebook.status == Settings.SUITEBOOK_RESERVATION_STATUS.UNPROCESSED || Settings.SUITEBOOK_RESERVATION_STATUS.ERROR)
      @form = ReservationManagement::Reservations::EntryForm.new(params: @suitebook.reservation_attributes)
      @form.valid? # エラーが有る場合は初期表示でエラー内容を表示させる
    else
      redirect_to reservation_management_suitebook_path, flash: {error: "不正なアクセスです。"}
    end
  end

  def create
    @suitebook = SuitebookReservation.find_by(id: create_params[:suitebook_reservation_id])
    @form = ReservationManagement::Reservations::EntryForm.new(params: create_params.to_hash)
    if @form.valid?
      ActiveRecord::Base.transaction do
        service = ReservationManagement::EntryService.new
        reservation = service.apply(@form.attributes.merge(registration_type: Settings.RESERVATION.REGISTRATION_TYPE.FORM_SUITEBOOK))
        @suitebook.status = Settings.SUITEBOOK_RESERVATION_STATUS.PROCESSED
        @suitebook.reservation = reservation
        @suitebook.save!
        redirect_to reservation_management_show_path(reservation.id, @callback), flash: { success: "予約情報を登録しました。日別の請求金額は自動で配分されていますので確認してください。" }
      end
    else
      render :entry
    end
  rescue => ex
    output_error_log(ex, 'suitebook 予約登録処理')
    flash[:error] = "エラーが発生したため、処理を中止ました。"
    render :entry
  end

  def entry_whole_select_building
    @suitebook_id = SuitebookReservation.find_by(id: params[:id]).id
  end

  def entry_whole
    id = params[:id]
    @suitebook = SuitebookReservation.find_by(id: id)
    building_id = params[:building_id]

    if @suitebook && (@suitebook.status == Settings.SUITEBOOK_RESERVATION_STATUS.UNPROCESSED || Settings.SUITEBOOK_RESERVATION_STATUS.ERROR)
      @form = ReservationManagement::WholeReservations::EntryForm.new(params: @suitebook.reservation_attributes.merge(building_id: building_id))
      @form.valid? # エラーが有る場合は初期表示でエラー内容を表示させる
    else
      redirect_to reservation_management_suitebook_path, flash: {error: "不正なアクセスです。"}
    end
  end

  def create_whole
    @suitebook = SuitebookReservation.find_by(id: create_whole_params[:suitebook_reservation_id])
    @form = ReservationManagement::WholeReservations::EntryForm.new(params: create_whole_params)
    if @form.valid?
      ActiveRecord::Base.transaction do
        service = ReservationManagement::EntryWholeReservationService.new(is_new: true)
        whole_reservation = service.create(@form.attributes.merge(registration_type: Settings.RESERVATION.REGISTRATION_TYPE.FORM_SUITEBOOK))
        @suitebook.status = Settings.SUITEBOOK_RESERVATION_STATUS.PROCESSED
        @suitebook.whole_reservation = whole_reservation
        @suitebook.save!
        flash[:success] = "#{I18n.l(@form.check_in_date, format: :middle)}の一棟貸の予約情報を登録しました。部屋別、日別の金額は自動で配分されていますので確認してください。"
        redirect_to reservation_management_whole_reservation_show_path(whole_reservation.id, nil, @callback)
      end
    else
      render :entry_whole
    end
  rescue => ex
    output_error_log(ex, 'suitebook 一棟貸登録処理')
    flash[:error] = "エラーが発生したため、処理を中止ました。"
    render :entry_whole
  end

  private

  def search_params
    params.require(:search_suitebook).permit(
      # TODO: 不要な項目の整理
      :search_import_date_from, :search_import_date_to, :search_check_in_date, :search_check_out_date, :search_status, :search_site_code,
      :search_visitor_name, :search_mail_address, :search_room_id, :search_check_in_month, :search_site_code, :search_prev_next_month, :search_original_month
      )
  end

  def create_params
    params.require(:suitebook_reservation_entry).permit(
      ReservationManagement::Reservations::EntryForm.attribute_keys +
      [:suitebook_reservation_id]
    )
  end

  def create_whole_params
    params.require(:reservation_management_whole_reservations_entry_form).permit(
      ReservationManagement::WholeReservations::EntryForm.attribute_keys +
      [:suitebook_reservation_id]
    )
  end
end
