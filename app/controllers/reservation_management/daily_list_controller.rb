class ReservationManagement::DailyListController < ApplicationController

  # コールバック変数のセット
  before_action -> {
    set_callback(:daily_list)
  }, only: [:index, :search, :transition_search]

  # GET /reservation/daily_list
  #
  def index
    clear_search_conditions
    @form = ReservationManagement::DailyListForm.new
    # @form.reservation_list
  end

  # POST /reservation/daily_list/search
  #
  def search
    @form = ReservationManagement::DailyListForm.new(search_params)
    set_search_conditions
  end

  # GET /reservation/daily_list/transition_search
  #
  # 検索条件をセッションから取得して一覧表示
  def transition_search
    @form = ReservationManagement::DailyListForm.new(fetch_search_conditions)
    render :index
  end

  private

  def search_params
    params.require(:reservation_management_daily_list_form).permit(:search_check_in_day, :search_check_in_date_flag)
  end
end
