class ReservationManagement::MonthlyListController < ApplicationController

  # https://qiita.com/ayacai115/items/ec7a621ec73692065d7a
  protect_from_forgery except: :search # searchアクションを除外

  # コールバック変数のセット
  before_action -> {
    set_callback(:monthly_list)
  }, only: [:index, :search, :transition_search]

  # GET /index
  #
  def index
    clear_search_conditions
    @form = ReservationManagement::MonthlyListForm.new(show_all_rooms: params.dig(:show_all_rooms).to_bool)
  end

  # POST /reservation/daily_list/search
  #
  def search
    @form = ReservationManagement::MonthlyListForm.new(search_params)
    set_search_conditions
  end

  # 検索条件をセッションから取得して一覧表示
  def transition_search
    @form = ReservationManagement::MonthlyListForm.new(fetch_search_conditions)
    render :index
  end

  def csv
    @form = ReservationManagement::MonthlyListForm.new(fetch_search_conditions, is_csv: true)
    # send_data render_to_string, filename: "monthly_list.csv", type: :csv
    send_data render_to_string, filename: @form.csv_file_name, type: 'text/csv; charset=shift_jis'
  end

  private

  def search_params
    params.require(:reservation_management_monthly_list_form).permit(
      :search_room_id, :search_check_in_month, :search_site_code, :search_prev_next_month, :search_original_month
      )
  end

end
