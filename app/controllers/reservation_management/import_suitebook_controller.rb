class ReservationManagement::ImportSuitebookController < ApplicationController

  # GET /import
  #
  def index
  end

  # POST /import/create
  #
  def create
    file_obj = csv_import_params[:csv_file]

    # CSVの行データ（フォーマットオブジェクト）
    import_obj_list = []

    # エラーメッセージを格納する配列
    error_list = []

    # 処理結果件数保持用ハッシュ
    count = Hash.new(0)

    # フォーマット作成
    # - CSVモジュールのデータパッチ対応を行ってオプションを追加している
    # - [Shift-JISなCSVを読み込む・書き出しするときにエラーを起こさない数少ない方法](https://qiita.com/yuuna/items/7e4e06a1b881d85697e9)
    #
    # 通常は以下のオプション
    # CSV.foreach(file_obj.tempfile, headers: true, encoding: Encoding::Shift_JIS).each.with_index(2) do |row, idx|
    # CP932でエンコーディングするのはローマ数字対応
    CSV.foreach(file_obj.tempfile, headers: true, encoding: "utf-8", undef: :replace, replace: "*").each.with_index(2) do |row, idx|
      # 取り込み済はスキップ
      # TODO: 失敗しているデータ等ステータスによっては更新するケースは？？ >> 一旦なし
      if SuitebookReservation.where(suitebook_idnum: row["suitebook予約ID"]).exists?
        count[:skip] += 1
        next
      end
      # フォーマット生成
      import_obj_list << FileObjects::SuitebookReservationCsv.new(row: row, row_index: idx)
    end

    # 全フォーマットをバリデーション
    import_obj_list.each do |obj|
      # エラーメッセージは最大１０まで保持
      break if error_list.size >= 10
      next if obj.valid?
      error_list = error_list + obj.full_error_messages
    end

    if error_list.present?
      set_error_to_flsh(label: "suitebook予約情報CSVインポート", filename: file_obj.original_filename, error_list: error_list)
      redirect_to action: :index
      return
    end

    # TODO: 登録処理の記述場所を考える（フラッシュメッセージの受け渡しに注意）
    begin
      # 登録処理
      ActiveRecord::Base.transaction do
        building_list = []
        import_obj_list.each do |obj|

          suitebook_reservation ||= SuitebookReservation.new
          suitebook_reservation.match_attributes = obj.attributes
          suitebook_reservation.save!
          count[:create] += 1
        end
      end
    rescue => ex
      flash[:error] = "システムエラーが発生したため全てのインポート処理をキャンセルしました。"
      ErrorUtil.log_and_notify(ex: ex, title: 'suitebook予約情報CSVインポート 登録処理')
      redirect_to action: :index
      return
    end

    flash[:success] = "suitebook予約情報をインポートしました。登録: #{count[:create]}件 / スキップ: #{count[:skip]}件"
    redirect_to action: :index
  end

  # POST /import/create
  #
  # def create
  #   p "create---------------------------------"
  #   redirect_to action: :result, wk_reservation_ids: params[:wk_reservation_ids]
  #   # if params.dig(:reservation_import_form).nil?
  #   #   flash.now[:error] = 'ファイルを選択してください。'
  #   # else
  #   #   @form = ReservationManagement::Import::CreateForm.new(file: import_params[:file])
  #   #   service = ReservationManagement::ImportService.new(form: @form)
  #   #   service.import
  #   # end
  # rescue => ex
  #   ErrorUtil.log_and_notify(ex: ex, title: 'ReservationManagement::ImportController#create')
  #   # render "shared/ajax_error"
  #   flash[:error] = I18n.t('errors.common.rollback', action: 'インポート')
  #   redirect_to action: :result
  # end

  def redirect_index
    redirect_to action: :indexsuitebook_reservations
  end

  def result
    p "result-------------------params"
    p params
  end

  private

  def csv_import_params
    params.permit(:csv_file)
  end

  def set_error_to_flsh(label:, filename:, error_list:)
    flash[:error] = ["<b>#{label}</b>"]
    flash[:error] << "ファイルの内容にエラーが見つかったため、インポートを中止しました。"
    flash[:error] << "ファイル名: #{filename}"
    flash[:error] << ""
    flash[:error] << error_list
    flash[:error] << "※以降の行のエラーチェックは省略されました。" if error_list.size >= 10
  end
end
