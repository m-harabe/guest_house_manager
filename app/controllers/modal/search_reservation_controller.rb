class Modal::SearchReservationController < ApplicationController

  # POST modal/search_reservation/previous_reservation
  #
  # 予約可否チェック
  # remote: true
  def previous_reservation
    @form = Modal::SearchReservation::PreviousReservationForm.new(params)
  rescue => ex
    @message = 'データを取得できませんでした。'
    render "shared/ajax_error"
  end
end
