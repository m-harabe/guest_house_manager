class ApplicationController < ActionController::Base
  # before_action :sign_in_required, :authenticate_user!, :set_current_user, :should_edit_user
  # before_action :authenticate_user!, :set_current_user, :should_edit_user
  before_action :authenticate_user!, except: :handle_404
  before_action :set_current_user, except: :handle_404
  # before_action :set_callback

  # Deviseのヘルパーメソッド current_user の値を常に保持しておく
  def set_current_user
    User.current_user = current_user unless current_user.nil?
  end

  def should_edit_user
    redirect_to edit_user_registration_path if User.current_user.name.nil?
  end

  # フォームオブジェクトからGONに変数をセット
  # form にセットメソッドを用意する
  def from_form_to_gon
    @form.send_to_gon gon
  end

  # ▼エラーハンドリング

  # 参考URL：http://ruby-rails.hatenadiary.com/entry/20141111/1415707101
  # 他のエラーハンドリングでキャッチできなかった場合に500 Internal Server Error(システムエラー)を発生させる
  # NOTE: rescue_from は下から評価されるので記載箇所はここが正解

  rescue_from Exception, with: :handle_500 unless Rails.env.development?

  # 例外に合わせたエラーハンドリング

  # 404 Not Found リソースが見つからない。アクセス権がない場合にも使用される
  # RoutingErrorはroutes.rbの最後に全てのURLをキャッチする記述あり
  rescue_from ActionController::RoutingError, with: :handle_404 unless Rails.env.development?

  # rescue_from ActiveRecord::RecordNotFound,   with: :handle_404 #unless Rails.env.development?
  # rescue_from ActiveRecord::RecordNotUnique, with: :render_409
  # rescue_from UnauthorizedError,             with: :render_401
  # rescue_from IllegalAccessError,            with: :render_403

  # エラーハンドリング処理
  def handle_500(exception = nil)
    output_error_log(exception, 'Rendering 500 with exception') if exception
    if request.xhr? # Ajaxのための処理
      render json: { error: '500 error' }, status: 500
    else
      render template: 'errors/error_500', status: 500, layout: 'application', content_type: 'text/html'
    end
  end

  def handle_404(exception = nil)
    # TODO Ajaxエラー時うまく動作していない
    if user_signed_in?
      # ログインしている場合のルーティングエラーはページレイアウトそのままでコンテンツ部にテンプレートメッセージを表示させる
      logger.info "Rendering 404 with exception: #{exception.message}" if exception
      if request.xhr? # Ajaxのための処理
        render json: { error: '404 error' }, status: 404
      else
        render template: 'errors/error_404', status: 404, layout: 'application', content_type: 'text/html'
      end
    else
      # ログインしていない場合の４０４を表示させる
      render file: 'public/404.html', layout: false, content_type: 'text/html'
    end
  end

  # ▼before_action

  # 検索画面のコントローラーでcallbackを設定しておくと、遷移先の画面のパンくずの生成時に考慮される
  def set_callback(callback)
    @callback = callback
  end

  def fetch_callback
    @callback = params[:callback]
  end

  # ▼検索条件のセッション保存サイクル

  # フォームの検索条件（Hash）をセッション（クライアント側）に保存
  # コールバック先のパスをインスタンス変数で保持して引き回す
  def set_search_conditions(params = nil)
    if params
      session[search_condition_key] = params
    else
      session[search_condition_key] = @form.search_conditions
    end
  end

  # 検索条件のハッシュをセッションから復元
  # @return [Hash] 検索条件 e.g) { :search_month => '2018-10-01', ... }
  def fetch_search_conditions
    # ハッシュのキーは文字列でもシンボルでもアクセス可能にして返す
    session[search_condition_key]&.with_indifferent_access || {}
  end

  # 検索条件とコールバックのセッション情報を破棄
  def clear_search_conditions
    session.delete(search_condition_key)
  end

  # TODO: 検索条件、コールバックの全セッションをクリアするアクションをroot_pathで呼び出す

  # ダウンロードファイルのWindows文字化け対策
  def encode_filename(filename)
    filename = ERB::Util.url_encode(filename) if msie?
    return filename
  end

  def msie?
    # (/MSIE/ =~ request.user_agent) || (/Trident/ =~ request.user_agent) || (/Windows/ =~ request.user_agent)
    (/MSIE/ =~ request.user_agent) || (/Trident/ =~ request.user_agent)
  end

private

  def search_condition_key
    Settings.SEARCH_CONDITION_NAMES[controller_name]
  end

  def output_error_log(ex, message = '')
    return unless user_signed_in?
    body = "################ GuestHouseManager ERROR: #{Time.now} ################\n"
    body << "【enviroment】#{Rails.env}" + "\n"
    # body << "user: #{User.current_user.name}" + "\n" if User.current_user
    body << "【user】#{User.current_user&.name}" + "\n"
    remote_ip = request.env["HTTP_X_FORWARDED_FOR"] || request.remote_ip
    body << "【IP】#{remote_ip}" + "\n" if remote_ip
    body << "【Request URL】#{request.url}" + "\n"
    body << "【params】#{params}" + "\n" if params
    # セッションから検索条件を受けっとている場合は出力する
    search_conditions = fetch_search_conditions
    body << "【session_search_condition_params】#{search_conditions}" + "\n" if search_conditions.present?
    body << "【message】#{message}"  + "\n" if message

    ErrorUtil.send_log(ex: ex, body: body)
  end

  def sign_in_required
    redirect_to new_user_session_path unless user_signed_in?
  end

end
