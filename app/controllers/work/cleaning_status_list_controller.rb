class Work::CleaningStatusListController < ApplicationController
  # コールバック変数のセット
  before_action -> {
    set_callback(:cleaning_status_list)
  }

  def index
    clear_search_conditions
    @form = Work::CleaningStatusListForm.new
    @form.fetch_list
  end

  def search
    @form = Work::CleaningStatusListForm.new(params[:work_cleaning_status_list_form])
    if @form.valid?
      @form.fetch_list
      set_search_conditions
    end
  rescue => ex
    render "shared/ajax_error"
  end

  # 検索条件をセッションから取得して一覧表示
  def transition_search
    @form = Work::CleaningStatusListForm.new(fetch_search_conditions)
    @form.fetch_list
    render :index
  end

  def forward_status
    _p = forward_status_params
    reservation = ReservationManagement::UpdateStatusService.forward_status(reservation_id: _p[:id], current_status: _p[:status], is_bulk: false)
    @message = create_update_status_message(reservation)
    @form = Work::CleaningStatusListForm.new(fetch_search_conditions)
    @form.fetch_list
  rescue Service::UpdateFailure => ex
    @message = ex.message + "再検索して画面を更新してください。"
    render "shared/ajax_error"
  rescue => ex
    render "shared/ajax_error"
  end

  private

  def create_update_status_message(reservation)
    room = Room.with_building.find_by(id: reservation.room_id)
    message = "#{reservation.check_in_date.to_s}: #{room.full_code_name} → #{reservation.status_name}"
  end

  # def search_params
  #   params.require(:work_cleaning_status_list_form).permit(
  #     %i(search_room_ids search_date_from search_date_to)
  #   )
  # end

  def forward_status_params
    params.permit(%i(id status))
  end

end
