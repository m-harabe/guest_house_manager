class Summary::ReportsController < ApplicationController
  def index
    @form = Summary::Reports::IndexForm.new
  end

  def accommodation_tax
    search_month = params.dig(:accommodation_tax_csv, :search_month)
    @form = Summary::Reports::AccommodationTaxCsvForm.new(search_month: search_month, action_name: action_name)
    send_data render_to_string, filename: @form.file_name, type: 'text/csv; charset=shift_jis'
  end

  def accommodation_tax_for_room
    search_month = params.dig(:accommodation_tax_csv_for_room, :search_month)
    @form = Summary::Reports::AccommodationTaxCsvForm.new(search_month: search_month, action_name: action_name)
    send_data render_to_string, filename: @form.file_name, type: 'text/csv; charset=shift_jis'
  end

  def accommodation_tax_for_building
    search_month = params.dig(:accommodation_tax_csv_for_building, :search_month)
    @form = Summary::Reports::AccommodationTaxCsvForm.new(search_month: search_month, action_name: action_name)
    send_data render_to_string, filename: @form.file_name, type: 'text/csv; charset=shift_jis'
  end

  # def accommodation_tax_for_date_room
  #   search_month = params.dig(:accommodation_tax_csv_for_building, :search_month)
  #   @form = Summary::Reports::AccommodationTaxCsvForm.new(search_month: search_month, action_name: action_name)
  # end

  def csv
    @form = Summary::SalesListForm.new(fetch_search_conditions)
    if @form.valid?
      @form.fetch_list(is_csv: true)
    end
    send_data render_to_string, filename: @form.csv_file_name, type: 'text/csv; charset=shift_jis'
  end
end
