class Summary::SalesListController < ApplicationController
  # コールバック変数のセット
  before_action -> {
    set_callback(:sales_list)
  }

  def index
    clear_search_conditions
    @form = Summary::SalesListForm.new(is_show_all_rooms: params.dig(:show_all_rooms).to_bool) # params は show_all_rooms の有無のみ
    # @form.show_all_rooms = true if params.dig(:show_all_rooms).to_bool
    @form.fetch_list
    @settlement_by_room_id = @form.get_settlement_by_room
  end

  def search
    @form = Summary::SalesListForm.new(search_params)
    if @form.valid?
      @form.fetch_list
      @settlement_by_room_id = @form.get_settlement_by_room
      set_search_conditions
    end
  rescue => ex
    render "shared/ajax_error"
  end

  # 検索条件をセッションから取得して一覧表示
  def transition_search
    @form = Summary::SalesListForm.new(fetch_search_conditions)
    @form.fetch_list
    @settlement_by_room_id = @form.get_settlement_by_room
    render :index
  end

  def csv
    # 直前の検索したIDを保持しているので、is_show_all_rooms は常にtrueでわたす >>> 2020-10-25 CSV出力時は問答無用でfalseを渡すように変更
    @form = Summary::SalesListForm.new(fetch_search_conditions, is_show_all_rooms: false)
    if @form.valid?
      @form.fetch_list(is_csv: true)
    end
    send_data render_to_string, filename: @form.csv_file_name, type: 'text/csv; charset=shift_jis'
  end

  private

  def search_params
    params.require(:summary_sales_list_form).permit(
      :search_check_in_month, :search_original_month, :search_prev_next_month,
      :show_all_rooms,
      search_room_ids: []
    )
  end
end
