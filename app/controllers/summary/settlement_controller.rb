class Summary::SettlementController < ApplicationController

  def new
    # TODO:年月の簡易バリデーション

    # 既にデータが存在する場合は編集画面へリダイレクト
    settlement = Settlement.find_by(room_id: params[:room_id], year_month: params[:year_month])
    if settlement
      flash[:notice] = "この精算書は既に作成されています。編集画面へ移動しました。"
      return redirect_to summary_settlement_edit_path(settlement.id)
    end

    # 前月のデータの存在チェック
    prev_year_month = (params[:year_month] + '01').to_date&.last_month.strftime("%Y%m")
    @is_prev_data_exists = Settlement.exists?(room_id: params[:room_id], year_month: prev_year_month)

    @year_month = params[:year_month]
    @year_month_label = "#{@year_month[0..3]}年#{@year_month[4..5]}月"

    @room = Room.find(params[:room_id])

    # オーナー存在チェック
    flash.now[:error] = "この施設のオーナーが未設定です。先に施設をオーナーに紐付けてください。" unless @room.building.owner
  end

  def create
    ActiveRecord::Base.transaction do
      room_id = create_params[:room_id]
      if create_params[:template]
        settlement = Settlement.initialized_by_template(room_id, create_params[:year_month])
      elsif create_params[:prev_data]
        prev_year_month = (create_params[:year_month] + '01').to_date&.last_month.strftime("%Y%m")
        settlement = Settlement.initialized_by_prev_settlement(room_id, create_params[:year_month], prev_year_month)
      end

      if settlement.valid?
        settlement.save!
        flash[:success] = '精算書データを作成しました。詳細を確認してください。'
        redirect_to summary_settlement_edit_path(settlement.id)
      else
        # TODO: ログ とりあえず画面出力（通常はありえない）
        flash[:error] = "不正な値が送信されました。システム管理者にお問い合わせください。System log:[ #{settlement.errors.messages.to_s} ]"
        redirect_to summary_sales_list_transition_search_path
      end
    end
  rescue => ex
    output_error_log(ex, "精算書 登録処理")
    flash[:error] = "エラーが発生したため、精算書の登録処理を中止ました。システム管理者にお問い合わせください。"
    redirect_to summary_sales_list_transition_search_path
  end

  def edit
    @settlement = Settlement.find_by(id: params[:id])
    if @settlement.nil?
      flash[:alert] = "精算書のデータが存在しませんでした。再度一覧から選択してください。"
      return redirect_to summary_sales_list_transition_search_path
    end
    @year_month_label = "#{@settlement.year_month[0..3]}年#{@settlement.year_month[4..5]}月"

    @settlement.set_analysis_info

    @owner = @settlement.room.building.owner

    # 売上０でも精算書を作成できるように仕様変更のためコメントアウト TODO: 確認後消去
    # if @settlement.analysis1.total.zero?
    #   flash[:alert] = "宿泊情報が存在しませんでした。再度一覧から選択してください。"
    #   return redirect_to summary_sales_list_transition_search_path
    # end
  rescue => ex
    output_error_log(ex, "精算書編集")
    flash[:error] = "エラーが発生しました。システム管理者にお問い合わせください。"
    redirect_to summary_sales_list_transition_search_path
  end

  # remote: true
  def update
    @target = update_params[:target]
    @settlement = Settlement.find(update_params[:id])
    @settlement.match_attributes = update_params
    # if @settlement.valid?
    @settlement.save!
    @settlement.set_analysis_info
  rescue => ex
    output_error_log(ex, "精算書編集 更新処理")
    @is_error = true
  end

  def delete
    settlement = Settlement.find(params[:id])
    if settlement.nil?
      flash[:error] = "既に削除されているか、有効な精算書情報が存在しません。"
    else
      settlement.delete
      flash[:success] = "精算書情報を削除しました。【#{settlement.year_month_label} #{settlement.room&.building&.name}】"
    end
    redirect_to summary_sales_list_transition_search_path
  end

  # 施設の最新の報酬率で精算書を更新
  def refresh_reward_rate
    settlement = Settlement.find(params[:id])
    settlement.refresh_reward_rate!
    flash[:success] = "報酬率を製設定しました。"
    redirect_to summary_settlement_edit_path(settlement.id)
  end

  def print
    settlement = Settlement.find_by(id: params[:id])
    if settlement.nil?
      render plain: '精算書情報が存在しませんでした。'
      return
    end

    # Thinreportsオブジェクトを生成してPDFファイルを作成
    pdf_file_obj = Pdf::SettlementService.new(settlement).generate

    respond_to do |format|
      format.pdf do
        # ブラウザでPDFを表示する
        # disposition: "inline" によりダウンロードではなく表示させている
        filename = encode_filename "精算書_#{settlement.year_month_label}_#{settlement.room.full_code_name}.pdf"
        send_data(pdf_file_obj,
                  filename:    filename,
                  type:        "application/pdf",
                  disposition: "inline")
      end
    end
  rescue => ex
    output_error_log(ex, message = '精算書PDF出力')
    render html: 'PDFを作成できませんでした。<a href="javascript:;" onclick="window.close();">このウィンドウを閉じる。</a>'.html_safe
  end

  # 精算書エクセルデータダウンロード
  # NOTE: 2021-07-28 〜未使用
  # gem 'axlsx' を使ったエクセルファイル生成
  # viewファイルは summary/settlement/excel.xlsx.axlsx を利用し、エクセルファイルのレイアウトもここで行う
  # 既存ファイルを読み込めないので複雑なレイアウトは厳しい
  def excel
    @settlement = Settlement.find_by(id: params[:id])
    if @settlement.nil?
      render plain: '精算書情報が存在しませんでした。'
      return
    end
    @settlement.set_analysis_info

    filename = "精算書_#{@settlement.year_month_label}_#{@settlement.room.full_code_name}.xlsx"

    respond_to do |format|
      # format.html { pagination(@users) }
      format.xlsx do
        response.headers['Content-Disposition'] = "attachment; filename=#{filename}"
      end
    end
  end

  # 精算書エクセルデータダウンロード
  # gem 'rubyXL' を使ったエクセルファイル生成
  # 書式設定、レイアウト済のテンプレートとして既存ファイルを読み込んで値を入れていく
  def xlsx
    settlement = Settlement.find_by(id: params[:id])
    if settlement.nil?
      render plain: '精算書情報が存在しませんでした。'
      return
    end

    service = Excel::SettlementService.new(settlement)
    filename = "精算書_#{settlement.year_month_label}_#{settlement.room.full_code_name}.xlsx".encode(Encoding::Windows_31J)

    respond_to do |format|
      # format.html
      format.xlsx do
        send_data service.generate, filename: filename
      end
    end
    # send_data book.stream.string, filename: filename
  end

  private

  def create_params
    params.require(:settlement).permit(
      :year_month, :room_id, :template, :prev_data
    )
  end

  def update_params
    params.require(:settlement).permit(%i(
      target
      id
      title
      owner_name building address
      company_name company_info1 company_info2 company_info3
      label_sales label_target_days label_working_days label_guest_count label_occ label_adr label_rev_par
      label_owner_sales label_other_income_total label_other_cost_total label_reward_rate label_payment_amount
      internet_income free_income1 free_income2 free_income3 free_income4 free_income5
      label_free_income1 label_free_income2 label_free_income3 label_free_income4 label_free_income5 label_total_income
      site_fee electrical_fee gas_fee water_fee internet_fee site_controller_fee
      label_system_fee1 system_fee1
      free_fee1 free_fee2 free_fee3 free_fee4 free_fee5
      label_free_fee1 label_free_fee2 label_free_fee3 label_free_fee4 label_free_fee5 label_total_fee
      note1 note2 note3 note4
    ))
  end
end