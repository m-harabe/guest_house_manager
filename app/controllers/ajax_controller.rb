class AjaxController < ApplicationController

  # remote: true
  # 施設選択セレクトボックス用オプション値の生成 app/views/shared/_replace_select_option_js.html.erb 用
  # エリアIDで絞った部屋を検索して返す
  # @param[String] area_id エリアID
  # @return[Hash<Array>] オプション生成用ハッシュ e.g){ id: 1, name: '1001: 紡 東福寺南門'}
  def select_room_options_by_area
    if params[:area_id] == '0'
      rooms = Room.with_building.visible.display_order
    else
      rooms = Room.with_building.visible.area_id_is(params[:area_id]).display_order
    end
    render json: rooms.inject([]){ |options, ar| options << {id: ar.id, name: ar.full_code_name} }
  end

end
