class DashboardController < ApplicationController
  def index
    # status = Settings.RESERVATION.STATUS
    # reservations = Reservation.STSATUS_LIST_NOT_CANCEL.check_in_day_is(Date.today)
    # @check_in_total = reservations.count
    # @check_in_not_yet = reservations.where(status: [status.PREPARING, status.POSSIBLE]).count
    # @check_in_staying = reservations.where(status: [status.STAYING, status.POSSIBLE]).count
    # Reservation.where(status: [status.PREPARING, status.POSSIBLE]).check_in_day_is(Date.today)

    @release_note = ReleaseNote.not_sheduled.limit(5)
    @scheduled_note = ReleaseNote.sheduled.reorder(nil).limit(5)
  end
end
