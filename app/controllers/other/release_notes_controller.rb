class Other::ReleaseNotesController < ApplicationController
  # リリースノート一覧
  def index
    @release_note = ReleaseNote.not_sheduled
  end
end
