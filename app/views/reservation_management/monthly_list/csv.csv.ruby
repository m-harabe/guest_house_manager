require 'csv'

CSV.generate(encoding: Encoding::SJIS, row_sep: "\r\n", force_quotes: true) do |csv|
  csv_column_names = %w(
    日付 施設名 予約情報ID ステータス ゲスト名 国籍 チェックイン日 予定チェックイン時刻 泊数 人数（合計） 大人 子供 宿泊料 清掃費 食事代 合計 予約サイト 手数料 予約日 備考
  )

  csv << csv_column_names
  @form.monthly_list.each do |row|
    csv_column_values = [
      l(row.date),
      row.room_name.to_s,
      row.reservation_id.to_s,
      row.status.to_s,
      row.visitor_name.to_s.sjisable,
      row.nationality_name.to_s,
      row.check_in_date.to_s,
      row.scheduled_check_in_time ? l(row.scheduled_check_in_time, format: :time) : '',
      row.number_of_nights.to_s,
      row.total_count.to_s,
      row.adult_count.to_s,
      row.child_count.to_s,
      row.room_charge.to_s,
      row.cleaning_fee.to_s,
      row.meal_fee.to_s,
      row.total.to_s,
      row.site_name.to_s,
      row.handling_charge.to_s,
      row.reserve_date.to_s,
      row.note.to_s.sjisable
    ]
    csv << csv_column_values
  end
end