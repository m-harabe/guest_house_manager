require 'csv'

abbr_day_names = I18n.t("date.abbr_day_names")
CSV.generate(encoding: Encoding::SJIS, row_sep: "\r\n", force_quotes: true) do |csv|
  csv_column_names = %w(
    日付 曜日 200円 500円 1000円 合計
  )
  csv << csv_column_names

  @form.csv_row_datas.each do |date, attr|
    csv_column_values = [
      l(date, format: :default),
      abbr_day_names[date.wday],
      attr[200],
      attr[500],
      attr[1000],
      attr[:tax_total],
    ]
    csv << csv_column_values
  end

end