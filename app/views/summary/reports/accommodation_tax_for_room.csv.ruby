require 'csv'

CSV.generate(encoding: Encoding::SJIS, row_sep: "\r\n", force_quotes: true) do |csv|
  csv_column_names = %w(
    コード 施設名称 表示順 部屋 200円 500円 1000円 合計
  )
  csv << csv_column_names

  @form.csv_row_datas.each do |room_id, attr|
    room = @form.rooms[room_id]

    # 非表示の部屋で0件のデータは出力しない（過去の物件で０件でないデータは表示）
    # TODO 部屋数が多くなってきた時のパフォーマンスを考慮する必要あり
    next if room.is_visible == false and attr[:total_tax] == 0
    csv_column_values = [
      room.building_code,
      room.building_name,
      room.room_no,
      room.name,
      attr[200],
      attr[500],
      attr[1000],
      attr[:tax_total],
    ]
    csv << csv_column_values
  end

end