require 'csv'

CSV.generate(encoding: Encoding::SJIS, row_sep: "\r\n", force_quotes: true) do |csv|
  csv_column_names = %w(
    年月 施設コード 施設名 部屋名 予約件数 売上 宿泊費 清掃費 食事代 稼働日 OCC ゲスト数 DOR ADR 人数単価 RevPAR
  )

  csv << csv_column_names
  @form.search_room_ids.each do |id|
    room = @form.rooms[id]
    next unless room

    values = @form.room_id_hash[id]
    values ||= {}
    csv_column_values = [
      @form.search_check_in_month,
      room.building_code,
      room.building_name,
      room.name,
      values[:reservation_count],
      values[:sum_total],
      values[:sum_room_charge],
      values[:sum_cleaning_fee],
      values[:sum_meal_fee],
      values[:operation_count],
      values[:occ],
      values[:sum_guest],
      values[:dor],
      values[:adr],
      values[:unit_price],
      values[:rev_par]
    ]
    csv << csv_column_values
  end
end