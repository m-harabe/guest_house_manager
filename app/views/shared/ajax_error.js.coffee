# 非同期処理中の例外発生時処理

# デフォルトメッセージの設定
<% @message ||= 'システムエラーが発生したため、処理を中断しました。お手数ですが再度処理を行ってください。' %>

# ローディングの停止とアラート
$('form').spin false
$('body').removeClass 'spin'

# Controller で @reload = true を指定したらモーダルを閉じた後にJSリロード。デフォルトはfalse
$('.modal-area').html('<%= j render("modal/common/alert", title: "エラー", body: @message ) %>')
$('#alertModal').modal('show')