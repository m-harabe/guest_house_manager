module FileImport
  class FormatError < StandardError; end
  class ParseFailure < StandardError; end
end
