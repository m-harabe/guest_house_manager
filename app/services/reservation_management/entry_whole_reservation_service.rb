class ReservationManagement::EntryWholeReservationService < ApplicationService

  def initialize(is_new: false)
    @is_new = is_new
  end

  # 登録
  # @param attributes [Hash]
  # @return [WholeReservation]
  def create(attributes)
    ActiveRecord::Base::transaction do
      # WholeReservationの登録
      WholeReservation.create_or_update(attributes, is_new: @is_new)
    end
  rescue => ex
    output_error_log(ex, '')
    raise ex
  end
end