class ReservationManagement::UpdateReservationDateService < ApplicationService
  # 登録・更新処理
  def self.update(reservation)
    ActiveRecord::Base::transaction do
      reservation.update_reservation_dates
    end
    reservation
  # rescue => ex
  #   output_error_log(ex, '')
  #   raise ex
  end
end