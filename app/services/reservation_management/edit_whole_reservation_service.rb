class ReservationManagement::EditWholeReservationService < ApplicationService
  # 基本情報の更新
  def self.update_basic(attributes)
    ActiveRecord::Base::transaction do
      whole_reservation = WholeReservation.update_basic(attributes)
      Reservation.update_all_basics(whole_reservation_id: whole_reservation.id,
                                    attr: whole_reservation.changed_attributes)
    end
  rescue => ex
    output_error_log(ex, '一棟貸 基本情報更新')
    raise ex
  end

  # 請求情報の更新
  def self.update_payment(attributes)
    ActiveRecord::Base::transaction do
      whole_reservation = WholeReservation.create_or_update(attributes)
    end
  rescue => ex
    output_error_log(ex, '一棟貸 請求情報更新')
    raise ex
  end

  # 各部屋毎の予約情報の更新
  def self.update_room_reservations(whole_reservation_id:, attr_list:)
    ActiveRecord::Base::transaction do
      whole_reservation = Reservation.update_share_price(whole_reservation_id: whole_reservation_id, attr_list: attr_list)
    end
  rescue => ex
    output_error_log(ex, '一棟貸 各部屋毎の予約情報更新')
    raise ex
  end

end