class ReservationManagement::ImportService < ApplicationService

  # def initialize(form:)
  #   @form = form
  # end

  # ワークテーブルへの登録
  def import(form:)
    success_count = 0
    error_count = 0
    form.import_reservation_list.each do |row|
      next error_count += 1 unless row.errors.empty?
      begin
        wk_reservation = WkReservation.new
        wk_reservation.match_attributes = row.attributes
        wk_reservation.save!
        row.wk_reservation_id = wk_reservation.id
        success_count += 1
      rescue => ex
        output_error_log(ex, 'インポート 登録処理')
        error_count += 1
        row.errors.add :base, '予期せぬエラーが発生しました。'
      end
    end
    form.success_count = success_count
    form.error_count = error_count
  end

  # def import
  #   success_count = 0
  #   error_count = 0
  #   @form.import_reservation_list.each do |row|
  #     next error_count += 1 unless row.errors.empty?
  #     begin
  #       ActiveRecord::Base::transaction do
  #         if row.is_whole
  #           whole_reservation = WholeReservation.create_or_update(row.attributes, is_new: true)
  #           row.whole_reservation_id = whole_reservation.id
  #           success_count += 1
  #         else
  #           reservation = Reservation.create_or_update(row.attributes, is_new: true)
  #           ReservationDate.reverse_entries(reservation)
  #           row.reservation_id = reservation.id
  #           success_count += 1
  #         end
  #       end
  #     rescue => ex
  #       error_count += 1
  #       output_error_log(ex, 'インポート 登録処理')
  #     end
  #   end
  #   @form.success_count = success_count
  #   @form.error_count = error_count
  # end

  # def do_transaction(row_hash_arr)
  #   begin
  #     @transaction_cnt += 1
  #     ActiveRecord::Base::transaction do
  #       # モデルに入れてバリデーションエラーがある場合はset_err
  #       # ロールバックをかけたい場合はraise ValidationErrorしてください
  #       do_update(row_hash_arr)
  #     end
  #   rescue ValidationError
  #     # ロールバック
  #     @transaction_err_cnt += 1
  #   rescue => ex
  #     p ex.class
  #     p ex.message
  #     p ex.backtrace
  #     raise ex
  #   end
  # end
end