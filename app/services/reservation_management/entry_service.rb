class ReservationManagement::EntryService < ApplicationService

  def initialize(is_new: false)
    @is_new = is_new
  end

  # 登録・更新処理
  def apply(attributes)
    ActiveRecord::Base::transaction do
      reservation = Reservation.create_or_update(attributes)
      ReservationDate.reverse_entries(reservation)
      return reservation
    end
  # TODO
  # rescue => ex
  #   output_error_log(ex, '予約 登録・更新')
  #   raise ex
  end

  # 更新処理
  # def update(attributes)
  #   ActiveRecord::Base::transaction do
  #     student = Student.update_student(attributes)
  #     contract = student.contract
  #     contract.update_contract(nil, User.current_user.id)
  #     return student
  #   end
  # rescue => ex
  #   raise ex
  # end

end