class ReservationManagement::UpdateStatusService < ApplicationService
  # ステータス更新
  # @param [Integer] reservation_id
  # @param [String] current_status 変更前のステータス
  # @param [Boolean] is_bulk - true: 一棟貸しの場合、紐付く予約情報のステータスを全て更新する
  #                          - false(default): 対象の予約情報のみを更新するが、紐付く予約が全て同じステータスなら一棟貸のステータスも更新する
  # @raise [Service::UpdateFailure] 更新できなかった際にコントローラに返すメッセージを保持
  # 変更前のステータスがDBと異なる場合は変更済と判断
  def self.forward_status(reservation_id:, current_status:, is_bulk: false)
    reservation = Reservation.find_by(id: reservation_id, status: current_status)
    raise Service::UpdateFailure.new I18n.t("errors.reservation.already_chage_status_or_lost") unless reservation

    setting = Settings.RESERVATION.STATUS
    case current_status
    when setting.PREPARING, setting.POSSIBLE, setting.STAYING
      target_status = (current_status.to_i + 1).to_s
    else
      raise Service::UpdateFailure.new I18n.t("errors.reservation.cant_update_status")
    end

    ActiveRecord::Base::transaction do
      # 通常の予約情報
      unless reservation.whole?
        reservation.update!(status: target_status)
        return reservation
      end

      # 一棟貸情報と、紐付く全て予約情報のステータスを一括更新するケース
      if is_bulk
        WholeReservation.update_all_status(whole_reservation_id: reservation.whole_reservation_id, target_status: target_status)

      # 対象の予約情報のステータスを更新し、一棟貸に紐付く他の予約情報も全てステータスが一致した場合のみ一棟貸情報のステータスを更新するケース
      else
        reservation.update!(status: target_status)
        whole_reservation = WholeReservation.eager_load(:reservations).find_by(id: reservation.whole_reservation_id)
        same_status_count = Reservation.where(whole_reservation_id: reservation.whole_reservation_id, status: target_status).size
        if whole_reservation.reservations.size == same_status_count
          whole_reservation.update!(status: target_status)
        end
      end
    end
    reservation
  rescue Service::UpdateFailure => ex
    raise ex # そのままコントローラに返す
  rescue => ex
    output_error_log(ex)
    raise Service::UpdateFailure.new I18n.t("errors.common.rollback", action: I18n.t("button.update"))
  end

  # キャンセル処理
  # @param current_status [String] 変更前のステータス
  # @param reservation_id [Integer]
  # @param whole_reservation_id [Integer] reservation_idかどちらか一方を指定すること
  # @raise Service::UpdateFailure 更新できなかった際にコントローラに返すメッセージを保持
  # 変更前のステータスがDBと異なる場合は変更済と判断
  def self.cancel_status(current_status:, reservation_id: nil, whole_reservation_id: nil)
    reservation = Reservation.find_by(id: reservation_id, status: current_status) if reservation_id
    reservation = WholeReservation.find_by(id: whole_reservation_id, status: current_status) if whole_reservation_id
    raise Service::UpdateFailure.new I18n.t("errors.common.already_update", label: 'ステータス') unless reservation

    unless Reservation::STSATUS_LIST_NOT_CANCEL.include?(current_status)
      raise Service::UpdateFailure.new I18n.t("errors.reservation.cant_update_status")
    end

    target_status = Settings.RESERVATION.STATUS.CANCEL
    ActiveRecord::Base::transaction do
      if whole_reservation_id
        WholeReservation.update_all_status(whole_reservation_id: whole_reservation_id, target_status: target_status)
      else
        reservation.update!(status: target_status)
      end
    end
  rescue Service::UpdateFailure => ex
    raise ex # そのままコントローラに返す
  rescue => ex
    output_error_log(ex)
    raise Service::UpdateFailure.new I18n.t("errors.common.rollback", action: I18n.t("button.update"))
  end
end