#
# Suitebookをスクレイピングして本日の予約情報CSVファイルをダウンロード
#
module Suitebook
  class DownloadCsvService < SuitebookService

    def initialize
      super

      # ディレクトリ生成
      # `mkdir -p #{SUITEBOOK_CSV_DIR}`
      FileUtils.mkdir_p SUITEBOOK_CSV_DIR
      FileUtils.mkdir_p SCREENSHOT_DIR

      # Capybara自体の設定、ここではどのドライバーを使うかを設定しています
      Capybara.configure do |capybara_config|
        capybara_config.default_driver = :chrome
        capybara_config.default_max_wait_time = 10 # 一つのテストに10秒以上かかったらタイムアウトするように設定しています
      end
      # Capybaraに設定したドライバーの設定をします
      Capybara.register_driver :selenium do |app|
        options = Selenium::WebDriver::Chrome::Options.new
        options.add_argument('headless') # ヘッドレスモードをonにするオプション（ローカル環境ではコメントアウトするとブラウザ起動）
        options.add_argument('--disable-gpu') # 暫定的に必要なフラグとのこと

        # 日本語で表示させるオプション >> CentOS7環境でうまくいかない
        # options.add_argument("--lang\=ja") # MacではOK
        options.add_argument('--lang=ja')

        options.add_argument('--window-size=1920,1080') # 画面サイズが小さいとヘッドレスモードで要素が取得できないことがある
        # options.add_argument('--start-maximized')  # 最大サイズでスタート
        # options.add_argument('--start-fullscreen')  # 全画面表示でスタート
        options.add_argument("--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
        options.add_preference(:download, default_directory: SUITEBOOK_CSV_DIR)
        Capybara::Selenium::Driver.new(app, browser: :chrome, options: options)
        # driver.browser.download_path = SUITEBOOK_CSV_DIR
      end
      Capybara.javascript_driver = :selenium
      @session = Capybara::Session.new(:selenium)
    rescue => ex
      # p ex.chromedriverchromedriver
      ErrorUtil::error_log_and_notify(ex: ex, message: 'Capybara設定エラー')
      @logger.error "Capybara設定エラー\n"
      raise 'Capybara setting Error!!'
    end

    # 登録・更新処理
    def scraping
      @logger.info '--------- Suitebook連携開始 ---------'

      @session.visit SIGNIN_URL

      # ID入力・PW入力
      # @session.find_by_id("email").set(Settings.SUITEBOOK.ID)
      # @session.find_by_id("password").set(Settings.SUITEBOOK.PW)
      @session.find_by_id("clr-form-control-1").set(Settings.SUITEBOOK.ID)
      @session.find_by_id("clr-form-control-2").set(Settings.SUITEBOOK.PW)
      # screenshot('ログイン画面')

      # ログインボタンクリック
      # @session.find_button('ログイン').click
      # @session.find('sq-submit').click
      @session.find('.btn.btn-primary.ng-star-inserted').click
      sleep 1

      # screenshot('ログイン直後')
      # サイドメニュー > 予約ページへのリンクをクリック
      @session.find_link('予約').click
      sleep 0.5

      # 直前のリンクが別タブで開くので操作画面を最後に開いたタブに切替える
      # @session.switch_to_window(@session.windows.last)

      # CSVダウンロードデータの抽出対象セレクトボックスの選択
      # @session.find('select.clr-select.ng-untouched.ng-pristine.ng-valid').select(get_target_option_label)
      term_select = @session.find('select[name="dateFilter"]')
      term_select.find("option[value='#{get_target_option_value}']").select_option

      sleep 0.5

      # ダウンロードボタン
      @session.click_button('CSVダウンロード')

      sleep 0.5

      # 出力列の選択で全てを出力するようチェックをいれる
      @session.click_button('全てを選択する')
      @session.click_button('次へ')
      @session.click_button('ダウンロードする')

      sleep 2 # ブラウザのダウンロードダイアログが表示されるまで待つ

    rescue => ex
      # p 'スクレイピング Error!!!!!!!!!!!!!!!!!!!'
      # p ex.class
      # p ex.message
      # puts ex.backtrace
      ErrorUtil::error_log_and_notify(ex: ex, message: 'スクレイピングエラー')
      @logger.error "スクレイピングエラー\n"
      screenshot('スクレイピング Error!!', is_open: ENV["RAILS_ENV"] == "development")
      raise 'Scraping Error!!'
    end


    private

    # 本番環境（CentOs）では日本語環境にできなかったのでローカルでしか動作しないメソッド >> cronに環境変数を指定することで利用可能になっている
    def get_target_option_label
      now = Time.zone.now
      # 日付が変わって最初の処理（00:00:00 〜 00:03:00 の間）は今週作成分を取得する >> 前日の23:59:59 等のデータを取得するため
      # その中でも日曜の場合は前日(土曜)の23:59:59 等のデータが「今週」では取れないため、「今月」で取得する
      # FIXME: 上記パターンでは月末日の 23:59:59 等のデータのみ取れない
      if now.hour == 0 && now.min < 3
        return '今月予約作成' if now.wday == 0
        return '今週予約作成'
      end
      '今日予約作成'
    end

    # 期間指定オプションの値とラベル
    # '12': 今日予約作成
    # '13': 今週予約作成
    # '14': 今月予約作成
    def get_target_option_value
      now = Time.zone.now
      # 日付が変わって最初の処理（00:00:00 〜 00:03:00 の間）は今週作成分を取得する >> 前日の23:59:59 等のデータを取得するため
      # その中でも日曜の場合は前日(土曜)の23:59:59 等のデータが「今週」では取れないため、「今月」で取得する
      # FIXME: 上記パターンでは月末日の 23:59:59 等のデータのみ取れない
      # if now.hour == 0 && now.min < 3
      #   return '14' if now.wday == 0
      #   return '13'
      # end
      # '12'


      # 2021/07/28 https://www.chatwork.com/#!rid122450124-1464115238163972096
      # 今日の予約だけ取得していると、suitebook側で連携が遅れ、翌日にデータ登録されて予約作成日は元の前日の日付になる不具合で漏れてしまう。
      # 暫定的に、常に今週分全てのデータを毎回取得
      '13'
    end

    # スクリーンショット
    # session.save_and_open_screenshot
    # session.save_screenshot('tmp/screenshots/suitebook.png')
    # session.save_and_open_screenshot('tmp/screenshots/suitebook.png')
    def screenshot(label, is_open: false)
      path = "#{SCREENSHOT_DIR}/#{Time.current.to_s :timestamp}_#{label}.png"
      is_open ? @session.save_and_open_screenshot(path) : @session.save_screenshot(path)
    end
  end
end



=begin
# 旧画面（2021-02-13 に変更したが、一応旧画面切替機能があるためのこしておく）

module Suitebook
  class DownloadCsvService < SuitebookService

    def initialize
      super

      # ディレクトリ生成
      # `mkdir -p #{SUITEBOOK_CSV_DIR}`
      FileUtils.mkdir_p SUITEBOOK_CSV_DIR
      FileUtils.mkdir_p SCREENSHOT_DIR

      # Capybara自体の設定、ここではどのドライバーを使うかを設定しています
      Capybara.configure do |capybara_config|
        capybara_config.default_driver = :chrome
        capybara_config.default_max_wait_time = 10 # 一つのテストに10秒以上かかったらタイムアウトするように設定しています
      end
      # Capybaraに設定したドライバーの設定をします
      Capybara.register_driver :selenium do |app|
        options = Selenium::WebDriver::Chrome::Options.new
        options.add_argument('headless') # ヘッドレスモードをonにするオプション（ローカル環境ではコメントアウトするとブラウザ起動）
        options.add_argument('--disable-gpu') # 暫定的に必要なフラグとのこと

        # 日本語で表示させるオプション >> CentOS7環境でうまくいかない
        # options.add_argument("--lang\=ja") # MacではOK
        # options.add_argument('--lang=ja')
        # options.add_argument('--lang=ja-jp')
        # options.add_argument('--lang=ja-JP')

        options.add_argument('--window-size=1200,900')
        # options.add_argument('--start-maximized')  # 最大サイズでスタート
        # options.add_argument('--start-fullscreen')  # 全画面表示でスタート
        options.add_argument("--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
        options.add_preference(:download, default_directory: SUITEBOOK_CSV_DIR)
        Capybara::Selenium::Driver.new(app, browser: :chrome, options: options)
        # driver.browser.download_path = SUITEBOOK_CSV_DIR
      end
      Capybara.javascript_driver = :selenium
      @session = Capybara::Session.new(:selenium)
    rescue => ex
      # p ex.class
      # p ex.message
      # p ex.backtrace
      ErrorUtil::error_log_and_notify(ex: ex, message: 'Capybara設定エラー')
      @logger.error "Capybara設定エラー\n"
      raise 'Capybara setting Error!!'
    end

    # 登録・更新処理
    def scraping
      @logger.info '--------- Suitebook連携開始 ---------'

      @session.visit SIGNIN_URL

      # ID入力・PW入力
      @session.find_by_id("email").set(Settings.SUITEBOOK.ID)
      @session.find_by_id("password").set(Settings.SUITEBOOK.PW)
      # screenshot('ログイン画面')

      # ログインボタンクリック
      # @session.find_button('ログイン').click
      @session.find('sq-submit').click
      sleep 1

      # screenshot('ログイン直後')
      # サイドメニュー > 予約管理ページへのリンクをクリック
      # @session.find_link('予約管理').click
      @session.find(:xpath, '//*[@id="sq-main-menu"]/sq-main-menu-item[3]/li/a').click
      sleep 0.5

      # データエクスポートアイコンクリック
      @session.click_button('file_download')
      sleep 0.5

      # data_typeのクリック・created at の選択
      @session.find('md-select[name="date_type"]').click
      sleep 0.5
      @session.find('md-option[value=created-at]').click

      # screenshot('日付選択前')

      # エクスポート日（From〜To）を指定
      date_from_id, date_to_id = get_target_days_str
      # p "date_from_id: #{date_from_id}    date_to_id: #{date_to_id}"

      @logger.info "【エクスポート日指定】FROM:#{date_from_id} TO:#{date_to_id}"

      # Fromの▼マーククリック・日付選択
      @session.find('.md-datepicker-triangle-button', match: :first).click
      sleep 0.5
      @session.find(date_from_id).click

      # Toの▼マーククリック・日付選択
      @session.all(".md-datepicker-triangle-button")[1].click # クラスで全てを配列で取得して２番目をクリック
      sleep 0.5
      @session.find(date_to_id).click

      # screenshot('エクスポート直前')

      # @session.click_button('エクスポート')
      @session.find(:xpath, '/html/body/div[3]/md-dialog/md-dialog-content/div/form/div[4]/button[1]').click

      sleep 2 # ブラウザのダウンロードダイアログが表示されるまで待つ

    rescue => ex
      # p 'スクレイピング Error!!!!!!!!!!!!!!!!!!!'
      # p ex.class
      # p ex.message
      # puts ex.backtrace
      ErrorUtil::error_log_and_notify(ex: ex, message: 'スクレイピングエラー')
      @logger.error "スクレイピングエラー\n"
      screenshot('スクレイピング Error!!', is_open: ENV["RAILS_ENV"] == "development")
      raise 'Scraping Error!!'
    end

    private

    # 日付が変わって最初の１回目は昨日の分を含めて取得する
    # return [Array<String>] 選択するカレンダーの日付のid属性セレクタの文字列を２つ配列で返す
    def get_target_days_str
      now = Time.zone.now

      # md-0, md-1 はカレンダー起動時に毎回タグが生成されてインクリメントされる。１回目が md-0
      # TOは当日
      date_to_id = "#md-1-month-#{now.year}-#{now.month - 1}-#{now.day}"

      # 日付が変わって最初の処理（00:00:00 〜 00:03:00 の間）は前日の分も含めて取得する >> 前日の23:59:59 等のデータを取得するため
      now = now - 1.day if now.hour == 0 && now.min < 3

      # FROMは前日 or 当日
      date_from_id = "#md-0-month-#{now.year}-#{now.month - 1}-#{now.day}"

      return date_from_id, date_to_id
    end

    # スクリーンショット
    # session.save_and_open_screenshot
    # session.save_screenshot('tmp/screenshots/suitebook.png')
    # session.save_and_open_screenshot('tmp/screenshots/suitebook.png')
    def screenshot(label, is_open: false)
      path = "#{SCREENSHOT_DIR}/#{Time.current.to_s :timestamp}_#{label}.png"
      is_open ? @session.save_and_open_screenshot(path) : @session.save_screenshot(path)
    end


    # スクレイピング処理
    # start_scraping 'https://www.google.com/' do
    #   # ここにスクレイピングのコードを書く
    #   p title #=> "Google"
    #   save_and_open_screenshot
    # end
    #
    # def start_scraping(url, &block)
    #   Capybara::Session.new(:selenium).tap { |session|
    #     session.visit url
    #     session.instance_eval(&block)
    #   }
    # end
  end
end
=end