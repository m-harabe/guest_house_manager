#
# suitebookからダウンロードしたCSVファイルをsuitebook予約情報テーブルに保存
#
module Suitebook
  class ImportCsvService < SuitebookService

    def initialize
      super

      # CSVの行データ（フォーマットオブジェクト）
      @import_obj_list = []

      # エラーメッセージを格納する配列
      @error_list = []

      # 処理結果件数保持用ハッシュ
      @count = Hash.new(0)

      # 削除対象日付
      # @target_day = Date.today - BACKUP_TERM
    end

    # 登録・更新処理
    def import
      # 格納されているCSVを全て処理（処理後に削除しているので基本的には１ファイルしかないはず）
      files = Dir.glob("#{Suitebook::SuitebookService::SUITEBOOK_CSV_DIR}/*.csv")
      if files.blank?
        message = '【バッチ処理アラート】suitebook予約情報CSVインポート - 対象ファイルがありません'
        @logger.error message
        NotificationUtil.post_message_to_slack(message: message)
        return
      end

      begin
        files.each do |filedir|
          parse_csv filedir
          FileUtils.rm filedir
        end
      rescue => ex
        # p ex.message
        # pp ex.backtrace
        # TODO: ログ
        message = '【バッチ処理エラー】suitebook予約情報CSVインポート - 解析処理'
        @logger.error message
        ErrorUtil.log_and_notify(ex: ex, title: message)
      end
    end

    private

    # フォーマット作成
    # - CSVモジュールのデータパッチ対応を行ってオプションを追加している
    # - [Shift-JISなCSVを読み込む・書き出しするときにエラーを起こさない数少ない方法](https://qiita.com/yuuna/items/7e4e06a1b881d85697e9)
    def parse_csv(filedir)

      # 通常は以下のオプション
      # CSV.foreach(file_obj.tempfile, headers: true, encoding: Encoding::Shift_JIS).each.with_index(2) do |row, idx|
      # CP932でエンコーディングするのはローマ数字対応
      # 2021-02-13 suitebook側の仕様変更によりBOM付きUTF-8になった
      CSV.foreach(open(filedir), headers: true, encoding: "BOM|utf-8", undef: :replace, replace: "*").each.with_index(2) do |row, idx|
        # 取り込み済はスキップ
        # TODO: 失敗しているデータ等ステータスによっては更新するケースは？？ >> 一旦なし

        if SuitebookReservation.where(suitebook_idnum: row["suitebook予約ID"]).exists?
          @count[:skip] += 1
          next
        end
        # フォーマット生成
        @import_obj_list << FileObjects::SuitebookReservationCsv.new(row: row, row_index: idx)
      end

      # 全フォーマットをバリデーション
      @import_obj_list.each do |obj|
        # エラーメッセージは最大１０まで保持
        break if @error_list.size >= 10
        next if obj.valid?
        @error_list = @error_list + obj.full_error_messages
      end

      if @error_list.present?
        # TODO:: ログテーブルへのデータ追加（エラーメッセージの格納等）
        # ErrorUtil.log_and_notify(ex: ex, title: '【バッチ処理エラー】suitebook予約情報CSVインポート')
        message = '【バッチ処理エラー】suitebook予約情報CSVインポート' + @error_list.join(' / ')
        NotificationUtil.post_message_to_cw(message)
        return
      end

      begin
        # 登録処理
        ActiveRecord::Base.transaction do
          building_list = []
          @import_obj_list.each do |obj|

            suitebook_reservation ||= SuitebookReservation.new
            suitebook_reservation.match_attributes = obj.attributes
            suitebook_reservation.save!
            @count[:create] += 1
          end
        end
        result = "【CSVインポート結果】登録: #{@count[:create]}件  スキップ: #{@count[:skip]}"
        # p result
        @logger.info result
      rescue => ex
        # １レコード（行）登録失敗しても処理続行
        # TODO: ログテーブル
        message = '【バッチ処理エラー】suitebook予約情報CSVインポート 登録処理（処理継続）'
        @logger.info message
        ErrorUtil.log_and_notify(ex: ex, title: message)
      end
    end

  end
end
