#
# ------------------------------------------------------------ #
# 2020-08-01 Dropbox の利用をやめたので現在は不使用
# ------------------------------------------------------------ #
#
#
class Suitebook::ImportDropboxCsvService < ApplicationService

  # 一時保存先ローカルディレクトリ
  SUITEBOOK_CSV_DIR = 'tmp/suitebook'

  # Dropboxのインポート対象ファイル保存先
  STRAGE_DIR = '/ghm/suitebook_csv/'

  # Dropboxの処理済ファイル保存先
  STRAGE_BACKUP_DIR = '/ghm/suitebook_csv_log/'

  # 保存期間
  BACKUP_TERM = 5

  # ファイル名の日付部分を抜き出す正規表現パターン
  FILE_NAME_REGEX = /(\d+-\d+-\d+).csv/

  def initialize
    # ディレクトリ生成
    `mkdir -p #{SUITEBOOK_CSV_DIR}`

    # CSVの行データ（フォーマットオブジェクト）
    @import_obj_list = []

    # エラーメッセージを格納する配列
    @error_list = []

    # 処理結果件数保持用ハッシュ
    @count = Hash.new(0)

    # DropboxAPIへ接続するためのクライアント（Gem）オブジェクトを生成
    @dropbox_client = DropboxApi::Client.new(Settings.DROPBOX.ACCESS_TOKEN)

    # 削除対象日付
    @target_day = Date.today - BACKUP_TERM
  end

  # 登録・更新処理
  def import

    result = @dropbox_client.list_folder(STRAGE_DIR)

    # TODO: ログテーブルの作成と実行結果の保存 ＞＞ 専用のファイル単位のログテーブルがいい
    if result.blank?
      NotificationUtil.post_message_to_slack(message: '【バッチ処理アラート】suitebook予約情報CSVインポート(Dropbox経由) CSVファイルがありません')
      return
    end

    result.entries.each do |data|
      begin
        link = @dropbox_client.get_temporary_link(data.path_display)&.link
        next if link.nil?

        filename = "#{Time.zone.now.to_s(:timestamp)}.csv"
        filedir = "#{SUITEBOOK_CSV_DIR}/#{filename}"
        open(filedir, 'wb') do |file|
          file << open(link).read
        end
        parse_csv(filedir)
        move_file(filedir, filename, data.path_lower)
      rescue => ex
        # p "error--------"
        # p ex.message
        # pp ex.backtrace
        # TODO: ログ
        ErrorUtil.log_and_notify(ex: ex, title: '【バッチ処理エラー】suitebook予約情報CSVインポート(Dropbox経由) 解析処理')
      end
    end
  end

  private

  # フォーマット作成
  # - CSVモジュールのデータパッチ対応を行ってオプションを追加している
  # - [Shift-JISなCSVを読み込む・書き出しするときにエラーを起こさない数少ない方法](https://qiita.com/yuuna/items/7e4e06a1b881d85697e9)
  def parse_csv(filedir)

    # 通常は以下のオプション
    # CSV.foreach(file_obj.tempfile, headers: true, encoding: Encoding::Shift_JIS).each.with_index(2) do |row, idx|
    # CP932でエンコーディングするのはローマ数字対応
    CSV.foreach(open(filedir), headers: true, encoding: "utf-8", undef: :replace, replace: "*").each.with_index(2) do |row, idx|
      # 取り込み済はスキップ
      # TODO: 失敗しているデータ等ステータスによっては更新するケースは？？ >> 一旦なし
      if SuitebookReservation.where(suitebook_idnum: row["suitebook予約ID"]).exists?
        @count[:skip] += 1
        next
      end
      # フォーマット生成
      @import_obj_list << FileObjects::SuitebookReservationCsv.new(row: row, row_index: idx)
    end

    # 全フォーマットをバリデーション
    @import_obj_list.each do |obj|
      # エラーメッセージは最大１０まで保持
      break if @error_list.size >= 10
      next if obj.valid?
      @error_list = error_list + obj.full_error_messages
    end

    if @error_list.present?
      # TODO:: ログテーブルへのデータ追加（エラーメッセージの格納等）
      ErrorUtil.log_and_notify(ex: ex, title: '【バッチ処理エラー】suitebook予約情報CSVインポート')
      return
    end

    begin
      # 登録処理
      ActiveRecord::Base.transaction do
        building_list = []
        @import_obj_list.each do |obj|

          suitebook_reservation ||= SuitebookReservation.new
          suitebook_reservation.match_attributes = obj.attributes
          suitebook_reservation.save!
          @count[:create] += 1
        end
      end
      p "@count:----------"
      pp @count
    rescue => ex
      # １レコード（行）登録失敗しても処理続行
      # TODO: ログテーブル
      ErrorUtil.log_and_notify(ex: ex, title: '【バッチ処理エラー】suitebook予約情報CSVインポート 登録処理')
    end
  end

  # ファイル移動
  def move_file(filedir, filename, path)
    # ローカルの一時ファイルは削除
    FileUtils.rm(filedir)

    # バックアップファイルの削除処理
    # result = @dropbox_client.list_folder(STRAGE_BACKUP_DIR)
    # result.entries.each do |data|
    #   p "data.path_lower: #{data.path_lower}"
    #   mache_data = FILE_NAME_REGEX.match(data.path_lower)
    #   next unless mache_data
    #   @dropbox_client.delete(data.path_lower) if mache_data[1].to_date < @target_day
    # end

    # Dropboxは一定期間保持
    # @dropbox_client.move("#{STRAGE_DIR}/#{filename}", "#{STRAGE_BACKUP_DIR}/#{filename}", autorename: true)
    @dropbox_client.delete(path) # 移動できないので削除

  # rescue => ex
  #   ErrorUtil.log_and_notify(ex: ex, title: '【バッチ処理エラー】suitebook予約情報CSVインポート ファイル移動処理')
  end

end