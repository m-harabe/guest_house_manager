#
# CSV1ファイル単位で
#
module Suitebook
  class SuitebookService < ApplicationService

    # ファイルダウンロードディレクトリ
    SUITEBOOK_CSV_DIR = Rails.root.join("tmp/data/suitebook").to_s.freeze

    # スクリーンショットディレクトリ
    SCREENSHOT_DIR = 'tmp/screenshots'.freeze

    # サイトURL
    SIGNIN_URL = "https://app.suitebook.io/host/sign-in/".freeze

    # 保存期間
    BACKUP_TERM = 5

    # ファイル名の日付部分を抜き出す正規表現パターン
    FILE_NAME_REGEX = /(\d+-\d+-\d+).csv/

    def initialize
      # ログファイル設定
      @logger = ActiveSupport::Logger.new("log/batch.log", "weekly")
      # フォーマット設定
      @logger.formatter = ::Logger::Formatter.new
    end
  end
end
