#
# Suitebook予約情報から予約情報を登録
#
module Suitebook
  class CreateReservationService < SuitebookService

    # 登録・更新処理
    def create
      begin
        result = create_reservations
        send_message(result) if result.present?
        @logger.info "--------- Suitebook連携終了 ---------\n"
      rescue => ex
        ErrorUtil.error_log_and_notify(ex: ex, message: 'Suitebook宿泊情報による予約一括登録処理')
      end
    end

    private

    # 未処理のデータで予約情報を一括登録処理
    def create_reservations

      targets = SuitebookReservation.where(status: Settings.SUITEBOOK_RESERVATION_STATUS.UNPROCESSED, reservation_id: nil, whole_reservation_id: nil)
      if targets.size == 0
        # @logger.info "対象データ（未処理のsuitebook予約情報）なし"
        return nil
      end
      # 処理結果件数保持用ハッシュ
      result = Hash.new(0)

      no_building_idnums = []
      targets.each do |suitebook|
        # 1件ずつトランザクションを貼る
        begin
          ActiveRecord::Base.transaction do
            # 画面からの登録と同じ処理
            form = ReservationManagement::Reservations::EntryForm.new(params: suitebook.reservation_attributes)
            if form.valid?
              # Reservation、ReservationDate の登録 >> 登録タイプ【自動（suitebook）】
              reservation = Reservation.create_or_update(form.attributes.merge(registration_type: Settings.RESERVATION.REGISTRATION_TYPE.AUTO_SUITEBOOK))
              ReservationDate.reverse_entries(reservation)

              suitebook.reservation = reservation
              suitebook.status = Settings.SUITEBOOK_RESERVATION_STATUS.PROCESSED
              suitebook.save!
              result[:succress] += 1
            else
              # p "error--------suitebook_reservation.id: #{suitebook.id}"
              suitebook.status = Settings.SUITEBOOK_RESERVATION_STATUS.ERROR
              suitebook.error_message = form.errors.full_messages.join(',')
              suitebook.save!
              result[:error] += 1
            end
          end
        rescue => ex
          result[:exception] += 1
          ErrorUtil.log_and_notify(ex: ex, title: "suitebook >> reservation 登録処理 suitebook_reservation.id: #{suitebook.id}")
          suitebook.status = Settings.SUITEBOOK_RESERVATION_STATUS.ERROR
          suitebook.error_message = "システムエラー"
          suitebook.save!
        end
      end
      return result
    end

    def send_message(result)
      server = Settings.SERVER_NAME || 'local'
      message = "【アプリ名】#{Settings.APP_NAME}\n"
      message += "【suitebook予約情報一括登録処理結果】#{I18n.l(Time.zone.now, format: :default_h)}\n"
      message += "【サーバー】#{server}\n"
      result_message = "【処理結果】登録件数：  #{result[:succress].to_i}件 / エラー件数： #{result[:error].to_i}件 / 例外件数：  #{result[:exception].to_i}件"
      @logger.info result_message
      message += result_message
      # message += "\n"
      # message += "\n"

      # TODO: 安定稼働後、メッセージ送信する環境を限定
      # if Rails.env.production? && EnvUtil.production_ip?
      # if Rails.env.production?
      #   NotificationUtil.post_message_to_slack(message: message)
      # end
      NotificationUtil.post_message_to_slack(message: message)
    end

  end
end
