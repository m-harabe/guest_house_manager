class ApplicationService
  def output_error_log(ex, title = '')
    ErrorUtil.error_log_and_notify ex: ex, message: "Service modules #{title}"
  end
end
