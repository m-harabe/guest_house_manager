require 'rubyXL/convenience_methods'

module Excel
  class SettlementService < ApplicationService

    def initialize(settlement)
      # テンプレートの読み込み
      @book = RubyXL::Parser.parse('app/assets/formats/settlement_20210728.xlsx')
      @settlement = settlement
      @template = settlement.settlement_template
    end

    def generate
      settlement.set_analysis_info
      setup
      book.stream.read
    end

    private

    attr_reader :settlement, :template, :book

    #--------------------------------------------------------------------------------
    # PDF 生成
    #--------------------------------------------------------------------------------
    def setup

      # Readme
      #
      # データ書き込み
      # 【add_cell】 は元の書式を捨てて上書き
      # 【change_contents】 は元の書式を保ったまま値だけ更新
      #
      # book[0].add_cell 0, 0, 'changed'
      # num = 1
      # @products.each{|product|
      #   sheet[num][0].change_contents(product.name)    # 商品名
      #   sheet[num][1].change_contents(product.size)    # サイズ
      #   sheet[num][2].change_contents(product.weight)  # 重さ
      #   num += 1
      # }
      # 数値はINTで渡す
      # sheet[3][0].change_contents(3)
      #
      # 保存すると、元のファイルを更新する（ここでは使わない）
      # book.save

      # ヘルパーメソッドを拝借
      helper = Summary::SettlementController.helpers

      # エクセルの数式を動作されるための設定
      book.calc_pr.full_calc_on_load = true
      book.calc_pr.calc_completed = true
      book.calc_pr.calc_on_save = true
      book.calc_pr.force_full_calc = true

      # 一番目のワークシート読み込み
      sheet = book[0]

      sheet.sheet_name = "#{settlement.building}_#{settlement.year_month_label}"

      # タイトル
      sheet[0][0].change_contents(settlement.title)

      #--------------------
      # オーナー情報
      #--------------------
      # オーナー名
      sheet[1][0].change_contents settlement.owner_name + '　' + template.honorific
      # 施設名称
      sheet[3][0].change_contents template.building + ' ' + settlement.building
      # 施設住所
      sheet[4][0].change_contents template.address + ' ' + settlement.address

      #--------------------
      # 自社情報
      #--------------------
      sheet[1][19].change_contents template.company_name
      sheet[2][19].change_contents template.company_info1
      sheet[3][19].change_contents template.company_info2
      sheet[4][19].change_contents template.company_info3

      #--------------------
      # 宿泊情報一覧
      #--------------------
      # Header
      sheet[8][2].change_contents template.visitor_name
      sheet[8][3].change_contents template.nationality
      sheet[8][4].change_contents template.number_of_nights
      sheet[8][5].change_contents template.total_count
      sheet[9][6].change_contents template.adult_count
      sheet[9][7].change_contents template.child_count
      sheet[8][8].change_contents template.room_charge
      sheet[8][9].change_contents template.cleaning_fee
      sheet[8][10].change_contents template.total
      sheet[8][11].change_contents template.site
      sheet[8][12].change_contents template.handling_charge
      sheet[8][13].change_contents template.reservation_date

      # Body
      row = 10
      # sum_list = Hash.new(0)
      settlement.reservation_list.each do |obj|

        # 連泊の場合はセル結合する
        # 予約が無い日や連泊の２日目以降の場合、number_of_nights は "0" となるため、予約の１日目のみ1以上の数値が入る
        num = obj.output_row_count.to_i
        if num > 0
          sheet[row][2].change_contents obj.visitor_name + '様' if obj.visitor_name
          sheet[row][3].change_contents obj.nationality_name
          sheet[row][4].change_contents obj.number_of_nights
          sheet[row][11].change_contents obj.site
          sheet[row][13].change_contents obj.reserve_date.to_s(:slash) if obj.reserve_date

          # ２泊以上の連泊の場合は拍数分結合
          if num > 1
            merge_count = row + num - 1
            sheet.merge_cells(row, 2, merge_count, 2)
            sheet.merge_cells(row, 3, merge_count, 3)
            sheet.merge_cells(row, 4, merge_count, 4)
            sheet.merge_cells(row, 11, merge_count, 11)
            sheet.merge_cells(row, 13, merge_count, 13)
          end
        end

        # 日付 02（月）を日付と曜日で分解し、曜日で色をつける
        sheet[row][0].change_contents obj.date[0,2].to_i
        weekday = obj.date[-2,1]
        sheet[row][1].change_contents weekday
        font_color =
          case weekday
          when '土'
            '0066cc'
          when '日'
            'ff3300'
          end

        if font_color
          sheet[row][0].change_font_color(font_color)
          sheet[row][1].change_font_color(font_color)
        end

        sheet[row][5].change_contents obj.total_count if obj.total_count.to_i > 0
        sheet[row][6].change_contents obj.adult_count if obj.adult_count.to_i > 0
        sheet[row][7].change_contents obj.child_count if obj.child_count.to_i > 0
        sheet[row][8].change_contents obj.room_charge if obj.room_charge.to_i > 0
        sheet[row][9].change_contents obj.cleaning_fee if obj.cleaning_fee.to_i > 0
        sheet[row][10].change_contents obj.total if obj.total.to_i > 0
        sheet[row][12].change_contents obj.handling_charge.floor if obj.handling_charge.to_i > 0

        # 合計計算
        # sum_list[:number_of_nights] += obj.number_of_nights.to_i
        # sum_list[:total_count]      += obj.total_count.to_i
        # sum_list[:adult_count]      += obj.adult_count.to_i
        # sum_list[:child_count]      += obj.child_count.to_i
        # sum_list[:room_charge]      += obj.room_charge.to_i
        # sum_list[:cleaning_fee]     += obj.cleaning_fee.to_i
        # sum_list[:total]            += obj.total.to_i
        # sum_list[:handling_charge]  += obj.handling_charge.to_i
        row += 1
      end

      # 一覧最下部の合計行の出力 MEMO: エクセル側で計算
      # if settlement.reservation_list.present?
      #   sheet[41][4].change_contents sum_list[:number_of_nights]
      #   sheet[41][5].change_contents sum_list[:total_count]
      #   sheet[41][6].change_contents sum_list[:adult_count]
      #   sheet[41][7].change_contents sum_list[:child_count]
      #   sheet[41][8].change_contents sum_list[:room_charge]
      #   sheet[41][9].change_contents sum_list[:cleaning_fee]
      #   sheet[41][10].change_contents sum_list[:total]
      #   sheet[41][12].change_contents sum_list[:handling_charge]
      # end

      # ページ右側タイトル
      sheet[8][15].change_contents helper.convert_year_month_format_title(settlement.settlement_template.analysis_title, settlement.year_month)

      #--------------------
      # 画面右の左側テーブル１
      #--------------------
      sheet[11][15].change_contents settlement.label_sales
      sheet[11][16].change_contents settlement.analysis1.total

      sheet[12][15].change_contents settlement.label_target_days
      sheet[12][16].change_contents settlement.analysis1.target_days

      sheet[13][15].change_contents settlement.label_working_days
      sheet[13][16].change_contents settlement.analysis1.working_days

      sheet[14][15].change_contents settlement.label_guest_count
      sheet[14][16].change_contents settlement.analysis1.guest_count

      # 以下、値はエクセル側で式を設定するので入れない
      sheet[15][15].change_contents settlement.label_occ
      # sheet[15][16].change_contents settlement.analysis1.occ

      sheet[16][15].change_contents settlement.label_adr
      # sheet[16][16].change_contents settlement.analysis1.adr

      sheet[17][15].change_contents settlement.label_rev_par
      # sheet[17][16].change_contents settlement.analysis1.rev_par

      #--------------------
      # 画面右の左側テーブル２
      #--------------------
      sheet[19][15].change_contents settlement.label_owner_sales
      sheet[19][16].change_contents settlement.analysis1.owner_total

      sheet[20][15].change_contents settlement.label_other_income_total
      sheet[20][16].change_contents settlement.total_income

      sheet[21][15].change_contents settlement.label_other_cost_total
      sheet[21][16].change_contents settlement.total_fee

      #--------------------
      # 画面右の左側テーブル３
      #--------------------
      # 報酬率
      sheet[24][15].change_contents settlement.label_reward_rate
      sheet[24][16].change_contents settlement.reward_rate / 100

      # 入金額
      sheet[26][15].change_contents settlement.label_payment_amount
      sheet[26][16].change_contents settlement.analysis1.owner_reward.delete(',').to_i # "12,345円" >> 12345 に戻す


      #--------------------
      # 画面右の左側テーブル４
      #--------------------
      # 次月予約状況
      sheet[29][15].change_contents helper.convert_year_month_format_title(@settlement.settlement_template.next_month_title, @settlement.year_month, true)
      # 売上
      sheet[30][15].change_contents settlement.label_sales
      sheet[30][16].change_contents settlement.analysis2.total if settlement.analysis2.total.to_i > 0
      # 日数
      sheet[31][15].change_contents settlement.label_target_days
      sheet[31][16].change_contents settlement.analysis2.target_days
      # 稼働日数
      sheet[32][15].change_contents settlement.label_working_days
      sheet[32][16].change_contents settlement.analysis2.working_days
      # 宿泊日数
      sheet[33][15].change_contents settlement.label_guest_count
      sheet[33][16].change_contents settlement.analysis2.guest_count
      # OCC
      sheet[34][15].change_contents settlement.label_occ
      sheet[34][16].change_contents settlement.analysis2.occ
      # ADR
      sheet[35][15].change_contents settlement.label_adr
      sheet[35][16].change_contents settlement.analysis2.adr
      # RevPAR
      sheet[36][15].change_contents settlement.label_rev_par
      sheet[36][16].change_contents settlement.analysis2.rev_par

      #--------------------
      # その他収入テーブル
      #--------------------
      # タイトルラベル
      sheet[11][18].change_contents settlement.label_other_income

      other_income_row = 12

      # インターネット代
      sheet[other_income_row][18].change_contents settlement.label_internet_income
      sheet[other_income_row][22].change_contents settlement.internet_income if settlement.internet_income
      other_income_row += 1

      # フリー項目
      if settlement.label_free_income1.present? || settlement.free_income1
        sheet[other_income_row][18].change_contents settlement.label_free_income1
        sheet[other_income_row][22].change_contents settlement.free_income1 if settlement.free_income1
        other_income_row += 1
      end

      if settlement.label_free_income2.present? || settlement.free_income2
        sheet[other_income_row][18].change_contents settlement.label_free_income2
        sheet[other_income_row][22].change_contents settlement.free_income2 if settlement.free_income2
        other_income_row += 1
      end

      if settlement.label_free_income3.present? || settlement.free_income3
        sheet[other_income_row][18].change_contents settlement.label_free_income3
        sheet[other_income_row][22].change_contents settlement.free_income3 if settlement.free_income3
        other_income_row += 1
      end

      if settlement.label_free_income4.present? || settlement.free_income4
        sheet[other_income_row][18].change_contents settlement.label_free_income4
        sheet[other_income_row][22].change_contents settlement.free_income4 if settlement.free_income4
        other_income_row += 1
      end

      if settlement.label_free_income5.present? || settlement.free_income5
        sheet[other_income_row][18].change_contents settlement.label_free_income5
        sheet[other_income_row][22].change_contents settlement.free_income5 if settlement.free_income5
        other_income_row += 1
      end

      # 合計ラベル（値はエクセル側で計算）
      sheet[18][18].change_contents settlement.label_total_income

      #--------------------
      # その他費用テーブル
      #--------------------
      # タイトルラベル
      sheet[21][18].change_contents settlement.label_other_cost

      other_cost_row = 22

      # サイト手数料
      sheet[other_cost_row][18].change_contents settlement.label_site_fee
      sheet[other_cost_row][22].change_contents settlement.site_fee if settlement.site_fee
      other_cost_row += 1

      # 電気代
      sheet[other_cost_row][18].change_contents settlement.label_electrical_fee
      sheet[other_cost_row][22].change_contents settlement.electrical_fee if settlement.electrical_fee
      other_cost_row += 1

      # ガス代
      sheet[other_cost_row][18].change_contents settlement.label_gas_fee
      sheet[other_cost_row][22].change_contents settlement.gas_fee if settlement.gas_fee
      other_cost_row += 1

      # 水道代
      sheet[other_cost_row][18].change_contents settlement.label_water_fee
      sheet[other_cost_row][22].change_contents settlement.water_fee if settlement.water_fee
      other_cost_row += 1

      # インターネット費用
      sheet[other_cost_row][18].change_contents settlement.label_internet_fee
      sheet[other_cost_row][22].change_contents settlement.internet_fee if settlement.internet_fee
      other_cost_row += 1

      # サイトコントローラ
      sheet[other_cost_row][18].change_contents settlement.label_site_controller_fee
      sheet[other_cost_row][22].change_contents settlement.site_controller_fee if settlement.site_controller_fee
      other_cost_row += 1

      # システム自動追加項目
      if settlement.label_system_fee1.present? || settlement.system_fee1
        sheet[other_cost_row][18].change_contents settlement.label_system_fee1
        sheet[other_cost_row][22].change_contents settlement.system_fee1 if settlement.system_fee1
        other_cost_row += 1
      end
      # フリー項目
      if settlement.label_free_fee1.present? || settlement.free_fee1
        sheet[other_cost_row][18].change_contents settlement.label_free_fee1
        sheet[other_cost_row][22].change_contents settlement.free_fee1 if settlement.free_fee1
        other_cost_row += 1
      end
      if settlement.label_free_fee2.present? || settlement.free_fee2
        sheet[other_cost_row][18].change_contents settlement.label_free_fee2
        sheet[other_cost_row][22].change_contents settlement.free_fee2 if settlement.free_fee2
        other_cost_row += 1
      end
      if settlement.label_free_fee3.present? || settlement.free_fee3
        sheet[other_cost_row][18].change_contents settlement.label_free_fee3
        sheet[other_cost_row][22].change_contents settlement.free_fee3 if settlement.free_fee3
        other_cost_row += 1
      end
      if settlement.label_free_fee4.present? || settlement.free_fee4
        sheet[other_cost_row][18].change_contents settlement.label_free_fee4
        sheet[other_cost_row][22].change_contents settlement.free_fee4 if settlement.free_fee4
        other_cost_row += 1
      end
      if settlement.label_free_fee5.present? || settlement.free_fee5
        sheet[other_cost_row][18].change_contents settlement.label_free_fee5
        sheet[other_cost_row][22].change_contents settlement.free_fee5 if settlement.free_fee5
        other_cost_row += 1
      end
      # 合計ラベル（値はエクセル側で計算）
      sheet[other_cost_row][18].change_contents settlement.label_total_fee

      #--------------------
      # 備考
      #--------------------
      note_row = 35
      if settlement.note1.present?
        sheet[note_row][18].change_contents settlement.note1
        note_row += 1
      end
      if settlement.note2.present?
        sheet[note_row][18].change_contents settlement.note2
        note_row += 1
      end
      if settlement.note3.present?
        sheet[note_row][18].change_contents settlement.note3
        note_row += 1
      end
      if settlement.note4.present?
        sheet[note_row][18].change_contents settlement.note4
        note_row += 1
      end

      note_row += 1

      #--------------------
      # 銀行
      #--------------------
      owner = settlement.room.building.owner
      if settlement.jp?
        if owner.bank_label_jp.present?
          sheet[note_row][18].change_contents '振込先'
          sheet[note_row][19].change_contents owner.bank_label_jp
          note_row += 1
          sheet[note_row][19].change_contents owner.account_name
        end

      else
        if owner.bank_name.present? || owner.account_number.present? || owner.account_name.present?
          sheet[note_row][18].change_contents "Bank Name"
          sheet[note_row][20].change_contents "#{owner.bank_name} #{owner.account_number}"
          note_row += 1

          if owner.address1.present? || owner.address2.present?
            sheet[note_row][18].change_contents "Address"
            sheet[note_row][20].change_contents owner.full_address
            note_row += 1
          end

          if owner.swift_code.present?
            sheet[note_row][18].change_contents "Swift Code"
            sheet[note_row][20].change_contents owner.swift_code
            note_row += 1
          end

          if owner.account_name.present?
            sheet[note_row][18].change_contents "Account"
            sheet[note_row][20].change_contents owner.account_name
          end
        end
      end


    end
  end
end
