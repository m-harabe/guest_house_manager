module Pdf
  class SettlementService < ApplicationService

    def initialize(settlement)
      @report = Thinreports::Report.new(layout: "#{Rails.root}/app/assets/formats/settlement.tlf")
      @settlement = settlement
      @template = settlement.settlement_template
    end

    def generate
      settlement.set_analysis_info
      setup
      report.generate
    end

    private

    attr_reader :settlement, :template, :report

    #--------------------------------------------------------------------------------
    # PDF 生成
    #--------------------------------------------------------------------------------
    def setup

      # helper = ApplicationController.helpers
      helper = Summary::SettlementController.helpers

      report.start_new_page

      # タイトル
      report.page.item(:title).value settlement.title

      #--------------------
      # オーナー情報
      #--------------------
      # オーナー名
      report.page.item(:owner_name).value settlement.owner_name + '　' + template.honorific
      # 施設名称
      report.page.item(:building).value template.building + ' ' + settlement.building
      # 施設住所
      report.page.item(:address).value template.address + ' ' + settlement.address

      #--------------------
      # 自社情報
      #--------------------
      report.page.item(:company_name).value template.company_name
      report.page.item(:company_info1).value template.company_info1
      report.page.item(:company_info2).value template.company_info2
      report.page.item(:company_info3).value template.company_info3

      #--------------------
      # 宿泊情報一覧
      #--------------------
      # Header
      report.list(:reservations).header.item(:h1).value template.visitor_name
      report.list(:reservations).header.item(:h2).value template.nationality
      report.list(:reservations).header.item(:h3).value template.number_of_nights
      report.list(:reservations).header.item(:h4).value template.total_count
      report.list(:reservations).header.item(:h5).value template.adult_count
      report.list(:reservations).header.item(:h6).value template.child_count
      report.list(:reservations).header.item(:h7).value template.room_charge
      report.list(:reservations).header.item(:h8).value template.cleaning_fee
      report.list(:reservations).header.item(:h9).value template.total
      report.list(:reservations).header.item(:h10).value template.site
      report.list(:reservations).header.item(:h11).value template.handling_charge
      report.list(:reservations).header.item(:h12).value template.reservation_date

      # Body
      sum_list = Hash.new(0)

      settlement.reservation_list.each do |obj|
        report.list(:reservations).add_row do |row|

          row.item(:v0).value obj.date
          row.item(:v1).value obj.visitor_name + '様' if obj.visitor_name
          row.item(:v2).value obj.nationality_name
          row.item(:v3).value obj.number_of_nights
          row.item(:v4).value obj.total_count if obj.total_count.to_i > 0
          row.item(:v5).value obj.adult_count if obj.adult_count.to_i > 0
          row.item(:v6).value obj.child_count if obj.child_count.to_i > 0
          row.item(:v7).value obj.room_charge.to_s(:delimited) if obj.room_charge.to_i > 0
          row.item(:v8).value obj.cleaning_fee.to_s(:delimited) if obj.cleaning_fee.to_i > 0
          row.item(:v9).value obj.total.to_s(:delimited) if obj.total.to_i > 0
          row.item(:v10).value obj.site
          row.item(:v11).value obj.handling_charge.floor.to_s(:delimited) if obj.handling_charge.to_i > 0
          row.item(:v12).value obj.reserve_date.to_s(:slash) if obj.reserve_date

          # 合計計算
          sum_list[:number_of_nights] += obj.number_of_nights.to_i
          sum_list[:total_count]      += obj.total_count.to_i
          sum_list[:adult_count]      += obj.adult_count.to_i
          sum_list[:child_count]      += obj.child_count.to_i
          sum_list[:room_charge]      += obj.room_charge.to_i
          sum_list[:cleaning_fee]     += obj.cleaning_fee.to_i
          sum_list[:total]            += obj.total.to_i
          sum_list[:handling_charge]  += obj.handling_charge.to_i
        end

      end
      # 一覧最下部の合計行の出力
      if settlement.reservation_list.present?
        report.list(:reservations).add_row do |row|
          row.item(:v3).value sum_list[:number_of_nights]
          row.item(:v4).value sum_list[:total_count]
          row.item(:v5).value sum_list[:adult_count]
          row.item(:v6).value sum_list[:child_count]
          row.item(:v7).value sum_list[:room_charge].to_s(:delimited)
          row.item(:v8).value sum_list[:cleaning_fee].to_s(:delimited)
          row.item(:v9).value sum_list[:total].to_s(:delimited)
          row.item(:v11).value sum_list[:handling_charge].to_s(:delimited)
        end
      else
        # 宿泊情報が無い場合は、空の一行を出力
        report.list(:reservations).add_row do |row|
          row.item(:v3).value ""
          row.item(:v4).value ""
          row.item(:v5).value ""
          row.item(:v6).value ""
          row.item(:v7).value ""
          row.item(:v8).value ""
          row.item(:v9).value ""
          row.item(:v11).value ""
        end
        # 宿泊情報なしメッセージを表示
        report.page.item(:no_list_msg).value '※宿泊情報はありません。'
      end

      # ページ右側タイトル
      report.page.item(:analysis_title).value helper.convert_year_month_format_title(@settlement.settlement_template.analysis_title, @settlement.year_month)

      #--------------------
      # 画面右の左側テーブル１
      #--------------------
      report.list(:left_1).add_row do |row|
        row.item(:key).value settlement.label_sales
        row.item(:val).value settlement.analysis1.total.to_s(:delimited) + '円'
      end
      report.list(:left_1).add_row do |row|
        row.item(:key).value settlement.label_target_days
        row.item(:val).value settlement.analysis1.target_days
      end
      report.list(:left_1).add_row do |row|
        row.item(:key).value settlement.label_working_days
        row.item(:val).value settlement.analysis1.working_days
      end
      report.list(:left_1).add_row do |row|
        row.item(:key).value settlement.label_guest_count
        row.item(:val).value settlement.analysis1.guest_count
      end
      report.list(:left_1).add_row do |row|
        row.item(:key).value settlement.label_occ
        row.item(:val).value settlement.analysis1.occ
      end
      report.list(:left_1).add_row do |row|
        row.item(:key).value settlement.label_adr
        row.item(:val).value settlement.analysis1.adr
      end
      report.list(:left_1).add_row do |row|
        row.item(:key).value settlement.label_rev_par
        row.item(:val).value "#{settlement.analysis1.rev_par || 0}円"
      end

      #--------------------
      # 画面右の左側テーブル２
      #--------------------
      report.list(:left_2).add_row do |row|
        row.item(:key).value settlement.label_owner_sales
        row.item(:val).value settlement.analysis1.owner_total.to_s(:delimited) + '円'
      end
      report.list(:left_2).add_row do |row|
        row.item(:key).value settlement.label_other_income_total
        row.item(:val).value settlement.total_income.to_s(:delimited) + '円'
      end
      report.list(:left_2).add_row do |row|
        row.item(:key).value settlement.label_other_cost_total
        row.item(:val).value settlement.total_fee.to_s(:delimited) + '円'
      end

      #--------------------
      # 画面右の左側テーブル３
      #--------------------
      report.list(:left_3).add_row do |row|
        row.item(:key).value settlement.label_reward_rate
        row.item(:val).value "#{settlement.reward_rate}%"
      end
      report.list(:left_3).add_row do |row|
        row.item(:key).value settlement.label_payment_amount
        row.item(:val).value settlement.analysis1.owner_reward
      end


      #--------------------
      # 画面右の左側テーブル４
      #--------------------
      # 次月予約状況
      report.page.item(:next_month_title).value helper.convert_year_month_format_title(@settlement.settlement_template.next_month_title, @settlement.year_month, true)

      report.list(:left_4).add_row do |row|
        row.item(:key).value settlement.label_sales
        row.item(:val).value settlement.analysis2.total.to_s(:delimited) + '円' if settlement.analysis2.total.to_i > 0
      end
      report.list(:left_4).add_row do |row|
        row.item(:key).value settlement.label_target_days
        row.item(:val).value settlement.analysis2.target_days
      end
      report.list(:left_4).add_row do |row|
        row.item(:key).value settlement.label_working_days
        row.item(:val).value settlement.analysis2.working_days
      end
      report.list(:left_4).add_row do |row|
        row.item(:key).value settlement.label_guest_count
        row.item(:val).value settlement.analysis2.guest_count
      end
      report.list(:left_4).add_row do |row|
        row.item(:key).value settlement.label_occ
        row.item(:val).value settlement.analysis2.occ
      end
      report.list(:left_4).add_row do |row|
        row.item(:key).value settlement.label_adr
        row.item(:val).value settlement.analysis2.adr
      end
      report.list(:left_4).add_row do |row|
        row.item(:key).value settlement.label_rev_par
        row.item(:val).value settlement.analysis2.rev_par + '円' if settlement.analysis2.rev_par
      end

      #--------------------
      # その他収入テーブル
      #--------------------
      # タイトルラベル
      report.page.item(:label_other_income).value settlement.label_other_income
      # インターネット代
      report.list(:income_table).add_row do |row|
        row.item(:key).value settlement.label_internet_income
        row.item(:val).value settlement.internet_income.to_s(:delimited) + '円' if settlement.internet_income
      end
      # フリー項目
      if settlement.label_free_income1.present? || settlement.free_income1
        report.list(:income_table).add_row do |row|
          row.item(:key).value settlement.label_free_income1
          row.item(:val).value settlement.free_income1.to_s(:delimited) + '円' if settlement.free_income1
        end
      end
      if settlement.label_free_income2.present? || settlement.free_income2
        report.list(:income_table).add_row do |row|
          row.item(:key).value settlement.label_free_income2
          row.item(:val).value settlement.free_income2.to_s(:delimited) + '円' if settlement.free_income2
        end
      end
      if settlement.label_free_income3.present? || settlement.free_income3
        report.list(:income_table).add_row do |row|
          row.item(:key).value settlement.label_free_income3
          row.item(:val).value settlement.free_income3.to_s(:delimited) + '円' if settlement.free_income3
        end
      end
      if settlement.label_free_income4.present? || settlement.free_income4
        report.list(:income_table).add_row do |row|
          row.item(:key).value settlement.label_free_income4
          row.item(:val).value settlement.free_income4.to_s(:delimited) + '円' if settlement.free_income4
        end
      end
      if settlement.label_free_income5.present? || settlement.free_income5
        report.list(:income_table).add_row do |row|
          row.item(:key).value settlement.label_free_income5
          row.item(:val).value settlement.free_income5.to_s(:delimited) + '円' if settlement.free_income5
        end
      end
      # 合計
      report.list(:income_table).add_row do |row|
        row.item(:key).value settlement.label_total_income
        row.item(:val).value settlement.total_income.to_s(:delimited) + '円'
      end

      #--------------------
      # その他費用テーブル
      #--------------------
      # タイトルラベル
      report.page.item(:label_other_cost).value settlement.label_other_cost

      # 備考の出力位置決め行数カウント
      row_count = 0
      # サイト手数料
      report.list(:cost_table).add_row do |row|
        row.item(:key).value settlement.label_site_fee
        row.item(:val).value settlement.site_fee.to_s(:delimited) + '円' if settlement.site_fee
        row_count += 1
      end
      # 電気代
      report.list(:cost_table).add_row do |row|
        row.item(:key).value settlement.label_electrical_fee
        row.item(:val).value settlement.electrical_fee.to_s(:delimited) + '円' if settlement.electrical_fee
        row_count += 1
      end
      # ガス代
      report.list(:cost_table).add_row do |row|
        row.item(:key).value settlement.label_gas_fee
        row.item(:val).value settlement.gas_fee.to_s(:delimited) + '円' if settlement.gas_fee
        row_count += 1
      end
      # 水道代
      report.list(:cost_table).add_row do |row|
        row.item(:key).value settlement.label_water_fee
        row.item(:val).value settlement.water_fee.to_s(:delimited) + '円' if settlement.water_fee
        row_count += 1
      end
      # インターネット費用
      report.list(:cost_table).add_row do |row|
        row.item(:key).value settlement.label_internet_fee
        row.item(:val).value settlement.internet_fee.to_s(:delimited) + '円' if settlement.internet_fee
        row_count += 1
      end
      # サイトコントローラ
      report.list(:cost_table).add_row do |row|
        row.item(:key).value settlement.label_site_controller_fee
        row.item(:val).value settlement.site_controller_fee.to_s(:delimited) + '円' if settlement.site_controller_fee
        row_count += 1
      end
      # システム自動追加項目
      if settlement.label_system_fee1.present? || settlement.system_fee1
        report.list(:cost_table).add_row do |row|
          row.item(:key).value settlement.label_system_fee1
          row.item(:val).value settlement.system_fee1.to_s(:delimited) + '円' if settlement.system_fee1
        end
        row_count += 1
      end
      # フリー項目
      if settlement.label_free_fee1.present? || settlement.free_fee1
        report.list(:cost_table).add_row do |row|
          row.item(:key).value settlement.label_free_fee1
          row.item(:val).value settlement.free_fee1.to_s(:delimited) + '円' if settlement.free_fee1
        end
        row_count += 1
      end
      if settlement.label_free_fee2.present? || settlement.free_fee2
        report.list(:cost_table).add_row do |row|
          row.item(:key).value settlement.label_free_fee2
          row.item(:val).value settlement.free_fee2.to_s(:delimited) + '円' if settlement.free_fee2
        end
        row_count += 1
      end
      if settlement.label_free_fee3.present? || settlement.free_fee3
        report.list(:cost_table).add_row do |row|
          row.item(:key).value settlement.label_free_fee3
          row.item(:val).value settlement.free_fee3.to_s(:delimited) + '円' if settlement.free_fee3
        end
        row_count += 1
      end
      if settlement.label_free_fee4.present? || settlement.free_fee4
        report.list(:cost_table).add_row do |row|
          row.item(:key).value settlement.label_free_fee4
          row.item(:val).value settlement.free_fee4.to_s(:delimited) + '円' if settlement.free_fee4
        end
        row_count += 1
      end
      if settlement.label_free_fee5.present? || settlement.free_fee5
        report.list(:cost_table).add_row do |row|
          row.item(:key).value settlement.label_free_fee5
          row.item(:val).value settlement.free_fee5.to_s(:delimited) + '円' if settlement.free_fee5
        end
        row_count += 1
      end
      # 合計
      report.list(:cost_table).add_row do |row|
        row.item(:key).value settlement.label_total_fee
        row.item(:val).value settlement.total_fee.to_s(:delimited) + '円'
        row_count += 1
      end

      #--------------------
      # 備考
      #--------------------
      table_name = "note_table" + (row_count < 10 ? '1' : '2')
      if settlement.note1.present?
        report.list(table_name).add_row do |row|
          row.item(:note).value settlement.note1
        end
      end
      if settlement.note2.present?
        report.list(table_name).add_row do |row|
          row.item(:note).value settlement.note2
        end
      end
      if settlement.note3.present?
        report.list(table_name).add_row do |row|
          row.item(:note).value settlement.note3
        end
      end
      if settlement.note4.present?
        report.list(table_name).add_row do |row|
          row.item(:note).value settlement.note4
        end
      end

      #--------------------
      # 銀行
      #--------------------
      owner = settlement.room.building.owner
      if settlement.jp?
        if owner.bank_label_jp.present?
          report.list(:bank_table).add_row do |row|
            row.item(:bank).value '振込先'
          end
          report.list(:bank_table).add_row do |row|
            row.item(:bank).value owner.bank_label_jp
          end
          report.list(:bank_table).add_row do |row|
            row.item(:bank).value owner.account_name
          end
        end
      else
        if owner.bank_name.present? || owner.account_number.present? || owner.account_name.present?
          report.list(:bank_table).add_row do |row|
            row.item(:bank).value "Bank Name #{owner.bank_name} #{owner.account_number}"
          end
          if owner.address1.present? || owner.address2.present?
            report.list(:bank_table).add_row do |row|
              row.item(:bank).value "Address #{owner.full_address}"
            end
          end
          if owner.swift_code.present?
            report.list(:bank_table).add_row do |row|
              row.item(:bank).value "Swift Code #{owner.swift_code}"
            end
          end
          if owner.account_name.present?
            report.list(:bank_table).add_row do |row|
              row.item(:bank).value "Account Name #{owner.account_name}"
            end
          end
        end

      end
    end
  end
end
