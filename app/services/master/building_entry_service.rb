class Master::BuildingEntryService < ApplicationService
  # 登録・更新処理
  def self.create_update(attributes:)
    ActiveRecord::Base::transaction do
      Building.update_building(attr: attributes)
    end
  # TODO
  # rescue => ex
  #   output_error_log(ex, '施設登録・更新')
  #   raise ex
  end
end