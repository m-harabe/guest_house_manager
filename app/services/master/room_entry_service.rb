class Master::RoomEntryService < ApplicationService
  # 登録・更新処理
  def self.create_update(attributes:)
    ActiveRecord::Base::transaction do
      Room.update_room(attr: attributes)
    end
  # TODO
  # rescue => ex
  #   output_error_log(ex, '部屋登録・更新')
  #   raise ex
  end
end