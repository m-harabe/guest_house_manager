module Presenter
  module Work
    class WeeklyScheduledPresenter < ApplicationForm

      # defaultをnewしたときの日付で作成 `https://qiita.com/yusuke-matsuda/items/683a0518ff7fec0074ed`
      # attribute :date, String, default: lambda { |daily_task, attribute| Time.zone.today.strftime('%Y/%m/%d') }
      # attribute :date,             Date,    default: -> page, attribute { I18n.l(Time.zone.today, format: :middle_h) }
      # attribute :date,             Date,    default: -> page, attribute { Time.zone.today }
      attribute :date_start,       Date
      attribute :room_id,          Integer
      attribute :term,             Integer
      attribute :out_reservations, Array
      attribute :in_reservations,  Array
      attribute :room_status,      String
      attribute :daily_task_list,  Array

      def initialize(**args)
        super
        # set_room_status 現在の部屋自体のステータスを取得（一旦保留中）
        set_daily_task_list
      end

      # １週間のIN/OUTの予定を作成
      # 日付ごとにIN/OUTそれぞれを取得し、カレンダーに始点あり矢印を引くイメージ
      # 2018-12-03 | 2018-12-04
      # IN ●------------▶ OUT
      def set_daily_task_list
        date_to_out, date_to_in = {}, {}
        @out_reservations.map {|r| date_to_out[r.check_out_date] = r }
        @in_reservations.map {|r| date_to_in[r.check_in_date] = r }

        # 表示を開始する日付（本日）より過去にチェックインしている場合はステータスを取得する
        prev_reservation = nil
        if @in_reservations.present? && @in_reservations.first.check_in_date < @date_start
          prev_reservation = @in_reservations.first
        end

        (@date_start..(@date_start + term)).each do |d|
          res_out = date_to_out[d]
          res_in = date_to_in[d]
          dtp = DailyTaskPresenter.new(target_date: d, res_out: res_out, res_in: res_in)
          dtp.set_display_type(prev_reservation)
          @daily_task_list << dtp
          prev_reservation = res_in if res_in
        end
      end

      private

      # 部屋の現在の清掃状況をチェック
      # def set_room_status
      #   if @out_reservations.blank? && @in_reservations.blank?
      #       # 予約なし（空室）
      #       # TODO この状態での部屋のステータスがわからない
      #       @room_status = nil
      #       return
      #   end

      #   if @out_reservations.present?
      #     if @in_reservations.blank?
      #       # INなしOUTのみでステータスがSTAY
      #       if @out_reservations.first.status == Settings.RESERVATION.STATUS.STAYING
      #         @room_status = @out_reservations.first.status
      #         return
      #       end
      #       # OUT済で次のIN予約なし（空室）
      #       # TODO この状態での部屋のステータスがわからない
      #       @room_status = nil
      #       return
      #     else
      #       # OUT/IN両方ある
      #       if @out_reservations.first.check_out_date <= @in_reservations.first.check_in_date
      #         if @out_reservations.first.status == Settings.RESERVATION.STATUS.STAYING
      #           # INよりOUTが早い、かつSTAY中
      #           @room_status = @out_reservations.first.status
      #           return
      #         end
      #       end

      #     end
      #   end
      #   # 上記のすべての条件に該当しない場合は（直近の）次の予約のステータスを保持
      #   @room_status = @in_reservations.first.status
      # end
    end # Class WeeklySucheduledPresenter

    class DailyTaskPresenter < ApplicationForm
      # セルの表示タイプ（矢印）
      DISPLAY_TYPE_BLANK  = 0  # なし
      DISPLAY_TYPE_OUT    = 1  # OUT
      DISPLAY_TYPE_IN     = 2  # IN
      DISPLAY_TYPE_STAY   = 3  # STAY
      DISPLAY_TYPE_OUT_IN = 4  # OUT/IN

      attribute :res_in,          Reservation
      attribute :res_out,         Reservation
      attribute :target_date,     Date    # 週表示の日付
      attribute :display_type,    Integer # セルの矢印表示タイプ

      attr_reader :is_stay_whole

      def initialize(**args)
        super
      end

      def set_display_type(prev_reservation)
        if @res_out.nil? && @res_in.nil?
          if prev_reservation && @target_date < prev_reservation.check_out_date
            # STAY中
            @display_type = DISPLAY_TYPE_STAY
            # STAY中の場合、直近IN時の予約情報から一棟貸しを判断
            @is_stay_whole = prev_reservation.whole?
          else
            @display_type = DISPLAY_TYPE_BLANK
          end
          return
        end

        if @res_out
          # OUTのみ / OUT-IN両方あり
          @display_type = @res_in.nil? ? DISPLAY_TYPE_OUT : DISPLAY_TYPE_OUT_IN
        elsif @res_in
          # INのみ
          @display_type = DISPLAY_TYPE_IN
        end
      end

      # ▼ 画面出力用

      # IN予約のCI時刻
      def in_ci_time
        @res_in&.scheduled_check_in_time ? I18n.l(@res_in.scheduled_check_in_time, format: :time) : '指定なし'
      end

      # IN予約のCO日
      def in_co_date
        "#{I18n.l(@res_in&.check_out_date, format: :min)} [#{@res_in.number_of_nights}泊]"
      end

      # OUT予約のCI日
      def out_ci_date
        "CI: #{I18n.l(@res_out.check_in_date, format: :min)} [#{@res_out.number_of_nights}泊]"
      end

      # OUT予約の人数
      def out_count
        "人数: #{@res_out.adult_count}, #{@res_out.child_count}"
      end

      # OUT予約のゲスト名
      def out_visitor
        "#{@res_out.display_visitor_name}様"
      end

      # 一棟貸判定
      # @param [Symbol] type in/out
      # @return [Boolean] true: 一棟貸 / false: 通常
      def whole?(type:)
        if @is_stay_whole == nil
          res = @res_in if type == :in
          res = @res_out if type == :out
          res&.whole?
        else
          @is_stay_whole
        end
      end

    end # Class DailyTaskPresenter

  end # module Work
end # module Presenter