class Presenter::Work::ReservationDatePresenter < ApplicationForm

  CELL_MODE_NONE          = 0 # 予約なし
  CELL_MODE_CHECK_IN_DATE = 1 # 予約あり（チェックイン日）
  CELL_MODE_STAYING       = 2 # 予約あり（チェックイン日以外の結合セル）

  attribute :date,                    Date
  attribute :cell_mode,               Integer,  default: CELL_MODE_CHECK_IN_DATE
  attribute :html_class,              String
  attribute :reservation_id,          Integer
  attribute :status,                  String
  attribute :scheduled_check_in_time, String
  attribute :check_in_date,           String
  attribute :check_out_date,          String
  attribute :colspan_number,          Integer
  attribute :adult_count,             Integer
  attribute :child_count,             Integer
  attribute :is_whole,                Boolean,   default: false
  attribute :is_check_in_today,       Boolean,   default: false
  attribute :max_cols,                Integer

  def initialize(date:, search_date_to:, reservation: nil, cell_mode: nil)
    super()
    @date = date
    if reservation
      @max_cols = (search_date_to - date).to_i
      set_reservation_info(reservation)
    elsif cell_mode
      @cell_mode = cell_mode
    end
    html_class_list = []
    html_class_list << 'whole' if @is_whole
    @html_class = html_class_list.join(' ')
  end

  private

  def set_reservation_info(reservation)
    @reservation_id = reservation.id
    @status = reservation.status
    if reservation.number_of_nights > 1
      # 検査期間以前にチェックインした予約情報が含まれる場合は表示させるセルの日付からCI日を引いた日数を算出
      # 泊数からこの日数を減らしてセルの結合数を求める
      @colspan_number = reservation.number_of_nights - (@date - reservation.check_in_date).to_i
      # @colspan_number = @max_cols if @max_cols < @colspan_number
    end
    @adult_count = reservation.adult_count
    @child_count = reservation.child_count
    @is_whole = reservation.whole?
    @check_in_date = set_check_in_date_str(reservation)
    @scheduled_check_in_time = set_check_in_time_str(reservation)
    @check_out_date = set_check_out_date_str(reservation)
    @is_check_in_today = set_is_check_in_today(reservation)
  end

  def set_check_in_date_str(reservation)
    I18n.l(reservation.check_in_date, format: :min)
  end

  def set_check_in_time_str(reservation)
    return '指定なし' unless reservation&.scheduled_check_in_time
    I18n.l(reservation.scheduled_check_in_time, format: :time)
  end

  def set_check_out_date_str(reservation)
    return '' unless reservation&.check_out_date
    I18n.l(reservation.check_out_date, format: :min)
  end

  def set_is_check_in_today(reservation)
    return false unless reservation.check_in_date == Date.today
    status = Settings.RESERVATION.STATUS
    (reservation.status == status.PREPARING || reservation.status == status.POSSIBLE)
  end

end