class Presenter::Summary::ReservationDatePresenter < ApplicationForm

  attr_accessor :position, :date, :date_list, :reservation, :html_class

  def initialize(date:, html_class:, reservation: nil)
    super()
    @date = date
    @html_class = html_class
    @reservation = reservation
    set_date(date)
  end

  private

  def set_date(date)
    @date_list = []
    end_number = @reservation.nil? ? 0 : (@reservation.number_of_nights - 1)
    (0..end_number).each do |i|
      staying_date = date + i.days
      @date_list << I18n.l(staying_date, format: :min)
    end
  end

end