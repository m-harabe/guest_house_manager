module Presenter
  module Summary
    class ReservationSettlementPresenter < ApplicationForm
      attr_accessor :date
      # attribute :search_prev_next_month,  Date

      # 親（予約情報）
      attr_accessor :visitor_name, :nationality_name, :number_of_nights, :total_count, :adult_count, :child_count, :site, :reserve_date, :output_row_count
      # 子（日別情報）
      attr_accessor :room_charge, :cleaning_fee, :total, :handling_charge

      def initialize(date, index = nil, output_row_count = nil, reservation = nil, reservation_date = nil)
        super()
        @date = I18n.l(date, format: :min)
        if index
          if index == 1
            self.match_attributes = reservation.attributes
            @nationality_name = reservation.nationality_name
            @site = reservation.site_name
            @output_row_count = output_row_count
          else
            @adult_count = reservation.adult_count
            @child_count = reservation.child_count
          end
          # カラムではなくメソッドを呼ぶ場合は手動で呼び出し
          @total_count = reservation.total_count
          self.match_attributes = reservation_date.attributes
        end
      end

    end
  end
end