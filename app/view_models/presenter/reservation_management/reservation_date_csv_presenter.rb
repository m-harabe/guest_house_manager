class Presenter::ReservationManagement::ReservationDateCsvPresenter < ApplicationForm

  attr_accessor :date, :check_in_date, :scheduled_check_in_time, :room_name, :reservation_id, :status, :visitor_name, :number_of_nights,
                :nationality_name, :total_count, :adult_count, :child_count,
                :room_charge, :cleaning_fee, :meal_fee, :total, :site_name, :handling_charge,
                :reserve_date, :note,
                # :reservation,
  # attribute :date,                Date

  def initialize
    super()
    @date = date
    @room_name = room_name
  end

  def set_date(date:, room_name:)
    @date = date
    @room_name = room_name
  end

  # private

  # def set_row
  #   return if @reservation.nil? && @reserve_date.nil?
  #   set_reservation
  #   set_reservation_date
  # end

  def set_reservation(reservation_date:)

    reservation = reservation_date.reservation
    # 予約情報の初日のデータか、月またぎで月初め一日のデータであれば親予約情報をセット
    if reservation.check_in_date == @date || reservation_date.check_in_day.day == 1
      self.match_attributes = reservation.attributes
      @visitor_name << '様'
      @status = reservation.status_name
      @site_name = reservation.site_name
      @nationality_name = reservation.nationality_name
      @total_count = reservation.total_count
      @check_in_date = I18n.l(@check_in_date)
    end
    self.match_attributes = reservation_date.attributes
  end

  # def set_reservation_date(reservation_date:)
  #   # return unless @reservation_date
  #   self.match_attributes = reservation_date.attributes
  # end

  # def set_reservation_attributes
  #   self.match_attributes = @reservation
  #   @visitor_name << '様'
  #   @number_of_nights << '泊'
  #   @nationality_name = @reservation.nationality_name
  #   @total_count = @reservation.total_count
  # end
end