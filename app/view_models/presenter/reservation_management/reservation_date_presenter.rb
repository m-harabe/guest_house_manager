class Presenter::ReservationManagement::ReservationDatePresenter < ApplicationForm

  attr_accessor :position, :date, :date_list, :reservation,
                :html_class, :room_id, :building_id,
                :is_building_reservation

  def initialize(date:, html_class:, room: nil, check_in_day_list: nil, reservation: nil)
    super()
    # @position = position
    @date = date
    @html_class = html_class
    if room
      @room_id = room.id
      @building_id = room.building_id
    end
    @reservation = reservation
    set_is_building_reservation(date, check_in_day_list)
    set_date(date)
    # set_reservation_attributes
    # set_status
  end

  private
  # 一棟貸し対応可能かをチェック
  # 該当の部屋＆日付で予約がなく、かつ一棟貸不可日リストの日でなければtrueを設定
  # @param date              [Date]  一覧の表示日（行）
  # @param check_in_day_list [Array] 一棟貸不可日リスト
  # is_building_reservation -> true: 一棟貸OK, false: 一棟貸NG
  def set_is_building_reservation(date, check_in_day_list)
    @is_building_reservation = false
    if @reservation.nil? && !check_in_day_list.nil?
      @is_building_reservation = true unless check_in_day_list.include?(date.to_s)
    end
  end

  def set_date(date)
    @date_list = []
    end_number = @reservation.nil? ? 0 : (@reservation.number_of_nights - 1)
    (0..end_number).each do |i|
      staying_date = date + i.days
      @date_list << I18n.l(staying_date, format: :min)
    end
  end

  # def set_reservation_attributes
  #   self.match_attributes = @reservation
  #   @visitor_name << '様'
  #   @number_of_nights << '泊'
  #   @nationality_name = @reservation.nationality_name
  #   @total_count = @reservation.total_count
  # end
end