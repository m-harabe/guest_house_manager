class Presenter::ReservationManagement::ImportReservationPresenter < ApplicationForm
  # CSV_ATTRIBUTES = %i(
  #   reserve_date
  #   building_code
  #   room_no
  # )

  # Excelファイルの値を格納する変数
  # 以下の配列の定義順は取込ファイルの列順に記載すること
  # Excelファイル含まれない情報は直接attr_accessorで設定すること
  module Const
    CSV_ATTRIBUTES = [
      :check_in_date,     # 宿泊日
      :building_code,     # 施設コード
      :room_no,           # 部屋番号（表示順）
      :number_of_nights,  # 泊数
      :visitor_name,      # ゲスト名
      :nationality,       # 国籍
      :mail_address,      # メールアドレス
      :tel,               # TEL
      :adult_count,       # 人数（大人）
      :child_count,       # 人数（子供）
      :room_charge,       # 宿泊料,
      :cleaning_fee,      # 清掃費
      :meal_fee,          # 食事代
      :site_code,         # 予約サイトコード
      :site_name,         # 予約サイト名
      :reserve_date,      # 予約日
      :note,              # 備考
    ].freeze

    CSV_HEADERS = [
      "宿泊日*",
      "施設コード*",
      "部屋番号（表示順）",
      "泊数*",
      "ゲスト名*",
      "国籍*",
      "メールアドレス",
      "TEL",
      "人数（大人）*",
      "人数（子供）",
      "宿泊料*",
      "清掃費*",
      "食事代*",
      "予約サイトコード(*)",
      "予約サイト名",
      "予約日",
      "備考"
    ].freeze

    freeze
  end
  # include Const
  # ↑上記の記述のままincludeすると破壊的メソッドは呼べないが、再代入はできてしまう
  # includeせずにConstを介して利用することで完全に定数をフリーズ
  # NOTE: ここまでやる必要があるかは要検討

  attr_accessor *Const::CSV_ATTRIBUTES

  attr_accessor :excel_row,
                :wk_reservation_id,
                :reservation_id, # reservation_id, whole_reservation_id は登録したIDを保持
                :whole_reservation_id,
                :building_id,
                :room_id,
                :nationality_id,
                :total,
                :is_whole

  # validation
  validates_with ReservationManagement::ImportValidator

  def initialize(excel_row:, nationalities:)
    super()
    @excel_row = excel_row
    fetch_excel_row
    set_reservation_info(nationalities)
    self.valid? if self.errors.empty?
  end

private

  def fetch_excel_row
    begin
      Const::CSV_ATTRIBUTES.each_with_index do |column, idx|
        send(column.to_s + '=', @excel_row[idx])
      end
    rescue => ex
      output_error_log(ex, 'インポート fetch_excel_row')
      raise FileImport::FormatError
    end
  end

  def set_reservation_info(nationalities)
    # 予約日のデフォルトは取込日
    @reserve_date ||= Date.current
    @cleaning_fee ||= 0
    @meal_fee     ||= 0
    @child_count  ||= 0

    # 国名から国籍IDを取得
    @nationality_id = nationalities[@nationality].try(:id)

    # サイト名のみ指定されている場合はサイトコードを検索
    # サイトコードが指定されている場合はサイト名を無視してバリデーションにかける
    if @site_code.nil?
      site_key = I18n.t("settings.site").key(@site_name).to_s
      @site_code = Settings.SITE.NAME.to_hash.key(site_key).to_s
    end

    @is_whole = false
    # 施設コードのみ指定（部屋番号なし）で複数部屋がある場合は一棟貸し情報と判断する
    rooms = Room.with_building.where('buildings.code': @building_code)
    rooms = rooms.where(room_no: @room_no) if @room_no
    if rooms.size == 1
      @room_id = rooms.first.id
    elsif rooms.size > 1
      @building_id = rooms.first.building_id
      @is_whole = true
    end
    @total = @room_charge.to_i + @cleaning_fee.to_i + @meal_fee.to_i
  rescue => ex
    output_error_log(ex, 'インポート データセット')
    self.errors.add :base, '予期せぬエラーが発生しました。'
  end

end