module ReservationManagement::EntryHelper
  # 請求内訳編集ボタン
  def edit_reservation_date_link_tag(reservation_id, reservation_date_size)
    link_to(reservation_management_edit_reservation_date_path(reservation_id), 'data-remote': true) do
      options = {class: 'btn btn-success input-sm', type: "button"}
      options[:disabled] = true if reservation_date_size == 1
      content_tag(:button, '請求内訳の編集', options)
    end
  end

  # ステータス変更ボタン
  def forward_status_btn(id:, status:)
    target_status = get_target_status(current_status: status)
    return if target_status.blank?

    label = '「' + reservation_status_name(status: target_status) + '」にする'
    button_tag(type: 'button', class: 'btn btn-warning input-sm', onclick: "forwardStatus('#{@callback}');") do
      label
    end
  end

  # 一棟貸一括ステータス変更ボタン
  def bulk_forward_status_btn(id:, status:)
    target_status = get_target_status(current_status: status)
    return if target_status.blank?

    label = '一棟全部屋を「' + reservation_status_name(status: target_status) + '」にする'
    button_tag(type: 'button', class: 'btn btn-danger input-sm', onclick: "bulkForwardStatus('#{@callback}');") do
      label
    end
  end

  private

  def get_target_status(current_status:)
    target_status = nil
    setting = Settings.RESERVATION.STATUS
    case current_status
    when setting.PREPARING, setting.POSSIBLE, setting.STAYING
      target_status = (current_status.to_i + 1).to_s
    end
    target_status
  end
end
