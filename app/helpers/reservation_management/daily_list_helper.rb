module ReservationManagement::DailyListHelper
  def detail_button_tag(row)
    if row.reservation
      link_btn_tag(
        '詳細',
        reservation_management_show_path(row.reservation.id, @callback),
        button_class: 'btn-block btn-success btn-sm')
    else
      return "" unless role_member?
      link_btn_tag(
        '新規',
        reservation_management_entry_date_path(row.search_check_in_day.to_s, row.room.id, @callback),
        button_class: 'btn-block btn-default btn-sm')
    end
  end

  def etc_column_tag(row)
    tag = ''
    if row.reservation&.whole_reservation_id
      tag << content_tag(:div, content_tag(:span, '一棟貸', class: 'badge bg-purple') )
    end
    if row.note.present?
      tag << content_tag(:div, content_tag(:span, '備考あり', class: 'badge bg-red', 'data-toggle': 'tooltip', title: row.note) )
    end
  end
end
