module ReservationManagement::MonthlyListHelper
  def day_tr_tag(row)
    tag = ''
    td_option = {}

    # データなし用
    td1 = td2 = td3 = td4 = td5 = td6 = td7 = td8 = td9 = td92 = td10 = td11 = td12 = td13 = td14 = td15 = ''

    td1 = ''
    row.date_list.each do |str|
      td1 += content_tag(:p, str, class: 'date')
    end

    if row.reservation
      # TDの表示内容の設定
      status_tag = reservation_status_badge(status: row.reservation.status)
      td2 = content_tag(:div, status_tag, class: 'status')
      td2 += content_tag(:div, row.reservation.display_visitor_name + '様')
      td2 += content_tag(:div, '【' + row.reservation.try(:nationality_name).to_s + '】')
      td4 = content_tag(:div, row.reservation.number_of_nights.to_s + '泊')
      td4 += content_tag(:div, l(row.reservation.scheduled_check_in_time, format: :time), class: '') if row.reservation.scheduled_check_in_time

      # 人数
      td7 = content_tag(:p, "#{row.reservation.adult_count + row.reservation.child_count}人", class: 'total_count')
      td7 << content_tag(:p, "（#{row.reservation.adult_count} / #{row.reservation.child_count}）", class: 'sub_count')

      td8 = ''
      td9 = ''
      td10 = ''
      td12 = ''
      handling_charge_total = 0
      row.reservation.reservation_dates.each do |rd|
        td8 << content_tag(:p, rd.room_charge.to_s(:delimited), class: 'subtotal')
        td9 << content_tag(:p, rd.cleaning_fee.to_s(:delimited), class: 'subtotal')
        td92 << content_tag(:p, rd.meal_fee.to_s(:delimited), class: 'subtotal')
        td10 << content_tag(:p, rd.total.to_s(:delimited), class: 'subtotal')
        td12 << content_tag(:p, rd.handling_charge.to_i.to_s(:delimited), class: 'subtotal')
        handling_charge_total += rd.handling_charge if rd.handling_charge
      end
      td8 << content_tag(:p, row.reservation.room_charge.to_s(:delimited), class: 'total')
      td9 << content_tag(:p, row.reservation.cleaning_fee.to_s(:delimited), class: 'total')
      td92 << content_tag(:p, row.reservation.meal_fee.to_s(:delimited), class: 'total')
      td10 << content_tag(:p, row.reservation.total.to_s(:delimited), class: 'total')
      td11 = site_name(site_code: row.reservation.site_code)
      td12 << content_tag(:p, handling_charge_total.to_s(:delimited), class: 'total')
      td13 = ''
      if row.reservation.whole_reservation_id.present?
        td13 << content_tag(:div, content_tag(:span, '一棟貸', class: 'badge bg-purple') )
      end
      if row.reservation.note.present?
        td13 << content_tag(:div, content_tag(:span, '備考あり', class: 'badge bg-red', 'data-toggle': 'tooltip', title: row.reservation.note) )
      end
      td13 << content_tag(:div, I18n.l(row.reservation.reserve_date, format: :default_h))
      td14 = content_tag(:a, content_tag(:button, '詳細', class: 'btn btn-block btn-success btn-sm', type: 'button'), href: reservation_management_show_path(row.reservation.id, @callback))
    else
      if row.is_building_reservation
        td13 = content_tag(:a, content_tag(:button, '一棟貸登録', class: 'btn btn-block btn-default btn-sm', type: 'button'), href: reservation_management_whole_reservation_new_path(row.date.to_s, row.building_id))
      end
      td14 = content_tag(:a, content_tag(:button, '新規', class: 'btn btn-block btn-default btn-sm', type: 'button'), href: reservation_management_entry_date_path(row.date.to_s, row.room_id, @callback))
    end

    # TDタグの生成
    td_tags = content_tag(:td, td1.html_safe, class: 'date')
    td_tags << content_tag(:td, td2, td_option.merge(class: 'visitor_name'))
    # td_tags << content_tag(:td, td3, td_option.merge(class: 'nationality_name'))
    td_tags << content_tag(:td, td4, td_option.merge(class: 'number_of_nights'))
    # td_tags << content_tag(:td, td5, td_option.merge(class: 'adult_count'))
    # td_tags << content_tag(:td, td6, td_option.merge(class: 'child_count'))
    td_tags << content_tag(:td, td7, td_option.merge(class: 'total_count'))
    td_tags << content_tag(:td, td8.html_safe, td_option.merge(class: 'room_charge'))
    td_tags << content_tag(:td, td9.html_safe, td_option.merge(class: 'cleaning_fee'))
    td_tags << content_tag(:td, td92.html_safe, td_option.merge(class: 'meal_fee'))
    td_tags << content_tag(:td, td10.html_safe, td_option.merge(class: 'total'))
    td_tags << content_tag(:td, td11, td_option.merge(class: 'site_name'))
    td_tags << content_tag(:td, td12.html_safe, td_option.merge(class: 'handling_charge'))
    td_tags << content_tag(:td, td13.html_safe, td_option.merge(class: 'reserve_date'))
    td_tags << content_tag(:td, td14.html_safe, td_option.merge(class: 'button_area'))
    content_tag(:tr, td_tags, class: row.html_class)
  end

  # def day_tr_tag___bk_2(row)
  #   tag = ''
  #   td_option = {}
  #   # td1 = I18n.l(row.date, format: :min)
  #   td1 = td2 = td3 = td4 = td5 = td6 = td7 = td8 = td9 = td10 = td11 = td12 = td13 = td14 = ''
  #   row.date_list.each do |str|
  #     td1 += content_tag(:p, str, class: 'date')
  #   end

  #   if row.reservation
  #     # TDの表示内容の設定
  #     td2 = reservation_status_badge(row.reservation.status)
  #     td3 = row.reservation.visitor_name << '様'
  #     td3 << row.reservation.nationality_name
  #     td5 = row.reservation.number_of_nights.to_s << '泊'
  #     td6 = row.reservation.adult_count
  #     td7 = row.reservation.child_count
  #     td8 = row.reservation.adult_count + row.reservation.child_count
  #     td9 = row.reservation.room_charge.to_s(:delimited)
  #     td10 = row.reservation.cleaning_fee.to_s(:delimited)
  #     td11 = row.reservation.total.to_s(:delimited)
  #     td12 = site_name(site_code: row.reservation.site_code)
  #     td13 = row.reservation.handling_charge.to_s(:delimited)
  #     td14 = I18n.l(row.reservation.reserve_date, format: :default_h)
  #   end

  #   # TDタグの生成
  #   td_tags = content_tag(:td, td1.html_safe, class: 'date')
  #   td_tags << content_tag(:td, td2, td_option.merge(class: 'status'))
  #   td_tags << content_tag(:td, td3, td_option.merge(class: 'visitor_name'))
  #   td_tags << content_tag(:td, td4, td_option.merge(class: 'nationality_name'))
  #   td_tags << content_tag(:td, td5, td_option.merge(class: 'number_of_nights'))
  #   td_tags << content_tag(:td, td6, td_option.merge(class: 'adult_count'))
  #   td_tags << content_tag(:td, td7, td_option.merge(class: 'child_count'))
  #   td_tags << content_tag(:td, td8, td_option.merge(class: 'total_count'))
  #   td_tags << content_tag(:td, td9, td_option.merge(class: 'room_charge'))
  #   td_tags << content_tag(:td, td10, td_option.merge(class: 'cleaning_fee'))
  #   td_tags << content_tag(:td, td11, td_option.merge(class: 'total'))
  #   td_tags << content_tag(:td, td12, td_option.merge(class: 'site_name'))
  #   td_tags << content_tag(:td, td13, td_option.merge(class: 'handling_charge'))
  #   td_tags << content_tag(:td, td14, td_option.merge(class: 'reserve_date'))
  #   content_tag(:tr, td_tags, class: row.html_class)
  # end

  # def day_tr_tag_bk____(row)
  #   tag = ''
  #   td_option = {}
  #   td1 = I18n.l(row.date, format: :min)
  #   td2 = td3 = td4 = td5 = td6 = td7 = td8 = td9 = td10 = td11 = td12 = td13 = ''

  #   # TDの行結合の設定
  #   td_option[:rowspan] = row.reservation.number_of_nights if row.start?

  #   # TDの表示内容の設定
  #   if row.single? || row.start?
  #     td2 = row.reservation.visitor_name << '様'
  #     td3 = row.reservation.nationality_name
  #     td4 = row.reservation.number_of_nights.to_s << '泊'
  #     td5 = row.reservation.adult_count
  #     td6 = row.reservation.child_count
  #     td7 = row.reservation.adult_count + row.reservation.child_count
  #     td8 = row.reservation.room_charge.to_s(:delimited)
  #     td9 = row.reservation.cleaning_fee.to_s(:delimited)
  #     td10 = row.reservation.total.to_s(:delimited)
  #     td11 = site_name(site_code: row.reservation.site_code)
  #     td12 = row.reservation.handling_charge.to_s(:delimited)
  #     td13 = I18n.l(row.reservation.reserve_date, format: :default_h)
  #   end

  #   # TDタグの生成
  #   # 1列目（日付）は必ず出力
  #   td_tags = content_tag(:td, td1, class: 'date')

  #   # 2列名以降は宿泊中（2日目以降）の結合される行以外であればTDを出力
  #   unless row.staying?
  #     td_tags << content_tag(:td, td2, td_option)
  #     td_tags << content_tag(:td, td3, td_option.merge(class: 'nationality_name'))
  #     td_tags << content_tag(:td, td4, td_option.merge(class: 'number_of_nights'))
  #     td_tags << content_tag(:td, td5, td_option.merge(class: 'adult_count'))
  #     td_tags << content_tag(:td, td6, td_option.merge(class: 'child_count'))
  #     td_tags << content_tag(:td, td7, td_option.merge(class: 'total_count'))
  #     td_tags << content_tag(:td, td8, td_option.merge(class: 'room_charge'))
  #     td_tags << content_tag(:td, td9, td_option.merge(class: 'cleaning_fee'))
  #     td_tags << content_tag(:td, td10, td_option.merge(class: 'total'))
  #     td_tags << content_tag(:td, td11, td_option.merge(class: 'site_name'))
  #     td_tags << content_tag(:td, td12, td_option.merge(class: 'handling_charge'))
  #     td_tags << content_tag(:td, td13, td_option.merge(class: 'reserve_date'))
  #   end
  #   content_tag(:tr, td_tags, class: row.html_class)
  # end

end
