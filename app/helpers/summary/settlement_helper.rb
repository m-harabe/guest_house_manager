module Summary::SettlementHelper
  # 年月表示プレースホルダー置換
  def convert_year_month_format_title(format, year_month, is_next = false)
    fromat_str = format.gsub('${Y}', '%Y')
    fromat_str = fromat_str.gsub('${M}', '%m')
    fromat_str = fromat_str.gsub('${m}', '%-m')

    # 年月6桁に01日を足して日付にしてフォーマット
    if is_next
      "#{year_month}01".to_date.next_month.strftime(fromat_str)
    else
      "#{year_month}01".to_date.strftime(fromat_str)
    end
  end

  # エクセル用シート名
  def sheet_name(year_month, building_name)

    # 年月6桁に01日を足して日付にしてフォーマット
    "#{year_month}01".to_date.strftime("%Y年%m月_#{building_name}")

  end

end
