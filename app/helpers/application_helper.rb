module ApplicationHelper

  # フルアクセス権限を持つロールのリスト
  ALL_AUTH_ROLES = [
    Settings.USER.ROLE_SYSTEM,  # システム管理者
    Settings.USER.ROLE_MANAGER, # 管理者
  ].freeze

  # ページごとの完全なタイトルを返す
  def full_title(page_title)
    base_title = Settings.SITE_TITLE
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  # メニューの活性化判定 / sidebar.html.erb の class の値を出力
  # @params[Array(String)] *names コントローラ名
  def sidebar_class(*names)
    names.index(controller_name) ? 'active' : ''
  end

  # メニューの活性化判定 コントローラ名＋アクション名
  def sidebar_action_class(*names)
    names.index(controller_name + '/' + action_name) ? 'active' : ''
  end

  def form_label(modle_name, column_name)
    t "activerecord.attributes.#{modle_name}.#{column_name}"
  end

  #-----------------------------------------------------------------------------
  # note_class に該当する学年を取得し、バッジで装飾したタグを返す
  #
  #-----------------------------------------------------------------------------
  def note_class_tag(note_class)
    case note_class.to_i
    when 1
      badge_color = "bg-yellow"
    when 2
      badge_color = "bg-green"
    else
      badge_color = "bg-blue"
    end
    option = {class: "text-center badge #{badge_color}"}
    content_tag(:span, Settings.RELEASE_NOTES.NOTE_CLASS_LABEL[note_class], option)
  end

  # Bootstrap のアラートのHTMLタグを出力
  # content_tag:
  # - <p>content</p>といった開始と終了のあるタグを生成します
  # concat:
  # - HTML内にprintします
  def get_alert(flash)
    return if flash.blank?

    # redirect_to メソッドのオプションとしては、notice, alert のみ
    flash.each do |key, value|
      value = value.join('<br>') if value.instance_of?(Array)
      value = ('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + value.to_s).html_safe
      case key.to_s
      when 'success'
        # "callout callout-info lead" で文字が大きくなる
        # concat content_tag(:div, value, :class => "callout callout-info alert-dismissible")
        concat content_tag(:div, value, class: "alert alert-info alert-dismissible")
      when 'notice'
        concat content_tag(:div, value, class: "alert alert-warning alert-dismissible")
      when 'error', 'alert'
        concat content_tag(:div, value, class: "alert alert-danger alert-dismissible")
      end
    end
  end

  # データテーブルのオプションを生成する Ver.2019-11-30
  # @params [Hash] **attributes: 可変長 Hash でオプションを受け取る
  #
  # DataTables は 1.9 以前と 1.10 でいろいろ違っているので注意
  # 参考URL： http://qiita.com/nissuk/items/7ac59af5de427c0585c5
  # 参考URL： https://datatables.net/upgrade/1.10-convert
  # 参考URL： http://qiita.com/nissuk/items/7ac59af5de427c0585c5
  #-----------------------------------------------------------------------------
  def data_table_options(**attributes)
    # オプション値の設定
    paging       = attributes.has_key?(:paging)       ? attributes[:paging].to_s       : "true"
    lengthMenu   = attributes.has_key?(:lengthMenu)   ? attributes[:lengthMenu].to_s   : Settings.TABLE_OPTIONS.LENGTH_MENU
    lengthChange = attributes.has_key?(:lengthChange) ? attributes[:lengthChange].to_s : Settings.TABLE_OPTIONS.LENGTHCHANGE
    pageLengh    = attributes.has_key?(:pageLengh)    ? attributes[:pageLengh].to_s    : Settings.TABLE_OPTIONS.PAGE_LENGH
    searching    = attributes.has_key?(:searching)    ? attributes[:searching].to_s    : Settings.TABLE_OPTIONS.SEARCHING
    ordering     = attributes.has_key?(:ordering)     ? attributes[:ordering].to_s     : Settings.TABLE_OPTIONS.ORDERING
    info         = attributes.has_key?(:info)         ? attributes[:info].to_s         : Settings.TABLE_OPTIONS.INFO
    autoWidth    = attributes.has_key?(:autoWidth)    ? attributes[:autoWidth].to_s    : Settings.TABLE_OPTIONS.AUTOWIDTH
    # stateSave    = attributes.has_key?(:stateSave)    ? attributes[:stateSave].to_s    : Settings.TABLE_OPTIONS.STATESAVE
    not_sort     = attributes.has_key?(:not_sort)     ? attributes[:not_sort].to_s     : Settings.TABLE_OPTIONS.NOT_SORT
    scrollX      = attributes.has_key?(:scrollY)      ? attributes[:scrollY].to_s      : nil
    scrollY      = attributes.has_key?(:scrollX)      ? attributes[:scrollX].to_s      : nil
    responsive   = attributes.has_key?(:responsive)   ? attributes[:responsive].to_s   : 'false'
    order        = attributes.has_key?(:order)        ? attributes[:order].to_s        : nil

    # ビューのJavascript に渡す文字列を生成
    option_str = ""
    option_str << "paging: #{paging}" + ', '
    option_str << "lengthMenu: #{lengthMenu}" + ', '
    option_str << "lengthChange: #{lengthChange}" + ', '
    option_str << "pageLengh: #{pageLengh}" + ', '
    option_str << "searching: #{searching}" + ', '
    option_str << "ordering: #{ordering}" + ', '
    option_str << "info: #{info}" + ', '
    option_str << "autoWidth: #{autoWidth}" + ', '
    option_str += "responsive: #{responsive}" + ', '
    option_str += "scrollCollapse: false" + ', '
    option_str += "order: #{order}" + ', ' if order # N列目を昇順にする ( [ [ 列番号, 昇順降順 ], ... ] の形式) ex) order: [[1, "desc"]],

    # option_str << "stateSave: #{stateSave}" + ', '
    # columnDefs 参考URL： https://datatables.net/reference/option/columnDefs

    # scrollX: 横スクロールバーを有効にする (scrollXはtrueかfalseで有効無効を切り替えます)
    # scrollY: 縦方向の高さを制限してスクローラーを表示 単位: 35vh(viewport height:モニターの35%の高さ), またはピクセル
    # scrollY を指定する際は、scrollX: '100%' も指定しないと、ブラウザの横幅変更でヘッダーがついてこない
    option_str += "scrollX: '#{scrollX}'" + ', ' if scrollX
    option_str += "scrollY: '#{scrollY}'" + ', ' if scrollY

    option_str << "columnDefs: [{ targets: #{not_sort}, orderable: false }]"

    return option_str.html_safe
  end

  def site_select_options(is_allow_blank: false)
    options = is_allow_blank ? {'指定しない': nil} : {}
    Settings.SITE.NAME.each do |key, val|
      label = I18n.t("settings.site.#{val}")
      options[label] = key
    end
    options
  end

  def language_select_options
    options = {}
    Settings.SETTLEMENT_LANGUAGE.each do |key, val|
      label = I18n.t("settings.site.#{val}")
      options[label] = key
    end
    options
  end

  def site_name(site_code:)
    I18n.t "settings.site.#{Settings.SITE.NAME[site_code]}"
  end

  # 施設（部屋）セレクトボックス用オプション値生成
  # @return [Hash] {"1001: 東福寺南門"=>1, "1002...}
  def room_select_options(show_all_rooms = false)
    rooms = show_all_rooms ? Room.with_invisible_building : Room.visible.with_building
    rooms.inject({}){ |options, ar| options.merge(ar.full_code_name => ar.id) }
  end

  # 施設セレクトボックス用オプション値生成
  # @return [Hash] キャンセル以外かつ部屋複数もつ施設 {"1005: Hostel円町"=>5, "1004: 高倉葛籠屋町"=>7}
  def building_select_options
    Building.ransack(room_count_gt: 1).result.inject({}) { |options, b| options.merge("#{b.code}: #{b.name}" => b.id) }
  end

  # ステータスセレクトボックス用オプション値設定
  # @return [Hash] キャンセル以外のステータス {"未清掃"=>"1", "CI可"...}
  def status_select_options
    Reservation::STSATUS_NAMES.invert.reject! {|k, v| v == Settings.RESERVATION.STATUS.CANCEL}
  end

  def all_status_select_options
    Reservation::STSATUS_NAMES.invert
  end

  def registration_type_select_options
    Settings.RESERVATION.REGISTRATION_TYPE_LABEL.to_h.invert
  end

  def nationality_select_options
    Nationality.selecter.pluck(:name, :id)
  end

  # ステータスラベル
  def reservation_status_name(status:)
    I18n.t "settings.reservation.status.#{Settings.RESERVATION.STATUS_NAME[status]}"
  end

  # 予約ステータスバッジ
  def reservation_status_badge(status:, option: {})
    return unless status
    const = Settings.RESERVATION
    case status.to_s
    when const.STATUS.PREPARING
      badge_color = "bg-yellow"
    when const.STATUS.POSSIBLE
      badge_color = "bg-red"
    when const.STATUS.STAYING
      badge_color = "bg-aqua"
    when const.STATUS.CO
      badge_color = "bg-gray"
    when const.STATUS.CANCEL
      # badge_color = "bg-black"
      badge_color = "bg-purple"
    else
      return
    end
    option = option.merge({class: "badge #{badge_color}"})
    label = reservation_status_name(status: status)
    content_tag(:span, label, option)
  end

  # suitebook連携ステータスバッジ
  def suitebook_status_badge(status:, option: {})
    return if status.blank?
    case status
    when Settings.SUITEBOOK_RESERVATION_STATUS.UNPROCESSED
      badge_color = "bg-yellow"
    when Settings.SUITEBOOK_RESERVATION_STATUS.PROCESSED
      badge_color = "bg-blue"
    when Settings.SUITEBOOK_RESERVATION_STATUS.CANCEL
      badge_color = "bg-black"
    when Settings.SUITEBOOK_RESERVATION_STATUS.ERROR
      badge_color = "bg-red"
    else
      return
    end
    option = option.merge({class: "badge #{badge_color}"})
    label = Settings.SUITEBOOK_RESERVATION_STATUS_LABEL[status]
    content_tag(:span, label, option)
  end

  # suitebookステータスセレクトボックス用オプション値生成
  def suitebook_status_select_options
    options = {'指定しない': nil}
    Settings.SUITEBOOK_RESERVATION_STATUS_LABEL.each do |key, val|
      options[val] = key
    end
    options
  end

  #-----------------------------------------------------------------------------
  # リンクボタンタグ
  # onClick アクションで遷移する
  #-----------------------------------------------------------------------------
  def link_btn_tag(label, path, icon_class: nil, button_class: nil, is_disabled: false, tabindex: nil)
    css_class = 'btn '
    css_class << (button_class || 'btn-default')
    if icon_class
      label = content_tag(:i, nil, class: "glyphicon #{icon_class} icon-right-label") + label
    end
    onclick = is_disabled ? nil : "location.href = ('#{path}')"
    button_tag(type: 'button', class: css_class, onclick: onclick, disabled: is_disabled, tabindex: tabindex) do
      label.html_safe
    end
  end

  #-----------------------------------------------------------------------------
  # リンクボタンタグ
  # Aタグ（Method:GET） で遷移する
  #-----------------------------------------------------------------------------
  def remote_link_btn_tag(label:, path:, button_class: 'btn-default', input_class: 'input-sm', icon_class: nil, is_disabled: false)
    button_class = 'btn ' << button_class << ' ' << input_class
    link_to(path, 'data-remote': true) do
      if icon_class
        label = content_tag(:i, nil, class: "glyphicon #{icon_class} icon-right-label") + label
      end
      options = {class: button_class, type: "button"}
      options[:disabled] = true if is_disabled
      content_tag(:button, label, options)
    end
  end

  #-----------------------------------------------------------------------------
  # ボタンタグ
  #-----------------------------------------------------------------------------
  def bts_btn_tag(label, icon_class: nil, button_class: nil, is_disabled: false, tabindex: nil)
    css_class = 'btn '
    css_class << (button_class || 'btn-default')
    if icon_class
      label = content_tag(:i, nil, class: "glyphicon #{icon_class} icon-right-label") + label
    end
    button_tag(type: 'button', class: css_class, disabled: is_disabled, tabindex: tabindex) do
      label.html_safe
    end
  end

  # 一覧画面のアクション transition_searchへのコールバックパスの生成
  # どの画面から来たかで戻る一覧画面を制御する
  # @return [String, nil] transition_searchアクションのパス、なければnilを返す
  def callback_path
    # @callback: コントローラのbefore_action -> fetch_callbackメソッドでセットされる
    case @callback
    when 'monthly_list'
      reservation_management_monthly_list_transition_search_path
    when 'cleaning_status_list'
      work_cleaning_status_list_transition_search_path
    when 'daily_list'
      reservation_management_daily_list_transition_search_path
    when 'daily_tasks'
      work_daily_tasks_transition_search_path
    when 'suitebook'
      reservation_management_suitebook_transition_search_path
    when 'reservations_search'
      reservation_management_reservations_transition_search_path
    end
  end

  # 表示・非表示バッジ
  def is_visible_tag(is_visible)
    if is_visible
      badge_color = "bg-green"
      label = "表示"
    else
      badge_color = "bg-gray"
      label = "非表示"
    end
    option = {class: "text-center badge #{badge_color}"}
    content_tag(:span, label, option)
  end

  # simple_format拡張
  def simple_format_ex(str)
    return '' if str.blank?
    safe_join(str.split("\n"),tag(:br))
  end

  # -----------------------------------------
  # ▼ サイドバー項目の権限別表示切り替え判定メソッド
  # -----------------------------------------
  def is_display?(roles = [])
    (ALL_AUTH_ROLES + roles).include?(User.current_user.role)
  end

  #-----------------------------------------------------------------------------
  # 権限関係のメソッド
  #-----------------------------------------------------------------------------

  def role_manager?
    User.current_user.role <= Settings.USER.ROLE_MANAGER
    is_display?
  end

  def role_member?
    is_display?([
      Settings.USER.ROLE_MEMBER,    # 社員
    ])
  end

  # 他のヘルパーから呼ばれるメソッド群
  private

  def foward_status_name(status:)
    return unless Reservation.updatable_status?(status: status)
    reservation_status_name status: (status.to_i + 1).to_s
  end

end
