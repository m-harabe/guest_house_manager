module Master::BuildingsHelper
  def room_no_tags(building)
    tags = ""
    building.rooms.each do |room|
      tags << content_tag(:div, room.room_no)
    end
    concat(tags.html_safe)
  end

  def room_name_tags(building)
    tags = ""
    # return tags if building.room_count == 1
    building.rooms.each do |room|
      room_name = room.name
      room_name += content_tag(:span, '非表示', class: 'badge bg-gray') unless room.is_visible
      tags += content_tag(:div, raw(room_name))
    end
    concat(tags.html_safe)
  end
end
