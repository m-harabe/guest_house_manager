module Master::OwnersHelper
  # 稼働中・非稼働バッジ
  def is_visible_owner_tag(is_visible)
    if is_visible
      badge_color = "bg-green"
      label = "稼働中"
    else
      badge_color = "bg-gray"
      label = "非稼働"
    end
    option = {class: "text-center badge #{badge_color}"}
    content_tag(:span, label, option)
  end

  def mailer_link(mail_address)
    return unless mail_address.presence
    tag = content_tag(:i, nil, class: 'fa fa-fw fa-envelope-o')
    tag += " #{mail_address} にメールを送信"
    link_to tag, "mailto:#{mail_address}", target: '_blank'
  end
end
