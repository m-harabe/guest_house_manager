module Master::AreasHelper
  # 状態バッジ（設定済・未設定）
  def has_area_tag(has_area_flag)
    if has_area_flag
      badge_color = "bg-gray"
      label = "設定済"
    else
      badge_color = "bg-blue"
      label = "未設定"
    end
    option = {class: "text-center badge #{badge_color}"}
    content_tag(:span, label, option)
  end
end
