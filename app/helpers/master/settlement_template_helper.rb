module Master::SettlementTemplateHelper
  # 年月表示プレースホルダー置換
  def convert_year_month_format(format)
    display_str = format.gsub('${Y}', '%Y')
    display_str = display_str.gsub('${M}', '%m')
    display_str = display_str.gsub('${m}', '%-m')
    display_str
  end
end
