module Master::UsersHelper

  def role_select_options
    options = {}
    Settings.USER.ROLES.each do |key, val|
      next if key.to_s == Settings.USER.ROLE_SYSTEM.to_s
      options[val] = key
    end
    options
  end
end
