module Work::CleaningStatusListHelper
  def td_tag_by_presenter(cell:, td_class:)
    td_body = ''
    td_options = {class: cell.html_class + ' ' + td_class}
    case cell.cell_mode
    when Presenter::Work::ReservationDatePresenter::CELL_MODE_CHECK_IN_DATE
      td_body << display_reservation_tags(cell: cell, td_options: td_options)
    when Presenter::Work::ReservationDatePresenter::CELL_MODE_STAYING
      return ''
    end
    content_tag(:td, td_body.html_safe, td_options)
  end

  private

  def display_reservation_tags(cell:, td_options:)
    settings = Settings.RESERVATION.STATUS
    td_options.merge! colspan: cell.colspan_number if cell.colspan_number.to_i > 1
    html = ''
    row1 = reservation_status_badge(status: cell.status)
    row1 << " (#{cell.adult_count}, #{cell.child_count})"
    html << content_tag(:div, row1.html_safe, class: 'status-in-td')

    # CI日時を表示
    ci_tag = content_tag(:div, "CI:", class: 'date-title')
    ci_body_tag = content_tag(:span, cell.check_in_date)
    ci_body_tag << content_tag(:span, cell.scheduled_check_in_time, class: 'ci-time')
    ci_tag << content_tag(:div, ci_body_tag)
    ci_date_class = 'ci-date'
    ci_date_class << ' today' if cell.is_check_in_today
    html << content_tag(:div, ci_tag, class: ci_date_class)

    # CO日を表示
    co_tag = content_tag(:div, "CO:", class: 'date-title')
    co_tag << content_tag(:div, content_tag(:span, cell.check_out_date))
    html << content_tag(:div, co_tag, class: 'co-date')

    # CO日を強調
    # co_tag = content_tag(:span, "CO：#{cell.check_out_date}")
    # co_tag << content_tag(:span, '本日CO', class: 'label label-danger') if cell.is_check_out_today
    # html << content_tag(:div, co_tag, class: 'co-date')

    # ボタンラベル用に次のステータスを取得
    target_status = foward_status_name(status: cell.status)
    row4 = ''
    row4 << link_btn_tag(
              '詳細',
              reservation_management_show_path(cell.reservation_id, @callback),
              button_class: 'btn-xs btn-default input-sm',
              icon_class: 'glyphicon-info-sign')

    if target_status
      row4 << remote_link_btn_tag(
                label: target_status,
                path: work_cleaning_status_list_forward_status_path(cell.reservation_id, cell.status),
                button_class: 'btn-xs btn-default update-status',
                icon_class: 'glyphicon-arrow-right')
    end
    html << content_tag(:div, row4.html_safe, class: 'button-in-td')
  end
end
