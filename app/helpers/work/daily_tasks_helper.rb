module Work::DailyTasksHelper

  # def room_status_tag(row:)
  #   reservation_status_badge(status: row.room_status)
  # end

  def td_reservation_tag(row:, td_class:)
    tags = ''
    row.daily_task_list.each do |daily_task|
      out_options, in_options = build_options(daily_task: daily_task, td_class: td_class)
      tags += generate_out_td_cell(daily_task: daily_task, td_options: out_options)
      tags += generate_in_cell(daily_task: daily_task, td_options: in_options)
    end
    tags
  end

  def td_arrow_tag(row:, td_class:)
    tags = ''
    row.daily_task_list.each do |daily_task|
      out_options, in_options = build_options(daily_task: daily_task, td_class: td_class)
      tags += generate_arrow_td_tag(display_type: daily_task.display_type, out_options: out_options, in_options: in_options)
    end
    tags
  end

  private

  def build_options(daily_task:, td_class:)
    out_options, in_options = {}, {}
    out_options[:class] = daily_task.whole?(type: :out) ? "#{td_class} whole" : td_class
    in_options[:class] = daily_task.whole?(type: :in) ? "#{td_class} whole" : td_class
    return out_options, in_options
  end

  def generate_out_td_cell(daily_task:, td_options:)
    contents = ''
    case daily_task.display_type
    when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_OUT, Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_OUT_IN
      tooltip_title = daily_task.out_ci_date + '<br/>'
      tooltip_title += daily_task.out_count + '<br/>'
      tooltip_title += daily_task.out_visitor
      info_icon = ('OUT ' + content_tag(:i, nil, class: 'fa fa-fw fa-tag')).html_safe
      contents += content_tag(:div, info_icon, class: 'badge bg-red out-badge',
        'data-toggle': 'tooltip', title: tooltip_title, 'data-html': true)
    end
    content_tag(:td,
                content_tag(:div, contents.html_safe, class: 'cell-out'),
                td_options)
  end

  def generate_in_cell(daily_task:, td_options:)
    contents = ''
    case daily_task.display_type
    when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_IN, Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_OUT_IN
      res_in = daily_task.res_in
      # ステータス・人数
      contents = reservation_status_badge(status: res_in.status)
      contents += " (#{res_in.adult_count}, #{res_in.child_count})"

      # CI予定（時刻だけ）
      contents += "<div class='ci-co'><div class='title'>CI:</div><div>#{daily_task.in_ci_time}</div></div>".html_safe
      contents += "<div class='ci-co'><div class='title'>CO:</div><div>#{daily_task.in_co_date}</div></div>".html_safe

      button = link_btn_tag('詳細',
                 reservation_management_show_path(res_in.id, @callback),
                 button_class: 'btn-xs btn-default input-xs',
                 icon_class: 'glyphicon-info-sign')

      # ボタンラベル用に次のステータスを取得
      target_status = foward_status_name(status: res_in.status)
      # ステータス更新ボタン（CI可 または STAY中 に更新）
      if res_in.preparing? || res_in.possible?
        button += remote_link_btn_tag(
                    label: target_status,
                    path: work_daily_tasks_forward_status_path(res_in.id, res_in.status),
                    button_class: 'btn-xs btn-default update-status',
                    input_class: 'input-xs',
                    icon_class: 'glyphicon-arrow-right')
      end
      contents += content_tag(:div, button, class: 'button_area')
    end
    content_tag(:td,
                content_tag(:div, contents.html_safe, class: 'cell-in'),
                td_options)
  end

  def generate_arrow_td_tag(display_type:, out_options:, in_options:)
    case display_type
    when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_IN
      td_arrow_in out_options, in_options
    when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_STAY
      td_arrow_stay out_options, in_options
    when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_OUT
      td_arrow_out out_options, in_options
    when Presenter::Work::DailyTaskPresenter::DISPLAY_TYPE_OUT_IN
      td_arrow_out_in out_options, in_options
    else
      td_blank out_options
    end
  end

  def td_arrow_in(out_options, in_options)
    td_tags = content_tag(:td, nil, out_options)
    td_tags + content_tag(:td, content_tag(:div, nil, class: 'arrow-type2'), in_options)
  end

  def td_arrow_stay(out_options, in_options)
    td_tags = content_tag(:td, content_tag(:div, nil, class: 'arrow-type3'), out_options)
    td_tags + content_tag(:td, content_tag(:div, nil, class: 'arrow-type3'), in_options)
  end

  def td_arrow_out(out_options, in_options)
    td_tags = content_tag(:td, content_tag(:div, nil, class: 'arrow-type1'), out_options)
    td_tags + content_tag(:td, nil, in_options)
  end

  def td_arrow_out_in(out_options, in_options)
    td_tags = content_tag(:td, content_tag(:div, nil, class: 'arrow-type1'), out_options)
    td_tags + content_tag(:td, content_tag(:div, nil, class: 'arrow-type2'), in_options)
  end

  def td_blank(td_options)
    content_tag(:td, nil, td_options) + content_tag(:td, nil, td_options)
  end

end
