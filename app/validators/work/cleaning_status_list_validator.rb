class Work::CleaningStatusListValidator < ApplicationValidator

  def validate(record)
    @record = record
    # 検証
    validate_require
    validate_format if record.errors.empty?
    validate_term if record.errors.empty?
    validate_comparison_date if record.errors.empty?
  end

  # ApplicationValidator から呼ばれる
  def get_label(key)
    I18n.t("activemodel.attributes.work/cleaning_status_list_form.#{key}")
  end

  def validate_require
    valid_presence :search_date_from
    valid_presence :search_date_to
    add_error :common, :invalid_more_one_select, label: '施設' if @record.search_room_ids.blank?
  end

  def validate_format
    valid_fomat_data    :search_date_from
    valid_fomat_data    :search_date_to
  end

  def validate_term
    # 1日 ~ 31日 で 30 >> 期間 = 31
    if (@record.search_date_to - @record.search_date_from).to_i > 30
      add_error :common, :invalid_term, label: "期間", term: "３１日"
    end
  end

  def validate_comparison_date
    # 期間の大小チェック
    if @record.search_date_to < @record.search_date_from
      add_error :common, :comparison_input_date, label: "期間"
    end
  end
end