class Master::BuildingValidator < ApplicationValidator
  def validate(record)
    @record = record
    # 検証
    validate_require
    return if record.errors.present?
    validate_length
    validate_rate
    validate_format
    validate_duplication
  end

  private

  # ApplicationValidator から呼ばれる
  def get_label(key)
    I18n.t("activerecord.attributes.building.#{key}")
  end

  def validate_require
    valid_presence :code
    valid_presence :name
    valid_presence :reward_rate
  end

  def validate_length
    valid_length              :code, 10
    valid_length              :name, 20
  end

  def validate_rate
    valid_rate :reward_rate, 0, 100
  end

  def validate_format
    valid_format_code         :code
    valid_format_num_hyphen   :postal_code
    valid_length              :postal_code, 8
    valid_format_num          :room_count
  end

  # 名称・コードの重複チェック
  def validate_duplication
    building = Building.where(code: @record.code)
    building = building.where.not(id: @record.id) if @record.id.present?
    if building.size > 0
      label = get_label(:code) + ": #{@record.code} "
      add_error("common", "already_exists", {label: label})
    end
    return if record.errors.present?

    building = Building.where(name: @record.name)
    building = building.where.not(id: @record.id) if @record.id.present?
    if building.size > 0
      label = get_label(:name) + ": #{@record.name} "
      add_error("common", "already_exists", {label: label})
    end
  end
end