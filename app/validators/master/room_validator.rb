class Master::RoomValidator < ApplicationValidator
  def validate(record)
    @record = record
    # 検証
    validate_require
    return if record.errors.present?
    validate_length
    # validate_duplication
  end

  # ApplicationValidator から呼ばれる
  def get_label(key)
    I18n.t("activerecord.attributes.room.#{key}")
  end

  def validate_require
    # if @record.room_id.blank? || @record.building.room_count > 1
    #   valid_presence :name
    # end
    valid_presence :room_no
  end

  def validate_length
    valid_length              :room_no, 5
    valid_length              :name, 20
  end

  # 名称の重複チェック
  def validate_duplication
    rooms = Room.where(building_id: @record.building_id, name: @record.name)
    rooms = rooms.where.not(id: @record.room_id) if @record.room_id
    if rooms.present?
      add_error("common", "already_exists", {label: get_label(:name)})
    end
  end
end