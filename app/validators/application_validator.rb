# モデルバリデーションの基底クラス
# ActiveModel::Validator を継承したこのクラスを継承した先でモデル毎のバリデーションを実装
# モデルのvalidates_withメソッドでクラス名を指定する
class ApplicationValidator < ActiveModel::Validator
  include CommonValidator

  attr_accessor :record

  # 継承先でオーバーライド
  def get_label
    # サブクラスでメソッド定義されていないとエラー
    # TODO: エラークラス
    raise "InheritanceError"
  end

  # モデルの値を取得
  def get_value(key)
    # return eval("@record.#{key.to_s}")
    return @record.send(key.to_s).to_s
  end

  # エラーメッセージのセット
  # @param [name] message.ja.yml のネームスペース
  # @param [code] エラーコード
  # @param [attributes] エラーメッセージ内で展開される変数を格納
  def add_error(name, code, attributes=nil)
    @record.errors.add :base, I18n.t("errors.#{name}.#{code}", attributes)
  end

  # エラー判定とメッセージの格納

  # 必須チェック
  def valid_presence(key)
    add_error("common", "require", {label: get_label(key)} ) if get_value(key).blank?
  end

  # コードチェック
  def valid_format_code(key, underscore = false)
    val = get_value(key)
    return if val.blank?
    unless code?(val, underscore)
      code = underscore ? "format_code_underscore" : "format_code"
      add_error("common", code, {label: get_label(key)} )
    end
  end

  # 半角数字チェック
  def valid_format_num(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "format_num", {label: get_label(key)} ) unless num?(val)
  end

  # 半角数字および「-」 チェック
  def valid_format_num_hyphen(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "format_num_hyphen", {label: get_label(key)} ) unless num_hyphen?(val)
  end

  # 電話番号用 半角数字および「-」、「()」 チェック
  def valid_format_tel(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "format_num_hyphen", {label: get_label(key)} ) unless tel?(val)
  end

  # 全角カナチェック
  def valid_format_kana(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "fomat_kana", {label: get_label(key)} ) unless kana?(val)
  end

  # メール形式チェック
  def valid_format_mail(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "format_mail", {label: get_label(key)} ) unless mail?(val)
  end

  # 日付形式チェック
  def valid_fomat_data(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "format_date", {label: get_label(key)} ) unless date?(val)
  end

  # 時刻形式チェック
  # このメソッドでは、日付部分だけあっていれば、時刻の部分はなんでも通してしまうので注意
  def valid_fomat_date_time(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "format_date", {label: get_label(key)} ) unless date_time?(val)
  end

  # 時刻形式チェック
  def valid_fomat_time(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "format_date", {label: get_label(key)} ) unless time?(val)
  end

  def valid_exists(key)
    val = get_value(key)
    return if val.blank?
    add_error("common", "already_exists", {label: get_label(key)} ) unless unique?({key => val})
  end

  def valid_length(key, length)
    val = get_value(key)
    return if val.blank?
    add_error("common", "max_length", {label: get_label(key), length: length} ) if val.length.to_i > length
  end

  def valid_rate(key, from, to)
    val = get_value(key)
    return if val.blank?
    val = val.to_f
    add_error("common", "comparison_input_number", {label: get_label(key), to: to, from: from} ) if val < from || val > to
  end

end