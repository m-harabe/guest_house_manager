class ReservationManagement::ReservationValidator < ApplicationValidator
  include ReservationManagement::Concerns::CommonValidator

  def validate(record)
    @record = record
    # 検証
    validate_require
    validate_format if record.errors.empty?
    validate_site_code if record.errors.empty?
    validate_check_in_day if record.errors.empty?
  end

  def validate_require
    super
    valid_presence :room_id
    # valid_presence :nationality_id
  end

end