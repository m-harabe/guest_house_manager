class ReservationManagement::ImportValidator < ApplicationValidator
  include ReservationManagement::Concerns::CommonValidator

  def validate(record)
    @record = record
    # 検証
    validate_require
    validate_format if record.errors.empty?
    validate_check_in_day if record.errors.empty?
  end

  def validate_require
    super
    if @record.is_whole
      # 一棟貸し情報登録時
      # valid_presence :building_id
      @record.errors.add :base, '施設を正しく指定してください。' if @record.building_id.blank?
    else
      # 通常の予約情報登録時
      # valid_presence :room_id
      @record.errors.add :base, '施設を正しく指定してください。' if @record.room_id.blank?
    end
    # @record.errors.add :base, '国籍' if @record.nationality_id.blank?
  end
end