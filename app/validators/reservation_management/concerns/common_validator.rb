module ReservationManagement::Concerns::CommonValidator

  # TODO 泊数最大値

  # ApplicationValidator から呼ばれる
  def get_label(key)
    I18n.t("activerecord.attributes.reservation.#{key}")
  end

  def validate_require
    valid_presence :site_code
    valid_presence :reserve_date
    valid_presence :check_in_date
    valid_presence :number_of_nights
    valid_presence :visitor_name
    valid_presence :adult_count
    valid_presence :child_count
    valid_presence :room_charge
    valid_presence :cleaning_fee
    valid_presence :meal_fee
  end

  def validate_format
    valid_fomat_data       :reserve_date
    valid_fomat_data       :check_in_date
    valid_format_num       :number_of_nights
    valid_format_mail       :mail_address
    valid_format_num       :adult_count
    valid_format_num       :child_count
    valid_format_num       :room_charge
    valid_format_num       :cleaning_fee
    valid_format_num       :meal_fee
    valid_format_num       :total
    valid_fomat_date_time  :scheduled_check_in_time
    valid_format_kana      :visitor_name_kana
  end

  # サイトの存在チェック
  def validate_site_code
    unless Settings.SITE.CODE.to_hash.values.include?(@record.site_code.to_s)
      add_error("common", "not_exist", {label: get_label(:site_code)})
    end
  end

  # 予約日の重複チェック
  # 一棟貸は宿泊日変更不可なので新規登録時のみ利用
  def validate_check_in_day
    conditions = {}.tap do |c|
      c[:room_id] = @record.room_id if @record.try(:room_id).present?
      c[:building_id] = @record.building_id if @record.try(:building_id).present?
      c[:check_in_date] = @record.check_in_date
      c[:number_of_nights] = @record.number_of_nights
      c[:reservation_id] = @record.reservation_id if @record.try(:reservation_id).present?
    end
    ReservationDate.search_room_reservation(conditions).preload(room: :building).each do |rd|
      add_error("reservation", "already_exists", {date: I18n.l(rd.check_in_day, format: :middle_h), room_name: rd.room.full_code_name,})
    end
  end
end
