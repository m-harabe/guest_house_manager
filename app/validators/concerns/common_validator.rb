module CommonValidator

  # 書式チェック用定数
  # メールアドレス
  EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # 電話番号
  # TELEPHONE_REGEX = /\A\d+-\d+-\d+\z/
  TELEPHONE_REGEX = /\A[()0-9\-]+\z/
  # 半角数字
  NUM_REGEX = /\A([0-9])+\z/
  # コード（半角数字および「-」）
  CODE_NUM_REGEX = /\A([0-9-])+\z/
  # コード（半角英数および「-」)
  CODE_ALPHANUMERIC_REGEX = /\A[0-9A-Za-z\-]+\z/
  # コード（半角英数,「-」, 「_」)
  CODE_UNDERSCORE_REGEX   = /\A[0-9A-Za-z\-_]+\z/
  # 全角カナ
  KANA_REGEX = /\A([ァ-ン　\sー])+\z/
  # 時刻（HH:MM）
  TIME_REGEX = /^([0-1][0-9]|[2][0-3]):[0-5][0-9]$/

  # 文字列判定メソッド

  # コードチェック（半角英数字, 「-」, および underscore:true の場合は「_」を含む）
  def code?(str, underscore=false)
    return true if str.blank?
    return str =~ CODE_UNDERSCORE_REGEX if underscore
    return str =~ CODE_ALPHANUMERIC_REGEX
  end

  # 半角数字チェック
  def num?(str)
    return true if str.blank?
    return str =~ NUM_REGEX
  end

  # 半角数字および「-」チェック
  def num_hyphen?(str)
    return true if str.blank?
    return str =~ CODE_NUM_REGEX
  end

  # 半角数字および「-」チェック
  def tel?(str)
    return true if str.blank?
    return str =~ TELEPHONE_REGEX
  end

  # 全角カナチェック
  def kana?(str)
    return true if str.blank?
    return str =~ KANA_REGEX
  end

  # メール形式チェック
  def mail?(str)
    return true if str.blank?
    return str =~ EMAIL_REGEX
  end

  # def date?(date_str, permit_linefeed = false)
  #   return true if date_str.blank?
  #   check_ok = false
  #   if permit_linefeed
  #     if date_str =~ /^(\d+)\/(\d+)\/(\d+)$/
  #       y = $1.to_i
  #       m = $2.to_i
  #       d = $3.to_i
  #       check_ok = true
  #     end
  #   else
  #     if date_str =~ /\A(\d+)\/(\d+)\/(\d+)\z/
  #       y = $1.to_i
  #       m = $2.to_i
  #       d = $3.to_i
  #       check_ok = true
  #     end
  #   end
  #   if permit_linefeed
  #     if date_str =~ /^(\d+)-(\d+)-(\d+)$/
  #       y = $1.to_i
  #       m = $2.to_i
  #       d = $3.to_i
  #       check_ok = true
  #     end
  #   else
  #     if date_str =~ /\A(\d+)-(\d+)-(\d+)\z/
  #       y = $1.to_i
  #       m = $2.to_i
  #       d = $3.to_i
  #       check_ok = true
  #     end
  #   end
  #   return false unless check_ok

  #   unless Date.valid_date?(y, m, d)
  #     return false
  #   end
  #   return true
  # end


  # 日付形式チェック
  # @param  [String] str 日付を示す文字列
  # @return [Boolean] 二重感嘆符でtrue/falseを返す
  # https://techracho.bpsinc.jp/hachi8833/2016_11_15/28993
  def date?(str)
    !! Date.parse(str) rescue false
  end

  # 日時形式チェック
  # @param  [String] str 日時を示す文字列
  # @return [Boolean] 二重感嘆符でtrue/falseを返す
  # このメソッドでは、日付部分だけあっていれば、時刻の部分はなんでも通してしまうので注意
  def date_time?(str)
    !! DateTime.parse(str) rescue false
  end

  def time?(str)
    return true if str.blank?
    return str =~ TIME_REGEX
  end

  # 一意性チェック
  def unique?(conditions)
    if @record.id
      check_obj = Object.const_get(@record.class.to_s).where(conditions).where.not(id: @record.id).last
    else
      check_obj = Object.const_get(@record.class.to_s).where(conditions).last
    end
    return (check_obj.nil? ? true : false)
  end
end