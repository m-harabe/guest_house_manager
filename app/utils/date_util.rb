class DateUtil

  # 検索条件用日時FROMの生成
  def self.get_datetime_for_search_condition_from(date)
    return get_datetime_for_search_condition(date)[0]
  end

  # 検索条件用日時TOの生成
  def self.get_datetime_for_search_condition_to(date)
    return get_datetime_for_search_condition(date)[1]
  end

  #---------------------------------------------------------------------------------------------------
  # 検索条件日時FROM、TO の初期値自動生成
  # 日付に開始時間、終了時間を付与して返却する
  #
  # 引数　：date       日付
  # 引数　：from       FROM時間(デフォルトは"00:00:00")
  # 引数　：to         TO時間(デフォルトは"23:59:59")
  # 戻り値：string　　 FROM日時'YYYY/MM/DD HH:MM:SS'形式
  # 戻り値：string　　 TO日時'YYYY/MM/DD HH:MM:SS'形式
  #---------------------------------------------------------------------------------------------------
  def self.get_datetime_for_search_condition(date, from: '00:00:00', to: '23:59:59')
    date = convert_format_date(date,'YYYY-MM-DD') + ' '
    return ["", ""] if date.blank?
    return date + from, date + to
  end

  #---------------------------------------------------------------------------------------------------
  # 日付分割処理（年）
  #
  # 引数　：date       
  # 戻り値：year　　　 年
  #---------------------------------------------------------------------------------------------------
  def self.split_date_year(date)
    begin
      @date = Date.parse(date.strftime("%Y-%m-%d %H:%M:%S"))
    rescue
      return ''
    end
    return @date.year
  end

  #---------------------------------------------------------------------------------------------------
  # 日付分割処理（月）
  #
  # 引数　：date       
  # 戻り値：month　　　月
  #---------------------------------------------------------------------------------------------------
  def self.split_date_month(date)

    begin
      @date = Date.parse(date.strftime("%Y-%m-%d %H:%M:%S"))

    rescue
      return ''
    end

    return @date.month
  end

  #---------------------------------------------------------------------------------------------------
  # 日付分割処理
  #
  # 引数　：date       
  # 戻り値：day　　　　日
  #---------------------------------------------------------------------------------------------------
  def self.split_date_day(date)

    begin
      @date = Date.parse(date.strftime("%Y-%m-%d %H:%M:%S"))

    rescue
      return ''
    end

    return @date.day
  end

  #---------------------------------------------------------------------------------------------------
  # 日付時刻分割処理
  #
  # 引数　：dateTime       
  # 戻り値：time　　　 時
  #---------------------------------------------------------------------------------------------------
  def self.split_time_hour(dateTime)

    begin
      @date_time = DateTime.parse(dateTime.strftime("%Y-%m-%d %H:%M:%S"))

    rescue
      return ''
    end

    return @date_time.hour
  end

  #---------------------------------------------------------------------------------------------------
  # 日付時刻分割処理
  #
  # 引数　：dateTime       
  # 戻り値：minutes　　分
  #---------------------------------------------------------------------------------------------------
  def self.split_time_minutes(dateTime)

    begin
      @date_time = DateTime.parse(dateTime.strftime("%Y-%m-%d %H:%M:%S"))

    rescue
      return ''
    end

    return @date_time.min
  end

  # YYYYMMフォーマット変換
  # 年と月をYYYYMMの文字列として返します。
  # [year] 年
  # [month] 月
  def self.format_YYYYMM(year, month)
    return year.to_s + sprintf("%02d", month)
  end

  #
  # 年月を配列に変換
  #
  # def self.create_period_array(start_year, start_month, end_year, end_month)
    
  #   period = Array.new  # 返却用リスト

  #   date = format_YYYYMM(start_year, start_month)
  #   end_date = format_YYYYMM(end_year, end_month)

  #   # 期間終了までのリストを作成する。
  #   until date.to_i > end_date.to_i
  #     period.push(date)
  #     # 12月まできたら年を繰り上げて1月にもどす
  #     if start_month == Util::SystemConst::TWELVE
  #       start_month = Util::SystemConst::ONE
  #       start_year = start_year + Util::SystemConst::ONE
  #     else
  #       start_month = start_month + Util::SystemConst::ONE
  #     end
      
  #     date = format_YYYYMM(start_year, start_month)
  #   end

  #   return period
  # end

  #---------------------------------------------------------------------------------------------------
  # 日時フォーマット変換
  #
  # 引数　：日付
  #     　：フォーマット YYYYMMDD,YYYY/MM/DD,YYYY-MM-DD
  #     　：             YYYYMMDDHHMMSS,YYYY/MM/DD HH:MM:SS,YYYY-MM-DD HH:MM:SS
  # 戻り値：変換後日付（文字列）
  #---------------------------------------------------------------------------------------------------
  def self.convert_format_date(d, format = nil)
      # 日付に変換
    if d.class.to_s == 'DateTime' || d.class.to_s == 'Date' || d.class.to_s == 'Time' || d.class.to_s == 'ActiveSupport::TimeWithZone'
      _date_time = d
    else
      begin
        _date_time = DateTime.parse(d)
      rescue
        return ''
      end
    end

    # フォーマット変換
    case format
    when 'YYYYMMDD'
      return _date_time.strftime('%Y%m%d')
    when 'YYYY/MM/DD'
      return _date_time.strftime('%Y/%m/%d')
    when 'YYYY-MM-DD'
      return _date_time.strftime('%Y-%m-%d')
    when 'YYYY/MM/DD HH:MM'
      return _date_time.strftime('%Y/%m/%d %H:%M')
    when 'YYYYMMDDHHMMSS'
      return _date_time.strftime('%Y%m%d%H%M%S')
    when 'YYYY/MM/DD HH:MM:SS'
      return _date_time.strftime('%Y/%m/%d %H:%M:%S')
    when 'YYYY-MM-DD HH:MM:SS'
      return _date_time.strftime('%Y-%m-%d %H:%M:%S')
    when 'HH:MM'
      return _date_time.strftime('%H:%M')
    else
      # フォーマットを指定していない場合、YYYY/MM/DD HH:MM:SSで返却
      return _date_time.strftime('%Y/%m/%d %H:%M:%S')
    end
  end
end
