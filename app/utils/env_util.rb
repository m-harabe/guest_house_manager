module EnvUtil
  class << self

    # 自分のIPアドレスを取得
    # 参考: https://qiita.com/saltheads/items/cc49fcf2af37cb277c4f
    def production_ip?
      udp = UDPSocket.new
      # クラスBの先頭アドレス,echoポート 実際にはパケットは送信されない。
      udp.connect("128.0.0.0", 7)
      adrs = Socket.unpack_sockaddr_in(udp.getsockname)[1]
      udp.close
      adrs == Settings.IP_ADDRESS.PRODUCTION
    end
  end
end
