class ErrorUtil
  def self.log_and_notify(ex:, title: '')
    body = "################ GuestHouseManager INFO: #{Time.now.to_s} ################\n"
    body << "title: #{title}" + "\n"
    body << "enviroment: #{Rails.env}" + "\n"
    body << "user: #{User.current_user.name}" + "\n" if User.current_user
    self.send_log(ex: ex, body: body)
  end

  # エラーログ出力
  def self.error_log_and_notify(ex:, message: '')
    body = "################ GuestHouseManager ERROR: #{Time.now} ################\n"
    body << "enviroment: #{Rails.env}" + "\n"
    body << "message: " + message + "\n\n" if message
    self.send_log(ex: ex, body: body)
  end

  def self.send_log(ex:, body:, log_type: 'error')
    body << ex.class.to_s + "\n"
    body << ex.message.to_s + "\n"
    body << ex.backtrace[0,20].join("\n")
    # ログ出力
    Rails.logger.send(log_type, body)
    # CWに送信
    NotificationUtil.post_message_to_cw("[code]#{body}[/code]") #unless Rails.env.development?
    # NotificationUtil.post_message_to_slack(message: body) #unless Rails.env.development?
  end

end
