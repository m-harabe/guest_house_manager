#
# Version.2019-11-30
#

require 'net/https'

module NotificationUtil
  class << self

    LINE_URI = URI.parse("https://notify-api.line.me/api/notify")

    def post_message_to_cw(body, api_key: Settings.CW.SYSTEM.API_KEY, room_id: Settings.CW.SYSTEM.ROOM_ID)
      time_out = 30
      uri = URI.parse("https://api.chatwork.com/v2/rooms/#{room_id}/messages")
      https = Net::HTTP.new(uri.host,uri.port)
      https.use_ssl = true
      https.verify_mode = OpenSSL::SSL::VERIFY_NONE
      #リクエストインスタンス生成
      request = Net::HTTP::Post.new(uri.path)
      https.use_ssl = true
      request["X-ChatWorkToken"] = api_key
      request_params = {}
      request_params["body"] = body
      request.set_form_data(request_params)
      https.open_timeout = time_out
      https.read_timeout = time_out
      response = https.request(request)
      return true
    rescue => ex
      return false, ex.class, ex.message, ex.backtrace
    end

    def post_message_to_line_notify(message, token:)
      request = Net::HTTP::Post.new(LINE_URI)
      request["Authorization"] = "Bearer #{token}"
      request.set_form_data(message: message)

      response = Net::HTTP.start(LINE_URI.hostname, LINE_URI.port, use_ssl: LINE_URI.scheme == 'https') do |https|
        https.request(request)
      end
      true
    rescue => ex
      return false, ex.class, ex.message, ex.backtrace
    end

    # Slackへの通知
    # @param [String] webhook_url SlackのIncoming Webhook で設定したURL
    # @param [Symbol, String] message_type: メッセージを生成するNotificationUtilのプライベートメソッド名
    # @param [String] message: メッセージ内容
    def post_db_dump_message(webhook_url: Settings.SLACK.WEBHOOK_URL, message_type: nil, message_by_option: {})
      message = send("build_slack_message_for_#{message_type}", message_by_option) if message_type
      notifier = Slack::Notifier.new webhook_url
      notifier.ping message
    end

    # TODO: [SlackのIncoming WebHooksを使わないで指定のChannelにPOSTする方法 - Qiita](https://qiita.com/ogawatachi/items/c89b1e92e877ef6b0698)
    # def post_message_to_slack(webhook_url: Settings.SLACK.WEBHOOK_URL, message:)
    #   notifier = Slack::Notifier.new webhook_url
    #   notifier.ping message
    # end

    # Slackへの通知
    # @param [String] webhook_url SlackのIncoming Webhook で設定したURL
    # @param [Symbol, String] message_type: メッセージを生成するNotificationUtilのプライベートメソッド名
    # @param [String] message: メッセージ内容
    def post_db_dump_message_to_slack(webhook_url: Settings.SLACK.WEBHOOK_URL, message_type: nil, message_by_option: {})
      message = send("build_slack_message_for_#{message_type}", message_by_option) if message_type
      notifier = Slack::Notifier.new webhook_url
      notifier.ping message
    end

    def post_message_to_slack(webhook_url: Settings.SLACK.WEBHOOK_URL, message:)
      notifier = Slack::Notifier.new webhook_url
      notifier.ping message
    end

    def test
      message = <<~STR
        予約送信テスト
        送信しました。
        送信日：#{Date.today}
        ええええええ
      STR
      NotificationUtil.post_message_to_cw message
      NotificationUtil.post_message_to_line_notify(message, token: Settings.LINE.TOKEN_1)
      NotificationUtil.post_message_to_slack(message: message)
    end

    private

    # TODO: 可変項目を引数にして汎用的にしたい
    def build_slack_message_for_db_dump(message_by_option = {})
      server = Settings.SERVER_NAME || 'local'
      value = "【アプリ名】#{Settings.APP_NAME} 【日時】#{I18n.l(Time.zone.now, format: :default_h)}\n"
      value += "【サーバー】#{server}\n"
      value += "【ファイル名】#{message_by_option[:filename]}\n"
      value += "【ファイルサイズ】#{message_by_option[:filesize]}"
      {
        # "icon_emoji": ":bow:",
        "username": "定期実行処理通知",
        "attachments":
          {
            "fallback": "",
            "pretext": "",
            "color": '#58FAF4',
            "title": "DBのバックアップ処理が完了しました。",
            # "title_link": "https://hoge",
            "fields": [
              {
                "value": value
              }
            ]
          }
      }
    end
  end
end