# gem config のSettingsクラスのオブジェクト加工用クラス
#
#
class SettingsUtil
  class << self
    def key_to_i(settings)
      settings.keys.map do |key|
        key.to_s.to_i
      end
    end
  end
end
