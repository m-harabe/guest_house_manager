# MySqlバックアップバッチ用クラス
# `lib/tasks/db.rake` から利用される
# 実行コマンド bundle exec rails db:dump_all
#
# gzip コマンドで圧縮
# gunzip コマンドで解凍 `gunzip tmp/backups/guest_house_manager_2018-10-31_14-21-16_dump.sql.gz`
#
# バックアップファイルをDropboxにも保存
#
# 参考
# - [mysqldumpでダンプするrakeタスク](https://qiita.com/hmuronaka/items/e778b234e8c4798953f6)
# - [Rails,Rubyでシェルを実行](https://qiita.com/kouuuki/items/927e6aaece0f654c03df)
# - [DropboxApi リファレンス](https://www.xuuso.com/dropbox_api/DropboxApi/Client.html)

class DbDumper

  # 保存先ローカルディレクトリ
  BACKUP_DIR = 'tmp/backups'

  # Dropboxの一番上の階層（アプリが判別できる名称）
  STRAGE_DIR = 'db_dump_ghm'

  # ファイル接尾語
  FILE_SUFFIX = 'dump.sql.gz'

  # 保存期間
  BACKUP_TERM = 10

  # ファイル名の日付部分を抜き出す正規表現パターン
  FILE_NAME_REGEX = /(\d+-\d+-\d+)_\d+-\d+-\d+_#{FILE_SUFFIX}/

  def self.run
    Rails.logger.info('start: rake db:dump_all')
    new.run
    Rails.logger.info('finish: rake db:dump_all')
  end

  def initialize
    @environment = Rails.env
    @configuration = ActiveRecord::Base.configurations[@environment]

    # ファイル名、ディレクトリを生成
    timestamp = Time.now.strftime("%Y-%m-%d_%H-%M-%S")
    @filename = "#{@configuration['database']}_#{timestamp}_#{FILE_SUFFIX}"
    @filedir = "#{BACKUP_DIR}/#{@filename}"
    @filesize = ''

    # DropboxAPIへ接続するためのクライアント（Gem）オブジェクトを生成
    @dropbox_client = DropboxApi::Client.new(Settings.DROPBOX.ACCESS_TOKEN)

    # 削除対象日付
    @target_day = Date.today - BACKUP_TERM
  end

  # 各処理のコール
  # TODO: 例外処理
  def run
    dump
    save_to_dropbox
    delete_olds
    delete_olds_in_dropbox
    NotificationUtil.post_db_dump_message_to_slack(
      message_type: :db_dump,
      message_by_option: {filename: @filename, filesize: @filesize}
    )
  end

  # バックアップファイルを暗号化して生成
  def dump
    # ディレクトリ生成
    `mkdir -p #{BACKUP_DIR}`

    cmd = "mysqldump -u #{@configuration['username']} -p#{@configuration['password']} #{@configuration['database']} | gzip -c > #{@filedir}"
    puts cmd
    # exec cmd # << この書き方だと後の処理してくれない？ので以下呼び出し
    # コマンド実行
    `#{cmd}`

    @filesize = (File.size(@filedir).to_i / 1000).to_s + 'KB'
  end

  # 指定日より古い日付のファイルを削除
  def delete_olds
    Dir.glob("#{BACKUP_DIR}/*").each do |f|
      # ファイル名から日付を抽出
      mache_data = FILE_NAME_REGEX.match(f)
      next unless mache_data
      FileUtils.rm(f) if mache_data[1].to_date < @target_day
    end
  end

  # Dropboxへのアップロード
  def save_to_dropbox
    file_content = IO.read @filedir
    @dropbox_client.upload "/#{STRAGE_DIR}/#{Settings.SERVER_NAME}/#{@filename}", file_content
  end

  # 保存時と同一のディレクトリ内で指定日より古い日付のファイルを削除
  def delete_olds_in_dropbox
    result = @dropbox_client.list_folder "/#{STRAGE_DIR}/#{Settings.SERVER_NAME}"
    result.entries.each do |data|
      mache_data = FILE_NAME_REGEX.match(data.path_lower)
      next unless mache_data
       @dropbox_client.delete(data.path_lower) if mache_data[1].to_date < @target_day
    end
  end
end