module CountUtil
  class << self
    # 除算して剰余を均等に分配
    # @param value [Integer] 割る金額、人数の数値
    # @param count [Integer] 分割する件数
    # @return [Array<Integer>] 分割結果を配列で返す
    # CountUtil.divide_with_surplus(value:12001, count:3)
    # => [4001, 4000, 4000]
    def divide_with_surplus(value:, count:)
      d = value / count
      s = value % count
      count_list = []
      # (0...count).reverse_each do |idx| # Rangeのオブジェクトを反転
      (0...count).each do |idx|
        count_list[idx] ||= 0
        count_list[idx] += d
        if s > 0
          count_list[idx] += 1
          s -= 1
        end
      end
      count_list
    end
  end
end
