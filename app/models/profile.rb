# == Schema Information
#
# Table name: profiles
#
#  id               :bigint(8)        not null, primary key
#  profilable_type  :string(255)
#  profilable_id    :bigint(8)
#  postal_code      :string(8)        default(""), not null
#  address          :string(100)      default(""), not null
#  total_floor_area :integer          default(0)
#  standard_people  :integer          default(0)
#  max_people       :integer          default(0)
#  bath_count       :integer          default(0)
#  toilet_count     :integer          default(0)
#  secret_number    :string(25)       default(""), not null
#

# 施設、部屋で共通の詳細情報を保存するための汎用モデル
# ポリモーフィック
class Profile < ApplicationRecord
  belongs_to :profilable, polymorphic: true

  # 登録・更新処理
  # @param attr [Hash<String>] 更新パラメータ
  # Integerのカラムが空文字 "" で送られてきたらnilに変換して登録
  def set_attributes(attr:)
    self.total_floor_area = attr[:total_floor_area].presence || nil
    self.standard_people  = attr[:standard_people].presence || nil
    self.max_people       = attr[:max_people].presence || nil
    self.bath_count       = attr[:bath_count].presence || nil
    self.toilet_count     = attr[:toilet_count].presence || nil
    self.secret_number    = attr[:secret_number] || ''
    self.save!
  end
end
