# == Schema Information
#
# Table name: release_notes
#
#  id              :bigint(8)        not null, primary key
#  open_date       :date             not null
#  title           :string(255)      not null
#  content         :text(65535)      not null
#  note_class      :string(1)
#  created_user_id :integer          default(0), not null
#  updated_user_id :integer          default(0), not null
#  lock_version    :integer          default(0), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class ReleaseNote < ActiveRecord::Base

  default_scope -> { order('open_date DESC, id DESC') }
  scope :sheduled, -> { where(note_class: Settings.RELEASE_NOTES.NOTE_CLASS_SCHEDULED) }
  scope :not_sheduled, -> { where(note_class: [Settings.RELEASE_NOTES.NOTE_CLASS_RELEASE, Settings.RELEASE_NOTES.NOTE_CLASS_NEWS]) }
end
