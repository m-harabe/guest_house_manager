# == Schema Information
#
# Table name: suitebook_reservations
#
#  id                      :bigint(8)        not null, primary key
#  reservation_id          :bigint(8)
#  whole_reservation_id    :bigint(8)
#  status                  :integer          default(0), not null
#  suitebook_idnum         :string(255)
#  site_idnum              :string(255)
#  site_name               :string(100)
#  check_in_date           :string(255)
#  check_out_date          :string(255)
#  reservation_cd          :string(255)
#  is_archive              :string(255)
#  number_of_nights        :string(255)
#  visitor_name            :string(255)
#  mail_address            :string(255)
#  total_count             :string(255)
#  adult_count             :string(255)
#  child_count             :string(255)
#  scheduled_check_in_time :string(255)
#  building_name           :string(255)
#  building_idnum          :string(255)
#  property_owner_idnum    :string(255)
#  owner_company_name      :string(255)
#  owner_name              :string(255)
#  site_account            :string(255)
#  contract_style          :string(255)
#  cancel                  :string(255)
#  currency                :string(255)
#  room_charge             :string(255)
#  cleaning_fee            :string(255)
#  total                   :string(255)
#  handling_charge         :string(255)
#  revenue                 :string(255)
#  data_careated_at        :string(255)
#  other_cost              :string(255)
#  room_idnum              :string(255)
#  room_name               :string(255)
#  memo                    :string(255)
#  created_user_id         :integer          default(0), not null
#  updated_user_id         :integer          default(0), not null
#  lock_version            :integer          default(0), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  room_id                 :integer
#  error_message           :text(65535)
#  building_id             :integer
#  property_name           :string(255)
#  tel                     :string(255)
#  cancel_fee              :string(255)
#  point_use               :string(255)
#  cancel_charge           :string(255)
#  total_minus_point       :string(255)
#  cancel_revenue          :string(255)
#  payment                 :string(255)
#  plan                    :string(255)
#  owner_note              :text(65535)
#  visitor_name_kana       :string(255)
#  postal_code             :string(8)        default(""), not null
#  address                 :string(255)
#

class SuitebookReservation < ApplicationRecord

  belongs_to :reservation
  belongs_to :whole_reservation
  belongs_to :room


  # scopes
  scope :display_order, -> { order(id: :desc) }
  scope :status_error, -> { where(status: Settings.SUITEBOOK_RESERVATION_STATUS.ERROR) }

  REGEX_VISITOR_NAME_KANA = /\((.+)\)/

  # Reservation登録用属性の生成
  def reservation_attributes
    {}.tap do |attr|
      attr[:room_id] = room_id
      attr[:building_id] = building_id # 一棟貸し用
      attr[:status] = Settings.RESERVATION.STATUS.PREPARING
      attr[:site_code] = set_site_code
      # ゲスト名とゲスト名カナを分割
      match_str = visitor_name&.match(REGEX_VISITOR_NAME_KANA)
      if match_str
        attr[:visitor_name] = visitor_name.gsub(match_str[0], '')
        attr[:visitor_name_kana] = match_str[1]
      else
        attr[:visitor_name] = visitor_name
      end
      attr[:mail_address] = mail_address
      attr[:tel] = tel
      attr[:adult_count] = adult_count
      attr[:child_count] = child_count
      attr[:room_charge] = room_charge
      attr[:cleaning_fee] = cleaning_fee
      attr[:point_use] = point_use
      attr[:total] = total
      attr[:owner_note] = owner_note
      attr[:reserve_date] = data_careated_at
      attr[:check_in_date] = check_in_date
      attr[:check_out_date] = check_out_date
      # 画面からの登録用と同じHH:MM形式でセット
      attr[:display_check_in_time] = scheduled_check_in_time[0,5] if scheduled_check_in_time.present?
      attr[:number_of_nights] = number_of_nights
      # その他
      attr[:nationality_id] = set_nationality_id
      attr[:meal_fee] = 0
    end
  end

  # def set_room_id
  #   SuitebookRoom.find_by(building_idnum: building_idnum)&.room_id
  # end

  def set_site_code
    Settings.SUITEBOOK_SITE_CODE_LABEL[site_idnum]
  end

  def processed?
    # (status == Settings.SUITEBOOK_RESERVATION_STATUS.PROCESSED || status == Settings.SUITEBOOK_RESERVATION_STATUS.CANCEL)
    status == Settings.SUITEBOOK_RESERVATION_STATUS.PROCESSED
  end

  def registerable?
    (status == Settings.SUITEBOOK_RESERVATION_STATUS.UNPROCESSED || status == Settings.SUITEBOOK_RESERVATION_STATUS.ERROR)
  end

  def error?
    status == Settings.SUITEBOOK_RESERVATION_STATUS.ERROR
  end

  def cancel?
    status == Settings.SUITEBOOK_RESERVATION_STATUS.CANCEL
  end

  # 国籍の取得
  def set_nationality_id
    # 郵便番号ありは日本
    return 1 if postal_code.present?
    return 0 if address.blank?
    Nationality.nationality_id_by_suitebook_address(address)
  end

  def error_message_list
    error_message.split(',')
  end

end
