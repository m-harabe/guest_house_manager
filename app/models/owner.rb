# == Schema Information
#
# Table name: owners
#
#  id              :bigint(8)        not null, primary key
#  name            :string(255)      default(""), not null
#  postal_code     :string(8)        default(""), not null
#  created_user_id :integer          default(0), not null
#  updated_user_id :integer          default(0), not null
#  lock_version    :integer          default(0), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  address1        :string(255)      default("")
#  address2        :string(255)      default("")
#  tel             :string(255)      default(""), not null
#  mail_address    :string(255)
#  bank_name       :string(255)      default("")
#  account_type    :integer          default(0)
#  account_number  :string(255)      default("")
#  swift_code      :string(255)      default("")
#  account_name    :string(255)      default("")
#  language        :string(255)      default("")
#  note            :text(65535)
#  is_visible      :boolean          default(TRUE), not null
#  mail_address2   :string(255)
#  mail_address3   :string(255)
#

class Owner < ApplicationRecord
  include Concerns::Visible

  # associations
  has_many :buildings

  # scopes

  # ------------------------------------------------------------------
  # Validations
  # ------------------------------------------------------------------

  # presence
  validates :name,            presence: true
  validates :language,        presence: true

  # format
  # validates :email, length: { maximum: 255 }, format: { with: CommonValidator::EMAIL_REGEX, message: I18n.t('activerecord.errors.messages.email_invalid') }, uniqueness: true
  validates :mail_address, length: { maximum: 255 }, format: { with: CommonValidator::EMAIL_REGEX }, allow_blank: true
  validates :mail_address2, length: { maximum: 255 }, format: { with: CommonValidator::EMAIL_REGEX }, allow_blank: true
  validates :mail_address3, length: { maximum: 255 }, format: { with: CommonValidator::EMAIL_REGEX }, allow_blank: true

  # inclusion
  validates :is_visible,     inclusion: { in: [true, false] }, allow_nil: false

  def account_type_label
    Settings.BANK_ACCOUNT_TYPE[account_type] || ''
  end

  def bank_label_jp
    [bank_name, account_type_label, account_number].join('　')
  end

  def full_address
    [address1, address2].join('　')
  end

  def language_label
    Settings.SETTLEMENT_LANGUAGE_LABEL[language]
  end

end
