# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  email                  :string(255)      default("")
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string(255)
#  kana                   :string(255)
#  tel                    :string(255)
#  account                :string(255)      not null
#  role                   :integer          default(0), not null
#  created_user_id        :integer          default(0), not null
#  updated_user_id        :integer          default(0), not null
#  deleted_at             :datetime
#  lock_version           :integer          default(0), not null
#  is_visible             :boolean          default(TRUE), not null
#

# class User < ApplicationRecord
#   # Include default devise modules. Others available are:
#   # :confirmable, :lockable, :timeoutable and :omniauthable
#   devise :database_authenticatable, :registerable,
#          :recoverable, :rememberable, :trackable, :validatable
# end

class User < ActiveRecord::Base
  include Concerns::MatchAttribute
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable,
         :authentication_keys => [:account]

  validates :role, presence: true, on: :update_other

  # accountを必須・一意とする
  validates_uniqueness_of :account
  validates_presence_of :account
  validates :account,
            uniqueness: { case_sensitive: false },
            length: { minimum: 4, maximum: 20 }#,
            #format: { with: BaseValidator::CODE_UNDERSCORE_REGEX, message: I18n.t("errors.common.format_code_underscore", {label: "アカウント"}) }

  # validates :name, presence: true, length: { maximum: 20 }

  cattr_accessor :current_user

  scope :list_by_role, -> { get_list_by_role }

  # Devise関連メソッド

  # email, account の両方を利用してログインするようにオーバーライド
  # def self.find_first_by_auth_conditions(warden_conditions)
  #   conditions = warden_conditions.dup
  #   if login = conditions.delete(:login)
  #     # 認証の条件式を変更する
  #     where(conditions).where(["account = :value", { :value => account }]).first
  #   else
  #     where(conditions).first
  #   end
  # end

  # Devise でユーザーがパスワードなしでアカウント情報を変更するのを許可
  def update_without_current_password(params, *options)
    params.delete(:current_password)
    if params[:password].blank? && params[:password_confirmation].blank?
      params.delete(:password)
      params.delete(:password_confirmation)
    end
    result = update_attributes(params, *options)
    clean_up_passwords
    result
  end

  # is_visibleカラムを利用してログインするようにオーバーライド
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    where(conditions).where(is_visible: true).first
  end

  # 登録時にemailを不要とする
  def email_required?
    false
  end

  def email_changed?
    false
  end

  # ユーザー最低限の情報（引数）で追加する
  def self.create_by_default!(role:, name:, account:)
    base_attr = {created_user_id: 0, updated_user_id: 0}
    User.create!({ role: role, name: name, account: account, email: "#{account}@aaa.bbb",
                   encrypted_password: "re#{account}", password: "re#{account}",
                   created_user_id: 0, updated_user_id: 0 })
  end

  # scope用メソッド
  def self.get_list_by_role
    return [] unless current_user
    return User.where(User.arel_table[:role].gteq current_user.role)
  end
end
