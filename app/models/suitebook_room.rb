# == Schema Information
#
# Table name: suitebook_rooms
#
#  id             :bigint(8)        not null, primary key
#  room_id        :bigint(8)
#  building_idnum :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class SuitebookRoom < ApplicationRecord

  belongs_to :room

end
