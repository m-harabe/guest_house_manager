# == Schema Information
#
# Table name: nationalities
#
#  id         :bigint(8)        not null, primary key
#  name       :string(255)      default(""), not null
#  order      :integer          default(0), not null
#  is_visible :boolean          default(TRUE), not null
#

class Nationality < ApplicationRecord
  scope :visible, -> { where(is_visible: true) }
  scope :selecter, -> { self.visible.order(:order) }

  def self.nationality_id_by_suitebook_address(str)
    # Clinton, CT（アメリカ）
    # Orange, CA（アメリカ）
    # ニューヨーク, ニューヨーク
    # Otsu, 日本
    # パリ, フランス
    # ルーアン, フランス
    # ヴェネツィア, イタリア
    # 上海, 中国

    return 1 if str.include?("Japan")
    return 1 if str.include?("日本")
    return 2 if str.include?("China")
    return 2 if str.include?("中国")
    return 15 if str.include?("Australia")
    return 15 if str.include?("オーストラリア")
    return 16 if str.include?("ニュージーランド")
    return 17 if str.include?("イギリス")
    return 18 if str.include?("ドイツ")
    return 19 if str.include?("スペイン")
    return 20 if str.include?("イタリア")
    return 21 if str.include?("フランス")
    return 0
  end
end
