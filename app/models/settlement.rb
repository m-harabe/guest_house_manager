# == Schema Information
#
# Table name: settlements
#
#  id                        :bigint(8)        not null, primary key
#  settlement_template_id    :bigint(8)
#  room_id                   :bigint(8)        not null
#  year_month                :string(255)      not null
#  download_count            :integer          default(0), not null
#  title                     :string(255)      default(""), not null
#  owner_name                :string(255)      default(""), not null
#  building                  :string(255)      default(""), not null
#  address                   :string(255)      default(""), not null
#  company_name              :string(255)      default(""), not null
#  company_info1             :string(255)      default(""), not null
#  company_info2             :string(255)      default(""), not null
#  company_info3             :string(255)      default(""), not null
#  reward_rate               :float(24)        default(0.0), not null
#  label_other_income        :string(255)      default(""), not null
#  label_internet_income     :string(255)      default(""), not null
#  label_free_income1        :string(255)      default(""), not null
#  label_free_income2        :string(255)      default(""), not null
#  label_free_income3        :string(255)      default(""), not null
#  label_free_income4        :string(255)      default(""), not null
#  label_free_income5        :string(255)      default(""), not null
#  label_total_income        :string(255)      default(""), not null
#  label_other_cost          :string(255)      default(""), not null
#  label_site_fee            :string(255)      default(""), not null
#  label_electrical_fee      :string(255)      default(""), not null
#  label_gas_fee             :string(255)      default(""), not null
#  label_water_fee           :string(255)      default(""), not null
#  label_internet_fee        :string(255)      default(""), not null
#  label_site_controller_fee :string(255)      default(""), not null
#  label_free_fee1           :string(255)      default(""), not null
#  label_free_fee2           :string(255)      default(""), not null
#  label_free_fee3           :string(255)      default(""), not null
#  label_free_fee4           :string(255)      default(""), not null
#  label_free_fee5           :string(255)      default(""), not null
#  label_total_fee           :string(255)      default(""), not null
#  internet_income           :integer
#  free_income1              :integer
#  free_income2              :integer
#  free_income3              :integer
#  free_income4              :integer
#  free_income5              :integer
#  site_fee                  :integer
#  electrical_fee            :integer
#  gas_fee                   :integer
#  water_fee                 :integer
#  internet_fee              :integer
#  site_controller_fee       :integer
#  free_fee1                 :integer
#  free_fee2                 :integer
#  free_fee3                 :integer
#  free_fee4                 :integer
#  free_fee5                 :integer
#  note1                     :string(255)      default(""), not null
#  note2                     :string(255)      default(""), not null
#  note3                     :string(255)      default(""), not null
#  note4                     :string(255)      default(""), not null
#  created_user_id           :integer          default(0), not null
#  updated_user_id           :integer          default(0), not null
#  lock_version              :integer          default(0), not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  system_fee1               :integer
#  label_system_fee1         :string(255)
#  label_sales               :string(255)
#  label_target_days         :string(255)
#  label_working_days        :string(255)
#  label_guest_count         :string(255)
#  label_occ                 :string(255)
#  label_adr                 :string(255)
#  label_rev_par             :string(255)
#  label_owner_sales         :string(255)
#  label_other_income_total  :string(255)
#  label_other_cost_total    :string(255)
#  label_reward_rate         :string(255)
#  label_payment_amount      :string(255)
#

class Settlement < ApplicationRecord

  # 入金額に清掃費を含めた総売上から報酬率を掛ける場合の設定報酬率（パターン１）
  # この値以外の報酬率が設定されている場合は総売上から清掃費を除く宿泊料売り上げに報酬率を掛け、そこから清掃費を実費で差し引く（パターン２）
  INCLUDE_CLEANING_FEE_REWARD = 50

  # associations
  belongs_to :room
  belongs_to :settlement_template

  # attributes
  attr_accessor :reservation_list, :analysis1, :analysis2

  # scopes

  # ------------------------------------------------------------------
  # Validations
  # ------------------------------------------------------------------

  # presence
  validates :room_id,        presence: true
  validates :year_month,     presence: true
  # TODO: validations

  def self.initialized_by_template(room_id, year_month)
    room = Room.find(room_id)
    template = SettlementTemplate.find_by(language: room&.building&.owner&.language)
    attr = {}.tap do |_attr|
      _attr[:settlement_template_id] = template.id
      _attr[:room_id]       = room.id
      _attr[:year_month]    = year_month
      _attr[:building]      = room&.building&.name
      _attr[:address]       = room&.building&.address
      _attr[:owner_name]    = room&.building&.owner&.name

      _attr[:title]         = template.title
      _attr[:company_name]  = template.company_name
      _attr[:company_info1] = template.company_info1
      _attr[:company_info2] = template.company_info2
      _attr[:company_info3] = template.company_info3
      _attr[:reward_rate]   = room&.building&.reward_rate
      # ラベル（収入）
      _attr[:label_other_income] = template.other_income
      _attr[:label_internet_income] = template.internet_income
      _attr[:label_free_income1] = template.free_income1
      _attr[:label_free_income2] = template.free_income2
      _attr[:label_free_income3] = template.free_income3
      _attr[:label_free_income4] = template.free_income4
      _attr[:label_free_income5] = template.free_income5
      _attr[:label_total_income] = template.total_income
      # ラベル（費用）
      _attr[:label_other_cost]   = template.other_cost
      _attr[:label_site_fee]     = template.site_fee
      _attr[:label_electrical_fee] = template.electrical_fee
      _attr[:label_gas_fee]      = template.gas_fee
      _attr[:label_water_fee]    = template.water_fee
      _attr[:label_internet_fee] = template.internet_fee
      _attr[:label_site_controller_fee] = template.site_controller_fee
      _attr[:label_free_fee1] = template.free_fee1
      _attr[:label_free_fee2] = template.free_fee2
      _attr[:label_free_fee3] = template.free_fee3
      _attr[:label_free_fee4] = template.free_fee4
      _attr[:label_free_fee5] = template.free_fee5
      _attr[:label_total_fee] = template.total_fee
      _attr[:note1] = template.note1
      _attr[:note2] = template.note2
      _attr[:note3] = template.note3
      _attr[:note4] = template.note4
      # ラベル（集計項目）
      _attr[:label_sales] = template.sales
      _attr[:label_target_days] = template.target_days
      _attr[:label_working_days] = template.working_days
      _attr[:label_guest_count] = template.guest_count
      _attr[:label_occ] = template.occ
      _attr[:label_adr] = template.adr
      _attr[:label_rev_par] = template.rev_par
      _attr[:label_owner_sales] = template.sales
      _attr[:label_other_income_total] = template.other_income
      _attr[:label_other_cost_total] = template.other_cost
      _attr[:label_reward_rate] = template.reward_rate
      _attr[:label_payment_amount] = template.payment_amount
    end

    settlement = self.new(attr)
    settlement.label_system_fee1 = '清掃費' unless settlement.reward_include_cleaning_fee?
    settlement.initialize_calc
    settlement
  end

  def self.initialized_by_prev_settlement(room_id, year_month, prev_year_month)
    room = Room.find(room_id)
    template = SettlementTemplate.find_by(language: room&.building&.owner&.language)
    prev_data = Settlement.find_by(room_id: room_id, year_month: prev_year_month)
    attr = {}.tap do |_attr|
      _attr[:settlement_template_id] = template.id
      _attr[:room_id]       = room.id
      _attr[:year_month]    = year_month
      _attr[:building]      = room&.building&.name
      _attr[:address]       = room&.building&.address
      _attr[:owner_name]    = room&.building&.owner&.name

      _attr[:title]         = template.title
      _attr[:company_name]  = template.company_name
      _attr[:company_info1] = template.company_info1
      _attr[:company_info2] = template.company_info2
      _attr[:company_info3] = template.company_info3
      _attr[:reward_rate]   = room&.building&.reward_rate
      # ラベル（収入）
      _attr[:label_other_income] = prev_data.label_other_income
      _attr[:label_internet_income] = prev_data.label_internet_income
      _attr[:label_free_income1] = prev_data.label_free_income1
      _attr[:label_free_income2] = prev_data.label_free_income2
      _attr[:label_free_income3] = prev_data.label_free_income3
      _attr[:label_free_income4] = prev_data.label_free_income4
      _attr[:label_free_income5] = prev_data.label_free_income5
      _attr[:label_total_income] = prev_data.label_total_income
      # ラベル（費用）
      _attr[:label_other_cost]   = prev_data.label_other_cost
      _attr[:label_site_fee]     = prev_data.label_site_fee
      _attr[:label_electrical_fee] = prev_data.label_electrical_fee
      _attr[:label_gas_fee]      = prev_data.label_gas_fee
      _attr[:label_water_fee]    = prev_data.label_water_fee
      _attr[:label_internet_fee] = prev_data.label_internet_fee
      _attr[:label_site_controller_fee] = prev_data.label_site_controller_fee
      _attr[:label_free_fee1] = prev_data.label_free_fee1
      _attr[:label_free_fee2] = prev_data.label_free_fee2
      _attr[:label_free_fee3] = prev_data.label_free_fee3
      _attr[:label_free_fee4] = prev_data.label_free_fee4
      _attr[:label_free_fee5] = prev_data.label_free_fee5
      _attr[:label_total_fee] = prev_data.label_total_fee
      _attr[:note1] = prev_data.note1
      _attr[:note2] = prev_data.note2
      _attr[:note3] = prev_data.note3
      _attr[:note4] = prev_data.note4
      # ラベル（集計項目）
      _attr[:label_sales] = prev_data.label_sales
      _attr[:label_target_days] = prev_data.label_target_days
      _attr[:label_working_days] = prev_data.label_working_days
      _attr[:label_guest_count] = prev_data.label_guest_count
      _attr[:label_occ] = prev_data.label_occ
      _attr[:label_adr] = prev_data.label_adr
      _attr[:label_rev_par] = prev_data.label_rev_par
      _attr[:label_owner_sales] = prev_data.label_owner_sales
      _attr[:label_other_income_total] = prev_data.label_other_income_total
      _attr[:label_other_cost_total] = prev_data.label_other_cost_total
      _attr[:label_reward_rate] = prev_data.label_reward_rate
      _attr[:label_payment_amount] = prev_data.label_payment_amount
    end
    settlement = self.new(attr)
    settlement.label_system_fee1 = (prev_data.label_system_fee1.presence || '清掃費') unless settlement.reward_include_cleaning_fee?
    settlement.initialize_calc
    settlement
  end

  # ------------------------------
  # 報酬率による清掃費の扱い
  # ------------------------------
  # パターン１
  #  - 報酬率が50%のところは清掃費も含めた総売上から報酬率を掛けています。
  # パターン２
  #  - 報酬率が50%以外のところ(オーナーの報酬率が75-85%)は総売上から清掃費を除く宿泊料売り上げに報酬率を掛けています。
  #  - 加えて、そこから清掃費を費用項目として差し引きしています。
  #  - この清掃費を除くケースの時は「システム自動追加費用１」項目に「清掃費」を自動で追加
  def initialize_calc
    # 予約情報のサイト手数料の合計を計算してセットする
    create_reservation_day_list "#{year_month}01".to_date
    self.site_fee = 0
    self.system_fee1 = 0
    reservation_list.each do |obj|
      self.site_fee += obj.handling_charge.to_i
      self.system_fee1 += obj.cleaning_fee.to_i
    end
    # TODO:意味なさそうなソースがあったのでコメントアウト、確認後削除 2021-08-01
    # self.site_fee = site_fee
    # self.system_fee1 = system_fee1
  end

  def jp?
    settlement_template.language == Settings.SETTLEMENT_LANGUAGE.JP
  end

  def year_month_label
    "#{year_month}01".to_date.strftime('%Y年%m月')
  end

  def year_month_to_date
    "#{year_month}01".to_date
  end

  def total_income
    total = 0
    %i(
      internet_income free_income1 free_income2 free_income3 free_income4 free_income5
    ).each do |fee|
      total += send(fee).to_i
    end
    total
  end

  def total_fee
    total = 0
    %i(
      site_fee electrical_fee gas_fee water_fee internet_fee site_controller_fee system_fee1 free_fee1 free_fee2 free_fee3 free_fee4 free_fee5
    ).each do |fee|
      total += send(fee).to_i
    end
    total
  end

  # 集計情報の取得
  # 精算書編集画面、精算書PDFで利用
  def set_analysis_info
    date_from = "#{year_month}01".to_date

    create_reservation_day_list(date_from)

    @analysis1 = totalize(date_from)
    @analysis2 = totalize(date_from.next_month)

    # 入金額 (売上合計 + その他収入) * 報酬率 - その他費用
    # @analysis1.owner_reward = ((@analysis1.owner_total + total_income) * reward_rate / 100 - total_fee).ceil.to_s(:delimited) + '円' rescue ''

    # 計算式変更 2021-12-03
    # 入金額 =（売上合計 x 報酬率）+ その他収入 - その他費用
    @analysis1.owner_reward = ((@analysis1.owner_total  * reward_rate / 100) + total_income - total_fee).ceil.to_s(:delimited) + '円' rescue ''
  end

  # 集計情報を取得して１日ごとのデータのリストを生成
  def create_reservation_day_list(date_from)
    @reservation_list = []
    date_to   = date_from.end_of_month

    # まずは対象日のReservationDateを取得して、紐付くReservationIDを抽出する
    # TODO: 2021-09-02 月マタギの表示がおかしかったので応急処置
    # そもそも、ReservationDate に Reservation をくっつけたデータを取り回す方がよかったので、今後改修があれば要検討（現状かなりややこしいデータの持ち方になっている）

    # reservation_ids = ReservationDate.where(check_in_day: date_from..date_to, room_id: room_id).pluck(:reservation_id)
    reservation_id_by_day = ReservationDate.where(check_in_day: date_from..date_to, room_id: room_id).pluck(:check_in_day, :reservation_id).to_h
    return if reservation_id_by_day.blank?

    # 検索月外への月またぎデータも取得
    reservations_by_id = Reservation.where(id: reservation_id_by_day.values.uniq)
                                    .search_not_cancel
                                    .eager_load(:reservation_dates, :nationality)
                                    .order(:check_in_date)
                                    .index_by(&:id)

    target_date = nil
    end_date = date_from.to_date.end_of_month
    (date_from.to_date..date_to.to_date).each do |date|
      # 連泊でデータが作成済（カレンダーが進んでいる）の場合はスキップする
      next if target_date && target_date > date

      id = reservation_id_by_day[date]
      target_date = push_reservation_date(date, reservations_by_id[id], end_date)
    end
  end

  def reward_rate_changed?
    reward_rate != room&.building&.reward_rate
  end

  def refresh_reward_rate!
    self.reward_rate = room&.building&.reward_rate
    self.save!
  end

  def reward_include_cleaning_fee?
    self.reward_rate == INCLUDE_CLEANING_FEE_REWARD
  end

  private

  # １日分のデータを作成してリストに格納
  def push_reservation_date(date, reservation, end_date)
    presenter = Presenter::Summary::ReservationSettlementPresenter
    target_date = date
    if reservation
      index = 0
      output_row_count = reservation.number_of_nights

      # ２泊以上の場合はカレンダーを進めながらデータを作成していく
      reservation.reservation_dates.each do |reservation_date|
        # 紐付くreservation_dateの中で、月マタギで先月の日付のデータがある場合はスキップ
        if reservation_date.check_in_day < target_date
          output_row_count -= 1
          next
        end
        # 翌月への月マタギをスキップ
        if reservation_date.check_in_day > end_date
          # この予約の１日目の宿泊データを取り出し、output_row_count エクセル行結合用の出力行数を修正（月末までの行数に変更する）
          last_reservation = @reservation_list[-index]
          last_reservation.output_row_count = index
          break
        end

        index += 1
        @reservation_list << presenter.new(target_date, index, output_row_count, reservation, reservation_date)
        target_date = target_date.tomorrow
      end
    else
      @reservation_list << presenter.new(target_date)
    end
    target_date
  end

  # def fetch_monthly_reservations(date_from)
  #   date_to = date_from.end_of_month

  #   # まずは対象日のReservationDateを取得して、紐付くReservationIDを抽出する
  #   reservation_ids = ReservationDate.where(check_in_day: date_from..date_to, room_id: room_id)
  #                                    .pluck(:reservation_id)

  #   if reservation_ids.present?
  #     # 検索月外への月またぎデータも取得
  #     reservation_by_date = Reservation.where(id: reservation_ids)
  #                                     .search_not_cancel
  #                                     .eager_load(:reservation_dates, :nationality)
  #                                     .order(:check_in_date)
  #                                     .index_by(&:check_in_date)
  #   end

  #   @monthly_reservations = []
  #   row_infos = Hash.new(nil)
  #   (date_from.to_date..date_to.to_date).each do |date|
  #     generate_reservation_date_table(date, reservation_by_date, row_infos)
  #   end
  # end

  # def generate_reservation_date_table(date, reservation_by_date, row_infos)
  #   reservation = reservation_by_date[date] if reservation_by_date.present?
  #   if reservation
  #     row_infos[:position] = 1
  #     row_infos[:row_count] = reservation.number_of_nights
  #     row_infos[:html_class] = (row_infos[:html_class] == 'odd' ? 'even' : 'odd')
  #     @monthly_reservations << Presenter::Summary::ReservationDatePresenter.new(date: date, html_class: row_infos[:html_class], reservation: reservation)
  #     return
  #   end

  #   if row_infos[:position]
  #     row_infos[:position] += 1

  #     # 連泊中の中日のケース
  #     if row_infos[:position] < row_infos[:row_count]
  #       return
  #     # 連泊最終日のケース
  #     elsif row_infos[:position] == row_infos[:row_count]
  #       row_infos[:position] = row_infos[:row_count] = nil
  #       return
  #     # 1泊の予約の翌日が予約のないケース
  #     elsif row_infos[:position] > row_infos[:row_count]
  #       row_infos[:position] = row_infos[:row_count] = nil
  #     end
  #   end
  #   # 予約がない場合は日付だけを持つオブジェクト作成
  #   row_infos[:html_class] = (row_infos[:html_class] == 'odd' ? 'even' : 'odd')
  #   @monthly_reservations << Presenter::Summary::ReservationDatePresenter.new(date: date, html_class: row_infos[:html_class])
  # end

  def totalize(date_from)
    date_to = date_from.end_of_month
    obj = OpenStruct.new(
      date_from: date_from,
      total: 0,
      target_days: date_to.day,
      working_days: 0,
      guest_count: 0,
      owner_total: 0
    )

    data = ReservationDate.search_not_cancel
                              .where(check_in_day: date_from..date_to, room_id: room_id)
                              .group(:room_id)
                              .pluck(Arel.sql(
                                'COUNT(*),
                                SUM(reservation_dates.total),
                                SUM(reservations.adult_count),
                                SUM(reservations.child_count),
                                SUM(reservation_dates.room_charge)'
                              )).first # 部屋が１つだけなので、常に１件

    return obj if data.blank?

    # 稼働日
    obj.working_days = data[0]

    # 売上合計
    obj.total = data[1]

    # 宿泊人数（ゲスト数）
    obj.guest_count = data[2].to_i + data[3].to_i

    # 報酬率計算用オーナー売上
    if reward_include_cleaning_fee?
      obj.owner_total = obj.total
    else
      obj.owner_total = data[4] # 宿泊売上
    end

    # OCC 稼働率 = 稼働日 / 日数
    occ = obj.working_days.quo(obj.target_days)
    obj.occ = ActiveSupport::NumberHelper.number_to_percentage(occ.to_f * 100, precision: 1)

    # ADR 平均客室単価 = 月間売上 / 稼働日 （四捨五入）
    adr = obj.total.quo(obj.working_days)
    obj.adr = adr.to_f.round.to_s(:delimited)

    # RevPAR 販売可能な客室1室あたりの売上 = ADR * OCC
    obj.rev_par = (adr * occ).round.to_s(:delimited)

    obj
  end

end
