# == Schema Information
#
# Table name: whole_reservations
#
#  id                      :bigint(8)        not null, primary key
#  building_id             :bigint(8)
#  status                  :string(1)        default("1"), not null
#  site_code               :string(4)        default(""), not null
#  nationality_id          :integer          default(0), not null
#  visitor_name            :string(255)      default(""), not null
#  mail_address            :string(255)      default(""), not null
#  tel                     :string(255)      default(""), not null
#  adult_count             :integer          default(0), not null
#  child_count             :integer          default(0), not null
#  room_charge             :integer          default(0), not null
#  cleaning_fee            :integer          default(0), not null
#  total                   :integer          default(0), not null
#  reserve_date            :date
#  check_in_date           :date
#  check_out_date          :date
#  number_of_nights        :integer          default(1), not null
#  note                    :text(65535)
#  created_user_id         :integer          default(0), not null
#  updated_user_id         :integer          default(0), not null
#  lock_version            :integer          default(0), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  scheduled_check_in_time :datetime
#  accommodation_tax       :integer
#  registration_type       :integer          default(0)
#  meal_fee                :integer          default(0), not null
#  visitor_name_kana       :string(255)      default(""), not null
#

class WholeReservation < ApplicationRecord

  # 税金計算用モジュール
  include ReservationCalc

  # associations
  belongs_to :building
  belongs_to :nationality

  has_many :reservations

  # delegate
  delegate :name, to: :nationality, prefix: :nationality, allow_nil: true

  # scopes
  scope :search_not_cancel, -> { where(status: Reservation::STSATUS_LIST_NOT_CANCEL) }

  # callbacks
  before_save :update_user

  # ▼登録・更新処理

  # 新規登録・更新
  def self.create_or_update(attributes, is_new: false)
    whole_reservation = self.find_or_initialize_by(id: attributes[:id])
    whole_reservation.match_attributes = attributes
    whole_reservation.check_out_date = whole_reservation.check_in_date + whole_reservation.number_of_nights
    whole_reservation.total = whole_reservation.room_charge + whole_reservation.cleaning_fee + whole_reservation.meal_fee
    whole_reservation.calc_accommodation_tax
    whole_reservation.save!

    Reservation.create_or_update_by_whole_reservation(whole_reservation: whole_reservation, is_new: is_new)
    whole_reservation
  end

  # そのまま更新
  def self.update_basic(attributes)
    whole_reservation = self.search_not_cancel.where(id: attributes[:id]).take
    # whole_reservation.update_user
    whole_reservation.match_attributes = attributes
    whole_reservation.save!
    whole_reservation
  end

  # 一棟貸情報と紐付く予約情報のステータスを一括更新
  # 現状のステータスを考慮しないので取扱注意
  def self.update_all_status(whole_reservation_id:, target_status:)
    whole_reservation = self.eager_load(:reservations).find_by(id: whole_reservation_id)
    whole_reservation.status = target_status
    whole_reservation.reservations.each {|r| r.update!(status: target_status)}
    whole_reservation.save!
  end

  def total_count
    self.adult_count + self.child_count
  end

  # whole_reservatonの更新後、変更したカラムだけののハッシュを生成する
  # reservation一括更新用
  def changed_attributes
    attributes = {}
    exclusion_key = %W(id lock_version created_at updated_at created_user_id updated_user_id)
    self.previous_changes.each do |k, v|
      attributes[k] = v[1] unless exclusion_key.include?(k)
    end
    attributes
  end

  def available?
    self.status != Settings.RESERVATION.STATUS.CANCEL
  end

  def status_name
    Reservation::STSATUS_NAMES[self.status]
  end
end
