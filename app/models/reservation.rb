# == Schema Information
#
# Table name: reservations
#
#  id                      :bigint(8)        not null, primary key
#  room_id                 :bigint(8)
#  whole_reservation_id    :integer
#  status                  :string(1)        default("1"), not null
#  site_code               :string(4)        default(""), not null
#  nationality_id          :integer          default(0), not null
#  visitor_name            :string(255)      default(""), not null
#  mail_address            :string(255)      default(""), not null
#  tel                     :string(255)      default(""), not null
#  adult_count             :integer          default(0), not null
#  child_count             :integer          default(0), not null
#  room_charge             :integer          default(0), not null
#  cleaning_fee            :integer          default(0), not null
#  meal_fee                :integer          default(0), not null
#  total                   :integer          default(0), not null
#  handling_charge_rate    :float(24)
#  reserve_date            :date
#  check_in_date           :date
#  check_out_date          :date
#  number_of_nights        :integer          default(1), not null
#  note                    :text(65535)
#  created_user_id         :integer          default(0), not null
#  updated_user_id         :integer          default(0), not null
#  lock_version            :integer          default(0), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  scheduled_check_in_time :datetime
#  accommodation_tax       :integer
#  registration_type       :integer          default(0)
#  visitor_name_kana       :string(255)      default(""), not null
#  point_use               :integer
#  owner_note              :text(65535)
#

class Reservation < ApplicationRecord

  # 税金計算用モジュール
  include ReservationCalc

  # associations
  belongs_to :room
  belongs_to :nationality
  belongs_to :whole_reservation

  has_many :reservation_dates

  # delegates
  delegate :name, to: :nationality, prefix: :nationality, allow_nil: true

  # const
  # [1,2...]
  STSATUS_LIST_NOT_CANCEL = Settings.RESERVATION.STATUS.map {|k, v| v if k != :CANCEL}.compact.freeze

  # {"1"=>"未清掃", "2"=>"CI可"...}
  STSATUS_NAMES = Settings.RESERVATION.STATUS.to_h.inject({}) { |hash, (k, v)|
    hash.merge(v => I18n.t("settings.reservation.status.#{k.to_s.downcase}"))
  }.freeze

  # {"100"=>"Airbnb", "200"=>"Booking"...}
  SITE_NAMES = Settings.SITE.CODE.to_h.inject({}) { |hash, (k, v)|
    hash.merge(v => I18n.t("settings.site.#{k.to_s.downcase}"))
  }.freeze

  # scopes
  scope :search_not_cancel, -> { where(status: STSATUS_LIST_NOT_CANCEL) }
  scope :check_in_day_is, -> day { eager_load(:reservation_dates).merge(ReservationDate.check_in_day_is day) }

  # callbacks
  # TODO モジュール可を検討？？
  before_save :update_user

  # ▼登録・更新処理

  # 一棟貸情報の登録・編集による予約情報の一括更新
  # 非表示の部屋の予約は作成しない
  def self.create_or_update_by_whole_reservation(whole_reservation:, is_new:)
    whole_reservation_attributes = whole_reservation.attributes.symbolize_keys
    whole_reservation_attributes[:whole_reservation_id] = whole_reservation.id
    whole_reservation_attributes.delete(:id)
    whole_reservation_attributes.delete(:accommodation_tax)
    rooms = Room.visible.where(building_id: whole_reservation.building_id)
    fees = divide_reservation_fee(whole_reservation: whole_reservation, count: rooms.size)

    reservation_hash = whole_reservation.reservations.index_by(&:room_id) unless is_new
    rooms.each_with_index do |room, idx|
      # Reservationの登録
      attributes = whole_reservation_attributes.merge(fees[idx])
      attributes[:room_id] = room.id
      if reservation_hash
        # 既存情報の更新
        reservation = reservation_hash[room.id]
        reservation.update_reservation(attributes)
      else
        # 新規登録時は次の予約のステータスを未清掃に戻す
        reservation = Reservation.create_or_update(attributes)
        reservation.restore_status_by_interrupt
      end
      ReservationDate.reverse_entries(reservation)
    end
  end

  def self.update_all_basics(whole_reservation_id:, attr:)
    self.search_not_cancel.where(whole_reservation_id: whole_reservation_id).each do |reservation|
      # reservation.update_user
      reservation.match_attributes = attr.merge(set_nil: true)
      reservation.save!
    end
  end

  def self.update_share_price(whole_reservation_id:, attr_list:)
    reservations = self.search_not_cancel.where(whole_reservation_id: whole_reservation_id).eager_load(room: :building).index_by(&:id)
    attr_list.each do |attr|
      reservation = reservations[attr.fetch(:reservation_id).to_i]
      reservation.match_attributes = attr

      # 変更がなければ明示的にSAVEせず、ReservationDateの洗い替え処理をさせない
      next unless reservation.changed?
      # reservation.update_user
      reservation.save!
      ReservationDate.reverse_entries(reservation)
    end
  end

  def self.create_or_update(attributes)
    reservation = self.find_or_initialize_by(id: attributes[:id])
    reservation.match_attributes = attributes
    reservation.status ||= Settings.RESERVATION.STATUS.PREPARING
    reservation.check_out_date = reservation.check_in_date + reservation.number_of_nights
    reservation.total = reservation.room_charge + reservation.cleaning_fee + reservation.meal_fee
    reservation.handling_charge_rate = Settings.SITE.RATE[reservation.site_code]
    reservation.calc_accommodation_tax unless reservation.whole?
    reservation.restore_status_by_interrupt if reservation.new_record?
    reservation.save!
    reservation
  end

  def self.updatable_status?(status:)
    setting = Settings.RESERVATION.STATUS
    [setting.PREPARING, setting.POSSIBLE, setting.STAYING].include?(status.to_s)
  end

  def update_reservation(attributes)
    # self.update_user
    self.match_attributes = attributes
    # self.status ||= Settings.RESERVATION.STATUS.PREPARING
    self.check_out_date = self.check_in_date + self.number_of_nights
    self.total = self.room_charge + self.cleaning_fee + self.meal_fee
    self.handling_charge_rate = Settings.SITE.RATE[self.site_code]
    self.save!
    self
  end

  # 請求内訳単独の更新
  def update_reservation_dates
    self.reservation_dates.each do |rd|
      rd.update_totla(handling_charge_rate: self.handling_charge_rate)
    end
    self.touch
    self.save!
  end

  # 今日チェックアウト予定のデータを自動でステータス更新
  # gem wheneverによって１3時にcronが起動してRakeタスク reservation_status から呼ばれる
  # @return [Array<Integer>] 更新件数を返す
  def self.update_status_to_check_out
    count_list = []
    update_attr = {status: Settings.RESERVATION.STATUS.CO, updated_user_id: 0}
    condition_attr = {check_out_date: Date.today, status: Settings.RESERVATION.STATUS.STAYING}
    count_list << Reservation.where(condition_attr).update_all(update_attr)
    count_list << WholeReservation.where(condition_attr).update_all(update_attr)
  end

  # 予約情報の割り込み時、次の予約がCI可（清掃済）の場合、該当する次の予約情報を未清掃に戻す
  def restore_status_by_interrupt
    # とりあえず自身が未清掃以外の場合はスルー（新規登録時しか呼ばない想定）
    return if self.status == Settings.RESERVATION.STATUS.PREPARING
    target = Reservation.ransack(
               room_id_eq: self.room_id,
               status_eq: Settings.RESERVATION.STATUS.POSSIBLE,
               check_in_date_gteq: self.check_out_date)
             .result.first
     target.status = Settings.RESERVATION.STATUS.PREPARING
     target.save!
  end

  # ▲ 登録・更新処理

  # 分配・計算処理

  # 一棟貸の部屋数で金額、人数を分割し、余りは最終日のデータに加算する
  # ex) 対象の値が割る値より小さい場合： 3/5 >> 0, 3%5 >> 3 となる
  def self.divide_reservation_fee(whole_reservation:, count:)
    # 除算と剰余
    # 宿泊
    room_charge_list = CountUtil.divide_with_surplus(value: whole_reservation.room_charge, count: count)

    # 清掃費
    cleaning_fee_list = CountUtil.divide_with_surplus(value: whole_reservation.cleaning_fee, count: count)

    # 食事代
    meal_fee_list = CountUtil.divide_with_surplus(value: whole_reservation.meal_fee, count: count)

    # 人数
    adult_count_list = CountUtil.divide_with_surplus(value: whole_reservation.adult_count, count: count)
    child_count_list = CountUtil.divide_with_surplus(value: whole_reservation.child_count, count: count)

    fees = []
    (0...count).each do |idx|
      fee = {}
      fee[:room_charge]  = room_charge_list[idx]
      fee[:cleaning_fee] = cleaning_fee_list[idx]
      fee[:meal_fee]     = meal_fee_list[idx]
      fee[:adult_count]  = adult_count_list[idx]
      fee[:child_count]  = child_count_list[idx]
      fee[:total] = (fee[:room_charge] + fee[:cleaning_fee] + fee[:meal_fee])
      fees << fee
    end
    fees
  end

  # その他メソッド

  def total_count
    self.adult_count + self.child_count
  end

  def handling_charge
    self.reservation_dates.inject(0) { |sum, rd| sum + rd.handling_charge }
  end

  # 一棟貸判定
  # @return [Boolean] 一棟貸ならtrue
  def whole?
    self.whole_reservation_id.present?
  end

  def available?
    self.status != Settings.RESERVATION.STATUS.CANCEL
  end

  def preparing?
    self.status == Settings.RESERVATION.STATUS.PREPARING
  end

  def possible?
    self.status == Settings.RESERVATION.STATUS.POSSIBLE
  end

  def staying?
    self.status == Settings.RESERVATION.STATUS.STAYING
  end

  def status_name
    STSATUS_NAMES[self.status]
  end

  def site_name
    SITE_NAMES[self.site_code]
  end

  def display_visitor_name
    suffix = "(#{visitor_name_kana})" if visitor_name_kana.present?
    "#{visitor_name}#{suffix}"
  end

end
