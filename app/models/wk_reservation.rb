# == Schema Information
#
# Table name: wk_reservations
#
#  id                :bigint(8)        not null, primary key
#  check_in_date     :date
#  building_code     :string(255)
#  building_id       :integer
#  room_no           :string(255)
#  room_id           :integer
#  number_of_nights  :integer
#  visitor_name      :string(255)
#  nationality       :string(255)
#  nationality_id    :integer
#  mail_address      :string(255)
#  tel               :string(255)
#  adult_count       :integer
#  child_count       :integer
#  room_charge       :integer
#  cleaning_fee      :integer
#  total             :integer
#  site_code         :string(255)
#  site_name         :string(255)
#  reserve_date      :date
#  note              :text(65535)
#  created_user_id   :integer          default(0), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  visitor_name_kana :string(255)
#

class WkReservation < ApplicationRecord

  # before_save :update_user

end
