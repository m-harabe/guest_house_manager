# == Schema Information
#
# Table name: areas
#
#  id              :bigint(8)        not null, primary key
#  name            :string(10)       default(""), not null
#  area_cd         :string(6)        default(""), not null
#  is_visible      :boolean          default(TRUE), not null
#  created_user_id :integer          default(0), not null
#  updated_user_id :integer          default(0), not null
#  lock_version    :integer          default(0), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Area < ApplicationRecord
  include Concerns::Visible

  # associations
  has_many :buildings

  # scopes
  scope :selecter, -> { self.order(:area_cd) }

  # ------------------------------------------------------------------
  # Validations
  # ------------------------------------------------------------------

  # presence
  validates :area_cd,         presence: true
  validates :name,            presence: true

  # uniqueness
  validates :area_cd,         uniqueness: true, on: :create

  # length
  validates :area_cd,         length: { maximum: 6 }
  validates :name,            length: { maximum: 10 }

  # inclusion
  validates :is_visible,     inclusion: { in: [true, false] }, allow_nil: false

  validate :validate_upadte_attr, on: :update

  def validate_upadte_attr
    return unless errors.empty?
    return unless area_cd_changed?

    if Area.where(area_cd: area_cd).where.not(id: id).present?
      errors.add(:area_cd, "はすでに存在します。")
    end
  end
end
