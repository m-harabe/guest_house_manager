# == Schema Information
#
# Table name: rooms
#
#  id                   :bigint(8)        not null, primary key
#  building_id          :bigint(8)
#  room_no              :integer          default(0), not null
#  name                 :string(20)       default(""), not null
#  is_visible           :boolean          default(TRUE), not null
#  note                 :text(65535)
#  created_user_id      :integer          default(0), not null
#  updated_user_id      :integer          default(0), not null
#  lock_version         :integer          default(0), not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  suitebook_room_idnum :integer
#

class Room < ApplicationRecord
  include Concerns::Visible
  # Profileを使うポリモーフィックなモデルはProfilableをincludeする
  include Profilable

  # associations
  belongs_to :building

  # has_many :suitebook_reservations, foreign_key: "room_idnum"

  # delegates
  delegate :name,       to: :building, prefix: :building
  delegate :code,       to: :building, prefix: :building
  delegate :room_count, to: :building, prefix: :building

  # scopes
  scope :with_building, -> { eager_load(:building).merge(Building.visible.display_order).display_order }
  scope :with_invisible_building, -> { eager_load(:building).merge(Building.display_order).display_order }
  scope :display_order, -> { order(:room_no, :id) }
  scope :area_id_is, -> area_id { joins(:building).merge(Building.area_id_is area_id) }

  # 登録更新処理
  def self.update_room(attr:)
    room = self.find_or_initialize_by(id: attr[:room_id])
    room.building_id = attr[:building_id]
    room.name        = attr[:name]
    room.room_no     = attr[:room_no]
    room.note        = attr[:note]
    room.suitebook_room_idnum = attr[:suitebook_room_idnum]

    # Profileの更新・登録
    room.profile = Profile.new unless room.profile
    room.profile.set_attributes(attr: attr)
    room.save!
    # 施設の部屋数更新
    Building.update_room_count(id: room.building_id)
    room
  end

  def full_code_name
    room_name = "#{self.building_code}: #{self.building_name}"
    room_name << '-' << self.name if self.name.present?
    room_name
  end

  def full_code_name_with_underscore
    room_name = "#{self.building_code}_#{self.building_name}"
    room_name << '_' << self.name if self.name.present?
    room_name
  end

  def full_code_name_with_space
    room_name = "#{self.building_code} #{self.building_name}"
    room_name << ' ' << self.name if self.name.present?
    room_name
  end

end
