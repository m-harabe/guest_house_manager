# Modelの共通処理
module Concerns::Visible
  extend ActiveSupport::Concern
  included do
    scope :visible, -> { where(is_visible: true) }
  end
end
