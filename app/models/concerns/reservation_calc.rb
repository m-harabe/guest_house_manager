# Reservation, WholeReservation 共通の計算メソッド
#
module ReservationCalc

  LIMITS = [20000, 50000]
  TAXES  = [  200,   500,   1000]

  # 宿泊税計算
  # 宿泊者1人1泊につき宿泊料金が
  # - 2万円未満のもの 200円
  # - 2万円以上5万円未満のもの 500円
  # - 5万円以上のもの 1,000円
  def calc_accommodation_tax
    total_count = self.adult_count + self.child_count
    target = self.total / self.number_of_nights / total_count
    base_tax = nil
    # 金額設定
    LIMITS.each_with_index do |limit, idx|
      if target < limit
        base_tax = TAXES[idx]
        break
      end
    end
    base_tax ||= TAXES[-1]
    self.accommodation_tax = base_tax * total_count * self.number_of_nights
  rescue => ex
    self.accommodation_tax = 0
    ErrorUtil.error_log_and_notify(ex: ex, message: '宿泊税の計算でエラーが発生したので0円で更新したよ。')
  end
end
