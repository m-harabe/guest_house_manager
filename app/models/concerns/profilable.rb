# インターフェースを明確化するために、moduleで固める
module Profilable
  extend ActiveSupport::Concern

  included do
    # has_many :profiles, as: :profilable
    has_one :profile, as: :profilable
  end

  # def sender_name
  #   # オーバーライドされなかった場合はエラーが上がるようにしておく
  #   raise NotImplementedError
  # end

  # def sender_email
  #   raise NotImplementedError
  # end
end