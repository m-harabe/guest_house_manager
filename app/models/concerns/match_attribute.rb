module Concerns::MatchAttribute

  # ハッシュを受け取ってキーとマッチするカラムに値をセット
  # @params[Hash] attributes key:カラム名, value: セットする値
  #  - set_nil:       渡された値がnilの場合、set_nilがfalseであればそのカラムを更新しない。trueであればnilで更新
  #  - except_blank:  渡された値がblankの場合、except_blankがfalseであればそのカラムを更新しない。trueであればで更新
  def match_attributes=(attributes)
    return if attributes.nil?

    set_nil = attributes[:set_nil] || false
    except_blank = attributes[:except_blank] || false
    common_field = [:set_nil, :except_blank, :id, :created_at, :updated_at, :created_user_id, :updated_user_id, :lock_version, :deleted_at]

    attributes.each do |key, value|
      next if value.nil? && !set_nil
      next if value.blank? && except_blank
      next if common_field.index(key.to_sym)

      send(key.to_s + '=', value) rescue nil
    end
  end
end
