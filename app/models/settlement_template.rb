# == Schema Information
#
# Table name: settlement_templates
#
#  id                  :bigint(8)        not null, primary key
#  language            :string(255)      default(""), not null
#  title               :string(255)      default(""), not null
#  honorific           :string(255)      default(""), not null
#  building            :string(255)      default(""), not null
#  address             :string(255)      default(""), not null
#  company_name        :string(255)      default(""), not null
#  company_info1       :string(255)      default(""), not null
#  company_info2       :string(255)      default(""), not null
#  company_info3       :string(255)      default(""), not null
#  visitor_name        :string(255)      default(""), not null
#  nationality         :string(255)      default(""), not null
#  number_of_nights    :string(255)      default(""), not null
#  total_count         :string(255)      default(""), not null
#  count               :string(255)      default(""), not null
#  adult_count         :string(255)      default(""), not null
#  child_count         :string(255)      default(""), not null
#  room_charge         :string(255)      default(""), not null
#  cleaning_fee        :string(255)      default(""), not null
#  total               :string(255)      default(""), not null
#  site                :string(255)      default(""), not null
#  handling_charge     :string(255)      default(""), not null
#  reservation_date    :string(255)      default(""), not null
#  analysis_title      :string(255)      default(""), not null
#  sales               :string(255)      default(""), not null
#  target_days         :string(255)      default(""), not null
#  working_days        :string(255)      default(""), not null
#  guest_count         :string(255)      default(""), not null
#  occ                 :string(255)      default(""), not null
#  adr                 :string(255)      default(""), not null
#  rev_par             :string(255)      default(""), not null
#  other_income        :string(255)      default(""), not null
#  other_cost          :string(255)      default(""), not null
#  reward_rate         :string(255)      default(""), not null
#  payment_amount      :string(255)      default(""), not null
#  next_month_title    :string(255)      default(""), not null
#  internet_income     :string(255)      default(""), not null
#  free_income1        :string(255)      default(""), not null
#  free_income2        :string(255)      default(""), not null
#  free_income3        :string(255)      default(""), not null
#  free_income4        :string(255)      default(""), not null
#  free_income5        :string(255)      default(""), not null
#  total_income        :string(255)      default(""), not null
#  site_fee            :string(255)      default(""), not null
#  electrical_fee      :string(255)      default(""), not null
#  gas_fee             :string(255)      default(""), not null
#  water_fee           :string(255)      default(""), not null
#  internet_fee        :string(255)      default(""), not null
#  site_controller_fee :string(255)      default(""), not null
#  free_fee1           :string(255)      default(""), not null
#  free_fee2           :string(255)      default(""), not null
#  free_fee3           :string(255)      default(""), not null
#  free_fee4           :string(255)      default(""), not null
#  free_fee5           :string(255)      default(""), not null
#  total_fee           :string(255)      default(""), not null
#  payee               :string(255)      default(""), not null
#  note1               :string(255)      default(""), not null
#  note2               :string(255)      default(""), not null
#  note3               :string(255)      default(""), not null
#  note4               :string(255)      default(""), not null
#  created_user_id     :integer          default(0), not null
#  updated_user_id     :integer          default(0), not null
#  lock_version        :integer          default(0), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class SettlementTemplate < ApplicationRecord

  LANGUAGE_LIST = Settings.SETTLEMENT_LANGUAGE.to_h.values

  # associations

  # scopes

  # ------------------------------------------------------------------
  # Validations
  # ------------------------------------------------------------------

  # presence
  # validates :language,        presence: true

end
