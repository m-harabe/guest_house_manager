class ApplicationRecord < ActiveRecord::Base
  include Concerns::MatchAttribute

  self.abstract_class = true

  # save 時にバリデーションを行わない
  def save(*params)
    super :validate => false
  end

  def save!(*params)
    super :validate => false
  end

  # save 時デフォルトでバリデーションを行わない
  # def save(*params, validate: false)
  #   super :validate => validate
  # end

  # def save!(*params, validate: false)
  #   super :validate => validate
  # end

  # SAVE時にユーザーIDを保持
  def update_user
    user_id = User.current_user&.id || 0
    self.created_user_id = user_id if self.new_record?
    self.updated_user_id = user_id
  end

  # scope :where_with_exception!, -> (args) { where_with_exception!(args) }

  # def self.human_attribute_name(attribute, options = {})
  #  I18n.t("activerecord.attributes.#{self.class.to_s.underscore}.#{attribute}")
  # end

end
