class FileObjects::SuitebookReservationCsv
  include ActiveModel::Model
  include ActiveModel::Validations

  # key: SuitebookReservation のカラム, value: CSVのヘッダーの文字列
  # CSVの列順に依存しないので、列を変えてもインポート可
  # CSVからはこの項目しか抽出しないので、ここで定義した項目以外の列は無視
  # CSVカラムが増えた場合はこのハッシュに項目を追加するだけで対応可
  COLUMN_NAME_TO_CSV_HEADER = {
    suitebook_idnum:         "suitebook予約ID",
    site_idnum:              "予約チャネル ID",
    site_name:               "予約チャネル",
    number_of_nights:        "泊数",
    check_in_date:           "チェックイン日",
    check_out_date:          "チェックアウト日",
    scheduled_check_in_time: "チェックイン予定時刻",
    reservation_cd:          "予約コード",
    contract_style:          "契約形態",
    cancel:                  "キャンセル",
    is_archive:              "アーカイブ済み",
    visitor_name:            "ゲスト名",
    visitor_name_kana:       "ゲスト名フリガナ",
    postal_code:             "郵便番号",
    address:                 "住所/国籍",
    tel:                     "電話番号",
    mail_address:            "メールアドレス",
    total_count:             "宿泊人数",
    adult_count:             "大人人数",
    child_count:             "子供人数",
    property_name:           "建物名",
    building_idnum:          "物件ID",
    building_name:           "物件名",
    room_idnum:              "部屋ID",
    room_name:               "部屋名",
    site_account:            "ホストアカウント",
    property_owner_idnum:    "プロパティオーナーID",
    owner_company_name:      "プロパティーオーナー 会社名",
    owner_name:              "プロパティーオーナー 代表者・契約者名",
    currency:                "通貨",
    room_charge:             "宿泊料金",
    cleaning_fee:            "清掃費",
    other_cost:              "部屋代金以外の費用",
    handling_charge:         "手数料",
    point_use:               "ポイント利用",                   # 無視（履歴表示のみ）
    total:                   "ゲストへの請求金額(ポイント割引前)", # 合計として使用
    total_minus_point:       "ゲストへの請求金額(ポイント割引後)", # 無視（履歴表示のみ）
    revenue:                 "あなたの収益",
    cancel_fee:              "キャンセル料",
    cancel_charge:           "手数料 (キャンセル時)",
    cancel_revenue:          "あなたの収益 (キャンセル時)",
    payment:                 "決済",
    data_careated_at:        "予約作成日",
    plan:                    "プラン名",
    owner_note:              "オーナー向けドキュメント",
    memo:                    "予約メモ"

    # その他未登録項目
    # 子予約
    # 要注意
    # 法人名
    # プランコード
    # "チェックアウト予定時刻"
    # "チェックイン時間"
    # "チェックアウト時間"
    # 事前チェックイン時間
    # 事前チェックインURL
    # キャンセル理由
    # "幼児人数"
    # キャンセル日時
    # 精算状態
    # 締め状態
    # etc...
  }

  # CSVに含まれる項目でBooleanに変換するリスト
  BOOLEAN_COLUMNS = %i()

  # 更新対象外リスト
  NOT_UPDATABLE_COLUMNS = %i( row_index, full_error_messages )

  # sjisに変換できない文字コードを含む可能性があるカラム
  SJISABLE_OUTPUT_COLUMNS = %i()

  # ファイル項目定義
  attr_accessor *COLUMN_NAME_TO_CSV_HEADER.keys
  attr_accessor :row_index,          # CSVの行番号
                :room_id,
                :building_id,
                :full_error_messages # エラーメッセージの配列

  # ------------------------------------------------------------------
  # Validations
  # ------------------------------------------------------------------

  # presence
  validates :suitebook_idnum, presence: true

  # length
  # validates :common_service_fee,             length: { maximum: 5 }

  # validate :validate_room_id

  # 初期化処理 CSVの行データから必要なカラムのみをセットする
  def initialize(row:, row_index:)
    @row_index = row_index
    COLUMN_NAME_TO_CSV_HEADER.each do |column, header_label|
      send(column.to_s + '=', row[header_label]) rescue nil
    end
    # set_boolean

    set_adult_child_count
    set_room_id
  end

  # モデルの更新に必要なハッシュを返す
  # 更新対象外リストのカラムを除外する
  # @return [Hash] key: インスタンス変数名（シンボル） value: 格納されている値
  def attributes
    return_attributes = {}
    instance_variables.each do |attr|
      next if NOT_UPDATABLE_COLUMNS.index(attr)
      return_attributes[attr[1..-1].to_sym] = instance_variable_get(attr)
    end
    return_attributes
  end

  def valid?
    result = super
    return result if result

    @full_error_messages = []
    self.errors.messages.each do |column, message_arr|
      column_name = COLUMN_NAME_TO_CSV_HEADER[column] || ""
      message_arr.each do |message|
        error_str = "#{row_index}行目: #{column_name}#{message}"
        @full_error_messages << error_str
      end
    end
    result
  end

  private

  def set_boolean
    BOOLEAN_COLUMNS.each do |column|
      value = send(column.to_s).to_bool
      # p "set #{column} :    #{value}"
      send(column.to_s + '=', value) rescue nil
    end
  end

  def set_adult_child_count
    @child_count ||= 0
    @adult_count ||= total_count.to_i
  end

  def set_room_id
    room = Room.select(:id, :building_id).visible.find_by(suitebook_room_idnum: building_idnum)
    if room
      @room_id = room.id
      @building_id = room.building_id
    end
  end

  # ----------------------
  # 個別バリデーションメソッド
  # ----------------------


end
