# == Schema Information
#
# Table name: reservation_dates
#
#  id              :bigint(8)        not null, primary key
#  reservation_id  :bigint(8)
#  building_id     :integer          default(0), not null
#  room_id         :integer          default(0), not null
#  check_in_day    :date
#  room_charge     :integer          default(0), not null
#  cleaning_fee    :integer          default(0), not null
#  meal_fee        :integer          default(0), not null
#  total           :integer          default(0), not null
#  handling_charge :decimal(7, 2)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class ReservationDate < ApplicationRecord

  belongs_to :reservation
  belongs_to :room
  has_one :nationality, through: :reservation

  scope :room_reservation, -> (attributes) { search_room_reservation(attributes) }
  scope :search_not_cancel, -> { eager_load(:reservation).merge(Reservation.search_not_cancel) }
  scope :check_in_day_is, -> day { where(check_in_day: day) }

  # ReservationDateの洗い替え処理
  # @params reservation [Reservation] 必ず、eager_load(room: :building)が必要
  def self.reverse_entries(reservation)
    # OPTIMIZE: 該当のデータがなくてもDELETE文を発行しているが、一度SELECTしてあればDELETEするのとどっちがいのか？？
    self.where(reservation_id: reservation.id).delete_all
    check_in_day = reservation.check_in_date
    fees = divide_reservation_fee(reservation)
    (1..reservation.number_of_nights).each do |idx|
      check_in_day += 1 if idx > 1
      self.create!(
        check_in_day: check_in_day,
        reservation_id: reservation.id,
        building_id: reservation.room.building.id,
        room_id: reservation.room.id,
        room_charge: fees[idx - 1][:room_charge],
        cleaning_fee: fees[idx - 1][:cleaning_fee],
        meal_fee: fees[idx - 1][:meal_fee],
        total: fees[idx - 1][:total],
        handling_charge: fees[idx - 1][:total] * reservation.handling_charge_rate
      )
    end
  end

  # 泊数分で金額を分割し、余りは最終日のデータに加算する
  def self.divide_reservation_fee(reservation)
    # 宿泊費
    room_charge_list = CountUtil.divide_with_surplus(value: reservation.room_charge, count: reservation.number_of_nights)

    # 清掃費
    cleaning_fee_list = CountUtil.divide_with_surplus(value: reservation.cleaning_fee, count: reservation.number_of_nights)

    # 食事代
    meal_fee_list = CountUtil.divide_with_surplus(value: reservation.meal_fee, count: reservation.number_of_nights)

    fees = []
    (0...reservation.number_of_nights).each do |idx|
      fee = {}
      fee[:room_charge]  = room_charge_list[idx]
      fee[:cleaning_fee] = cleaning_fee_list[idx]
      fee[:meal_fee]     = meal_fee_list[idx]
      fee[:total]        = (fee[:room_charge] + fee[:cleaning_fee] + fee[:meal_fee])
      fees << fee
    end
    fees
  end

  # 重複検索
  def self.search_room_reservation(**args)
    conditions = {reservation_status_in: Reservation::STSATUS_LIST_NOT_CANCEL}
    conditions[:room_id_eq] = args[:room_id] if args[:room_id]
    conditions[:building_id_eq] = args[:building_id] if args[:building_id]
    conditions[:check_in_day_gteq] = args[:check_in_date]
    conditions[:check_in_day_lteq] = args[:check_in_date] + (args[:number_of_nights] - 1)
    conditions[:reservation_id_not_eq] = args[:reservation_id] if args[:reservation_id]
    self.ransack(conditions).result
  end

  # 金額更新
  def update_totla(handling_charge_rate:)
    self.total = self.room_charge + self.cleaning_fee + self.meal_fee
    self.handling_charge = self.total * handling_charge_rate
    self.save!
  end
end
