# == Schema Information
#
# Table name: buildings
#
#  id              :bigint(8)        not null, primary key
#  owner_id        :integer
#  code            :string(10)       default(""), not null
#  name            :string(20)       default(""), not null
#  postal_code     :string(8)        default(""), not null
#  address         :string(256)      default(""), not null
#  room_count      :integer          default(1), not null
#  is_visible      :boolean          default(TRUE), not null
#  note            :text(65535)
#  created_user_id :integer          default(0), not null
#  updated_user_id :integer          default(0), not null
#  lock_version    :integer          default(0), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  area_id         :integer
#  reward_rate     :float(24)        default(0.0), not null
#

class Building < ApplicationRecord
  include Concerns::Visible
  # Profileを使うポリモーフィックなモデルはProfilableをincludeする
  include Profilable

  # associations
  has_many :rooms
  belongs_to :owner, required: false
  belongs_to :area, required: false

  # delegates

  # scopes
  scope :display_order, -> { order(:code, :id) }
  scope :area_id_is, -> area_id { where(area_id: area_id).visible.display_order }

  # callbacks
  before_save :update_user

  # 施設登録・更新
  # 新規登録時のみRoomを登録
  def self.update_building(attr:)
    building = self.find_or_initialize_by(id: attr[:id])
    building.code        = attr[:code]
    building.owner_id    = attr[:owner_id]
    building.name        = attr[:name]
    building.reward_rate = attr[:reward_rate]
    building.postal_code = attr[:postal_code]
    building.address     = attr[:address]
    building.note        = attr[:note]

    # Profileの更新・登録
    building.profile = Profile.new unless building.profile
    building.profile.set_attributes(attr: attr)

    # 新規の場合は部屋作成
    if building.rooms.blank?
      # パスワードはデフォルト部屋情報へコピーしない
      attr.delete(:secret_number)
      room = Room.new(room_no: 1)
      room.profile = Profile.new
      room.profile.set_attributes(attr: attr)
      building.rooms << room
    end
    building.tap{ |b| b.save! }
  end
  # def self.update_building(attributes:)
  #   building = self.find_or_initialize_by(id: attributes[:id])
  #   attributes = attributes.merge(set_nil: true)
  #   building.match_attributes = attributes
  #   # Profileの更新・登録
  #   building.profile = Profile.new unless building.profile
  #   building.profile.match_attributes = attributes

  #   # 新規の場合は部屋作成
  #   if building.rooms.blank?
  #     room = Room.new(room_no: 1)
  #     room.profile = Profile.new
  #     room.profile.match_attributes = attributes
  #     building.rooms << room
  #   end
  #   building.tap{ |b| b.save! }
  # end

  def self.update_room_count(id:)
    building = self.eager_load(:rooms).find_by(id: id)
    building.room_count = building.rooms.count
    building.tap{ |b| b.save! }
  end

  def code_name
    self.code + self.name
  end

end
