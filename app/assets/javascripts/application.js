// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require activestorage


// ▼ node_module 配下のファイル ▼

//= require bootstrap/dist/js/bootstrap.min
//= require admin-lte/dist/js/adminlte.min
//= require admin-lte/plugins/iCheck/icheck
//= require datatables.net/js/jquery.dataTables
//= require datatables.net-bs/js/dataTables.bootstrap

//= require bootstrap-datepicker/dist/js/bootstrap-datepicker.min
//= require bootstrap-datepicker/dist/locales/bootstrap-datepicker.ja.min
//= require select2/dist/js/select2.min
//= require select2/dist/js/i18n/ja
//= require bootstrap-timepicker/js/bootstrap-timepicker.min

// ▼ vendor assets 以下 ▼
//= require bootstrap-maxlength
//= require moment-with-locales
//= require jquery.cookie


// ▼ Gem ▼
//= require spin
//= require jquery.spin

// require jquery.remotipart

//= require_tree .