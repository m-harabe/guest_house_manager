$ ->
  # 予定CI時刻タイムピッカー
  # bootstrap-timepicker by https://jdewit.github.io/bootstrap-timepicker/
  $('#suitebook_reservation_entry_display_check_in_time').timepicker
    minuteStep: 5
    template: 'dropdown'
    appendWidgetTo: 'body'
    showMeridian: false
    modalBackdrop: true
    defaultTime: false

  # 時刻クリアボタン
  $('#time_clear').click ->
    $('#suitebook_reservation_entry_display_check_in_time').val ''

  # 自動計算処理
  $('#suitebook_reservation_entry_adult_count').change ->
    getTotalCount()

  $('#suitebook_reservation_entry_child_count').change ->
    getTotalCount()

  $('#suitebook_reservation_entry_room_charge').change ->
    getTotal()

  $('#suitebook_reservation_entry_cleaning_fee').change ->
    getTotal()


getTotalCount =  ->
  # +演算子(Numberと多分同じ)でint型に変換
  adult_count = +$('#suitebook_reservation_entry_adult_count').val()
  child_count = +$('#suitebook_reservation_entry_child_count').val()
  $('#suitebook_reservation_entry_total_count').val(adult_count + child_count)

getTotal = ->
  room_charge  = +$('#suitebook_reservation_entry_room_charge').val()
  cleaning_fee = +$('#suitebook_reservation_entry_cleaning_fee').val()
  total = room_charge + cleaning_fee
  if Number.isNaN(total)
    $('#suitebook_reservation_entry_total').val('')
  else
    $('#suitebook_reservation_entry_total').val(total)
