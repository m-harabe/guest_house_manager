$ ->
  # 予定CI時刻タイムピッカー
  # bootstrap-timepicker by https://jdewit.github.io/bootstrap-timepicker/
  $('#reservation_management_reservations_entry_form_display_check_in_time').timepicker
    minuteStep: 5
    template: 'dropdown'
    appendWidgetTo: 'body'
    showMeridian: false
    modalBackdrop: true
    defaultTime: false

  # 時刻クリアボタン
  $('#time_clear').click ->
    $('#reservation_management_reservations_entry_form_display_check_in_time').val ''

  # 自動計算処理
  $('#reservation_management_reservations_entry_form_adult_count').change ->
    getTotalCount()

  $('#reservation_management_reservations_entry_form_child_count').change ->
    getTotalCount()

  $('#reservation_management_reservations_entry_form_room_charge').change ->
    getTotal()

  $('#reservation_management_reservations_entry_form_cleaning_fee').change ->
    getTotal()

  $('#reservation_management_reservations_entry_form_meal_fee').change ->
    getTotal()


getTotalCount =  ->
  # +演算子(Numberと多分同じ)でint型に変換
  adult_count = +$('#reservation_management_reservations_entry_form_adult_count').val()
  child_count = +$('#reservation_management_reservations_entry_form_child_count').val()
  $('#reservation_management_reservations_entry_form_total_count').val(adult_count + child_count)

getTotal = ->
  room_charge  = +$('#reservation_management_reservations_entry_form_room_charge').val()
  cleaning_fee = +$('#reservation_management_reservations_entry_form_cleaning_fee').val()
  meal_fee = +$('#reservation_management_reservations_entry_form_meal_fee').val()
  total = room_charge + cleaning_fee + meal_fee
  if Number.isNaN(total)
    $('#reservation_management_reservations_entry_form_total').val('')
  else
    $('#reservation_management_reservations_entry_form_total').val(total)

# ステータス一括変更ボタン
@bulkForwardStatus = (callback) ->
  # サブミットするフォームを取得
  # http://hensa40.cutegirl.jp/archives/690
  f = document.forms['reservation_show_form']
  f.action = '/reservation_management/entry/bulk_forward_status/' + callback
  f.submit()
  true

# ステータス変更ボタン
@forwardStatus = (callback) ->
  f = document.forms['reservation_show_form']
  f.action = '/reservation_management/entry/forward_status/' + callback
  f.submit()
  true

# キャンセルボタン >> モーダル >> はい
@cancel = (callback) ->
  # サブミットするフォームを取得
  # http://hensa40.cutegirl.jp/archives/690
  f = document.forms['reservation_show_form']
  f.action = '/reservation_management/entry/cancel/' + callback
  f.submit()
  true
