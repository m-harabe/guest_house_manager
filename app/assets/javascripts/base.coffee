$ ->

  # Enterキーでのフォーム送信を無効化
  $(document).on "keypress", "input:not(.allow_submit)", (event) -> event.which != 13

  # フォームの検索・登録ボタンクリック時のローディング gem spinjs_rails
  # http://wp.tech-style.info/archives/397
  # 非同期処理で手動でSTOPさせる場合は@stopSpinソッドを呼び出す
  $('form').submit ->
    startSpin $(this)

  $('.select2').select2(
    language: "ja"
  )

  $('.select2-include-blank').select2(
    placeholder: "指定しない",
    allowClear: true
    language: "ja"
  )

  $('.select2-multi').select2
    placeholder: "複数選択可",
    allowClear: true,
    language: "ja"

  # Bootstrapのツールチップだが、動かないのでslimに直ガキ
  $('[data-toggle=tooltip]').tooltip()

  # dataTable デフォルトの設定を変更 日本語化
  $.extend $.fn.dataTable.defaults, language: url: 'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Japanese.json'

  # text フィールドにBootstrapMaxlengthを設定
  $('.maxlength').maxlength
    alwaysShow: true
    warningClass: "label label-success"
    limitReachedClass: "label label-danger"
    validate: true
    # threshold: 10, # しきい値、slwaysShow: true の場合は無効
    # separator: ' of '
    # preText: 'You have '
    # postText: ' chars remaining.'


  # Bootstrap-datepicker の設定
  # https://uxsolutions.github.io/bootstrap-datepicker/
  $('.input-group.date').datepicker
    format: 'yyyy-mm-dd'
    weekStart: 1
    language: 'ja'
    todayHighlight: true
    orientation: 'bottom left'
    autoclose: true
    keyboardNavigation: true

  $('.input-daterange').datepicker
    format: "yyyy-mm-dd"
    weekStart: 1
    language: 'ja'
    todayHighlight: true
    autoclose: true
    keyboardNavigation: true

  # 年月のみの選択
  $('.input-group .year_month').datepicker
    format: 'yyyy年mm月'
    language: 'ja'
    minViewMode: 1
    orientation: 'bottom right'
    autoclose: true
    keyboardNavigation: true

  # 年月のみの選択の際のアイコンクリック時
  $('.input-group-addon').click ->
    $('.year_month').datepicker 'show'
    return

@formatDateYMD = (date) ->
  y = date.getFullYear()
  m = ("00" + (date.getMonth()+1)).slice(-2)
  d = ("00" + date.getDate()).slice(-2)
  y + "-" + m + "-" + d

@startSpin = (fomrObj) ->
  $(fomrObj).spin()
  $('body').addClass('spin')

@stopSpin = (formObj) ->
  formObj.spin false
  $('body').removeClass 'spin'
  # $('section.content').removeClass 'spin'

# 検索フォームの入力値をリセット
@resetTextForm = () ->
  # $('input[type="text"], input[type="radio"], input[type="checkbox"], select').val('').removeAttr('checked').removeAttr 'selected'
  $('input[type="text"]').val('')
  return

# サイドバーの開閉状態をクッキーに保持
@changeSidebar = () ->
  # body のclassを確認。 ※ hasClass()が開閉アクションの直前の状態を取得するため、真偽を入れ替えて登録
  # プラグイン jquery.cookie を利用
  $.cookie 'sidebar_collapse', !$('body').hasClass('sidebar-collapse'), {path: '/', expires: 30}
  return

# デスクトップ通知
# Notification対応していない、また許可されていなければアラート

@doNotification = (message) ->
  if window.Notification
    # Permissionの確認
    if Notification.permission == 'granted'
      # 許可されている場合はNotificationで通知
      n = new Notification(message)
    else if Notification.permission == 'denied'
      # 許可されていない場合はアラート
      alert message
    else if Notification.permission == 'default'
      # 許可が取れていない場合はNotificationの許可を取る
      Notification.requestPermission (result) ->
        if result == 'denied'
          # 許可されていない場合はアラート
          alert message
        else if result == 'default'
          # 不明な場合もアラート
          alert message
        else if result == 'granted'
          n = new Notification(message)
        return
