Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  # false: public/404.html, 500.htmlとかを開発環境で表示する
  # true: 開発用の画面を表示する
  config.consider_all_requests_local = true
  # config.consider_all_requests_local = false

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  # mailer setting - Gem Devise の設定
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }

  # bulletの設定
  config.after_initialize do
    Bullet.enable = true
    Bullet.alert = true
    Bullet.bullet_logger = true
    Bullet.console = true
    Bullet.rails_logger = true
    Bullet.add_footer = true
  end

  # 開発環境vagrant用にwhitelist の追加
  # https://obel.hatenablog.jp/entry/20170522/1495418016
  config.web_console.whitelisted_ips = '0.0.0.0/0'

  config.log_level = :debug
  # config.log_formatter = Logger::Formatter.new
  config.log_formatter = proc do |severity, datetime, progname, msg|
    # "[#{severity}]#{datetime}: #{progname} | #{msg}\n"

    # "2018-09-19 16:51:32" 形式
    # "#{I18n.l(datetime, format: :default_h)}: #{progname} | #{msg}\n"

    # 時刻のみ "16:51:32" 形式
    "#{datetime.strftime("%H:%M:%S")}| #{msg}\n"
  end
end
