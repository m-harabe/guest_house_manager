Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'dashboard#index'

  devise_for :users, :controllers => {
    :sessions => 'auth/sessions',
    :registrations => 'auth/registrations'
  }

  get 'dashboard', to: 'dashboard#index'

  # マスタ
  namespace :master do
    # オーナー
    get 'owners', to: 'owners#index'
    get 'owners/new', to: 'owners#new'
    post 'owners/create', to: 'owners#create'
    get 'owners/edit/(:id)', to: 'owners#edit', as: :owners_edit
    post 'owners/update', to: 'owners#update'

    # 施設
    get 'buildings', to: 'buildings#index'
    get 'buildings/new', to: 'buildings#new'
    post 'buildings/create', to: 'buildings#create'
    get 'buildings/show/(:id)', to: 'buildings#show', as: :buildings_show
    get 'buildings/edit/(:id)', to: 'buildings#edit', as: :buildings_edit
    post 'buildings/update', to: 'buildings#update'
    get 'buildings/invisible/(:id)', to: 'buildings#invisible', as: :buildings_invisible
    get 'buildings/visible/(:id)', to: 'buildings#visible', as: :buildings_visible
    get 'buildings/new_room/(:building_id)', to: 'buildings#new_room', as: :buildings_new_room
    post 'buildings/create_room', to: 'buildings#create_room'
    get 'buildings/edit_room/(:room_id)', to: 'buildings#edit_room', as: :buildings_edit_room
    post 'buildings/update_room', to: 'buildings#update_room'
    get 'buildings/(:building_id)/invisible_room/(:room_id)', to: 'buildings#invisible_room', as: :buildings_invisible_room
    get 'buildings/(:building_id)/visible_room/(:room_id)', to: 'buildings#visible_room', as: :buildings_visible_room

    # エリア
    get 'areas', to: 'areas#index'
    get 'areas/new', to: 'areas#new'
    post 'areas/create', to: 'areas#create'
    get 'areas/edit/(:id)', to: 'areas#edit', as: :areas_edit
    post 'areas/update', to: 'areas#update'

    # 精算書設定
    get 'settlement_template', to: 'settlement_template#index'
    post 'settlement_template/update', to: 'settlement_template#update'

    # アカウント
    get 'users', to: 'users#index'
    get 'users/new', to: 'users#new'
    get 'users/(:id)/edit', to: 'users#edit', as: :users_edit
    post 'users/create', to: 'users#create'
    post 'users/update', to: 'users#update'
    # post 'users/update', to: 'users#update'
    get 'users/(:id)/delete', to: 'users#delete', as: :users_delete
    get 'users/(:id)/restore', to: 'users#restore', as: :users_restore

  end

  # モーダル
  namespace :modal do
    post 'search_reservation/previous_reservation', to: 'search_reservation#previous_reservation'
    post 'search_reservation/whole_reservation_status', to: 'search_reservation#whole_reservation_status'
  end

  # 予約管理
  # 各種一覧加減を起点としてアクセスされる（パンくずリストで一覧に戻る）可能性のあるアクションは全て/(:callback)をもたせる
  namespace :reservation_management do
    get 'entry/', to: 'entry#new'
    get 'entry/(:check_in_date)/(:room_id)/(:callback)', to: 'entry#new', as: :entry_date
    get 'show/(:id)/(:callback)', to: 'entry#show', as: :show
    get 'edit/(:id)/(:callback)', to: 'entry#edit', as: :edit
    post 'entry/create/(:callback)', to: 'entry#create', as: :create
    post 'entry/update/(:callback)', to: 'entry#update', as: :update
    get 'edit_reservation_date/(:reservation_id)', to: 'entry#edit_reservation_date', as: :edit_reservation_date
    post 'update_reservation_date', to: 'entry#update_reservation_date'
    post 'entry/bulk_forward_status/(:callback)', to: 'entry#bulk_forward_status', as: :bulk_forward_status
    post 'entry/forward_status/(:callback)', to: 'entry#forward_status', as: :forward_status
    post 'entry/cancel/(:callback)', to: 'entry#cancel', as: :cancel

    # 一棟貸
    get 'whole_reservation/new/(:check_in_date)/(:building_id)/(:callback)', to: 'whole_reservation#new', as: :whole_reservation_new
    get 'whole_reservation/show/(:id)/(:reservation_id)/(:callback)', to: 'whole_reservation#show', as: 'whole_reservation_show'
    post 'whole_reservation/create/(:callback)', to: 'whole_reservation#create', as: :whole_reservation_create
    get 'whole_reservation/edit_basic/(:id)', to: 'whole_reservation#edit_basic', as: :whole_reservation_edit_basic
    post 'whole_reservation/update_basic', to: 'whole_reservation#update_basic', as: :whole_reservation_update_basic
    get 'whole_reservation/edit_payment/(:id)', to: 'whole_reservation#edit_payment', as: :whole_reservation_edit_payment
    post 'whole_reservation/update_payment', to: 'whole_reservation#update_payment', as: :whole_reservation_update_payment
    get 'whole_reservation/edit_room_reservations/(:id)', to: 'whole_reservation#edit_room_reservations', as: :whole_reservation_edit_room_reservations
    post 'whole_reservation/update_room_reservations', to: 'whole_reservation#update_room_reservations', as: :whole_reservation_update_room_reservations
    post 'whole_reservation/cancel/(:callback)', to: 'whole_reservation#cancel', as: :whole_reservation_cancel

    # インポート
    get 'import/', to: 'import#index'
    post 'import/comfirm', to: 'import#comfirm'
    post 'import/create', to: 'import#create'
    get 'import/result', to: 'import#result'

    # 施設別月間予約一覧
    get 'monthly_list', to: 'monthly_list#index'
    post 'monthly_list/search', to: 'monthly_list#search'
    get 'monthly_list/transition_search', to: 'monthly_list#transition_search'
    get 'monthly_list/csv', to: 'monthly_list#csv'

    # 日別予約一覧
    get 'daily_list', to: 'daily_list#index'
    post 'daily_list/search', to: 'daily_list#search'
    get 'daily_list/transition_search', to: 'daily_list#transition_search'

    # インポート
    get 'import_suitebook/', to: 'import_suitebook#index'
    post 'import_suitebook/create', to: 'import_suitebook#create'

    # 予約検索
    get 'reservations', to: 'reservations#index'
    post 'reservations/search', to: 'reservations#search'
    get 'reservations/transition_search', to: 'reservations#transition_search'

    # suitebook連携
    get 'suitebook', to: 'suitebook#index'
    post 'suitebook/search', to: 'suitebook#search'
    get 'suitebook/transition_search', to: 'suitebook#transition_search'
    get 'suitebook/:id/cancel', to: 'suitebook#cancel', as: :suitebook_cancel
    get 'suitebook/:id/show/(:callback)', to: 'suitebook#show', as: :suitebook_show
    get 'suitebook/:id/entry/(:callback)', to: 'suitebook#entry', as: :suitebook_entry
    get 'suitebook/:id/entry_whole_select_building/(:callback)', to: 'suitebook#entry_whole_select_building', as: :suitebook_entry_whole_select_building
    post 'suitebook/:id/entry_whole/(:callback)', to: 'suitebook#entry_whole', as: :suitebook_entry_whole
    post 'suitebook/create/(:callback)', to: 'suitebook#create', as: :suitebook_create
    post 'suitebook/create_whole/(:callback)', to: 'suitebook#create_whole', as: :suitebook_create_whole
  end

  # 作業管理
  namespace :work do
    get 'daily_tasks', to: 'daily_tasks#index'
    post 'daily_tasks/search', to: 'daily_tasks#search'
    get 'daily_tasks/transition_search', to: 'daily_tasks#transition_search'
    get 'daily_tasks/forward_status/(:id)/(:status)', to: 'daily_tasks#forward_status', as: :daily_tasks_forward_status

    get 'cleaning_status_list', to: 'cleaning_status_list#index'
    post 'cleaning_status_list/search', to: 'cleaning_status_list#search'
    get 'cleaning_status_list/transition_search', to: 'cleaning_status_list#transition_search'
    get 'cleaning_status_list/forward_status/(:id)/(:status)', to: 'cleaning_status_list#forward_status', as: :cleaning_status_list_forward_status
  end

  # 月事管理
  namespace :summary do
    # 売上一覧
    get 'sales_list', to: 'sales_list#index'
    post 'sales_list/search', to: 'sales_list#search'
    get 'sales_list/transition_search', to: 'sales_list#transition_search'
    get 'sales_list/csv', to: 'sales_list#csv'

    # 精算書
    get  'settlement/(:room_id)/(:year_month)/new', to: 'settlement#new', as: :settlement
    post 'settlement/create', to: 'settlement#create', as: :settlement_create
    get  'settlement/(:id)/edit', to: 'settlement#edit', as: :settlement_edit
    get  'settlement/(:id)/delete', to: 'settlement#delete', as: :settlement_delete
    patch 'settlement/update', to: 'settlement#update', as: :settlement_update
    get  'settlement/(:id)/refresh_reward_rate', to: 'settlement#refresh_reward_rate', as: :settlement_refresh_reward_rate
    get  'settlement/(:id)/print', to: 'settlement#print', as: :settlement_print
    get  'settlement/(:id)/excel', to: 'settlement#excel', as: :settlement_excel
    get  'settlement/(:id)/xlsx', to: 'settlement#xlsx', as: :settlement_xlsx

    # 月次レポート
    get 'reports', to: 'reports#index'
    post 'reports/accommodation_tax', to: 'reports#accommodation_tax'
    post 'reports/accommodation_tax_for_room', to: 'reports#accommodation_tax_for_room'
    post 'reports/accommodation_tax_for_building', to: 'reports#accommodation_tax_for_building'
    post 'reports/accommodation_tax_for_daily_room', to: 'reports#accommodation_tax_for_daily_room'
  end

  # 共通
  # Ajax用
  get 'select_room_options_by_area/(:area_id)', to: 'ajax#select_room_options_by_area', as: :select_room_options_by_area

  # その他
  namespace :other do
    get 'release_notes', to: 'release_notes#index'
  end

  # ルーティングエラーは（これより上で指定したルーティングに引っかからない）全てのURLをキャッチして処理
  match "*path" => "application#handle_404", via: :all unless Rails.env.development?
end
