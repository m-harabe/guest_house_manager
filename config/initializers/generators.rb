# Railsのgenerateコマンドをカスタムするためのファイル
# rails5からはconfig/application.rbではなくinitializersに置いたほうがよい
Rails.application.config.generators do |g|
  g.orm :active_record
  g.assets false
  g.helper false
  g.test_framework :rspec,
    fixture: true,
    fixture_replacement: :factory_bot,
    view_specs: false,
    routing_specs: false,
    helper_specs: false,
    integration_tool: false
end