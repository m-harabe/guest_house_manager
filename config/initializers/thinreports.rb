# Thinreprtsで中国語が文字化けする件に対応
#
# Thinreprts エディターでは現状フォントは固定で、持ち込みのフォントは指定できない様子。
# このまま中国語（韓国語でも）を入れようとすると、PDFの状態で文字化けを起こす
# 対策としては、ttfフォントで、NOTOフォントを使う必要がある
# 参考: https://qiita.com/github0013@github/items/9f8bd2992cc886e62ea0
#

Thinreports.configure do |config|
  config.fallback_fonts <<
    Rails.root.join("app/thinreports/NotoSansCJKtc-Regular.ttf").to_s
end