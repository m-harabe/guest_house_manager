require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GuestHouseManager03
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Don't generate system test files.
    config.generators.system_tests = nil

    # 表示時のタイムゾーンをJSTに変更
    config.time_zone = 'Asia/Tokyo'

    # DB保存時のタイムゾーンをJSTに変更
    config.active_record.default_timezone = :local

    # ロケールを日本語に変更
    # config.i18n.default_locale = :ja
    # 以前までは config/application.rb にデフォルトの設定を書いていましたが、initializers/locale.rb に書くようになったみたいです。

    # config/application.rb
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]

    # AdminLte(Bootstrap)のアイコンフォントが読み込めない問題で /vendor/assets/以下にもコピー
    config.assets.paths << Rails.root.join('vendor', 'assets', 'fonts')



    config.batch_logger = Logger.new('log/batch.log', 'monthly')
  end

end