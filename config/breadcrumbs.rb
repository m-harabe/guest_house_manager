crumb :root do
  link "ダッシュボード", dashboard_path
end

# /reservation_management/entry/
crumb :reservations_entry_new do |callback|
  link I18n.t("title.reservations.entry.new"), reservation_management_entry_path
  if callback.blank?
    parent :root
  else
    parent callback.to_sym
  end
end

# /reservation_management/entry/show(/:id)/(:callback)
crumb :reservations_entry_show do |reservation_id, callback|
  link I18n.t("title.reservations.entry.show"), reservation_management_show_path(reservation_id, callback)
  if callback.blank?
    parent :root
  else
    parent callback.to_sym
  end
end

# /reservation_management/entry/edit(/:id)/(:callback)
crumb :reservations_entry_edit do |reservation_id, callback|
  link I18n.t("title.reservations.entry.edit"), reservation_management_edit_path(reservation_id)
  parent :reservations_entry_show, reservation_id, callback
end

# /reservation_management/monthly_list
crumb :monthly_list do
  link I18n.t("title.monthly_list"), reservation_management_monthly_list_transition_search_path
  parent :root
end

# /whole_reservation/show/(:id)/(:reservation_id)/(:callback)
crumb :whole_reservations_show do |whole_reservation_id, reservation_id, callback|
  link I18n.t("title.whole_reservations.show"), reservation_management_whole_reservation_show_path(whole_reservation_id)
  if reservation_id.to_i > 0
    parent :reservations_entry_show, reservation_id, callback
  elsif callback.blank?
    parent :root
  else
    parent callback.to_sym
  end
end

# /reservation_management/whole_reservations/new/
crumb :whole_reservations_new do |check_in_date, building_id|
  link I18n.t("title.whole_reservations.new"), reservation_management_whole_reservation_new_path(check_in_date, building_id)
  parent :monthly_list
end

# /reservation_management/suitebook
crumb :suitebook do
  link I18n.t("title.suitebook.index"), reservation_management_suitebook_transition_search_path
  parent :root
end

# /reservation_management/suitebook/:id/show/(:callback)
crumb :suitebook_show do |id|
  link I18n.t("title.suitebook.show"), reservation_management_suitebook_show_path(id)
  parent :suitebook
end

# /reservation_management/suitebook/:id/entry/(:callback)
crumb :suitebook_entry do |id|
  link I18n.t("title.suitebook.entry")
  parent :suitebook_show, id
end

# /reservation_management/suitebook/:id/entry_whole/(:callback)
crumb :suitebook_entry_whole do |id|
  link I18n.t("title.suitebook.entry_whole")
  parent :suitebook_show, id
end

# /reservation_management/suitebook/:id/entry_whole/(:callback)
crumb :suitebook_entry_whole do |id|
  link I18n.t("title.suitebook.entry_whole")
  parent :suitebook_show, id
end

# /reservation_management/reservations
crumb :reservations_search do
  link I18n.t("title.reservations.search"), reservation_management_reservations_transition_search_path
  parent :root
end

# /reservation/import_suitebook
crumb :reservation_import_suitebook do
  link I18n.t("title.reservations.import_suitebook.index"), reservation_management_import_suitebook_path
  parent :suitebook
end

# /reservation/daily_list
crumb :daily_list do
  link I18n.t("title.daily_list"), reservation_management_daily_list_transition_search_path
  parent :root
end

# /reservation/import
crumb :reservation_import do
  link I18n.t("title.reservations.import.index"), reservation_management_import_path
  parent :root
end

crumb :reservation_import_comfirm do
  link I18n.t("title.reservations.import.comfirm"), reservation_management_import_comfirm_path
  parent :reservation_import
end

# /reservation/import/edit
# crumb :reservation_import_edit do
#   link I18n.t("title.reservations.import.edit"), reservation_management_import_edit_path
#   parent :reservation_import
# end

# /reservation/import/result
crumb :reservation_import_result do
  link I18n.t("title.reservations.import.result"), reservation_management_import_result_path
  parent :reservation_import
end

# /master/owners
crumb :owner_list do
  link I18n.t("title.owners.list"), master_owners_path
  parent :root
end

# /master/owners/new
crumb :owner_new do
  link I18n.t("title.owners.new"), master_owners_new_path
  parent :owner_list
end

# /master/owners/edit
crumb :owner_edit do |owner_id|
  link I18n.t("title.owners.edit"), master_owners_edit_path(owner_id)
  parent :owner_list
end

# /master/buildings
crumb :building_list do
  link I18n.t("title.buildings.list"), master_buildings_path
  parent :root
end

# /master/buildings/new
crumb :building_new do
  link I18n.t("title.buildings.new"), master_buildings_new_path
  parent :root
end

# /master/buildings/show
crumb :building_show do |building_id|
  link I18n.t("title.buildings.show"), master_buildings_show_path(building_id)
  parent :building_list
end

# /master/buildings/edit
crumb :building_edit do |building_id|
  link I18n.t("title.buildings.edit"), master_buildings_edit_path(building_id)
  parent :building_show
end

# /master/areas
crumb :area_list do
  link I18n.t("title.areas.list"), master_areas_path
  parent :root
end

# /master/areas/new
crumb :area_new do
  link I18n.t("title.areas.new")
  parent :root
end

# /master/areas/edit
crumb :area_edit do
  link I18n.t("title.areas.edit")
  parent :root
end

# /master/settlement_template
crumb :settlement_template do
  link I18n.t("title.settlement_template.index")
  parent :root
end

# /master/users
crumb :users do
  link I18n.t("title.users.list"), master_users_path
  parent :root
end

# /master/users/new
crumb :users_new do
  link I18n.t("title.users.new")
  parent :users
end

# /master/users/edit
crumb :users_edit do
  link I18n.t("title.users.edit")
  parent :users
end

crumb :cleaning_status_list do
  link I18n.t("title.work.cleaning_status_list"), work_cleaning_status_list_transition_search_path
  parent :root
end

crumb :daily_tasks do
  link I18n.t("title.work.daily_tasks"), work_daily_tasks_transition_search_path
  parent :root
end

crumb :sales_list do
  link I18n.t("title.summary.sales_list"), summary_sales_list_transition_search_path
  parent :root
end

crumb :summary_reports do
  link I18n.t("title.summary.reports"), summary_reports_path
  parent :root
end

crumb :settlement_new do
  link I18n.t("title.settlement.new")
  parent :sales_list
end

crumb :settlement_edit do
  link I18n.t("title.settlement.edit")
  parent :sales_list
end

crumb :release_note do
  link I18n.t("title.other.release_note.index"), other_release_notes_path
  parent :root
end



