# Learn more: http://github.com/javan/whenever

# Rails.rootを使用するために必要
require File.expand_path(File.dirname(__FILE__) + "/environment")

# cronのログの吐き出し場所
set :output, "#{Rails.root}/log/crontab.log"

# ジョブの実行環境の指定
set :environment, :production
# set :environment, :development

# 5分毎に実行するスケジューリング
# 毎*2分、毎*7 分のCSVアップロード（Dropbox利用）に１分程要するため、それに合わせたスケジュール
# every '4,9,14,19,24,29,34,39,44,49,54,59 * * * *' do
#   rake 'suitebook:import_suitebook'
# end

# 3分毎に実行するスケジューリング
# suitebook連携 1.スクレイピング 2.CSV解析 3.予約登録 を全て行う
every 3.minute do
  rake 'suitebook:import_reservations'
end


# 毎日 am 5:12 のスケジューリング
# データベースのバックアップ
every 1.day, at: '5:12 am' do
  rake 'db:dump_all'
end

# 毎日 pm 1:00 のスケジューリング
# STAY中ステータスの一括チェックアウト
every 1.day, at: '1:00 pm' do
  rake 'reservation_status:check_out'
end

# suitebookスクレイピング NodeJS スクリプトの実行
# job_type :suitebook_scraping, 'cd /home/tsumugi/suitebook/scraping_csv && export PATH=/usr/local/bin:$PATH && bash scraping.sh :output'
# # 末尾が7の分で10分間隔に実行するスケジューリング
# every '7,17,27,37,47,57 * * * *' do
#   suitebook_scraping 'get csv from suitebook and upload dropbox'
# end

# 別サーバーにて、５分ずらして同じ処理を行う
# scraping suitebook for tsumugi production.
# 2,12,22,32,42,52 * * * * /bin/bash -l -c 'cd /home/adw/script/scraping_suitebook && sh scraping.sh >> cron.log 2>&1'
