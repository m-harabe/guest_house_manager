source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.4.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.4.4', '< 0.6.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# Rails5でturbolinksをオフにする時に気をつけること https://goo.gl/pQfPLh
# gem 'turbolinks', '~> 5'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# 認証
gem 'devise'

# 定数管理
gem 'config'
# gem 'dry-types'

# Formオブジェクトの導入
gem 'virtus'

# viewでslimを使う
gem 'slim-rails'

# ViewのJSの変数をRailsから渡す
gem 'gon'

# ローディング画像
# http://fgnass.github.io/spin.js/
# http://qiita.com/knt45/items/f77019100878a311312b
gem 'spinjs-rails'

# パンくずリスト
gem 'gretel'

gem 'rails-i18n'

# 検索機能
gem 'ransack'

# モデル単位の管理画面生成
gem 'activeadmin'

# Excelファイルxlsxの読み込み（パースのみ）
# https://github.com/roo-rb/roo
gem "roo", "~> 2.7.0"

# Excelファイルの作成
gem 'rubyzip', '>= 1.2.1'
gem 'axlsx', git: 'https://github.com/randym/axlsx.git', ref: 'c8ac844'
gem 'axlsx_rails'

# 既存Excelファイルの編集
gem 'rubyXL'

gem 'remotipart', github: 'mshibuya/remotipart', ref: '88d9a7d'

# crontab管理
gem 'whenever', :require => false

# Slack通知
gem 'slack-notifier'

# https://github.com/Jesus/dropbox_api
gem 'dropbox_api'

# scraping
# gem 'mechanize'
gem 'selenium-webdriver'
gem 'capybara'
# gem 'poltergeist'

gem 'dotenv-rails'

# For PDF Generation
gem 'thinreports'

group :production do
  # Use Unicorn as the app server
  gem 'unicorn'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  gem 'pry-rails'
  gem 'pry-doc'                  # methodを表示
  gem 'pry-byebug'               # デバッグを実施
  gem 'pry-stack_explorer'
  gem 'hirb'                     # モデルの出力結果を表形式で表示するGem ※設定ファイル /.pryrc
  gem 'hirb-unicode'             # 日本語を扱う場合には必要
  gem 'rubocop', require: false
  gem 'meowcop', require: false  # RuboCop導入に最適な設定 https://github.com/sideci/meowcop
  # gem 'brakeman', require: false

  gem 'seed_dump'                # DBからseed作成

  # For test
  gem 'rspec-rails'
  gem 'factory_bot_rails'
  gem 'rails-controller-testing'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'better_errors'

  # 現状のスキーマの注釈をコメントとしてファイルの上部や下部に追記してくれる。
  # 参考 http://qiita.com/satton_maroyaka/items/8aa7441f21bc5ac763db
  # 使用したコマンド （モデルのみ） bundle exec annotate --exclude tests,fixtures,factories
  gem 'annotate'

  # 開発環境で送信したメールをWeb上で確認できる
  # http://localhost:3000/letter_opener
  gem 'letter_opener_web'

  gem 'bullet'

end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
