# ゲストハウス宿泊情報管理システム

# 開発

- Ruby 2.4.0
- RubyOnRails 5.2.0
- Mysql 5.7.23
- nginx 1.12.2

## 開発環境

- Mysqlのみ、docker-composeを利用し、Ruby以下はローカルで構築

### 起動までの手順

```
# リポジトリからクローン
$ mkdir tsumugi
$ cd tsumugi
$ git clone git@bitbucket.org:m-harabe/guest_house_manager.git .
$ git checkout develop

# gem インストール
$ bundle install

# yarn インストール
$ yarn install

# DB作成
# DockerでMysqlを起動
$ cd docker
$ docker-compose up -d

$ rake db:create
$ rake db:migrate
$ rake db:seed
```


