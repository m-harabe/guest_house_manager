FactoryBot.define do
  factory :building do
    sequence(:code) { |i| "100#{i}" }
    sequence(:first_name) { |i| "施設名_#{i}" }
    # name  ''

    created_user_id { User.where(email: 'factory_user@anchor-design-works.com').take || User.first.try(:id) }
    updated_user_id { User.where(email: 'factory_user@anchor-design-works.com').take || User.first.try(:id) }
  end
end