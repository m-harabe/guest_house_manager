FactoryBot.define do
  factory :user do
    email { 'factory_user@anchor-design-works.com' }
    password {'zaq12wsx'}
    encrypted_password {'password'}
    sequence(:name) { |i| "指名_#{i}" }
    sequence(:kana) { |i| "カナ_#{i}" }
    sequence(:account) { |i| "account_#{i}" }

    trait :system do
      name 'システム管理者'
    end

    trait :admin do
      name '管理者'
    end

    trait :unko do
      email 'unko'
    end

    factory :test_users, traits: [:admin, :unko]
  end
end