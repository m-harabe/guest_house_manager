require("rails_helper")

describe Reservation do
  # カレントユーザの設定
  let!(:user) { create(:user) }

  let(:today) { Date.today }
  let(:create_attributes)  do
    {
      reserve_date: today,
      room_id: 1,
      check_in_date: today + 10,
      number_of_nights: 3,
      visitor_name: '山本譲二',
      nationality_id: 1,
      tel: '090-0000-0001',
      mail_address: 'test@abc.def',
      adult_count: 5,
      child_count: 2,
      room_charge: 51000,
      cleaning_fee: 2000,
      # meal_fee: 1000,
      site_code: Settings.SITE.CODE.EXPEDIA,
      note: '備考'
    }
  end

  describe "#self.update_reservation" do
    context "新規登録の場合" do
      let(:reservation) { Reservation.create_or_update(create_attributes, is_new: true) }
      example "正しく予約情報が登録されること" do
        expect(reservation.reserve_date).to eq(today)
      end
    end
  end
end
