# MySqlバックアップバッチ処理
# 実行コマンド `bundle exec rails db:dump_all`
#
# １）gzip コマンドで圧縮したDumpファイルを `tmp/backups/` およびDropbox（暫定）に保存する
# ２）保存と同時に１０日間を過ぎたファイルがあれば削除する
# ３）処理が完了したらSlackに通知
# 処理本体は `app/utils/db_dumper.rb` に記述

namespace :db do
  desc "Dumps the database to tmp/backups/"
  task dump_all: [:environment, :load_config] do
    DbDumper.run
  end
end