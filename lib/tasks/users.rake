# MySqlバックアップバッチ処理
# 実行コマンド `bundle exec rails users:create`
namespace :users do
  desc "ユーザーの一括追加処理"
  task :create => :environment do |task|
    p '-------- ユーザー追加処理トランザクション開始 --------'
    ActiveRecord::Base::transaction do
      # User.create_by_default!(role: 1, name: '吉村', account: '0003')
      # User.create_by_default!(role: 3, name: '峰尾', account: '2007')
      # User.create_by_default!(role: 3, name: '岩井', account: '2008')
      # User.create_by_default!(role: 3, name: '三木', account: '2009')
      # User.create_by_default!(role: 3, name: '臼井', account: '2010')
      # User.create_by_default!(role: 3, name: '黄', account: '2011')
      # User.create_by_default!(role: 3, name: '陳', account: '2012')
      User.create_by_default!(role: 3, name: 'ソウ', account: '2015')
    end
    p '-------- ユーザー追加処理トランザクション終了 --------'
  end
end
