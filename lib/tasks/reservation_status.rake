namespace :reservation_status do
  desc "予約ステータスの一括変更処理"
  task :check_out => :environment do |task|
    p "-------------- Rake reservation_status:check_out Start [#{Date.today}]--------------"
    begin
      ActiveRecord::Base::transaction do
        send_message Reservation.update_status_to_check_out
      end
    rescue => ex
      ErrorUtil.error_log_and_notify(ex: ex, message: 'rake reservation_status:check_out')
    end
    p "-------------- Rake reservation_status:check_out End ------------------------------"
  end

  def send_message(count_list)
    server = Settings.SERVER_NAME || 'local'
    message = "【一括チェックアウト処理結果】#{I18n.l(Time.zone.now, format: :default_h)}\n"
    message += "【アプリ名】#{Settings.APP_NAME}\n"
    message += "【サーバー】#{server}\n"
    message += "予約情報：  #{count_list[0].to_i}件\n"
    message += "一棟貸情報： #{count_list[1].to_i}件"

    if Rails.env.production? #&& production_ip?
      NotificationUtil.post_message_to_slack(message: message)
    end
  end
end
