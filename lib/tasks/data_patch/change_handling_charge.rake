# 手数料再計算バッチ処理
# 実行コマンド RAILS_ENV=development bundle exec rails data_patch:change_handling_charge
#
namespace :data_patch do
  desc '税率を変更した場合に、過去データを日付を遡って手数料再計算する'
  task :change_handling_charge => :environment do
    p 'Start change_handling_charge ...'

    ActiveRecord::Base.transaction do

      # とりあえず、2021-01-01 以降のチェックインのみを対象として、月マタギは無視
      target_date = Time.current.beginning_of_year
      # target_date = Time.current.prev_year # テスト用 去年

      conditions = {reservation_status_in: Reservation::STSATUS_LIST_NOT_CANCEL}
      conditions[:check_in_date_gteq] = target_date
      # conditions[:check_in_date_gteq] = '2020-11-01'
      conditions[:site_code_eq] = Settings.SITE.CODE.AIRBNB

      # 予約の取得
      reservations = Reservation.ransack(conditions).result
      p "SQL: #{reservations.to_sql}"
      p "予約件数: #{reservations.count}"

      update_count = 0
      reservations.each do |r|
        r.handling_charge_rate = Settings.SITE.RATE[r.site_code]
        r.save!

        r.reservation_dates.each do |rd|
          next if rd.check_in_day < target_date
          p "更新前手数料: #{rd.handling_charge}"
          rd.handling_charge = rd.total * Settings.SITE.RATE[r.site_code]
          rd.save!
          p "更新後手数料： #{rd.handling_charge}"
          p "==============="
          update_count += 1
        end
      end
      p "ReservationDate更新件数: #{update_count}"
    end

    p 'End'
  end
end
