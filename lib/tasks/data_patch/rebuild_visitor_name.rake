# 宿泊税計算バッチ処理
# 実行コマンド bundle exec rails data_patch:visitor_name_kana
#
namespace :data_patch do
  desc 'ゲスト名を「ゲスト名」と「ゲスト名（カナ）」に分ける'
  task :visitor_name_kana => :environment do
    p 'Start visitor_name_kana ...'

    regex = /\((.+)\)/

    ActiveRecord::Base.transaction do

      # e.g.） 大阪太郎(オオサカタロウ) の()内の文字を抜き出す
      WholeReservation.all.each do |wr|
        match_str = wr.visitor_name.match(regex)
        wr.update!(visitor_name_kana: match_str[1]) if match_str
      end

      Reservation.all.each do |r|
        match_str = r.visitor_name.match(regex)
        r.update!(visitor_name_kana: match_str[1]) if match_str
      end

    end

    p 'End'
  end
end
