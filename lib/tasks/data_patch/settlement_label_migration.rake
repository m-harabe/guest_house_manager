namespace :data_patch do
  # 精算書ラベル更新バッチ処理
  # 実行コマンド bundle exec rails data_patch:settlement_label
  #
  desc '精算書データの新規追加ラベルをテンプレートからコピーする'
  task :settlement_label => :environment do
    p 'Start settlement_label ...'

    ActiveRecord::Base.transaction do

      Settlement.all.each do |st|
        template = st.settlement_template
        st.label_sales              = template.sales          if st.label_sales.nil?
        st.label_target_days        = template.target_days    if st.label_target_days.nil?
        st.label_working_days       = template.working_days   if st.label_working_days.nil?
        st.label_guest_count        = template.guest_count    if st.label_guest_count.nil?
        st.label_occ                = template.occ            if st.label_occ.nil?
        st.label_adr                = template.adr            if st.label_adr.nil?
        st.label_rev_par            = template.rev_par        if st.label_rev_par.nil?
        st.label_owner_sales        = template.sales          if st.label_owner_sales.nil?
        st.label_other_income_total = template.other_income   if st.label_other_income_total.nil?
        st.label_other_cost_total   = template.other_cost     if st.label_other_cost_total.nil?
        st.label_reward_rate        = template.reward_rate    if st.label_reward_rate.nil?
        st.label_payment_amount     = template.payment_amount if st.label_payment_amount.nil?

        p st.id if st.changed?

        st.save!
      end
    end

    p 'End'
  end

  # 清掃費ラベルデータ移行
  # 実行コマンド bundle exec rails data_patch:settlement_cleaning_fee
  #
  desc '精算書データのその他費用計上の清掃費を移行する'
  task :settlement_cleaning_fee => :environment do
    p 'Start settlement_cleaning_fee ...'

    ActiveRecord::Base.transaction do

      Settlement.all.each do |st|
        next if st.label_system_fee1.present?
        st.label_system_fee1 = '清掃費' unless st.reward_include_cleaning_fee?

        p st.id if st.changed?
        st.save!
      end
    end

    p 'End'
  end

  # 精算書ラベル更新バッチ処理
  # 実行コマンド bundle exec rails data_patch:settlement_label_20210702
  #
  desc '精算書データのラベル更新'
  task :settlement_label_20210702 => :environment do
    p 'Start settlement_label ...'

    ActiveRecord::Base.transaction do

      Settlement.all.each do |st|
        template = st.settlement_template

        st.label_total_income = template.total_income if st.label_total_income.blank?
        st.label_total_fee    = template.total_fee    if st.label_total_fee.blank?

        p "id:#{st.id}  label_total_fee: #{st.label_total_fee}" if st.changed?
        st.save!
      end
    end

    p 'End'
  end
end
