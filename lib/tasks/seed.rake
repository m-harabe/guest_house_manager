# 指定した環境に指定したseedファイルを読み込む
# 使用例:
# ```
# $ touch db/seeds/<environment>/sample.rb
# $ bundle exec rake db:seed:sample RAILS_ENV=<environment>
# ```
# RAILS_ENVは省略可能 => default: development
ENV["RAILS_ENV"] ||= "development"
Dir.glob(File.join(Rails.root, "db", "seeds", ENV["RAILS_ENV"], "*.rb")).each do |file|

  desc "Load the seed data from db/seeds/#{ENV["RAILS_ENV"]}/#{File.basename(file)}."
  task "db:seed:#{File.basename(file).gsub(/\..+$/, '')}" => :environment do
    load(file)
  end
end
