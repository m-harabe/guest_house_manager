# 宿泊税計算バッチ処理
# 実行コマンド bundle exec rails accommodation_tax:calc
#
namespace :accommodation_tax do
  desc '全データの宿泊税洗い替え'
  task :calc => :environment do
    p 'Start ...'

    # カウンタ
    whole_reservation_count = 0
    reservation_count = 0

    # 宿泊税適応日 2018-10-01 以降
    start_date = Date.new(2018, 10, 1)
    end_date = Date.new(2030, 12, 31) # 適当にこれ以降データが存在しない日付

    WholeReservation.search_not_cancel.where(check_in_date: start_date..end_date).each do |whole_reservation|
      begin
        whole_reservation.calc_accommodation_tax
        # whole_reservation.accommodation_tax = nil
        whole_reservation.save!
        whole_reservation_count += 1
      rescue => ex
        p "Error!! whole_reservation_id: #{whole_reservation.id}"
      end
    end

    Reservation.search_not_cancel.where(check_in_date: start_date..end_date).each do |reservation|
      begin
        # 一棟貸の予約はスキップ
        next if reservation.whole?

        reservation.calc_accommodation_tax
        # reservation.accommodation_tax = nil
        reservation.save!
        reservation_count += 1
      rescue => ex
        p "Error!! reservation_id: #{reservation.id}"
      end
    end

    p "WholeReservation 登録数: #{whole_reservation_count}件"
    p "Reservation 登録数: #{reservation_count}件"
    p 'End'
  end
end
