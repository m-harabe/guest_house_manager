# suitebook連携バッチ
#
namespace :suitebook do

  # suitebook連携 宿泊情報CSV取得バッチ処理
  # 実行コマンド bundle exec rails suitebook:import_suitebook
  #
  # desc 'Dropboxから宿泊情報CSVを取得してインポート'
  # task :import_suitebook => :environment do |task|

  #   # CSV解析バッチ
  #   p "-------------- Rake suitebook:import_suitebook Start [#{I18n.l(Time.zone.now, format: :default_h)}]--------------"
  #   Suitebook::ImportCsvService.new.import
  #   p "-------------- Rake suitebook:import_suitebook End [#{I18n.l(Time.zone.now, format: :default_h)}]----------------"

  #   # 予約情報一括登録バッチ
  #   p "-------------- Rake suitebook:create_reservations Start [#{I18n.l(Time.zone.now, format: :default_h)}]-----------"
  #   Suitebook::CreateReservationService.new.create
  #   p "-------------- Rake suitebook:create_reservations End [#{I18n.l(Time.zone.now, format: :default_h)}]-------------"
  # end

  # suitebook連携 予約情報一括登録バッチ
  # 実行コマンド bundle exec rails suitebook:create_reservations
  #
  # desc 'suitebook宿泊情報（suitebook_reservations）から予約情報（reservations）を登録'
  # task :create_reservations => :environment do |task|
  #   p "-------------- Rake suitebook:create_reservations Start [#{I18n.l(Time.zone.now, format: :default_h)}]-----------"
  #   Suitebook::CreateReservationService.new.create
  #   p "-------------- Rake suitebook:create_reservations End [#{I18n.l(Time.zone.now, format: :default_h)}]-------------"
  # end




  # suitebook連携 予約情報一括登録バッチ（自己完結型・このコマンドで、スクレイピング、CSV解析、予約登録まで全て行う）
  # CSVはローカルにダウンロードし、Dropboxを利用しない
  #
  # 実行コマンド bundle exec rails suitebook:import_reservations
  #
  desc 'suitebook宿泊情報（suitebook_reservations）から予約情報（reservations）を登録'
  task :import_reservations => :environment do |task|

    # p "-------------- スクレイピング CSV ダウンロード Start [#{I18n.l(Time.zone.now, format: :default_h)}]--------------"
    Suitebook::DownloadCsvService.new.scraping

    # p "-------------- CSVインポート Start [#{I18n.l(Time.zone.now, format: :default_h)}]-----------"
    Suitebook::ImportCsvService.new.import

    # p "-------------- suitebook予約情報から予約登録 Start [#{I18n.l(Time.zone.now, format: :default_h)}]-----------"
    Suitebook::CreateReservationService.new.create

  end

end

# [DropBoxのファイルをダウンロードする。 - Qiita](https://qiita.com/yoshi1729/items/51a03760478ce3139782)

