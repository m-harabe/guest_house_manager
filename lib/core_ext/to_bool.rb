class Object
  def to_bool
    case self.to_s.downcase
      when "true", "1", "ok"
        true
      else
        false
    end
  end
end