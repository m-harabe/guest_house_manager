class String

  # 変換テーブル上の文字を下の文字に置換する
  # 共にUTF8の文字コード（先頭のuをとったもの）
  #   from_chr がsjisに変換できない文字コード
  #   to_chr が代替え文字
  # https://glyphwiki.org/wiki/u82cf
  # https://www.marbacka.net/msearch/tool.php#str2enc
  # 変換後は要サーバー再起動
  
  #                                                                               ▼ここから中国語対応
  FROM_CHR_UTF8 = "\u{301C 2212 00A2 00A3 00AC 2013 2014 2016 203E 00A0 00F8 203A 82cf 8c22 8D75 6b22 6768 90b9 5f20 987e 8f6e 84dd 85b7 9648 5a67 51ef 5e86 8854 4e50 83b9 51e4 8fdf 70e8 4E66 949F 8273}"
  TO_CHR_SJIS   = "\u{FF5E FF0D FFE0 FFE1 FFE2 FF0D 2015 2225 FFE3 0020 03A6 3009 8607 8b1d 8d99 6b53 694a 9112 5f35 9867 8f2a 85cd 9700 9673 9751 51f1 6176 884C 6a02 7469 9cf3 9072 71c1 672C 9418 8277}"

  def sjisable
    str = self
    return if str.blank?

    str.tr!(FROM_CHR_UTF8, TO_CHR_SJIS)

    # 変換テーブルから漏れた不正文字は?に変換し、さらにUTF8に戻すことで今後例外を出さないようにする
    str = str.encode(Encoding::SJIS, Encoding::UTF_8, invalid: :replace, undef: :replace).encode(Encoding::UTF_8, Encoding::SJIS)
  end
end

####################
# 変換リスト
####################

# 苏 82cf -> 蘇 8607
# 谢 8c22 -> 謝 8b1d
# 赵 8D75 -> 趙 8d99
# 欢 6b22 -> 歓 6b53
# 杨 6768 -> 楊 694a
# 邹 90b9 -> 鄒 9112
# 张 5f20 -> 張 5f35
# 顾 987e -> 顧 9867
# 轮 8f6e -> 輪 8f2a
# 蓝 84dd -> 藍 85cd
# 薷 85b7 -> 需 9700
# 陈 9648 -> 陳 9673
# 婧 5a67 -> 靑 9751
# 凯 51ef -> 凱 51f1
# 庆 5e86 -> 慶 6176
# 衍 884d ->         #そのままで変換問題なし。
# 衔 8854 -> 行 884C
# 乐 4e50 -> 樂 6a02
# 莹 83b9 -> 瑩 7469
# 凤 51e4 -> 鳳 9cf3
# 迟 8fdf -> 遲 9072
# 烨 70e8 -> 燁 71c1
# 书 4E66 -> 本 672C
# 钟 949F -> 鐘 9418
# 艳 8273 -> 艷 8277

