############################
# サンプルコード
############################
__END__

# for ruby 2.5.1
require 'net/http'
require 'uri'
require 'json'

class Gitlab

  # レビュアーの人数
  REVIREWER_COUNT = 5
  # 対象のプロジェクトID
  # 対象のプロジェクトID検索 -> https://gitlab.point-service.jp/api/v4/projects?private_token=cq19z_42e4FfB2sj5kJU
  TARGET_PROJECT_IDS = [129]

  # Gitlabにアクセスするようのプライベートトークン
  PRIVATE_TOKEN = "cq19z_42e4FfB2sj5kJU".freeze

  attr_reader :merge_requests
  # コンストラクタ
  def initialize
    @merge_requests = []
  end

  # getリクエストののレスポンスを取得する
  # @param [String] request_url getリクエストのURL
  # @return [Array<Hash<String: Object>>] レスポンス
  def get_responses(request_url:)
    responses = []
    page_number = 1
    request_url.include?("?") ? request_url << "&"  : request_url << "?"

    loop do
      uri = URI.parse("#{request_url}per_page=100&page=#{page_number}")
      request = Net::HTTP::Get.new(uri)
      request["Private-Token"] = PRIVATE_TOKEN

      req_options = {
        use_ssl: uri.scheme == "https",
      }

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end
      response_body = JSON.parse(response.body).instance_of?(Array) ? JSON.parse(response.body) : [JSON.parse(response.body)]
      responses.concat(response_body)
      total_page = response.header["X-Total-Pages"].to_i
      break if page_number >= total_page

      page_number += 1
    end
    responses
  end

  # フロントエンド側のプロジェクトだけを抜き出す
  # @param [Array<Hash<String: Object>>] base_merge_requests すべてのプロジェクトのMRの一覧
  def extraction_merge_requests(base_merge_requests:)
    base_merge_requests.each do |merge_request|
      next unless TARGET_PROJECT_IDS.include?(merge_request["project_id"])
      # titleにWIP:がついているものも除外する
      next if /\AWIP:/ =~ merge_request["title"]
      hash = {}
      hash[:iid] = merge_request["iid"]
      hash[:project_id] = merge_request["project_id"]
      hash[:project_name] = get_project_name(project_id: merge_request["project_id"])
      hash[:web_url] = merge_request["web_url"]
      hash[:title] = merge_request["title"]
      hash[:assignee_name] = merge_request.dig("assignee", "name")
      @merge_requests.push(hash)
    end
  end

  # イイネの数を集計する
  def thumbsup_count
    @merge_requests.each do |merge_request|
      award_emojis = self.get_responses(request_url: "https://gitlab.point-service.jp/api/v4/projects/#{merge_request[:project_id]}/merge_requests/#{merge_request[:iid]}/award_emoji")
      merge_request[:thumbsup] = award_emojis.map{ |award_emoji| award_emoji.dig("user", "name") if award_emoji["name"] == "thumbsup" }.compact
      merge_request[:thumbsdown] = award_emojis.map{ |award_emoji| award_emoji.dig("user", "name") if award_emoji["name"] == "thumbsdown" }.compact
      merge_request[:merge_flag] = merge_request[:thumbsup].size == REVIREWER_COUNT
    end

  end

  # slackに通知するメッセージの生成
  # return [Hash<String, Object>] slackに通知するメッセージ
  def build_message
    {
      "icon_emoji": ":bow:",
      "username": "レビュー催促するマン",
      "attachments": @merge_requests.map do |merge_request|
        {
          "fallback": "",
          "pretext": "",
          "color": merge_request[:merge_flag] ? "00D000" : "#D00000",
          "title": merge_request[:web_url],
          "title_link": merge_request[:web_url],
          "fields": [
            {
              "title": "#{merge_request[:title]} : #{merge_request[:assignee_name]}",
              "value": ":+1:✕#{merge_request[:thumbsup].size} #{merge_request[:thumbsup]}\n:-1:✕#{merge_request[:thumbsdown].size}#{merge_request[:thumbsdown]}"
            }
          ]
        }
      end
    }
  end

  private
  # プロジェクトIDからプロジェクト名を取得
  # @param [Integer] project_id プロジェクトID
  # @return [String] プロジェクト名
  def get_project_name(project_id:)
    case project_id.to_i
    when 129
      "vx_2"
    else
      "no name"
    end
  end
end

class Slack
  def initialize
    request_url = "https://hooks.slack.com/services/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"
    @uri = URI.parse(request_url)
  end

  # スラックに通知する
  # @param [Array<Hash<Symbol, Object>>]
  def notify(message:)
    res = Net::HTTP.post_form(@uri, { payload: message.to_json })
  end
end


begin
  gitlab = Gitlab.new
  base_merge_requests = gitlab.get_responses(request_url: "https://gitlab.point-service.jp/api/v4/merge_requests?scope=all&state=opened")
  gitlab.extraction_merge_requests(base_merge_requests: base_merge_requests)
  gitlab.thumbsup_count

  slack = Slack.new
  slack.notify(message: gitlab.build_message)

rescue => ex
  puts ex.class
  puts ex.message
  puts ex.backtrace
end