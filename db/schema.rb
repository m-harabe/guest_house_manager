# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_01_080000) do

  create_table "active_admin_comments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "areas", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "エリア", force: :cascade do |t|
    t.string "name", limit: 10, default: "", null: false, comment: "エリア名"
    t.string "area_cd", limit: 6, default: "", null: false, comment: "コード（並び順）"
    t.boolean "is_visible", default: true, null: false, comment: "表示"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_cd"], name: "index_areas_on_area_cd", unique: true
    t.index ["is_visible"], name: "index_areas_on_is_visible"
  end

  create_table "buildings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "物件", force: :cascade do |t|
    t.integer "owner_id", comment: "オーナーID"
    t.string "code", limit: 10, default: "", null: false, comment: "コード"
    t.string "name", limit: 20, default: "", null: false, comment: "名称"
    t.string "postal_code", limit: 8, default: "", null: false, comment: "郵便番号"
    t.string "address", limit: 256, default: "", null: false, comment: "住所"
    t.integer "room_count", default: 1, null: false, comment: "部屋数"
    t.boolean "is_visible", default: true, null: false, comment: "表示"
    t.text "note", comment: "備考"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "area_id", comment: "エリアID"
    t.float "reward_rate", default: 0.0, null: false, comment: "報酬比率"
    t.index ["code"], name: "index_buildings_on_code"
  end

  create_table "nationalities", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", default: "", null: false, comment: "国籍名"
    t.integer "order", default: 0, null: false, comment: "並び順"
    t.boolean "is_visible", default: true, null: false, comment: "表示"
  end

  create_table "owners", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "オーナー", force: :cascade do |t|
    t.string "name", default: "", null: false, comment: "氏名"
    t.string "postal_code", limit: 8, default: "", null: false, comment: "郵便番号"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "address1", default: "", comment: "住所１"
    t.string "address2", default: "", comment: "住所２"
    t.string "tel", default: "", null: false, comment: "TEL"
    t.string "mail_address", comment: "メールアドレス"
    t.string "bank_name", default: "", comment: "振込先銀行名（支店名）"
    t.integer "account_type", limit: 1, default: 0, comment: "口座種別（0: 指定なし  1:普通  2:当座）"
    t.string "account_number", default: "", comment: "口座番号"
    t.string "swift_code", default: "", comment: "SWIFTコード"
    t.string "account_name", default: "", comment: "口座名義"
    t.string "language", default: "", comment: "精算書言語"
    t.text "note", comment: "メモ"
    t.boolean "is_visible", default: true, null: false, comment: "表示"
    t.string "mail_address2", comment: "メールアドレス２"
    t.string "mail_address3", comment: "メールアドレス３"
    t.index ["is_visible"], name: "index_owners_on_is_visible"
  end

  create_table "profiles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "物件詳細情報", force: :cascade do |t|
    t.string "profilable_type"
    t.bigint "profilable_id"
    t.string "postal_code", limit: 8, default: "", null: false, comment: "郵便番号"
    t.string "address", limit: 100, default: "", null: false, comment: "住所"
    t.integer "total_floor_area", default: 0, comment: "延床面積"
    t.integer "standard_people", default: 0, comment: "標準人数"
    t.integer "max_people", default: 0, comment: "最大人数"
    t.integer "bath_count", default: 0, comment: "風呂数"
    t.integer "toilet_count", default: 0, comment: "トイレ数"
    t.string "secret_number", limit: 25, default: "", null: false, comment: "暗証番号"
    t.index ["profilable_type", "profilable_id"], name: "index_profiles_on_profilable_type_and_profilable_id"
  end

  create_table "release_notes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "リリースノート", force: :cascade do |t|
    t.date "open_date", null: false, comment: "公開日"
    t.string "title", null: false, comment: "タイトル"
    t.text "content", null: false, comment: "内容"
    t.string "note_class", limit: 1, comment: "リリースノート区分"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["note_class"], name: "index_release_notes_on_note_class"
    t.index ["open_date"], name: "index_release_notes_on_open_date"
  end

  create_table "reservation_dates", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "宿泊日別情報", force: :cascade do |t|
    t.bigint "reservation_id", comment: "予約ID"
    t.integer "building_id", default: 0, null: false, comment: "物件ID"
    t.integer "room_id", default: 0, null: false, comment: "部屋ID"
    t.date "check_in_day", comment: "宿泊日"
    t.integer "room_charge", default: 0, null: false, comment: "宿泊料"
    t.integer "cleaning_fee", default: 0, null: false, comment: "清掃費"
    t.integer "total", default: 0, null: false, comment: "合計（宿泊費＋清掃費）"
    t.decimal "handling_charge", precision: 7, scale: 2, comment: "手数料"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "meal_fee", default: 0, null: false, comment: "食事代"
    t.index ["reservation_id"], name: "index_reservation_dates_on_reservation_id"
  end

  create_table "reservations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "予約情報", force: :cascade do |t|
    t.bigint "room_id", comment: "部屋ID"
    t.integer "whole_reservation_id", comment: "一棟貸情報ID"
    t.string "status", limit: 1, default: "1", null: false, comment: "ステータス 1: CI不可（清掃待）, 2: CI可, 3: CI中, 4:CO済, 9: キャンセル"
    t.string "site_code", limit: 4, default: "", null: false, comment: "予約サイトコード"
    t.integer "nationality_id", default: 0, null: false, comment: "国籍ID"
    t.string "visitor_name", default: "", null: false, comment: "ゲスト名"
    t.string "mail_address", default: "", null: false, comment: "メールアドレス"
    t.string "tel", default: "", null: false, comment: "TEL"
    t.integer "adult_count", default: 0, null: false, comment: "宿泊人数（大人）"
    t.integer "child_count", default: 0, null: false, comment: "宿泊人数（子供）"
    t.integer "room_charge", default: 0, null: false, comment: "宿泊料"
    t.integer "cleaning_fee", default: 0, null: false, comment: "清掃費"
    t.integer "meal_fee", default: 0, null: false, comment: "食事代"
    t.integer "total", default: 0, null: false, comment: "合計（宿泊費＋清掃費＋食事代）"
    t.float "handling_charge_rate", comment: "手数料率"
    t.date "reserve_date", comment: "予約日"
    t.date "check_in_date", comment: "チェックイン日"
    t.date "check_out_date", comment: "チェックアウト日"
    t.integer "number_of_nights", default: 1, null: false, comment: "宿泊日数"
    t.text "note", comment: "備考"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "scheduled_check_in_time", comment: "予定チェックイン時刻"
    t.integer "accommodation_tax", comment: "宿泊税"
    t.integer "registration_type", limit: 1, default: 0, comment: "登録タイプ e.g.) 0:手動 1:手動（suitebook） 2:自動（suitebook）"
    t.string "visitor_name_kana", default: "", null: false, comment: "ゲスト名カナ"
    t.integer "point_use", comment: "ポイント利用"
    t.text "owner_note", comment: "オーナー向けドキュメント"
    t.index ["check_in_date"], name: "index_reservations_on_check_in_date"
    t.index ["room_id"], name: "index_reservations_on_room_id"
    t.index ["status"], name: "index_reservations_on_status"
    t.index ["whole_reservation_id"], name: "index_reservations_on_whole_reservation_id"
  end

  create_table "rooms", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "部屋", force: :cascade do |t|
    t.bigint "building_id", comment: "物件ID"
    t.integer "room_no", default: 0, null: false, comment: "表示順"
    t.string "name", limit: 20, default: "", null: false, comment: "名称"
    t.boolean "is_visible", default: true, null: false, comment: "表示"
    t.text "note", comment: "備考"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "suitebook_room_idnum", comment: "suitebook room ID"
    t.index ["building_id"], name: "index_rooms_on_building_id"
    t.index ["room_no"], name: "index_rooms_on_room_no"
  end

  create_table "settlement_templates", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "精算書テンプレート", force: :cascade do |t|
    t.string "language", default: "", null: false, comment: "言語"
    t.string "title", default: "", null: false, comment: "タイトル"
    t.string "honorific", default: "", null: false, comment: "敬称"
    t.string "building", default: "", null: false, comment: "施設名"
    t.string "address", default: "", null: false, comment: "施設住所"
    t.string "company_name", default: "", null: false, comment: "自社名"
    t.string "company_info1", default: "", null: false, comment: "会社情報１"
    t.string "company_info2", default: "", null: false, comment: "会社情報２"
    t.string "company_info3", default: "", null: false, comment: "会社情報３"
    t.string "visitor_name", default: "", null: false, comment: "宿泊者名"
    t.string "nationality", default: "", null: false, comment: "国籍"
    t.string "number_of_nights", default: "", null: false, comment: "泊数"
    t.string "total_count", default: "", null: false, comment: "合計人数"
    t.string "count", default: "", null: false, comment: "人数"
    t.string "adult_count", default: "", null: false, comment: "大人"
    t.string "child_count", default: "", null: false, comment: "小人"
    t.string "room_charge", default: "", null: false, comment: "宿泊料"
    t.string "cleaning_fee", default: "", null: false, comment: "清掃費"
    t.string "total", default: "", null: false, comment: "合計"
    t.string "site", default: "", null: false, comment: "予約サイト"
    t.string "handling_charge", default: "", null: false, comment: "手数料"
    t.string "reservation_date", default: "", null: false, comment: "予約日"
    t.string "analysis_title", default: "", null: false, comment: "集計タイトル"
    t.string "sales", default: "", null: false, comment: "売上"
    t.string "target_days", default: "", null: false, comment: "日数"
    t.string "working_days", default: "", null: false, comment: "稼働日数"
    t.string "guest_count", default: "", null: false, comment: "宿泊人数"
    t.string "occ", default: "", null: false, comment: "OCC(稼働率)"
    t.string "adr", default: "", null: false, comment: "ADR(客室販売単価)"
    t.string "rev_par", default: "", null: false, comment: "RevPAR(稼働率×単価)"
    t.string "other_income", default: "", null: false, comment: "その他収入"
    t.string "other_cost", default: "", null: false, comment: "その他費用"
    t.string "reward_rate", default: "", null: false, comment: "報酬率"
    t.string "payment_amount", default: "", null: false, comment: "入金額"
    t.string "next_month_title", default: "", null: false, comment: "次月予約状況"
    t.string "internet_income", default: "", null: false, comment: "インターネット代"
    t.string "free_income1", default: "", null: false, comment: "その他収入１"
    t.string "free_income2", default: "", null: false, comment: "その他収入２"
    t.string "free_income3", default: "", null: false, comment: "その他収入３"
    t.string "free_income4", default: "", null: false, comment: "その他収入４"
    t.string "free_income5", default: "", null: false, comment: "その他収入５"
    t.string "total_income", default: "", null: false, comment: "その他収入合計"
    t.string "site_fee", default: "", null: false, comment: "サイト手数料"
    t.string "electrical_fee", default: "", null: false, comment: "電気代"
    t.string "gas_fee", default: "", null: false, comment: "ガス代"
    t.string "water_fee", default: "", null: false, comment: "水道代"
    t.string "internet_fee", default: "", null: false, comment: "インターネット費用"
    t.string "site_controller_fee", default: "", null: false, comment: "サイトコントローラー"
    t.string "free_fee1", default: "", null: false, comment: "その他費用１"
    t.string "free_fee2", default: "", null: false, comment: "その他費用２"
    t.string "free_fee3", default: "", null: false, comment: "その他費用３"
    t.string "free_fee4", default: "", null: false, comment: "その他費用４"
    t.string "free_fee5", default: "", null: false, comment: "その他費用５"
    t.string "total_fee", default: "", null: false, comment: "その他費用合計"
    t.string "payee", default: "", null: false, comment: "振込先"
    t.string "note1", default: "", null: false, comment: "備考１"
    t.string "note2", default: "", null: false, comment: "備考２"
    t.string "note3", default: "", null: false, comment: "備考３"
    t.string "note4", default: "", null: false, comment: "備考４"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["language"], name: "index_settlement_templates_on_language", unique: true
  end

  create_table "settlements", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "精算書", force: :cascade do |t|
    t.bigint "settlement_template_id", comment: "精算書テンプレートID"
    t.bigint "room_id", null: false, comment: "部屋ID"
    t.string "year_month", null: false, comment: "年月"
    t.integer "download_count", default: 0, null: false, comment: "ダウンロード回数"
    t.string "title", default: "", null: false, comment: "タイトル"
    t.string "owner_name", default: "", null: false, comment: "オーナー名称"
    t.string "building", default: "", null: false, comment: "施設名"
    t.string "address", default: "", null: false, comment: "施設住所"
    t.string "company_name", default: "", null: false, comment: "自社名"
    t.string "company_info1", default: "", null: false, comment: "会社情報１"
    t.string "company_info2", default: "", null: false, comment: "会社情報２"
    t.string "company_info3", default: "", null: false, comment: "会社情報３"
    t.float "reward_rate", default: 0.0, null: false, comment: "報酬比率"
    t.string "label_other_income", default: "", null: false, comment: "その他収入ラベル"
    t.string "label_internet_income", default: "", null: false, comment: "その他収入ラベル"
    t.string "label_free_income1", default: "", null: false, comment: "その他収入１ラベル"
    t.string "label_free_income2", default: "", null: false, comment: "その他収入２ラベル"
    t.string "label_free_income3", default: "", null: false, comment: "その他収入３ラベル"
    t.string "label_free_income4", default: "", null: false, comment: "その他収入４ラベル"
    t.string "label_free_income5", default: "", null: false, comment: "その他収入５ラベル"
    t.string "label_total_income", default: "", null: false, comment: "その他収入合計ラベル"
    t.string "label_other_cost", default: "", null: false, comment: "その他費用ラベル"
    t.string "label_site_fee", default: "", null: false, comment: "サイト手数料ラベル"
    t.string "label_electrical_fee", default: "", null: false, comment: "電気代ラベル"
    t.string "label_gas_fee", default: "", null: false, comment: "ガス代ラベル"
    t.string "label_water_fee", default: "", null: false, comment: "水道代ラベル"
    t.string "label_internet_fee", default: "", null: false, comment: "インターネット費用ラベル"
    t.string "label_site_controller_fee", default: "", null: false, comment: "サイトコントローラーラベル"
    t.string "label_free_fee1", default: "", null: false, comment: "その他費用１ラベル"
    t.string "label_free_fee2", default: "", null: false, comment: "その他費用２ラベル"
    t.string "label_free_fee3", default: "", null: false, comment: "その他費用３ラベル"
    t.string "label_free_fee4", default: "", null: false, comment: "その他費用４ラベル"
    t.string "label_free_fee5", default: "", null: false, comment: "その他費用５ラベル"
    t.string "label_total_fee", default: "", null: false, comment: "その他費用合計ラベル"
    t.integer "internet_income", comment: "インターネット代"
    t.integer "free_income1", comment: "その他収入１"
    t.integer "free_income2", comment: "その他収入２"
    t.integer "free_income3", comment: "その他収入３"
    t.integer "free_income4", comment: "その他収入４"
    t.integer "free_income5", comment: "その他収入５"
    t.integer "site_fee", comment: "サイト手数料"
    t.integer "electrical_fee", comment: "電気代"
    t.integer "gas_fee", comment: "ガス代"
    t.integer "water_fee", comment: "水道代"
    t.integer "internet_fee", comment: "インターネット費用"
    t.integer "site_controller_fee", comment: "サイトコントローラー"
    t.integer "free_fee1", comment: "その他費用１"
    t.integer "free_fee2", comment: "その他費用２"
    t.integer "free_fee3", comment: "その他費用３"
    t.integer "free_fee4", comment: "その他費用４"
    t.integer "free_fee5", comment: "その他費用５"
    t.string "note1", default: "", null: false, comment: "備考１"
    t.string "note2", default: "", null: false, comment: "備考２"
    t.string "note3", default: "", null: false, comment: "備考３"
    t.string "note4", default: "", null: false, comment: "備考４"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "system_fee1", comment: "システム自動追加費用１"
    t.string "label_system_fee1", comment: "システム自動追加費用１ラベル"
    t.string "label_sales", comment: "売上"
    t.string "label_target_days", comment: "日数"
    t.string "label_working_days", comment: "稼働日数"
    t.string "label_guest_count", comment: "宿泊人数"
    t.string "label_occ", comment: "OCC(稼働率)"
    t.string "label_adr", comment: "ADR(客室販売単価)"
    t.string "label_rev_par", comment: "RevPAR(稼働率×単価)"
    t.string "label_owner_sales", comment: "オーナー売上"
    t.string "label_other_income_total", comment: "その他収入（合計）"
    t.string "label_other_cost_total", comment: "その他費用（合計）"
    t.string "label_reward_rate", comment: "報酬率"
    t.string "label_payment_amount", comment: "入金額"
    t.index ["room_id", "year_month"], name: "index_settlements_on_room_id_and_year_month", unique: true
    t.index ["settlement_template_id"], name: "index_settlements_on_settlement_template_id"
  end

  create_table "suitebook_reservations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "suitebook予約情報", force: :cascade do |t|
    t.bigint "reservation_id", comment: "予約ID"
    t.bigint "whole_reservation_id", comment: "一棟貸情報ID"
    t.integer "status", limit: 1, default: 0, null: false, comment: "ステータス e.g.) 0:未処理 1:処理済 8:キャンセル 9エラー"
    t.string "suitebook_idnum", comment: "suitebook予約ID"
    t.string "site_idnum", comment: "予約サイトID"
    t.string "site_name", limit: 100, comment: "サイト名"
    t.string "check_in_date", comment: "チェックイン"
    t.string "check_out_date", comment: "チェックアウト"
    t.string "reservation_cd", comment: "予約コード"
    t.string "is_archive", comment: "アーカイブ済み"
    t.string "number_of_nights", comment: "宿泊数"
    t.string "visitor_name", comment: "ゲスト名"
    t.string "mail_address", comment: "ゲストemail"
    t.string "total_count", comment: "宿泊人数"
    t.string "adult_count", comment: "大人人数"
    t.string "child_count", comment: "子供人数"
    t.string "scheduled_check_in_time", comment: "チェックイン予定時刻"
    t.string "building_name", comment: "物件名"
    t.string "building_idnum", comment: "物件ID"
    t.string "property_owner_idnum", comment: "プロパティオーナーID"
    t.string "owner_company_name", comment: "会社名(オーナー)"
    t.string "owner_name", comment: "代表者・契約者(オーナー)"
    t.string "site_account", comment: "予約サイトアカウント(email)"
    t.string "contract_style", comment: "契約形態"
    t.string "cancel", comment: "キャンセル"
    t.string "currency", comment: "通貨"
    t.string "room_charge", comment: "宿泊料金"
    t.string "cleaning_fee", comment: "清掃費(ゲスト)"
    t.string "total", comment: "宿泊料金(ゲスト支払い金額)"
    t.string "handling_charge", comment: "手数料"
    t.string "revenue", comment: "あなたの収益"
    t.string "data_careated_at", comment: "予約作成日"
    t.string "other_cost", comment: "部屋代金以外の費用"
    t.string "room_idnum", comment: "部屋ID"
    t.string "room_name", comment: "部屋名"
    t.string "memo", comment: "予約メモ"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "room_id", comment: "部屋ID"
    t.text "error_message", comment: "エラーメッセージ"
    t.integer "building_id", comment: "物件ID"
    t.string "property_name", comment: "建物名"
    t.string "tel", comment: "電話番号"
    t.string "cancel_fee", comment: "キャンセル料"
    t.string "point_use", comment: "ポイント利用"
    t.string "cancel_charge", comment: "手数料 (キャンセル時)"
    t.string "total_minus_point", comment: "ゲストへの請求金額(ポイント割引後)"
    t.string "cancel_revenue", comment: "あなたの収益 (キャンセル時)"
    t.string "payment", comment: "決済"
    t.string "plan", comment: "プラン名"
    t.text "owner_note", comment: "オーナー向けドキュメント"
    t.string "visitor_name_kana", comment: "ゲスト名フリガナ"
    t.string "postal_code", limit: 8, default: "", null: false, comment: "郵便番号"
    t.string "address", comment: "住所/国籍"
    t.index ["reservation_id"], name: "index_suitebook_reservations_on_reservation_id"
    t.index ["status"], name: "index_suitebook_reservations_on_status"
    t.index ["whole_reservation_id"], name: "index_suitebook_reservations_on_whole_reservation_id"
  end

  create_table "suitebook_rooms", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "suitebook部屋情報", force: :cascade do |t|
    t.bigint "room_id", comment: "部屋ID"
    t.integer "building_idnum", null: false, comment: "suitebook物件ID"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["building_idnum"], name: "index_suitebook_rooms_on_building_idnum"
    t.index ["room_id"], name: "index_suitebook_rooms_on_room_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: ""
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "kana"
    t.string "tel"
    t.string "account", null: false
    t.integer "role", default: 0, null: false
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.timestamp "deleted_at"
    t.integer "lock_version", default: 0, null: false
    t.boolean "is_visible", default: true, null: false, comment: "表示"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "whole_reservations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "一棟貸情報", force: :cascade do |t|
    t.bigint "building_id", comment: "物件ID"
    t.string "status", limit: 1, default: "1", null: false, comment: "入力ステータス 1: 変更可, 2: 変更不可, 9: キャンセル"
    t.string "site_code", limit: 4, default: "", null: false, comment: "予約サイトコード"
    t.integer "nationality_id", default: 0, null: false, comment: "国籍ID"
    t.string "visitor_name", default: "", null: false, comment: "ゲスト名"
    t.string "mail_address", default: "", null: false, comment: "メールアドレス"
    t.string "tel", default: "", null: false, comment: "TEL"
    t.integer "adult_count", default: 0, null: false, comment: "宿泊人数（大人）"
    t.integer "child_count", default: 0, null: false, comment: "宿泊人数（子供）"
    t.integer "room_charge", default: 0, null: false, comment: "宿泊料"
    t.integer "cleaning_fee", default: 0, null: false, comment: "清掃費"
    t.integer "total", default: 0, null: false, comment: "合計（宿泊費＋清掃費＋食事代）"
    t.date "reserve_date", comment: "予約日"
    t.date "check_in_date", comment: "チェックイン日"
    t.date "check_out_date", comment: "チェックアウト日"
    t.integer "number_of_nights", default: 1, null: false, comment: "宿泊日数"
    t.text "note", comment: "備考"
    t.integer "created_user_id", default: 0, null: false
    t.integer "updated_user_id", default: 0, null: false
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "scheduled_check_in_time", comment: "予定チェックイン時刻１"
    t.integer "accommodation_tax", comment: "宿泊税"
    t.integer "registration_type", limit: 1, default: 0, comment: "登録タイプ e.g.) 0:手動 1:手動（suitebook） 2:自動（suitebook）"
    t.integer "meal_fee", default: 0, null: false, comment: "食事代"
    t.string "visitor_name_kana", default: "", null: false, comment: "ゲスト名カナ"
    t.index ["building_id"], name: "index_whole_reservations_on_building_id"
    t.index ["check_in_date"], name: "index_whole_reservations_on_check_in_date"
    t.index ["status"], name: "index_whole_reservations_on_status"
  end

  create_table "wk_reservations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "一時予約情報", force: :cascade do |t|
    t.date "check_in_date", comment: "チェックイン日"
    t.string "building_code", comment: "施設コード"
    t.integer "building_id", comment: "施設ID"
    t.string "room_no", comment: "部屋番号（表示順）"
    t.integer "room_id", comment: "部屋ID"
    t.integer "number_of_nights", comment: "泊数"
    t.string "visitor_name", comment: "ゲスト名"
    t.string "nationality", comment: "国籍"
    t.integer "nationality_id", comment: "国籍ID"
    t.string "mail_address", comment: "メールアドレス"
    t.string "tel", comment: "TEL"
    t.integer "adult_count", comment: "宿泊人数（大人）"
    t.integer "child_count", comment: "宿泊人数（子供）"
    t.integer "room_charge", comment: "宿泊料"
    t.integer "cleaning_fee", comment: "清掃費"
    t.integer "total", comment: "合計（宿泊費＋清掃費）"
    t.string "site_code", comment: "予約サイトコード"
    t.string "site_name", comment: "予約サイト名"
    t.date "reserve_date", comment: "予約日"
    t.text "note", comment: "備考"
    t.integer "created_user_id", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "visitor_name_kana", comment: "ゲスト名カナ"
  end

  add_foreign_key "reservation_dates", "reservations"
  add_foreign_key "reservations", "rooms"
  add_foreign_key "rooms", "buildings"
  add_foreign_key "settlements", "rooms"
  add_foreign_key "settlements", "settlement_templates"
  add_foreign_key "suitebook_reservations", "reservations"
  add_foreign_key "suitebook_reservations", "whole_reservations"
  add_foreign_key "suitebook_rooms", "rooms"
  add_foreign_key "whole_reservations", "buildings"
end
