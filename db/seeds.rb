AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?

def reset_db(model)
  model.delete_all
  if Rails.env.development?
    ActiveRecord::Base.connection.execute("ALTER TABLE #{model.class_name.downcase.pluralize} AUTO_INCREMENT = 1")
  end
end
reset_db(User)
reset_db(Owner)
reset_db(Profile)
reset_db(Room)
reset_db(Building)
reset_db(Nationality)

User.create!({email: "m-harabe@anchor-design-works.com", encrypted_password: "zaq12wsx", password: "zaq12wsx", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-06-11 16:47:05", last_sign_in_at: "2018-06-11 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", name: "システム管理者", kana: "システムカンリシャ", tel: "090-0000-0000", account: "admin", role: 999, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 4})

User.create!({email: "0001@aaa.bbb", name: "久米", account: "0001", role: 1, encrypted_password: "re0001", password: "re0001", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-06-11 16:47:05", last_sign_in_at: "2018-06-11 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "0002@aaa.bbb", name: "佐塚", account: "0002", role: 1, encrypted_password: "re0002", password: "re0002", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-06-11 16:47:05", last_sign_in_at: "2018-06-11 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})

User.create!({email: "1001@aaa.bbb", name: "李", account: "1001", role: 2, encrypted_password: "re1001", password: "re1001", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "1002@aaa.bbb", name: "姜", account: "1002", role: 2, encrypted_password: "re1002", password: "re1002", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "1003@aaa.bbb", name: "福山", account: "1003", role: 2, encrypted_password: "re1003", password: "re1003", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "1004@aaa.bbb", name: "土井", account: "1004", role: 2, encrypted_password: "re1004", password: "re1004", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "1005@aaa.bbb", name: "中川", account: "1005", role: 2, encrypted_password: "re1005", password: "re1005", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})

User.create!({email: "2001@aaa.bbb", name: "湯浅", account: "2001", role: 3, encrypted_password: "re2001", password: "re2001", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "2002@aaa.bbb", name: "鈴木", account: "2002", role: 3, encrypted_password: "re2002", password: "re2002", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "2003@aaa.bbb", name: "孫", account: "2003", role: 3, encrypted_password: "re2003", password: "re2003", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "2004@aaa.bbb", name: "土屋", account: "2004", role: 3, encrypted_password: "re2004", password: "re2004", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "2005@aaa.bbb", name: "惠", account: "2005", role: 3, encrypted_password: "re2005", password: "re2005", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "2006@aaa.bbb", name: "安", account: "2006", role: 3, encrypted_password: "re2006", password: "re2006", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})

User.create!({email: "2013@aaa.bbb", name: "吉川", account: "2013", role: 3, encrypted_password: "re2013", password: "re2013", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})
User.create!({email: "2014@aaa.bbb", name: "吉川", account: "2014", role: 3, encrypted_password: "re2014", password: "re2014", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})

User.create!({email: "5001@aaa.bbb", name: "パートナーズ", account: "5001", role: 2, encrypted_password: "re5001", password: "re5001", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-09-09 16:47:05", last_sign_in_at: "2018-09-09 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 1})

Owner.create!([
  {code: "owner_1", name: "オーナー1", postal_code: "", address: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil,lock_version: 0},
  {code: "owner_2", name: "オーナー2", postal_code: "", address: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
  {code: "owner_3", name: "オーナー3", postal_code: "", address: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0}
])

# buildings = {postal_code: "", address: "", room_count: 1, total_floor_area: "", standart_people: 1, max_people: 1, bath_count: 1, toilet_count: 1, secret_number: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0}buildings = {postal_code: "", address: "", room_count: 1, total_floor_area: "", standart_people: 1, max_people: 1, bath_count: 1, toilet_count: 1, secret_number: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0}
buildings = {}
building_params = {is_visible: true, postal_code: "", address: "", created_user_id: 0, updated_user_id: 0, lock_version: 0}
buildings["1001"] = Building.create!(building_params.merge(owner_id: 1, code: "1001", name: "紡 東福寺南門", room_count: 1))
buildings["1002"] = Building.create!(building_params.merge(owner_id: 1, code: "1002", name: "紡 六条西洞院", room_count: 1))
buildings["1003"] = Building.create!(building_params.merge(owner_id: 1, code: "1003", name: "紡 八条室町", room_count: 1))
buildings["1004"] = Building.create!(building_params.merge(owner_id: 2, code: "1004", name: "紡 神泉苑南", room_count: 1))
buildings["1005"] = Building.create!(building_params.merge(owner_id: 2, code: "1005", name: "紡 Hostel円町", room_count: 3))
buildings["1006"] = Building.create!(building_params.merge(owner_id: 2, code: "1006", name: "紡 松原堺町", room_count: 1))
buildings["1007"] = Building.create!(building_params.merge(owner_id: 2, code: "1007", name: "紡 高倉葛籠屋町", room_count: 3))
buildings["1008"] = Building.create!(building_params.merge(owner_id: 2, code: "1008", name: "紡 清水清閑寺", room_count: 1))

buildings["1009"] = Building.create!(building_params.merge(owner_id: 2, code: "1009", name: "紡 東寺西門前", room_count: 2))
buildings["1010"] = Building.create!(building_params.merge(owner_id: 2, code: "1010", name: "紡 伏見稲荷", room_count: 1))
buildings["1011"] = Building.create!(building_params.merge(owner_id: 2, code: "1011", name: "紡 伏見稲荷別邸", room_count: 1))
buildings["1012"] = Building.create!(building_params.merge(owner_id: 2, code: "1012", name: "紡 島原大門別邸", room_count: 1))
buildings["1013"] = Building.create!(building_params.merge(owner_id: 2, code: "1013", name: "紡 島原大門", room_count: 3))
buildings["1014"] = Building.create!(building_params.merge(owner_id: 2, code: "1014", name: "紡 東寺南門", room_count: 2))
buildings["1015"] = Building.create!(building_params.merge(owner_id: 2, code: "1015", name: "紡 泉涌寺", room_count: 1))
buildings["1016"] = Building.create!(building_params.merge(owner_id: 2, code: "1016", name: "紡 三十三間堂", room_count: 1))
buildings["1099"] = Building.create!(building_params.merge(owner_id: 2, code: "1099", name: "紡 京都駅前", room_count: 3))
buildings["2001"] = Building.create!(building_params.merge(owner_id: 2, code: "2001", name: "Connect inn 京都西陣", room_count: 9))
buildings["2002"] = Building.create!(building_params.merge(owner_id: 2, code: "2002", name: "F's flats", room_count: 2))

# Profile
profile_params = {total_floor_area: 1, standard_people: 10, max_people: 12, bath_count: 2, toilet_count: 2, secret_number: 'zaq12wsx'}
Building.all.each do |b|
  b.profile = Profile.create!(profile_params)
end

rooms = {is_visible: true, created_user_id: 0, updated_user_id: 0, lock_version: 0}
Room.create!([
  rooms.merge(building_id: buildings["1001"].id, room_no: 1, name: ""),
  rooms.merge(building_id: buildings["1002"].id, room_no: 1, name: ""),
  rooms.merge(building_id: buildings["1003"].id, room_no: 1, name: ""),
  rooms.merge(building_id: buildings["1004"].id, room_no: 1, name: ""),

  rooms.merge(building_id: buildings["1005"].id, room_no: 1, name: "1F"),
  rooms.merge(building_id: buildings["1005"].id, room_no: 2, name: "2F和室"),
  rooms.merge(building_id: buildings["1005"].id, room_no: 3, name: "2F洋室"),

  rooms.merge(building_id: buildings["1006"].id, room_no: 1, name: ""),

  rooms.merge(building_id: buildings["1007"].id, room_no: 1, name: "1F"),
  rooms.merge(building_id: buildings["1007"].id, room_no: 2, name: "2F洋室"),
  rooms.merge(building_id: buildings["1007"].id, room_no: 3, name: "2F和室"),

  rooms.merge(building_id: buildings["1008"].id, room_no: 1, name: ""),

  rooms.merge(building_id: buildings["1009"].id, room_no: 1, name: "1階和洋室"),
  rooms.merge(building_id: buildings["1009"].id, room_no: 2, name: "2階洋室"),

  rooms.merge(building_id: buildings["1010"].id, room_no: 1, name: ""),
  rooms.merge(building_id: buildings["1011"].id, room_no: 1, name: ""),
  rooms.merge(building_id: buildings["1012"].id, room_no: 1, name: ""),

  rooms.merge(building_id: buildings["1013"].id, room_no: 1, name: "1F坪庭付和洋室"),
  rooms.merge(building_id: buildings["1013"].id, room_no: 2, name: "2F和洋室(東)"),
  rooms.merge(building_id: buildings["1013"].id, room_no: 3, name: "2F和洋室(西)"),

  rooms.merge(building_id: buildings["1014"].id, room_no: 1, name: "東"),
  rooms.merge(building_id: buildings["1014"].id, room_no: 2, name: "西"),

  rooms.merge(building_id: buildings["1015"].id, room_no: 1, name: ""),

  rooms.merge(building_id: buildings["1016"].id, room_no: 1, name: ""),

  rooms.merge(building_id: buildings["1099"].id, room_no: 1, name: "201"),
  rooms.merge(building_id: buildings["1099"].id, room_no: 2, name: "202"),
  rooms.merge(building_id: buildings["1099"].id, room_no: 3, name: "203"),

  rooms.merge(building_id: buildings["2001"].id, room_no: 1, name: "201"),
  rooms.merge(building_id: buildings["2001"].id, room_no: 2, name: "202"),
  rooms.merge(building_id: buildings["2001"].id, room_no: 3, name: "203"),
  rooms.merge(building_id: buildings["2001"].id, room_no: 4, name: "301"),
  rooms.merge(building_id: buildings["2001"].id, room_no: 5, name: "302"),
  rooms.merge(building_id: buildings["2001"].id, room_no: 6, name: "303"),
  rooms.merge(building_id: buildings["2001"].id, room_no: 7, name: "304"),
  rooms.merge(building_id: buildings["2001"].id, room_no: 8, name: "305"),
  rooms.merge(building_id: buildings["2001"].id, room_no: 9, name: "306"),

  rooms.merge(building_id: buildings["2002"].id, room_no: 1, name: "104号室"),
  rooms.merge(building_id: buildings["2002"].id, room_no: 2, name: "301号室"),
])

Room.all.each do |r|
  r.profile = Profile.create!(profile_params)
end

Building.all.each { |b| Building.update_room_count(id: b.id)}

Nationality.create!([
  {name: "日本", order: 1, is_visible: true},
  {name: "中国", order: 2, is_visible: true},
  {name: "台湾", order: 3, is_visible: true},
  {name: "韓国", order: 4, is_visible: true},
  {name: "香港", order: 5, is_visible: true},
  {name: "シンガポール", order: 6, is_visible: true},
  {name: "タイ", order: 7, is_visible: true},
  {name: "マレーシア", order: 8, is_visible: true},
  {name: "インドネシア", order: 9, is_visible: true},
  {name: "インド", order: 10, is_visible: true},
  {name: "マカオ", order: 11, is_visible: true},
  {name: "アメリカ", order: 12, is_visible: true},
  {name: "カナダ", order: 13, is_visible: true},
  {name: "メキシコ", order: 14, is_visible: true},
  {name: "オーストラリア", order: 15, is_visible: true},
  {name: "ニュージーランド", order: 16, is_visible: true},
  {name: "イギリス", order: 17, is_visible: true},
  {name: "ドイツ", order: 18, is_visible: true},
  {name: "スペイン", order: 19, is_visible: true},
  {name: "イタリア", order: 20, is_visible: true},
  {name: "フランス", order: 21, is_visible: true},
  {name: "スイス", order: 22, is_visible: true},
  {name: "デンマーク", order: 23, is_visible: true},
  {name: "アイルランド", order: 24, is_visible: true},
  {name: "ロシア", order: 25, is_visible: true},
  {name: "スウェーデン", order: 26, is_visible: true},
  {name: "ノルウェー", order: 27, is_visible: true},
  {name: "フィンランド", order: 28, is_visible: true},
  {name: "アンゴラ", order: 29, is_visible: true},
  {name: "オランダ", order: 30, is_visible: true},
])

# 精算書テンプレート
attrs = {
  title: '精算書', honorific: '様', building: '施設名', address: '施設住所',
  company_name: '株式会社レ・コネクション', company_info1: '〒600-8216', company_info2: '京都府京都市下京区東塩小路町684',
  company_info3: 'Tel：075-352-8400　　Fax：075-352-8700',
  visitor_name: '宿泊者名', nationality: '国籍', number_of_nights: '泊数', total_count: '合計人数', count: '人数',
  adult_count: '大人', child_count: '小人', room_charge: '宿泊料', cleaning_fee: '清掃費', total: '合計',
  site: '予約サイト', handling_charge: '手数料', reservation_date: '予約日',
  analysis_title: '${Y}年${m}月分精算金合計',
  sales: '売上', target_days: '日数', working_days: '稼働日数', guest_count: '宿泊人数',
  occ: 'OCC(稼働率)', adr: 'ADR(客室販売単価)', rev_par: 'RevPAR(稼働率×単価)',
  other_income: 'その他収入', other_cost: 'その他費用', reward_rate: '報酬率', payment_amount: '入金額',
  next_month_title: '次月予約状況',
  internet_income: 'インターネット代', free_income1: '食事代', total_income: '合計',
  site_fee: 'サイト手数料', electrical_fee: '電気代', gas_fee: 'ガス代', water_fee: '水道代', internet_fee: 'インターネット費用', site_controller_fee: 'サイトコントローラー',
  note1: '※振込手数料はオーナー様のご負担とさせていただきます。', note2: '※ご入金は翌月末となります。'
}
SettlementTemplate::LANGUAGE_LIST.each do |lang|
  template = SettlementTemplate.find_or_create_by(language: lang)
  template.update! attrs
end
