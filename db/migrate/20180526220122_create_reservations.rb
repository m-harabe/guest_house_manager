class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations, comment: '予約情報' do |t|
      t.references :room, foreign_key: true,                                      comment: "部屋ID"

      t.integer :whole_reservation_id,                                            comment: "一棟貸情報ID"
      t.string  :status,                null: false,  default: "1",   limit: 1,   comment: "ステータス 1: CI不可（清掃待）, 2: CI可, 3: CI中, 4:CO済, 9: キャンセル"
      t.string  :site_code,             null: false,  default: "",    limit: 4,   comment: "予約サイトコード"
      t.integer :nationality_id,        null: false,  default: 0,                 comment: "国籍ID"
      t.string  :visitor_name,          null: false,  default: "",                comment: "ゲスト名"
      t.string  :mail_address,          null: false,  default: "",                comment: "メールアドレス"
      t.string  :tel,                   null: false,  default: "",                comment: "TEL"
      t.integer :adult_count,           null: false,  default: 0,                 comment: "宿泊人数（大人）"
      t.integer :child_count,           null: false,  default: 0,                 comment: "宿泊人数（子供）"
      t.integer :room_charge,           null: false,  default: 0,                 comment: "宿泊料"
      t.integer :cleaning_fee,          null: false,  default: 0,                 comment: "清掃費"
      t.integer :total,                 null: false,  default: 0,                 comment: "合計（宿泊費＋清掃費）"
      t.float   :handling_charge_rate,                                            comment: "手数料率"
      t.date    :reserve_date,                                                    comment: "予約日"
      t.date    :check_in_date,                                                   comment: "チェックイン日"
      t.date    :check_out_date,                                                  comment: "チェックアウト日"
      t.integer :number_of_nights,      null: false,  default: 1,                 comment: "宿泊日数"
      t.text    :note,                                                            comment: "備考"

      t.integer    :created_user_id,    null: false,  default: 0
      t.integer    :updated_user_id,    null: false,  default: 0
      t.integer    :lock_version,       null: false,  default: 0
      t.timestamps null: false

    end
  end
end
