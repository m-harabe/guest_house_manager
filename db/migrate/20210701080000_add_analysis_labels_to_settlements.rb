class AddAnalysisLabelsToSettlements < ActiveRecord::Migration[5.2]
  def change
    # 集計情報ラベルの追加
    add_column  :settlements,  :label_sales,               :string,  comment: "売上"
    add_column  :settlements,  :label_target_days,         :string,  comment: "日数"
    add_column  :settlements,  :label_working_days,        :string,  comment: "稼働日数"
    add_column  :settlements,  :label_guest_count,         :string,  comment: "宿泊人数"
    add_column  :settlements,  :label_occ,                 :string,  comment: "OCC(稼働率)"
    add_column  :settlements,  :label_adr,                 :string,  comment: "ADR(客室販売単価)"
    add_column  :settlements,  :label_rev_par,             :string,  comment: "RevPAR(稼働率×単価)"

    add_column  :settlements,  :label_owner_sales,         :string,  comment: "オーナー売上"
    add_column  :settlements,  :label_other_income_total,  :string,  comment: "その他収入（合計）"
    add_column  :settlements,  :label_other_cost_total,    :string,  comment: "その他費用（合計）"

    add_column  :settlements,  :label_reward_rate,         :string,  comment: "報酬率"
    add_column  :settlements,  :label_payment_amount,      :string,  comment: "入金額"
  end
end
