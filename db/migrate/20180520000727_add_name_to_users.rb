class AddNameToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column   :users, :name,            :string
    add_column   :users, :kana,            :string
    add_column   :users, :tel,             :string
    add_column   :users, :account,         :string,               null: false
    add_column   :users, :role,            :integer,  default: 0, null: false

    add_column   :users, :created_user_id, :integer,  default: 0, null: false
    add_column   :users, :updated_user_id, :integer,  default: 0, null: false
    add_column   :users, :deleted_at,      :timestamp
    add_column   :users, :lock_version,    :integer,  default: 0, null: false
  end
end
