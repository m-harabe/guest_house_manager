class CreateOwners < ActiveRecord::Migration[5.2]
  def change
    create_table :owners, comment: 'オーナー' do |t|
      t.string :code,        null: false,  default: "",  index: true,  comment: 'コード'
      t.string :name,        null: false,  default: "",  limit: 20,    comment: '名称'
      t.string :postal_code, null: false,  default: "",  limit: 7,     comment: '郵便番号'
      t.string :address,     null: false,  default: "",  limit: 256,   comment: '住所'

      t.integer    :created_user_id,  null: false,  default: 0
      t.integer    :updated_user_id,  null: false,  default: 0
      t.timestamp  :deleted_at
      t.integer    :lock_version,     null: false,  default: 0
      t.timestamps null: false
    end
  end
end
