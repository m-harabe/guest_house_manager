class AddMailAddressToOwners < ActiveRecord::Migration[5.2]
  def change
    add_column :owners, :mail_address2,  :string,  comment: "メールアドレス２"
    add_column :owners, :mail_address3,  :string,  comment: "メールアドレス３"
  end

  def down
    remove_column :owners, :mail_address2,     :string
    remove_column :owners, :mail_address3,     :string
  end
end
