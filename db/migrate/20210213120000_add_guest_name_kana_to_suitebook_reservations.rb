class AddGuestNameKanaToSuitebookReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :suitebook_reservations, :visitor_name_kana,    :string,                                     comment: "ゲスト名フリガナ"
    add_column :suitebook_reservations, :postal_code,          :string, limit: 8, default: "", null: false, comment: "郵便番号"
    add_column :suitebook_reservations, :address,              :string,                                     comment: "住所/国籍"
    
    # add_column :reservations,           :postal_code,          :string, limit: 8, default: "", null: false, comment: "郵便番号"
    # add_column :whole_reservations,     :postal_code,          :string, limit: 8, default: "", null: false, comment: "郵便番号"
  end
end
