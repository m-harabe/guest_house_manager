class AddScheduledCheckInTimeToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :scheduled_check_in_time, :datetime, comment: "予定チェックイン時刻"
    add_column :reservations, :accommodation_tax,       :integer,  comment: "宿泊税"

    add_index :reservations, :whole_reservation_id
    add_index :reservations, :status
    add_index :reservations, :check_in_date
  end
end
