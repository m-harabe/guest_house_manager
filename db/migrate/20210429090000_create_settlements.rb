class CreateSettlements < ActiveRecord::Migration[5.2]
  def change

    # 精算書
    create_table :settlements, comment: "精算書" do |t|

      t.references :settlement_template, foreign_key: true,          comment: "精算書テンプレートID"
      t.bigint  :room_id,          null: false,                      comment: "部屋ID"

      # メタ情報
      t.string  :year_month,       null: false,                      comment: "年月"
      t.integer :download_count,   null: false,  default: 0,         comment: "ダウンロード回数"

      # タイトル、オーナー情報
      t.string  :title,            null: false,  default: "",        comment: "タイトル"
      t.string  :owner_name,       null: false,  default: "",        comment: "オーナー名称"
      t.string  :building,         null: false,  default: "",        comment: "施設名"
      t.string  :address,          null: false,  default: "",        comment: "施設住所"

      # 自社情報
      t.string  :company_name,     null: false,  default: "",        comment: "自社名"
      t.string  :company_info1,    null: false,  default: "",        comment: "会社情報１"
      t.string  :company_info2,    null: false,  default: "",        comment: "会社情報２"
      t.string  :company_info3,    null: false,  default: "",        comment: "会社情報３"

      t.float   :reward_rate,      null: false,  default: 0.0,       comment: "報酬比率"

      # ラベル（収入）
      t.string  :label_other_income,     null: false,  default: "",      comment: "その他収入ラベル"
      t.string  :label_internet_income,  null: false,  default: "",      comment: "その他収入ラベル"
      t.string  :label_free_income1,     null: false,  default: "",      comment: "その他収入１ラベル"
      t.string  :label_free_income2,     null: false,  default: "",      comment: "その他収入２ラベル"
      t.string  :label_free_income3,     null: false,  default: "",      comment: "その他収入３ラベル"
      t.string  :label_free_income4,     null: false,  default: "",      comment: "その他収入４ラベル"
      t.string  :label_free_income5,     null: false,  default: "",      comment: "その他収入５ラベル"
      t.string  :label_total_income,     null: false,  default: "",      comment: "その他収入合計ラベル"
      # ラベル（費用）
      t.string  :label_other_cost,       null: false,  default: "",      comment: "その他費用ラベル"
      t.string  :label_site_fee,         null: false,  default: "",      comment: "サイト手数料ラベル"
      t.string  :label_electrical_fee,   null: false,  default: "",      comment: "電気代ラベル"
      t.string  :label_gas_fee,          null: false,  default: "",      comment: "ガス代ラベル"
      t.string  :label_water_fee,        null: false,  default: "",      comment: "水道代ラベル"
      t.string  :label_internet_fee,     null: false,  default: "",      comment: "インターネット費用ラベル"
      t.string  :label_site_controller_fee, null: false,  default: "",   comment: "サイトコントローラーラベル"
      t.string  :label_free_fee1,        null: false,  default: "",      comment: "その他費用１ラベル"
      t.string  :label_free_fee2,        null: false,  default: "",      comment: "その他費用２ラベル"
      t.string  :label_free_fee3,        null: false,  default: "",      comment: "その他費用３ラベル"
      t.string  :label_free_fee4,        null: false,  default: "",      comment: "その他費用４ラベル"
      t.string  :label_free_fee5,        null: false,  default: "",      comment: "その他費用５ラベル"
      t.string  :label_total_fee,        null: false,  default: "",      comment: "その他費用合計ラベル"

      # 収入internet_income
      t.integer :internet_income,                                 comment: "インターネット代"
      t.integer :free_income1,                                    comment: "その他収入１"
      t.integer :free_income2,                                    comment: "その他収入２"
      t.integer :free_income3,                                    comment: "その他収入３"
      t.integer :free_income4,                                    comment: "その他収入４"
      t.integer :free_income5,                                    comment: "その他収入５"
      # 費用
      t.integer :site_fee,                                        comment: "サイト手数料"
      t.integer :electrical_fee,                                  comment: "電気代"
      t.integer :gas_fee,                                         comment: "ガス代"
      t.integer :water_fee,                                       comment: "水道代"
      t.integer :internet_fee,                                    comment: "インターネット費用"
      t.integer :site_controller_fee,                             comment: "サイトコントローラー"
      t.integer :free_fee1,                                       comment: "その他費用１"
      t.integer :free_fee2,                                       comment: "その他費用２"
      t.integer :free_fee3,                                       comment: "その他費用３"
      t.integer :free_fee4,                                       comment: "その他費用４"
      t.integer :free_fee5,                                       comment: "その他費用５"

      t.string  :note1,            null: false,  default: "",                comment: "備考１"
      t.string  :note2,            null: false,  default: "",                comment: "備考２"
      t.string  :note3,            null: false,  default: "",                comment: "備考３"
      t.string  :note4,            null: false,  default: "",                comment: "備考４"

      t.integer    :created_user_id,  null: false,  default: 0
      t.integer    :updated_user_id,  null: false,  default: 0
      t.integer    :lock_version,     null: false,  default: 0
      t.timestamps null: false
    end

    add_index :settlements, [:room_id, :year_month], unique: true
    add_foreign_key :settlements, :rooms
  end
end
