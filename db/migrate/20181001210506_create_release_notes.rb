class CreateReleaseNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :release_notes, comment: "リリースノート" do |t|
      t.date       :open_date,          null: false,                          index: true,  comment: "公開日"
      t.string     :title,              null: false,                                        comment: "タイトル"
      t.text       :content,            null: false,                                        comment: "内容"
      t.string     :note_class,                        limit: 1,              index: true,  comment: "リリースノート区分"

      t.integer    :created_user_id,    null: false,             default: 0
      t.integer    :updated_user_id,    null: false,             default: 0
      t.integer    :lock_version,       null: false,             default: 0
      t.timestamps null: false
    end
  end
end
