class CreateBuildings < ActiveRecord::Migration[5.2]
  def change
    create_table :buildings, comment: "物件" do |t|

      t.integer :owner_id,            null: false,  default: 0,                              comment: "オーナーID"
      t.string  :code,                null: false,  default: '',   limit: 10,  index: true,  comment: "コード"
      t.string  :name,                null: false,  default: '',   limit: 20,                comment: "名称"
      t.string  :postal_code,         null: false,  default: '',   limit: 8,                 comment: "郵便番号"
      t.string  :address,             null: false,  default: '',   limit: 256,               comment: "住所"
      t.integer :room_count,          null: false,  default: 1,                              comment: "部屋数"
      t.boolean :is_visible,          null: false,  default: true,                           comment: "表示"
      t.text    :note,                                                                       comment: "備考"

      # また、別途添付ファイル管理テーブルを作成する STIにする

      t.integer    :created_user_id,  null: false,  default: 0
      t.integer    :updated_user_id,  null: false,  default: 0
      t.integer    :lock_version,     null: false,  default: 0
      t.timestamps null: false
    end
  end
end
