class AddErrorMessageToSuitebookReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :suitebook_reservations, :error_message, :text, comment: "エラーメッセージ"
    add_column :suitebook_reservations, :building_id,  :integer, comment: "物件ID"
  end
end
