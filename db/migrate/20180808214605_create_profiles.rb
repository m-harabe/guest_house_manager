class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles, comment: "物件詳細情報" do |t|
      t.references :profilable, polymorphic: true, index: true
      # t.integer :building_id,                                                    comment: "施設ID"
      # t.integer :room_id,                                                        comment: "部屋ID"

      t.string  :postal_code,         null: false,  default: '',  limit: 8,      comment: "郵便番号"
      t.string  :address,             null: false,  default: '',  limit: 100,    comment: "住所"
      t.integer :total_floor_area,                  default: 0,                  comment: "延床面積"
      t.integer :standard_people,                   default: 0,                  comment: "標準人数"
      t.integer :max_people,                        default: 0,                  comment: "最大人数"
      t.integer :bath_count,                        default: 0,                  comment: "風呂数"
      t.integer :toilet_count,                      default: 0,                  comment: "トイレ数"
      t.string  :secret_number,       null: false,  default: '',  limit: 25,     comment: "暗証番号"
    end
  end
end
