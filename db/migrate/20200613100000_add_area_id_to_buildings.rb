class AddAreaIdToBuildings < ActiveRecord::Migration[5.2]
  def change
    add_column :buildings, :area_id,  :integer, comment: "エリアID"
  end
end
