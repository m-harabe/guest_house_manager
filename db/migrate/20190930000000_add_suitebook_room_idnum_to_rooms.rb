class AddSuitebookRoomIdnumToRooms < ActiveRecord::Migration[5.2]
  def change
    add_column :rooms, :suitebook_room_idnum,  :integer, comment: "suitebook room ID"
  end
end
