class AddTotalCommentsToReservations < ActiveRecord::Migration[5.2]

  # 食事代のカラム追加に伴う、total （合計）カラムのコメント変更
  def up
    change_column :reservations,       :total,  :integer,  null: false,  default: 0,  comment: "合計（宿泊費＋清掃費＋食事代）"
    change_column :whole_reservations, :total,  :integer,  null: false,  default: 0,  comment: "合計（宿泊費＋清掃費＋食事代）"
  end

  def down
    change_column :reservations,       :total,  :integer,  null: false,  default: 0,  comment: "合計（宿泊費＋清掃費）"
    change_column :whole_reservations, :total,  :integer,  null: false,  default: 0,  comment: "合計（宿泊費＋清掃費）"
  end
end
