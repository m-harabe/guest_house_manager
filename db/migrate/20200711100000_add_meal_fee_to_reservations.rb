class AddMealFeeToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations,       :meal_fee,  :integer,  null: false,  default: 0, comment: "食事代"
    add_column :whole_reservations, :meal_fee,  :integer,  null: false,  default: 0, comment: "食事代"
    add_column :reservation_dates,  :meal_fee,  :integer,  null: false,  default: 0, comment: "食事代"
  end
end
