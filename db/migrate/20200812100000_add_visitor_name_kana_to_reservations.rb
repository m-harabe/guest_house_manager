class AddVisitorNameKanaToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations,       :visitor_name_kana,  :string, default: "", null: false, comment: "ゲスト名カナ"
    add_column :whole_reservations, :visitor_name_kana,  :string, default: "", null: false, comment: "ゲスト名カナ"
    add_column :wk_reservations,    :visitor_name_kana,  :string,                           comment: "ゲスト名カナ" # 現在はテーブル不使用
  end
end
