class CreateReservationDates < ActiveRecord::Migration[5.2]
  def change
    create_table :reservation_dates, comment: '宿泊日別情報' do |t|
      t.references :reservation, foreign_key: true,                         comment: "予約ID"

      t.integer :building_id,      null: false,  default: 0,                comment: "物件ID"
      t.integer :room_id,          null: false,  default: 0,                comment: "部屋ID"
      t.date    :check_in_day,                                              comment: "宿泊日"
      t.integer :room_charge,      null: false,  default: 0,                comment: "宿泊料"
      t.integer :cleaning_fee,     null: false,  default: 0,                comment: "清掃費"
      t.integer :total,            null: false,  default: 0,                comment: "合計（宿泊費＋清掃費）"
      t.decimal :handling_charge,  precision: 7, scale: 2,                  comment: "手数料"

      t.timestamps null: false
    end
  end
end
