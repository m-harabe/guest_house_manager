class AddScheduledCheckInTimeToWholeReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :whole_reservations, :scheduled_check_in_time, :datetime, comment: "予定チェックイン時刻１"
    add_column :whole_reservations, :accommodation_tax,       :integer,  comment: "宿泊税"

    add_index :whole_reservations, :status
    add_index :whole_reservations, :check_in_date
  end
end
