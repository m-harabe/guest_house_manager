class AddRewardRateToBuildings < ActiveRecord::Migration[5.2]
  def change
    add_column :buildings, :reward_rate,  :float,  null: false,  default: 0.0,  comment: "報酬比率"
  end
end
