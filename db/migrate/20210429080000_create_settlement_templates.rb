class CreateSettlementTemplates < ActiveRecord::Migration[5.2]
  def change

    # 精算書の言語ごと（日本語・中国語の２レコード）のラベル設定
    create_table :settlement_templates, comment: "精算書テンプレート" do |t|

      # タイトル、オーナー情報
      t.string  :language,         null: false,  default: "",                comment: "言語"
      t.string  :title,            null: false,  default: "",                comment: "タイトル"
      t.string  :honorific,        null: false,  default: "",                comment: "敬称"
      t.string  :building,         null: false,  default: "",                comment: "施設名"
      t.string  :address,          null: false,  default: "",                comment: "施設住所"

      # 自社情報
      t.string  :company_name,     null: false,  default: "",                comment: "自社名"
      t.string  :company_info1,    null: false,  default: "",                comment: "会社情報１"
      t.string  :company_info2,    null: false,  default: "",                comment: "会社情報２"
      t.string  :company_info3,    null: false,  default: "",                comment: "会社情報３"

      # 一覧項目
      t.string  :visitor_name,     null: false,  default: "",                comment: "宿泊者名"
      t.string  :nationality,      null: false,  default: "",                comment: "国籍"
      t.string  :number_of_nights, null: false,  default: "",                comment: "泊数"
      t.string  :total_count,      null: false,  default: "",                comment: "合計人数"
      t.string  :count,            null: false,  default: "",                comment: "人数" # 未使用
      t.string  :adult_count,      null: false,  default: "",                comment: "大人"
      t.string  :child_count,      null: false,  default: "",                comment: "小人"
      t.string  :room_charge,      null: false,  default: "",                comment: "宿泊料"
      t.string  :cleaning_fee,     null: false,  default: "",                comment: "清掃費"
      t.string  :total,            null: false,  default: "",                comment: "合計"
      t.string  :site,             null: false,  default: "",                comment: "予約サイト"
      t.string  :handling_charge,  null: false,  default: "",                comment: "手数料"
      t.string  :reservation_date, null: false,  default: "",                comment: "予約日"

      # 集計情報
      t.string  :analysis_title,   null: false,  default: "",                comment: "集計タイトル"
      t.string  :sales,            null: false,  default: "",                comment: "売上"
      t.string  :target_days,      null: false,  default: "",                comment: "日数"
      t.string  :working_days,     null: false,  default: "",                comment: "稼働日数"
      t.string  :guest_count,      null: false,  default: "",                comment: "宿泊人数"
      t.string  :occ,              null: false,  default: "",                comment: "OCC(稼働率)"
      t.string  :adr,              null: false,  default: "",                comment: "ADR(客室販売単価)"
      t.string  :rev_par,          null: false,  default: "",                comment: "RevPAR(稼働率×単価)"

      t.string  :other_income,     null: false,  default: "",                comment: "その他収入"
      t.string  :other_cost,       null: false,  default: "",                comment: "その他費用"

      t.string  :reward_rate,      null: false,  default: "",                comment: "報酬率"
      t.string  :payment_amount,   null: false,  default: "",                comment: "入金額"

      t.string  :next_month_title, null: false,  default: "",                comment: "次月予約状況"

      t.string  :internet_income,  null: false,  default: "",                comment: "インターネット代"
      t.string  :free_income1,     null: false,  default: "",                comment: "その他収入１"
      t.string  :free_income2,     null: false,  default: "",                comment: "その他収入２"
      t.string  :free_income3,     null: false,  default: "",                comment: "その他収入３"
      t.string  :free_income4,     null: false,  default: "",                comment: "その他収入４"
      t.string  :free_income5,     null: false,  default: "",                comment: "その他収入５"
      t.string  :total_income,     null: false,  default: "",                comment: "その他収入合計"

      t.string  :site_fee,         null: false,  default: "",                comment: "サイト手数料"
      t.string  :electrical_fee,   null: false,  default: "",                comment: "電気代"
      t.string  :gas_fee,          null: false,  default: "",                comment: "ガス代"
      t.string  :water_fee,        null: false,  default: "",                comment: "水道代"
      t.string  :internet_fee,     null: false,  default: "",                comment: "インターネット費用"
      t.string  :site_controller_fee, null: false,  default: "",             comment: "サイトコントローラー"
      t.string  :free_fee1,        null: false,  default: "",                comment: "その他費用１"
      t.string  :free_fee2,        null: false,  default: "",                comment: "その他費用２"
      t.string  :free_fee3,        null: false,  default: "",                comment: "その他費用３"
      t.string  :free_fee4,        null: false,  default: "",                comment: "その他費用４"
      t.string  :free_fee5,        null: false,  default: "",                comment: "その他費用５"
      t.string  :total_fee,        null: false,  default: "",                comment: "その他費用合計"

      t.string  :payee,            null: false,  default: "",                comment: "振込先"
      t.string  :note1,            null: false,  default: "",                comment: "備考１"
      t.string  :note2,            null: false,  default: "",                comment: "備考２"
      t.string  :note3,            null: false,  default: "",                comment: "備考３"
      t.string  :note4,            null: false,  default: "",                comment: "備考４"

      t.integer    :created_user_id,  null: false,  default: 0
      t.integer    :updated_user_id,  null: false,  default: 0
      t.integer    :lock_version,     null: false,  default: 0
      t.timestamps null: false
    end

    add_index :settlement_templates, :language, unique: true
  end
end
