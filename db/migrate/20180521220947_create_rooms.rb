class CreateRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms, comment: "部屋" do |t|
      t.references :building, foreign_key: true, comment: "物件ID"

      t.integer :room_no,             null: false,  default: 0,      index: true,   comment: "表示順"
      t.string  :name,                null: false,  default: "",     limit: 20,     comment: "名称"
      t.boolean :is_visible,          null: false,  default: true,                  comment: "表示"
      t.text    :note,                                                              comment: "備考"

      t.integer    :created_user_id,  null: false,  default: 0
      t.integer    :updated_user_id,  null: false,  default: 0
      t.integer    :lock_version,     null: false,  default: 0
      t.timestamps null: false
    end
  end
end
