class AddRoomIdToSuitebookReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :suitebook_reservations, :room_id,  :integer, comment: "部屋ID"
  end
end
