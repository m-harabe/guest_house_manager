class CreateNationalities < ActiveRecord::Migration[5.2]
  def change
    create_table :nationalities do |t|
      t.string  :name,             null: false,  default: "",                comment: "国籍名"
      t.integer :order,            null: false,  default: 0,                 comment: "並び順"
      t.boolean :is_visible,       null: false,  default: true,              comment: "表示"
    end
  end
end
