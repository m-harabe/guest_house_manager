class RecreateOwners < ActiveRecord::Migration[5.2]
  def up
    remove_column :owners, :code,        :string
    remove_column :owners, :address,     :string
    remove_column :owners, :deleted_at,  :timestamps

    change_column :owners, :name,        :string,                default: '',             comment: "氏名"
    change_column :owners, :postal_code, :string,  null: false,  default: '',  limit: 8,  comment: "郵便番号"

    add_column :owners, :address1,      :string,                default: "",                comment: "住所１"
    add_column :owners, :address2,      :string,                default: "",                comment: "住所２"
    add_column :owners, :tel,           :string,  null: false,  default: "",                comment: "TEL"
    add_column :owners, :mail_address,  :string,                                            comment: "メールアドレス"
    add_column :owners, :bank_name,     :string,                default: "",                comment: "振込先銀行名（支店名）"
    add_column :owners, :account_type,  :integer,               default: 0,     limit: 1,   comment: "口座種別（0: 指定なし  1:普通  2:当座）"
    add_column :owners, :account_number,:string,                default: "",                comment: "口座番号"
    add_column :owners, :swift_code,    :string,                default: "",                comment: "SWIFTコード"
    add_column :owners, :account_name,  :string,                default: "",                comment: "口座名義"
    add_column :owners, :language,      :string,                default: "",                comment: "精算書言語"
    add_column :owners, :note,          :text,                                              comment: "メモ"
    add_column :owners, :is_visible,    :boolean, null: false,  default: true,              comment: "表示"

    add_index  :owners, :is_visible
  end

  def down
    add_column :owners, :code, :string
    add_column :owners, :address, :string
    add_column :owners, :deleted_at, :timestamp

    remove_column :owners, :address1,     :string
    remove_column :owners, :address2,     :string
    remove_column :owners, :tel,          :string
    remove_column :owners, :mail_address, :string
    remove_column :owners, :bank_name,    :string
    remove_column :owners, :account_type, :integer
    remove_column :owners, :account_number, :string
    remove_column :owners, :swift_code,   :string
    remove_column :owners, :account_name, :string
    remove_column :owners, :language,     :string
    remove_column :owners, :note,         :text
    remove_column :owners, :is_visible,   :boolean
  end

end
