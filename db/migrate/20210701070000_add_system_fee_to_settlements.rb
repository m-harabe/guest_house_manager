class AddSystemFeeToSettlements < ActiveRecord::Migration[5.2]
  def change
    add_column :settlements, :system_fee1,        :integer,  comment: "システム自動追加費用１"
    add_column :settlements, :label_system_fee1,  :string,   comment: "システム自動追加費用１ラベル"
  end
end
