class AddIsVisibleToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :is_visible,  :boolean, null: false, default: true, comment: "表示"
  end
end
