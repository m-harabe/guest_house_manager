class CreateAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :areas, comment: "エリア" do |t|

      t.string  :name,             null: false,  default: "",    limit: 10,  comment: "エリア名"
      t.string  :area_cd,          null: false,  default: "",    limit: 6,   comment: "コード（並び順）"
      t.boolean :is_visible,       null: false,  default: true,              comment: "表示"

      t.integer    :created_user_id,  null: false,  default: 0
      t.integer    :updated_user_id,  null: false,  default: 0
      t.integer    :lock_version,     null: false,  default: 0
      t.timestamps null: false
    end
    add_index  :areas, :area_cd, unique: true
    add_index  :areas, :is_visible

  end
end
