class AddPointToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :point_use,      :integer, comment: "ポイント利用"
    add_column :reservations, :owner_note,     :text,    comment: "オーナー向けドキュメント"
  end
end