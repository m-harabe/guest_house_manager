class AddTelToSuitebookReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :suitebook_reservations, :property_name,     :string, comment: "建物名"
    add_column :suitebook_reservations, :tel,               :string, comment: "電話番号"
    add_column :suitebook_reservations, :cancel_fee,        :string, comment: "キャンセル料"
    add_column :suitebook_reservations, :point_use,         :string, comment: "ポイント利用"
    add_column :suitebook_reservations, :cancel_charge,     :string, comment: "手数料 (キャンセル時)"
    add_column :suitebook_reservations, :total_minus_point, :string, comment: "ゲストへの請求金額(ポイント割引後)"
    add_column :suitebook_reservations, :cancel_revenue,    :string, comment: "あなたの収益 (キャンセル時)"
    add_column :suitebook_reservations, :payment,           :string, comment: "決済"
    add_column :suitebook_reservations, :plan,              :string, comment: "プラン名"
    add_column :suitebook_reservations, :owner_note,        :text,   comment: "オーナー向けドキュメント"
  end
end
