class CreateWkReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :wk_reservations, comment: '一時予約情報' do |t|
      # t.boolean :is_error,       default: false  comment: "エラーの有無 true: エラーあり"
      t.date    :check_in_date,                  comment: "チェックイン日"
      t.string  :building_code,                  comment: "施設コード"
      t.integer :building_id,                    comment: "施設ID"
      t.string  :room_no,                        comment: "部屋番号（表示順）"
      t.integer :room_id,                        comment: "部屋ID"
      t.integer :number_of_nights,               comment: "泊数"
      t.string  :visitor_name,                   comment: "ゲスト名"
      t.string  :nationality,                    comment: "国籍"
      t.integer :nationality_id,                 comment: "国籍ID"
      t.string  :mail_address,                   comment: "メールアドレス"
      t.string  :tel,                            comment: "TEL"
      t.integer :adult_count,                    comment: "宿泊人数（大人）"
      t.integer :child_count,                    comment: "宿泊人数（子供）"
      t.integer :room_charge,                    comment: "宿泊料"
      t.integer :cleaning_fee,                   comment: "清掃費"
      t.integer :total,                          comment: "合計（宿泊費＋清掃費）"
      t.string  :site_code,                      comment: "予約サイトコード"
      t.string  :site_name,                      comment: "予約サイト名"
      t.date    :reserve_date,                   comment: "予約日"
      t.text    :note,                           comment: "備考"

      t.integer :created_user_id, null: false, default: 0
      t.timestamps null: false
    end
  end
end
