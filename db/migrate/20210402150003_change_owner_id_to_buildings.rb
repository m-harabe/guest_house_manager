class ChangeOwnerIdToBuildings < ActiveRecord::Migration[5.2]

  # 未使用yだったownersテーブルの利用開始に伴い、owners_id カラムの再定義
  def up
    change_column :buildings,  :owner_id,  :integer,  null: true,  default: nil,  comment: "オーナーID"
  end

  def down
    change_column :buildings,  :owner_id,  :integer,  null: false,  default: 0,   comment: "オーナーID"
  end
end
