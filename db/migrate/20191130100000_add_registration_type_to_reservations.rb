class AddRegistrationTypeToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations,       :registration_type, :integer, limit: 1, default: 0, comment: "登録タイプ e.g.) 0:手動 1:手動（suitebook） 2:自動（suitebook）"
    add_column :whole_reservations, :registration_type, :integer, limit: 1, default: 0, comment: "登録タイプ e.g.) 0:手動 1:手動（suitebook） 2:自動（suitebook）"
  end
end
