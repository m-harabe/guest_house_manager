class CreateSuitebookReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :suitebook_reservations, comment: "suitebook予約情報" do |t|

      t.references :reservation,       foreign_key: true, comment: "予約ID"
      t.references :whole_reservation, foreign_key: true, comment: "一棟貸情報ID"

      t.integer  :status, null: false, default: 0, limit: 1, index: true, comment: "ステータス e.g.) 0:未処理 1:処理済 8:キャンセル 9エラー"
      t.text     :note,                comment: "システム内備考"

      ######## ▼連携するCSVの項目 ########
      t.string   :suitebook_idnum,     comment: "suitebook予約ID"
      t.string   :site_idnum,          comment: "予約サイトID"
      t.string   :site_name,           comment: "予約サイト名"
      t.string   :check_in_date,       comment: "チェックイン"
      t.string   :check_out_date,      comment: "チェックアウト"
      t.string   :reservation_cd,      comment: "予約コード"
      t.string   :is_archive,          comment: "アーカイブ済み"
      t.string   :number_of_nights,    comment: "宿泊数"
      t.string   :visitor_name,        comment: "ゲスト名"
      t.string   :mail_address,        comment: "ゲストemail"
      t.string   :total_count,         comment: "宿泊人数"
      t.string   :adult_count,         comment: "大人人数"
      t.string   :child_count,         comment: "子供人数"
      t.string   :scheduled_check_in_time, comment: "チェックイン予定時刻"
      t.string   :building_name,       comment: "物件名"
      t.string   :building_idnum,       comment: "物件ID"
      t.string   :property_owner_idnum, comment: "プロパティオーナーID"
      t.string   :owner_company_name,  comment: "会社名(オーナー)"
      t.string   :owner_name,          comment: "代表者・契約者(オーナー)"
      t.string   :site_account,        comment: "予約サイトアカウント(email)"
      t.string   :contract_style,      comment: "契約形態"
      t.string   :cancel,              comment: "キャンセル"
      t.string   :currency,            comment: "通貨"
      t.string   :room_charge,         comment: "宿泊料金"
      t.string   :cleaning_fee,        comment: "清掃費(ゲスト)"
      t.string   :total,               comment: "宿泊料金(ゲスト支払い金額)"
      t.string   :handling_charge,     comment: "手数料"
      t.string   :revenue,             comment: "あなたの収益"
      t.string   :data_careated_at,    comment: "予約作成日"
      t.string   :other_cost,          comment: "部屋代金以外の費用"
      t.string   :room_idnum,          comment: "部屋ID"
      t.string   :room_name,           comment: "部屋名"
      t.string   :memo,                comment: "予約メモ"
      ######## ▲連携するCSVの項目 ########

      t.integer    :created_user_id,  null: false,  default: 0
      t.integer    :updated_user_id,  null: false,  default: 0
      t.integer    :lock_version,     null: false,  default: 0
      t.timestamps null: false
    end

  end
end


