# bundle exec rake db:seed:reservations

ReservationDate.delete_all
Reservation.delete_all

# (Date.parse('2018-06-01')..Date.parse('2018-07-31')).each do |date|
# end

@site_codes = Settings.SITE.CODE.inject([]){|list, (key, val)| list << val}
@nationalitys = Nationality.pluck :id
@names = ['ヤン・ウェンリー', 'ワルター・フォン・シェーンコップ', 'ユリアン・ミンツ', 'オスカー・フォン・ロイエンタール', 'ウォルフガング・ミッターマイヤー', 'ジークフリード・キルヒアイス', 'アレックス・キャゼルヌ', 'ダスティ・アッテンボロー']
@date = Date.parse('2018-07-01')

50.times do |number|
  # @date = @date + number
  attr = {}
  attr[:reserve_date]          = @date
  attr[:check_in_date]         = attr[:reserve_date] + 10
  attr[:number_of_nights]      = [1,2,3,4,5].sample
  attr[:site_code]             = @site_codes.sample
  attr[:nationality_id]        = @nationalitys.sample
  attr[:visitor_name]          = @names.sample
  attr[:adult_count]           = [2,3,4,5,6].sample
  attr[:child_count]           = [0,1,2,3,4].sample
  attr[:room_charge]           = 7800 * attr[:number_of_nights] * (attr[:adult_count] + attr[:child_count])
  attr[:cleaning_fee]          = 2000 * attr[:number_of_nights] * (attr[:adult_count] + attr[:child_count])
  attr[:meal_fee]              = 1500 * attr[:number_of_nights] * (attr[:adult_count] + attr[:child_count])
  attr[:handling_charge_rate]  = Settings.SITE.RATE[attr.fetch(:site_code)]

  attr[:status]           = Settings.RESERVATION.STATUS.PREPARING
  attr[:tel]              = "090-0000-000#{attr[:number_of_nights]}"
  attr[:room_id]          = 1

  reservation = Reservation.create_or_update(attr)
  ReservationDate.reverse_entries(reservation)
  @date = @date + attr[:number_of_nights]
end