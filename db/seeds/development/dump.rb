# Gem seed_dump
# bundle exec rake db:seed:dump


# ActiveRecord::Base.transaction do
  # User.create!([
  #   {email: "m-harabe@anchor-design-works.com", encrypted_password: "$2a$11$v94KkDy8Woq2r7A5SnFXnuy4mFkJes5k1sFezHaYXx8uXjKpGOI1u", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-06-12 01:47:05", last_sign_in_at: "2018-06-11 10:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", name: "システム管理者", kana: "システムカンリシャ", tel: "090-0000-0000", account: "admin", role: 9, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 4}
  # ])

  User.create!([
    {email: "m-harabe@anchor-design-works.com", encrypted_password: "zaq12wsx", password: "zaq12wsx", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2018-06-11 16:47:05", last_sign_in_at: "2018-06-11 01:05:30", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", name: "システム管理者", kana: "システムカンリシャ", tel: "090-0000-0000", account: "admin", role: 9, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 4}
  ])

  Nationality.create!([
    {name: "日本", order: 1, is_visible: true},
    {name: "韓国", order: 2, is_visible: true},
    {name: "アメリカ", order: 3, is_visible: true},
    {name: "中国", order: 4, is_visible: true}
  ])

  Owner.create!([
    {code: "owner_1", name: "オーナー1", postal_code: "", address: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {code: "owner_2", name: "オーナー2", postal_code: "", address: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {code: "owner_3", name: "オーナー3", postal_code: "", address: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0}
  ])

  Building.create!([
    {owner_id: 1, code: "1001", name: "東福寺南門", postal_code: "", address: "", room_count: 1, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {owner_id: 1, code: "1002", name: "六条西洞院", postal_code: "", address: "", room_count: 1, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {owner_id: 1, code: "1003", name: "八条室町", postal_code: "", address: "", room_count: 1, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {owner_id: 2, code: "1004", name: "神泉苑南", postal_code: "", address: "", room_count: 1, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {owner_id: 2, code: "1005", name: "Hostel円町", postal_code: "", address: "", room_count: 3, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {owner_id: 2, code: "1004", name: "松原堺町", postal_code: "", address: "", room_count: 1, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {owner_id: 2, code: "1004", name: "高倉葛籠屋町", postal_code: "", address: "", room_count: 3, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {owner_id: 2, code: "1004", name: "清水清閑寺", postal_code: "", address: "", room_count: 1, created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0}
  ])

  Room.create!([
    {building_id: 1, room_no: 1, name: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 2, room_no: 1, name: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 3, room_no: 1, name: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 4, room_no: 1, name: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 5, room_no: 1, name: "1F", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 5, room_no: 1, name: "2F和室", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 5, room_no: 1, name: "2F洋室", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 6, room_no: 1, name: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 7, room_no: 1, name: "1F", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 7, room_no: 1, name: "2F洋室", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 7, room_no: 1, name: "2F和室", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0},
    {building_id: 8, room_no: 1, name: "", created_user_id: 0, updated_user_id: 0, deleted_at: nil, lock_version: 0}
  ])
  Reservation.create!([
    {room_id: 1, status: "1", site_code: "200", nationality_id: 3, visitor_name: "ヤン・ウェンリー", mail_address: "", tel: "09000001111", adult_count: 4, child_count: 1, room_charge: 28000, cleaning_fee: 3500, total: 31500, handling_charge: 945, reserve_date: "2018-06-07", check_in_date: "2018-07-21", check_out_date: "2018-07-24", number_of_nights: 3, note: "備考です。", created_user_id: 1, updated_user_id: 1, lock_version: 1},
    {room_id: 2, status: "1", site_code: "200", nationality_id: 1, visitor_name: "ワルター・フォン・シェーンコップ", mail_address: "", tel: "", adult_count: 4, child_count: 0, room_charge: 36000, cleaning_fee: 6000, total: 42000, handling_charge: 1740, reserve_date: "2018-06-01", check_in_date: "2018-06-21", check_out_date: "2018-06-24", number_of_nights: 3, note: "", created_user_id: 1, updated_user_id: 1, lock_version: 1},
    {room_id: 2, status: "1", site_code: "200", nationality_id: 1, visitor_name: "ワルター・フォン・シェーンコップ", mail_address: "", tel: "", adult_count: 4, child_count: 0, room_charge: 36000, cleaning_fee: 6000, total: 42000, handling_charge: 1740, reserve_date: "2018-06-01", check_in_date: "2018-06-01", check_out_date: "2018-06-04", number_of_nights: 3, note: "", created_user_id: 1, updated_user_id: 1, lock_version: 0},
    {room_id: 1, status: "1", site_code: "100", nationality_id: 1, visitor_name: "ジークフリード・キルヒアイス", mail_address: "", tel: "", adult_count: 2, child_count: 0, room_charge: 36000, cleaning_fee: 6000, total: 42000, handling_charge: 780, reserve_date: "2018-06-09", check_in_date: "2018-06-07", check_out_date: "2018-06-10", number_of_nights: 3, note: "", created_user_id: 1, updated_user_id: 1, lock_version: 0},
    {room_id: 6, status: "1", site_code: "400", nationality_id: 2, visitor_name: "テスト", mail_address: "", tel: "", adult_count: 4, child_count: 2, room_charge: 19800, cleaning_fee: 3000, total: 22800, handling_charge: 684, reserve_date: "2018-06-13", check_in_date: "2018-06-29", check_out_date: "2018-07-01", number_of_nights: 2, note: "備考です", created_user_id: 1, updated_user_id: 1, lock_version: 0}
  ])
  ReservationDate.create!([
    {reservation_id: 1, check_in_day: "2018-07-21", room_charge: 9333, cleaning_fee: 1166, total: 10499},
    {reservation_id: 1, check_in_day: "2018-07-22", room_charge: 9333, cleaning_fee: 1166, total: 10499},
    {reservation_id: 1, check_in_day: "2018-07-23", room_charge: 9334, cleaning_fee: 1168, total: 10502},

    {reservation_id: 2, check_in_day: "2018-06-21", room_charge: 12000, cleaning_fee: 2000, total: 14000},
    {reservation_id: 2, check_in_day: "2018-06-22", room_charge: 12000, cleaning_fee: 2000, total: 14000},
    {reservation_id: 2, check_in_day: "2018-06-23", room_charge: 12000, cleaning_fee: 2000, total: 14000},

    {reservation_id: 3, check_in_day: "2018-06-01", room_charge: 12000, cleaning_fee: 2000, total: 14000},
    {reservation_id: 3, check_in_day: "2018-06-02", room_charge: 12000, cleaning_fee: 2000, total: 14000},
    {reservation_id: 3, check_in_day: "2018-06-03", room_charge: 12000, cleaning_fee: 2000, total: 14000},

    {reservation_id: 4, check_in_day: "2018-06-07", room_charge: 12000, cleaning_fee: 2000, total: 14000},
    {reservation_id: 4, check_in_day: "2018-06-08", room_charge: 12000, cleaning_fee: 2000, total: 14000},
    {reservation_id: 4, check_in_day: "2018-06-09", room_charge: 12000, cleaning_fee: 2000, total: 14000},

    {reservation_id: 5, check_in_day: "2018-06-29", room_charge: 9900, cleaning_fee: 1500, total: 11400},
    {reservation_id: 5, check_in_day: "2018-06-30", room_charge: 9900, cleaning_fee: 1500, total: 11400}
  ])
# end